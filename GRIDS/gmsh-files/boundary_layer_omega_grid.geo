// This is a small script which can be used to determine the BL grid

// There are three parameters which control the BL grid:
//
//   bl_N:     the number of levels
//   bl_yp1:   the coordinate of the first point
//   bl_thick: the thickness of the boundary layer
//
// The output is
//
//   bl_levs:  bl_N+1 levels ranging from 0 to bl_thick
//
//
// The generated levels range from 0 up to bl_N. More precisely,
// bl_N+1 values are generated, identifying bl_N cells. The first and
// last values are always 0 and bl_thick.
//
// Once The number of points and the thickness are fixed, reducing
// bl_yp1 leads to a more pronounced grid refinement close to the
// wall.
//
// The general expression for the computation of the levels is
//
//  levs = bl_thick*(1 - Tanh( omega*(1-i/bl_N) )/Tanh( omega ))
//
// for i=0,...,bl_N. The coefficient omega is computed here as a
// function of bl_N, bl_yp1 and bl_thick.
//
// Note: it makes no sense to pass  bl_yp1 >= bl_thick/bl_N  since
// this would correspond to a *derefinement* in the wall region. In
// fact, the Newton method used here to find omega does not work in
// such a case.
//
// Note: local variables use the prefix bl_locv_ to avoid polluting
// the global namespace.


// The computation is divided in two steps: first omega is computed
// working with normalized coordinates in the interval [0,1], then the
// levels are generated.


// -------------------- Step 1: compute omega --------------------------

// normalize the position of the first point
bl_locv_nyp1 = bl_yp1 / bl_thick;

// solve iteratively for omega: Newton iterations for
//
//   (1-nyp1)*Tanh(omega) - Tanh( (1-1/N)*omega ) = 0

bl_locv_omega = 1.0;
bl_locv_omegas = { bl_locv_omega };
bl_locv_fmt = Str("   %g");
For i In {1:10}
  bl_locv_fomega = (1-bl_locv_nyp1)*Tanh(bl_locv_omega) 
                 - Tanh( (1.0-1.0/bl_N)*bl_locv_omega );
  bl_locv_dfomega = (1-bl_locv_nyp1)/(Cosh(bl_locv_omega)^2)
         - (1.0-1.0/bl_N)/(Cosh( (1.0-1.0/bl_N)*bl_locv_omega )^2);
  bl_locv_omega = bl_locv_omega - bl_locv_fomega/bl_locv_dfomega;
  
  // provide some diagnostics
  bl_locv_omegas = { bl_locv_omegas[] , bl_locv_omega };
  bl_locv_fmt = StrCat(bl_locv_fmt,"  %g"); // build the output format
EndFor

Printf(" ");
Printf("Convergence of omega in the Newton iterations:");
Printf(bl_locv_fmt, bl_locv_omegas[]);

// -------------------- End step 1: compute omega ----------------------


// -------------------- Step 2: compute the levels ---------------------

// define the levels
bl_locv_fmt = Str(" ");
For i In {0:bl_N}

  bl_levs[i] = bl_thick*( 1.0 
     - Tanh( bl_locv_omega*(1.0-i/bl_N) )/Tanh( bl_locv_omega ) );

  bl_locv_fmt = StrCat(bl_locv_fmt,"  %g"); // build the output format
EndFor

Printf("Boundary layer levels:");
Printf(bl_locv_fmt, bl_levs[]);
Printf(" ");

// -------------------- End step 2: compute the levels -----------------

