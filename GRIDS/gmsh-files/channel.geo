// Unstructured channel, uniform anisotropic resolution

// To obtain the anisotropic resolution, an isotropic mesh is
// generated on a deformed domain, which should then be mapped to
//  [ 0,4*pi , -1,1 , 0,4/3*pi ]
Nx = 7;
Ny = 8;
Nz = 5;

// Build the mesh
h = 1.0;
Point(1) = {0.0,-Ny/2.0,0.0,h};
Point(2) = { Nx,-Ny/2.0,0.0,h};
Point(3) = { Nx,-Ny/2.0, Nz,h};
Point(4) = {0.0,-Ny/2.0, Nz,h};
Point(5) = {0.0,+Ny/2.0,0.0,h};
Point(6) = { Nx,+Ny/2.0,0.0,h};
Point(7) = { Nx,+Ny/2.0, Nz,h};
Point(8) = {0.0,+Ny/2.0, Nz,h};
// Bottom lines
Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};
// Top lines
Line(5) = {5,6};
Line(6) = {6,7};
Line(7) = {7,8};
Line(8) = {8,5};
// Side lines
Line( 9) = {1,5};
Line(10) = {2,6};
Line(11) = {3,7};
Line(12) = {4,8};


b1 = newreg; Line Loop(b1) = { 1, 2, 3,  4};
b2 = newreg; Line Loop(b2) = { 5, 6, 7,  8};
b3 = newreg; Line Loop(b3) = {-1, 9, 5,-10};
b4 = newreg; Line Loop(b4) = { 3,12,-7,-11};
b5 = newreg; Line Loop(b5) = { 2,11,-6,-10};
b6 = newreg; Line Loop(b6) = {-4,12, 8, -9};

Bottom = 3;
Top    = 5;
Inflow = 2;
Ouflow = 4;
Right  = 1;
Left   = 6;

Plane Surface(Bottom) = {b1};
Plane Surface( Top  ) = {b2};
Plane Surface(Inflow) = {b6};
Plane Surface(Ouflow) = {b5};
Plane Surface(Right ) = {b4};
Plane Surface(Left  ) = {b3};
// Specify periodicity
Periodic Surface Top   {5, 6, 7,  8} = Bottom{ 1, 2,3,  4}; // not really needed
Periodic Surface Ouflow{2,11,-6,-10} = Inflow{-4,12,8, -9};
Periodic Surface Right {3,12,-7,-11} = Left  {-1, 9,5,-10};

// Now the 3D mesh
Surface Loop(1) = {Bottom,Inflow,Left,-Top,-Ouflow,-Right};
Volume(1) = {1};

