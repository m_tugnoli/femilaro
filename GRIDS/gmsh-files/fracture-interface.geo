// Domain dimensions
Lx = 1;
Ly = 0.5;
Sf = 0.1;

// Mesh size
h  = 0.05;

// Define the reference points
A = newp; Point(A) = {-Lx,-Ly,0.0,h};
B = newp; Point(B) = { Lx,-Ly,0.0,h};
C = newp; Point(C) = { Lx, Ly,0.0,h};
D = newp; Point(D) = {-Lx, Ly,0.0,h};

E = newp; Point(E) = {0.0,-Ly,0.0,h};
G = newp; Point(G) = {0.0, Ly,0.0,h};

I = newp; Point(I) = {0.0,-(Ly-Sf),0.0,h};
M = newp; Point(M) = {0.0, (Ly-Sf),0.0,h};

// Define the two subdomains
b1 = newc; Line(b1) = {A,E}; 
b2 = newc; Line(b2) = {E,B}; 
t1 = newc; Line(t1) = {G,D}; 
t2 = newc; Line(t2) = {C,G}; 
rr = newc; Line(rr) = {B,C}; 
ll = newc; Line(ll) = {D,A}; 

fb = newc; Line(fb) = {E,I}; 
ff = newc; Line(ff) = {I,M}; 
ft = newc; Line(ft) = {M,G}; 

bO1 = newreg; Line Loop(bO1) = {b1,fb,ff, ft, t1, ll}; 
bO2 = newreg; Line Loop(bO2) = {b2,rr,t2,-ft,-ff,-fb}; 

Omega1 = news; Plane Surface(Omega1) = {bO1};
Omega2 = news; Plane Surface(Omega2) = {bO2};

// Display boundary labels
//View "Boundary labels" {
//  T2(10, 20, 0){ StrCat( 
//    "Hole labels:    ",Sprintf("%.0f, %.0f, %.0f, %.0f",a1,a2,a3,a4)) };
//  T2(10, 40, 0){ StrCat( 
//    "Inflow label:   ",Sprintf("%.0f",lef)) };
//  T2(10, 60, 0){ StrCat( 
//    "Outflow labels: ",Sprintf("%.0f, %.0f, %.0f",bot,rig,top)) };
//};

