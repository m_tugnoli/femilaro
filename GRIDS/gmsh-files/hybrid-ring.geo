// Mesh size
ri = 0.8;
re = 1.2;
theta = 60.0 * (3.1415/180.0);

// grid resolution
n_upper = 160;
n_width = 160;
n_lower = (3.1415/2-theta) * ri/(re-ri) * n_width;

// Build the upper and the lower parts of the ring
cth = Cos(theta);
sth = Sin(theta);

C = newp; Point(C) = {0.0,0.0,0.0};

Ei = newp; Point(Ei) = { cth*ri,-sth*ri,0.0};
Ni = newp; Point(Ni) = {    0.0,     ri,0.0};
Wi = newp; Point(Wi) = {-cth*ri,-sth*ri,0.0};
Si = newp; Point(Si) = {    0.0,    -ri,0.0};

Ee = newp; Point(Ee) = { cth*re,-sth*re,0.0};
Ne = newp; Point(Ne) = {    0.0,     re,0.0};
We = newp; Point(We) = {-cth*re,-sth*re,0.0};
Se = newp; Point(Se) = {    0.0,    -re,0.0};

a1i = newreg; Circle(a1i) = {Ei,C,Ni};
a2i = newreg; Circle(a2i) = {Ni,C,Wi};
a3i = newreg; Circle(a3i) = {Wi,C,Si};
a4i = newreg; Circle(a4i) = {Si,C,Ei};

a1e = newreg; Circle(a1e) = {Ee,C,Ne};
a2e = newreg; Circle(a2e) = {Ne,C,We};
a3e = newreg; Circle(a3e) = {We,C,Se};
a4e = newreg; Circle(a4e) = {Se,C,Ee};

sE = newc; Line(sE) = {Ei,Ee}; 
sW = newc; Line(sW) = {Wi,We}; 

NorthLine = newreg; Line Loop(NorthLine) = {sE,a1e,a2e,-sW,-a2i,-a1i}; 
SouthLine = newreg; Line Loop(SouthLine) = {sE,-a4e,-a3e,-sW,a3i,a4i}; 

North = news; Plane Surface(North) = {NorthLine};
South = news; Plane Surface(South) = {SouthLine};

// Define the 1D grid on the curves
//Transfinite Line{sE} = n_width Using Bump 4.0;
//Transfinite Line{sW} = n_width Using Bump 4.0;
Transfinite Line{sE} = n_width;
Transfinite Line{sW} = n_width;

// Upper region: rectangular, structured
Transfinite Line{a1i} = n_upper;
Transfinite Line{a2i} = n_upper;
Transfinite Line{a1e} = n_upper;
Transfinite Line{a2e} = n_upper;
// specify the limits of the transfinite (structured) grid
Transfinite Surface{North} = {Ei,Ee,We,Wi};
// make quadrilaterals
Recombine Surface{North};

// Lower region: rectangular, structured
nsub_tri = n_lower;
Transfinite Line{a3i} = nsub_tri;
Transfinite Line{a4i} = nsub_tri;
Transfinite Line{a3e} = Ceil(re/ri*nsub_tri);
Transfinite Line{a4e} = Ceil(re/ri*nsub_tri);

// Display boundary labels
View "Boundary labels" {
  T2(10, 20, 0){ StrCat( 
    "Internal labels:    ",Sprintf("%.0f, %.0f, %.0f, %.0f",a1i,a2i,a3i,a4i)) };
  T2(10, 40, 0){ StrCat( 
    "External labels:    ",Sprintf("%.0f, %.0f, %.0f, %.0f",a1e,a2e,a3e,a4e)) };
};

