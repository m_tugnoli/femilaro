// Mesh for the periodic hill test case: boundary layer

// This file describes the boundary layer. It is meant to be included
// in a second file which defines the bulk grid, using either
// structured or unstructured elements.


// -------------------- General Setup ----------------------------------

// domain size

H  = 28; // hill height
Lx = 9.0*H;
Ly = 3.036*H;
Lz = 4.5*H;

// boundary layer

bl_thick = H/4;  // thickness of the boundary region
bl_N     =  6;   // elements in the boundary layer (vertical)
y_wall   = 0.65; // y^+
Re_tau   = 400.0;
// dimensional position of the first point of the BL
bl_yp1 = (y_wall/Re_tau) * (35*6)^(1/3) * H;

// BL grid resolution

n_hill_l = 17; // elements on the left hill (horizontal)
n_hill_r = 11; // elements on the right hill (horizontal)
n_center = 15; // elements in the central region (horizontal)
n_z_layers = 10; // number of layers in z (spanwise, must be even)

// Bulk region

n_upper  =  6; // elements in the upper region (vertical)
upper_progr = 1.3; // refinement in the upper region


// -------------------- End General Setup ------------------------------
 

// -------------------- Hill polynomial representation -----------------

Lx1 =  1.0/28.0  * Lx; // lenght (in x) of each arch
Lx2 =  1.0/18.0  * Lx;
Lx3 =  5.0/63.0  * Lx;
Lx4 =  5.0/42.0  * Lx;
Lx5 = 10.0/63.0  * Lx;
Lx6 = 27.0/126.0 * Lx;
Lx7 =  1.0/2.0   * Lx;

hill_prof_pts = { 0.0 , Lx1 , Lx2 , Lx3 , Lx4 , Lx5 , Lx6 , Lx7 };

// Note: the variable i_arc before calling this function
Function hill_profile

  If(i_arc==1)
    y = 28 + 0.006775071*x^2 - 0.0021245778*x^3;
  EndIf

  If(i_arc==2)
    y = 25.07355893 + 0.975480356*x - 0.101611635*x^2
       + 0.001889794678*x^3;
  EndIf

  If(i_arc==3)
    y = 25.7960105257 + 0.8206693007457*x - 0.09055370274339*x^2 
       + 0.001626570569859*x^3;
  EndIf

  If(i_arc==4)
    y = 40.46435022819 - 1.379581654948*x + 0.01945884504128*x^2
       - 0.0002070318932190*x^3;
  EndIf

  If(i_arc==5)
    y = 17.92461334664 + 0.8743920332081*x - 0.05567361123058*x^2
       + 0.0006277731764683*x^3;
  EndIf

  If(i_arc==6)
    y = 56.39011190988 - 2.010520359035*x + 0.01644919857549*x^2
       + 0.00002674976141766*x^3;
  EndIf

Return

// -------------------- End hill polynomial representation -------------


// -------------------- Hill spline representation ---------------------

// The profile is treated as a spline: we define many points of the
// profile and then represent it as a unique spline; each arc uses
// Narc points.

Narcs  =  6; // number of arcs
Np_arc = 10; // number of points per arc

ip = 0;
For i_arc In {1:Narcs}
  start = 1;
  If(i_arc==1)
    start = 0; // include the left point for the first arc
  EndIf
  For t In {start:Np_arc}
    dx = ( hill_prof_pts[i_arc] - hill_prof_pts[i_arc-1] )/Np_arc;
    x  = dx*t + hill_prof_pts[i_arc-1];
    Call hill_profile; // returns  y = profile(x)
    pp = newp; Point(pp) = {x,y,0.0,0.0};
    points_lh[ip] = pp;
    ip = ip+1;
  EndFor
EndFor
np_lh = ip; // dimension of points_lh

// First and last points
P11 = points_lh[0];
P12 = points_lh[np_lh-1];

// Generate a spline for left hill

bottom_left = newreg; Spline(bottom_left) = points_lh[];
Transfinite Line{bottom_left} = n_hill_l;

// -------------------- End hill spline representation -----------------


// -------------------- Boundary layer: left hill ----------------------

// Compute the vertical levels for the boundary layer

Include "boundary_layer_omega_grid.geo";

// Skip the first level (i.e. 0.0), scale the levels to [0,1] as
// requested by the Extrude command and define an array of ones.
For i In {1:bl_N}
  levs[i-1] = bl_levs[i]/bl_thick; 
  ones[i-1] = 1; // used later for the extrusion
EndFor

// Define the boundary layer

// Note: the Extrude function returns, for a Line entity, four indexes
// as follows:
// * the index of the extruded line
// * the index of the surface created by the extrusion
// * the two indexes of the sides created by the extrusion, each of
//   which with a sign so that the resulting loop is consistently
//   ordered; both sides are directed along the extrusion

pp = Extrude{0.0,bl_thick,0.0}{
  Line{bottom_left}; Layers{ones[],levs[]};
};
bottom_left_bl = pp[0];  // spline, upper boundary of the BL
bl_hl_s = pp[1];         // newly created surface
left_bl_1 = Fabs(pp[3]); // left side (upwards)
left_bl_2 = Fabs(pp[2]); // right side (upwards)
// get the points on bottom_left_bl
pp = Boundary{Line{bottom_left_bl[0]};};
P21 = pp[0];
P22 = pp[1];

// -------------------- End boundary layer: left hill ------------------


// -------------------- Right hill -------------------------------------

// The right hill is defined using a symmetry transformation of the
// left one. First we duplicate the points defining the bottom spline,
// then we create a new spline, and finally we extrude this spline to
// define the boundary layer on the right hill.

For i In {0:np_lh-1} // duplicate the points in the right order
  pp = Symmetry{1,0,0,-Lx/2}{Duplicata{Point{points_lh[i]};}};
  points_rh[np_lh-i-1] = pp;
EndFor

// the rest of this section is the same as for the left hill

P13 = points_rh[0];
P14 = points_rh[np_lh-1];

bottom_right = newreg; Spline(bottom_right) = points_rh[];
Transfinite Line{bottom_right} = n_hill_r;

pp = Extrude{0.0,bl_thick,0.0}{
  Line{bottom_right}; Layers{ones[],levs[]};
};
bottom_right_bl = pp[0];
bl_hr_s = pp[1];
right_bl_1 = Fabs(pp[3]);
right_bl_2 = Fabs(pp[2]);
pp = Boundary{Line{bottom_right_bl[0]};};
P23 = pp[0];
P24 = pp[1];

// -------------------- End right hill ---------------------------------


// -------------------- Central region ---------------------------------

// This section is very simple, because all the points have been
// already created, and the vertical lines already have the 1D grid of
// the boundary layer.

// define the two horizontal lines
bottom_center    = newreg; Line(bottom_center   ) = {P12,P13};
bottom_center_bl = newreg; Line(bottom_center_bl) = {P22,P23};

// define the bottom surface and set the resolution

bl_c = newreg;
Line Loop(bl_c) = 
  { bottom_center , right_bl_1 , -bottom_center_bl , -left_bl_2 };
bl_c_s = news;
Plane Surface(bl_c_s) = {bl_c};

Transfinite Line{bottom_center,bottom_center_bl} = n_center;

// -------------------- End central region -----------------------------


// -------------------- Three-dimensional BL: left hill ----------------

// Each portion of the boundary layer is now extruded in the z
// direction twice, once towards +z and once towards -z. This ensures
// a symmetric grid of the boundary layer.

// Note: the Extrude function returns, for a Surface entity, six
// indexes as follows:
// * the index of the extruded surface
// * the index of the volume created by the extrusion
// * the four indexes of the sides (surfaces) created by the extrusion
//
// The indexes of the four sides created by the extrusion are ordered
// according to line ordering defining the extruded surface.

// Extrude the left hill in the +z direction
pp[] = Extrude{0,0, Lz/2}{ Surface{bl_hl_s}; Layers{n_z_layers}; };
bl_hl_sp = pp[0]; // extruded surface
bottom_left_zp    = pp[2];
inflow_bl_zp      = pp[5];
bottom_left_bl_zp = pp[4];

// same for the -z direction
pp[] = Extrude{0,0,-Lz/2}{ Surface{bl_hl_s}; Layers{n_z_layers}; };
bl_hl_sm = pp[0]; // extruded surface
bottom_left_zm    = pp[2];
inflow_bl_zm      = pp[5];
bottom_left_bl_zm = pp[4];

// keep track of the relevant surfaces
inflow_surfaces = { inflow_bl_zp , inflow_bl_zm };
bottom_surfaces = { bottom_left_zp , bottom_left_zm };
z_max_surfaces  = { bl_hl_sp };
z_min_surfaces  = { bl_hl_sm };

// -------------------- End three-dimensional BL: left hill ------------


// -------------------- Three-dimensional BL: center and right hill ----

// This is very similar to the case of the left hill

// right hill
pp[] = Extrude{0,0, Lz/2}{ Surface{bl_hr_s}; Layers{n_z_layers}; };
bl_hr_sp = pp[0]; // extruded surface
bottom_right_zp    = pp[2];
outflow_bl_zp      = pp[3];
bottom_right_bl_zp = pp[4];
pp[] = Extrude{0,0,-Lz/2}{ Surface{bl_hr_s}; Layers{n_z_layers}; };
bl_hr_sm = pp[0]; // extruded surface
bottom_right_zm    = pp[2];
outflow_bl_zm      = pp[3];
bottom_right_bl_zm = pp[4];

// keep track of the relevant surfaces
outflow_surfaces = { outflow_bl_zp    , outflow_bl_zm };
bottom_surfaces = { bottom_surfaces[] , bottom_right_zp , bottom_right_zm };
z_max_surfaces  = { z_max_surfaces[]  , bl_hr_sp };
z_min_surfaces  = { z_min_surfaces[]  , bl_hr_sm };

// center
pp[] = Extrude{0,0, Lz/2}{ Surface{bl_c_s}; Layers{n_z_layers}; };
bl_c_sp = pp[0]; // extruded surface
bottom_center_zp    = pp[2];
bottom_center_bl_zp = pp[4];
pp[] = Extrude{0,0,-Lz/2}{ Surface{bl_c_s}; Layers{n_z_layers}; };
bl_c_sm = pp[0]; // extruded surface
bottom_center_zm    = pp[2];
bottom_center_bl_zm = pp[4];

// keep track of the relevant surfaces
bottom_surfaces = { bottom_surfaces[] , 
                    bottom_center_zp , bottom_center_zm };
z_max_surfaces  = { z_max_surfaces[]  , bl_c_sp };
z_min_surfaces  = { z_min_surfaces[]  , bl_c_sm };

// -------------------- End three-dimensional BL: center and right hill 

Printf(" ");
Printf("Surface indexes for the boundary layer:");
Printf(" -> inflow surfaces:   %g %g",inflow_surfaces[]);
Printf(" -> outflow surfaces:  %g %g",outflow_surfaces[]);
Printf(" -> bottom surfaces:   %g %g %g %g %g %g",bottom_surfaces[]);
Printf(" -> z = zmax surfaces: %g %g %g",z_max_surfaces[]);
Printf(" -> z = zmin surfaces: %g %g %g",z_min_surfaces[]);
Printf(" ");


