// Periodic hill test case, partially structured grid

Include "periodic_hill_BL_geometry.geo";


// General idea: create the four upper points delimiting the top
// surface, then defined the corresponding lines, surfaces and volume


// -------------------- Upper region: points ---------------------------

// get the four upper points of the boundary layer
pp[] = Boundary{ Surface{bl_hl_sp}; };
pp[] = Boundary{ Line{pp[2]}; };
P21p = pp[1];
pp[] = Boundary{ Surface{bl_hl_sm}; };
pp[] = Boundary{ Line{pp[2]}; };
P21m = pp[1];
pp[] = Boundary{ Surface{bl_hr_sp}; };
pp[] = Boundary{ Line{pp[2]}; };
P24p = pp[0];
pp[] = Boundary{ Surface{bl_hr_sm}; };
pp[] = Boundary{ Line{pp[2]}; };
P24m = pp[0];

// define the new four points
P31p = newp; Point(P31p) = {0.0,Ly, Lz/2.0,0.0};
P31m = newp; Point(P31m) = {0.0,Ly,-Lz/2.0,0.0};
P34p = newp; Point(P34p) = { Lx,Ly, Lz/2.0,0.0};
P34m = newp; Point(P34m) = { Lx,Ly,-Lz/2.0,0.0};

// -------------------- End upper region: points -----------------------


// -------------------- Upper region: edges ----------------------------

// get the outer edges of the boundary layer, as well as the
// inflow/outflow edges
pp[] = Boundary{ Surface{bottom_left_bl_zp}; };
bl_edge_left_zp = Fabs(pp[2]);
bl_edge_infl_zp = Fabs(pp[1]);
pp[] = Boundary{ Surface{bottom_left_bl_zm}; };
bl_edge_left_zm = Fabs(pp[2]);
bl_edge_infl_zm = Fabs(pp[1]);

pp[] = Boundary{ Surface{bottom_center_bl_zp}; };
bl_edge_center_zp = Fabs(pp[2]);
pp[] = Boundary{ Surface{bottom_center_bl_zm}; };
bl_edge_center_zm = Fabs(pp[2]);

pp[] = Boundary{ Surface{bottom_right_bl_zp}; };
bl_edge_right_zp = Fabs(pp[2]);
bl_edge_outf_zp  = Fabs(pp[3]);
pp[] = Boundary{ Surface{bottom_right_bl_zm}; };
bl_edge_right_zm = Fabs(pp[2]);
bl_edge_outf_zm  = Fabs(pp[3]);

// define the four vertical lines
side_l_zp = newreg; Line(side_l_zp) = {P21p,P31p};
side_l_zm = newreg; Line(side_l_zm) = {P21m,P31m};
side_r_zp = newreg; Line(side_r_zp) = {P24p,P34p};
side_r_zm = newreg; Line(side_r_zm) = {P24m,P34m};

// define the four top edges
side_top_inf = newreg; Line(side_top_inf) = {P31m,P31p};
side_top_out = newreg; Line(side_top_out) = {P34m,P34p};
side_top_zp = newreg; Line(side_top_zp) = {P31p,P34p};
side_top_zm = newreg; Line(side_top_zm) = {P31m,P34m};

// -------------------- End upper region: edges ------------------------


// -------------------- Upper region: 1D grid --------------------------

// set the vertical resolution
Transfinite Line{side_l_zp,side_l_zm,side_r_zp,side_r_zm}
  = n_upper Using Progression upper_progr;

// -------------------- End upper region: 1D grid ----------------------


// -------------------- Upper region: surfaces -------------------------

// inflow
bulk_inf = newreg;
Line Loop(bulk_inf) =
  { bl_edge_infl_zp , side_l_zp , -side_top_inf , -side_l_zm,
   -bl_edge_infl_zm };
bulk_inf_s = news;
Plane Surface(bulk_inf_s) = {bulk_inf};

// outflow
bulk_out = newreg;
Line Loop(bulk_out) =
  { bl_edge_outf_zp , side_r_zp , -side_top_out , -side_r_zm,
   -bl_edge_outf_zm };
bulk_out_s = news;
Plane Surface(bulk_out_s) = {bulk_out};

// zp
bulk_zp = newreg;
Line Loop(bulk_zp) =
  { bl_edge_left_zp , side_l_zp , side_top_zp , -side_r_zp ,
    bl_edge_right_zp , bl_edge_center_zp };
bulk_zp_s = news;
Plane Surface(bulk_zp_s) = {bulk_zp};

// zm
bulk_zm = newreg;
Line Loop(bulk_zm) =
  { bl_edge_left_zm , side_l_zm , side_top_zm , -side_r_zm ,
    bl_edge_right_zm , bl_edge_center_zm };
bulk_zm_s = news;
Plane Surface(bulk_zm_s) = {bulk_zm};

// top
bulk_top = newreg;
Line Loop(bulk_top) =
  { side_top_inf , side_top_zp , -side_top_out , -side_top_zm };
bulk_top_s = news;
Plane Surface(bulk_top_s) = {bulk_top};

// keep track of the relevant surfaces

inflow_surfaces  = { inflow_surfaces[]  , bulk_inf_s };
outflow_surfaces = { outflow_surfaces[] , bulk_out_s };
z_max_surfaces   = { z_max_surfaces[]  , bulk_zp_s };
z_min_surfaces   = { z_min_surfaces[]  , bulk_zm_s };
top_surfaces     = { bulk_top_s };

// -------------------- End upper region: surfaces ---------------------


// -------------------- Periodicity ------------------------------------

// inflow/outflow
Periodic Surface (bulk_out_s) { bl_edge_outf_zp , side_r_zp , 
  -side_top_out , -side_r_zm, -bl_edge_outf_zm }
               = (bulk_inf_s) { bl_edge_infl_zp , side_l_zp ,
  -side_top_inf , -side_l_zm, -bl_edge_infl_zm };

// spanwise direction
Periodic Surface (bulk_zm_s) { bl_edge_left_zm , side_l_zm , 
  side_top_zm , -side_r_zm , bl_edge_right_zm , bl_edge_center_zm }
               = (bulk_zp_s) { bl_edge_left_zp , side_l_zp ,
  side_top_zp , -side_r_zp , bl_edge_right_zp , bl_edge_center_zp };

// -------------------- End periodicity ------------------------------------


// -------------------- Bulk -----------------------------------------------

bulk = newsl; Surface Loop(bulk) = {
  bottom_left_bl_zp ,   bottom_left_bl_zm ,
  bottom_center_bl_zp , bottom_center_bl_zm ,
  bottom_right_bl_zp ,  bottom_right_bl_zm ,
  bulk_inf_s , bulk_out_s , bulk_zp_s , bulk_zm_s , bulk_top_s 
};
bulk_v = newv; Volume(bulk_v) = {bulk};

// -------------------- End bulk -------------------------------------------


// -------------------- Bulk: grid -----------------------------------------

Mesh.Algorithm3D = 4; // Frontal

// Localized refinement
Field[1] = MathEval;
pp[] = Point{P22}; // get the upper y of the boundary layer
y_bl = pp[1];
Field[1].F = Sprintf(
  "10.0 + 8.0*(y-%g)/(%g-%g)", y_bl, Ly, y_bl );
Background Field = 1;

// -------------------- End bulk: grid -------------------------------------


Printf(" ");
Printf("Surface indexes for the complete domain:");
Printf(" -> inflow surfaces:   %g %g %g",inflow_surfaces[]);
Printf(" -> outflow surfaces:  %g %g %g",outflow_surfaces[]);
Printf(" -> bottom surfaces:   %g %g %g %g %g %g",bottom_surfaces[]);
Printf(" -> top surfaces:      %g",top_surfaces[]);
Printf(" -> z = zmax surfaces: %g %g %g %g",z_max_surfaces[]);
Printf(" -> z = zmin surfaces: %g %g %g %g",z_min_surfaces[]);
Printf(" ");


