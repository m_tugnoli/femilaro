# Copyright (C) 2009,2010,2011,2012  Marco Restelli
#
# This file is part of:
#   FEMilaro -- Finite Element Method toolkit
#
# FEMilaro is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# FEMilaro is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
#
# author: Marco Restelli                   <marco.restelli@gmail.com>


#---------------------------------------------------------
# General comments
#---------------------------------------------------------
# This is the main makefile of FEMilaro, the only one which should be
# directly referenced by the user. The overall idea is as follows:
# - this makefile includes a configuration file Makefile.inc where the
#   user should set all the compiler and linker options
# - this makefile exports the relevant variables to the specific
#   makefiles located in the src tree and uses such specific makefiles
#   to compile the related parts of FEMilaro
# - there are two types of targets, depending on whether the user
#   wants to build one of the executables included in FEMilaro or
#   build only the finite element library to link inside an external
#   code.
#
# All the object and library files are placed in the build tree, which
# mirrors the src tree. This avoids name conflicts from files located
# in different branches of the src tree. In the makefiles of the src
# tree, the relevant folders are selected through the VPATH variable.
#
# The .mod files are copied in the lib directory, together with the .a
# files. This is not an optimal solution since it could result in name
# conflicts. An alternative solution would be repeating the src tree
# in lib as well.
#
# NOTE: no changes to this file should be required!


#---------------------------------------------------------
# Import configuration
#---------------------------------------------------------
# Select file with local configuration info. Examples of how to build
# this file for different architectures are in the make.inc directory
include Makefile.inc

# Let's do some checks
$(if $(wildcard $(FEMILARO_DIR)),,$(error Non existent folder $(FEMILARO_DIR)))

# Simplify the naming scheme
FC     :=$(FEMILARO_FC)
FC_BASE:=$(shell basename $(FC))
FC_VENDOR:=$(FEMILARO_FC_VENDOR)
MODFLAG:=$(FEMILARO_MODFLAG)
FFLAGS :=$(FEMILARO_FFLAGS)
CC        :=$(FEMILARO_CC)
CC_INCLUDE:=$(FEMILARO_CC_INCLUDE)
CFLAGS    :=$(FEMILARO_CFLAGS)
LD     :=$(FEMILARO_LD)
LDFLAGS:=$(FEMILARO_LDFLAGS)
LIBS   :=$(FEMILARO_LIBS)
AR:=$(FEMILARO_AR)
USING_OMP    :=$(FEMILARO_USING_OMP)
USING_MPI    :=$(FEMILARO_USING_MPI)
USING_UMFPACK:=$(FEMILARO_USING_UMFPACK)
USING_MUMPS  :=$(FEMILARO_USING_MUMPS)
MUMPS_INCLUDE:=$(FEMILARO_MUMPS_INCLUDE)
USING_PASTIX :=$(FEMILARO_USING_PASTIX)
PASTIX_INCLUDE:=$(FEMILARO_PASTIX_INCLUDE)
USING_MAPHYS :=$(FEMILARO_USING_MAPHYS)
MAPHYS_INCLUDE:=$(FEMILARO_MAPHYS_INCLUDE)
USING_OCTAVE :=$(FEMILARO_USING_OCTAVE)

BUILDDIR:=$(FEMILARO_DIR)/build
LIBDIR  :=$(FEMILARO_DIR)/lib
BINDIR  :=$(FEMILARO_DIR)/bin
SRCDIR  :=$(FEMILARO_DIR)/src
TESTDIR :=$(FEMILARO_DIR)/test

# Check that FC_VENDOR is valid and set compiler specific flags
ifeq ($(FC_VENDOR),GNU)
  FFLAGS_RECURSIVE:=-frecursive
else ifeq ($(FC_VENDOR),Intel)
  FFLAGS_RECURSIVE:=-recursive
else
  $(error 'Unknown FC_VENDOR "'$(FC_VENDOR)'"')
endif
# Flags to allow recursive calls, anticipating the f2015 standard
FFLAGS := $(FFLAGS) $(FFLAGS_RECURSIVE)

# Define a whitespace character: sometimes it can be useful. Notice
# that this works concatenating two null strings with a whitespace
# character
WHITESPACE :=
WHITESPACE +=

UTILS = WHITESPACE

#---------------------------------------------------------
# Export all relevant variables to src makefiles
#---------------------------------------------------------
EXPORTED_VARS=\
  FC FC_BASE MODFLAG FFLAGS \
  CC CC_INCLUDE CFLAGS \
  LD LDFLAGS LIBS \
  AR \
  USING_OMP USING_MPI USING_UMFPACK USING_MUMPS MUMPS_INCLUDE \
  USING_PASTIX PASTIX_INCLUDE USING_MAPHYS MAPHYS_INCLUDE \
  BUILDDIR LIBDIR BINDIR SRCDIR \
  USING_OCTAVE MKOCTFILE OCTLIBS FEMILARO_OCTAVE \
  TESTDIR TEST_TARGETS

export EXPORTED_VARS $(EXPORTED_VARS)


#---------------------------------------------------------
# Set other make options
#---------------------------------------------------------
# clear unfinished targets
.DELETE_ON_ERROR:

# define the phony targets
.PHONY: all presently_working tarfile doc clean
.PHONY: $(TEST_TARGETS)


#---------------------------------------------------------
# Main targets -------------------------------------------
#---------------------------------------------------------

ALL=\
  library \
  wandzuraxiao cools-encyclopedia \
  cg ldgh ldghf psi \
  cg-stokes cg-ns dg-comp \
  eigen-test octave-io-test ode-test linsolver-test \
  scharfetter_gummel \
  lc-cn hmap \
  vp-dg vp-hj-dg \
  hj-dg \
  b-adr b-sol-minimal br1 \
  doc

PRESENTLY_WORKING=\
  library ldgh dg-comp scharfetter_gummel vp-dg hj-dg b-sol-minimal doc

TEST_TARGETS=tests-build tests-run tests-clean

# Make sure when somebody runs "make" he/she gets something...
presently_working: $(PRESENTLY_WORKING)

all: $(ALL)

$(ALL):
	$(MAKE) --directory=$(SRCDIR) $@

list help:
	echo "Available targets: $(ALL)"

list-vars:
	echo "Exported Variables : $(EXPORTED_VARS)"
	for THEVAR in $(EXPORTED_VARS); \
	do \
	   echo  "$${THEVAR} : $${!THEVAR}"; \
	done

clean:
	$(MAKE) --directory=$(SRCDIR) $@

$(TEST_TARGETS):
	$(MAKE) --directory=$(TESTDIR) $@

#---------------------------------------------------------
#---------------------------------------------------------
#---------------------------------------------------------

