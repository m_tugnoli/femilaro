# Copyright (C) 2009,2010,2011,2012  Marco Restelli
#
# This file is part of:
#   FEMilaro -- Finite Element Method toolkit
#
# FEMilaro is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# FEMilaro is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
#
# author: Marco Restelli                   <marco.restelli@gmail.com>

#---------------------------------------------------------
# General comments
#---------------------------------------------------------
# Set the general options for the compilation of FEMilaro. This file
# will be included by the main makefile, which could be the FEMilaro
# makefile or a user makefile linking the FEMilaro library. For this
# reason, one should not use here commands like $(shell pwd), which
# would give different results depending on where this file is
# included. For the same reason, relative paths should be also
# avoided.


#---------------------------------------------------------
# Initializations (do not change these settings!)
#---------------------------------------------------------
FEMILARO_CC_INCLUDE:=
FEMILARO_LDFLAGS:=
FEMILARO_LIBS:=


#-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-
#X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X
#
# Configuration section
#
#X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X
#-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-

#---------------------------------------------------------
# Main Directory
#---------------------------------------------------------
FEMILARO_DIR:=$(HOME)/FEMilaro/trunk

#---------------------------------------------------------
# Compiler
#---------------------------------------------------------
# F90 Compiler
FEMILARO_FC:=ifort
#FEMILARO_FC:=gfortran
#FEMILARO_FC:=/home/marre/PROGI/src/gcc-trunk-install/bin/gfortran
#FEMILARO_FC:=/home/marre/PROGI/src/gcc-versions/gcc-5.1.0-install/bin/gfortran
#FEMILARO_FC:=/home/marre/PROGI/src/gcc-versions/gcc-4.9.2-install/bin/gfortran
#FEMILARO_FC:=/home/marre/PROGI/src/gcc-versions/gcc-4.8.4-install/bin/gfortran
# How to tell the compiler where to look for .mod files
FEMILARO_MODFLAG:=-I
# C Compiler
FEMILARO_CC=gcc

#---------------------------------------------------------
# MPI compiler (only necessary if MPI is used)
#---------------------------------------------------------
FEMILARO_MPI_IMPL:=mpich
FEMILARO_MPIF90:=mpif90

#---------------------------------------------------------
# Compiler flags
#---------------------------------------------------------

# ifort flags
#FEMILARO_FFLAGS:= -g -O0 -stand f08 -check all -check noarg_temp_created -warn all -fpe0 -traceback -ftrapuv -fp-stack-check
#FEMILARO_FFLAGS:= -g -O0
#FEMILARO_FFLAGS:= -O3 -g -funroll-loops -stand f08
# Use ipo (set also FEMILARO_AR accordingly)
#FEMILARO_FFLAGS:=$(FEMILARO_FFLAGS) -ipo
#FEMILARO_LDFLAGS:=$(FEMILARO_LDFLAGS) -ipo
FEMILARO_FFLAGS:=$(FEMILARO_FFLAGS) -heap-arrays
FEMILARO_FFLAGS:=$(FEMILARO_FFLAGS) -g
FEMILARO_FFLAGS:=$(FEMILARO_FFLAGS) -assume byterecl -assume minus0 -assume noold_maxminloc -assume noold_unit_star -assume noold_xor -assume std_mod_proc_name
FEMILARO_FFLAGS:=$(FEMILARO_FFLAGS) -standard-semantics -assume realloc_lhs

# gfortran flags
#FEMILARO_FFLAGS:= -g -O0
#FEMILARO_FFLAGS:= -ggdb -g -O0 -pedantic -Wall -Wconversion -Wconversion-extra -fcheck=all -std=f2008ts -fall-intrinsics -finit-real=snan -ffpe-trap=invalid -finit-integer=-9999 -Wintrinsics-std -fbacktrace
#FEMILARO_FFLAGS:= -g -O0 -fcheck=all -fbacktrace
#FEMILARO_FFLAGS:= -g -ggdb -O3 -funroll-loops -ftree-vectorize -ftree-vectorizer-verbose=1 -msse3

# C compiler flags
FEMILARO_CFLAGS:= -DDLONG

# This is for profiling
#FEMILARO_FFLAGS:=$(FEMILARO_FFLAGS) -pg
#FEMILARO_LDFLAGS:=$(FEMILARO_LDFLAGS) -pg

#---------------------------------------------------------
# Linker
#---------------------------------------------------------
FEMILARO_LD:=$(FEMILARO_FC)

#---------------------------------------------------------
# Archive
#---------------------------------------------------------
FEMILARO_AR:=xiar
# With the Intel compiler, xiar can be also used. This indeed is the
# only way to get ipo in the library.
#FEMILARO_AR:=$(realpath $(FEMILARO_FC))
#FEMILARO_AR:=$(dir $(FEMILARO_AR))xiar

# Some variables used in the following configurations
FEMILARO_FC_VENDOR := $(notdir $(FEMILARO_FC))

#---------------------------------------------------------
# Octave
#---------------------------------------------------------
# Note: the two variables \<FEMILARO_OCTAVE\> and MKOCTFILE can be set
# to use a different octave/mkoctfile from $(shell which octave) and
# $(shell which mkoctfile)

FEMILARO_USING_OCTAVE:=No
ifeq ($(strip $(FEMILARO_USING_OCTAVE)),Yes)
  ifndef FEMILARO_OCTAVE
    FEMILARO_OCTAVE:=$(shell which octave)
    ifeq ($(strip $(FEMILARO_OCTAVE)),) 
      FEMILARO_USING_OCTAVE:=No
    endif
  endif
  ifndef MKOCTFILE
    MKOCTFILE:=$(shell which mkoctfile)
    ifeq ($(strip $(MKOCTFILE)),) 
      FEMILARO_USING_OCTAVE:=No 
    endif
  endif
else
  FEMILARO_USING_OCTAVE:=No
endif
ifeq ($(strip $(FEMILARO_USING_OCTAVE)),Yes)
  OCTLIBS:=$(shell $(MKOCTFILE) -p LFLAGS) \
           $(shell $(MKOCTFILE) -p OCTAVE_LIBS)     
endif

#---------------------------------------------------------
# OpenMP
#---------------------------------------------------------
# Note: select the number of threads with
#   export OMP_NUM_THREADS=n
# and optionally set the stack size of each thread with
#   export OMP_STACKSIZE=16M
FEMILARO_USING_OMP:=No
ifeq ($(strip $(FEMILARO_USING_OMP)),Yes)
  FEMILARO_FFLAGS:=$(FEMILARO_FFLAGS) -openmp
  FEMILARO_LDFLAGS:=$(FEMILARO_LDFLAGS) -openmp
  # For the intel compiler, add some additional symbols
  ifeq ($(FEMILARO_FC_VENDOR),ifort)
    FEMILARO_FFLAGS:= $(FEMILARO_FFLAGS) -parallel-source-info=2
  endif
else
  # make sure FEMILARO_USING_OMP is set to either "Yes" or "No", to
  # simplify subsequent checks
  FEMILARO_USING_OMP:=No
endif

#---------------------------------------------------------
# MPI
#---------------------------------------------------------
FEMILARO_USING_MPI:=Yes
ifeq ($(strip $(FEMILARO_USING_MPI)),Yes)
  # The Fortran compiler is redefined to the MPI wrapper
  ifeq ($(strip $(FEMILARO_MPI_IMPL)),mpich)
    FEMILARO_FC:=$(FEMILARO_MPIF90) -fc=$(FEMILARO_FC)
  else ifeq ($(strip $(FEMILARO_MPI_IMPL)),OpenMPI)
    FEMILARO_FC:=$(FEMILARO_MPIF90)
  else
    $(error "Please add options for your MPI implementation.")
  endif
  FEMILARO_LD:=$(FEMILARO_FC)
else
  # make sure FEMILARO_USING_MPI is set to either "Yes" or "No", to
  # simplify subsequent checks
  FEMILARO_USING_MPI:=No
endif

#---------------------------------------------------------
# UMFPACK
#---------------------------------------------------------
FEMILARO_USING_UMFPACK:=No
ifeq ($(strip $(FEMILARO_USING_UMFPACK)),Yes)
  FEMILARO_LIBS:=$(FEMILARO_LIBS) -lumfpack -lamd -lsuitesparseconfig -lgfortran
  FEMILARO_LDFLAGS:=$(FEMILARO_LDFLAGS)
else
  # make sure FEMILARO_USING_UMFPACK is set to either "Yes" or "No", to
  # simplify subsequent checks
  FEMILARO_USING_UMFPACK:=No
endif

#---------------------------------------------------------
# MaPHyS
#---------------------------------------------------------
FEMILARO_USING_MAPHYS:=No

# Note: MaPHyS requires at least one among MUMPS and PaStiX. Handling
# these dependencies can be complicated; here, we leave it to the user
# to select the required libraries.

ifeq ($(strip $(FEMILARO_USING_MAPHYS)),Yes)
  ifneq ($(FEMILARO_USING_MPI),Yes)
    $(error "To use MaPHyS you need MPI.")
  endif
  FEMILARO_LIBS:=$(FEMILARO_LIBS) -lmaphys \
    -lpackcg -lpackgmres -lslatec -lmetis 
  ifeq ($(FEMILARO_FC_VENDOR),ifort)
    $(error "MaPHyS with ifort not configured.")
  else ifeq ($(FEMILARO_FC_VENDOR),gfortran)
    FEMILARO_LDFLAGS:=$(FEMILARO_LDFLAGS) \
      -L/home/marre/PROGI/src/maphys_0.9.2/lib
    FEMILARO_MAPHYS_INCLUDE:= \
      -I/home/marre/PROGI/src/maphys_0.9.2/modules
  else
    $(error "Can not find the corresponding MaPHyS directory.")
  endif
else
  # make sure FEMILARO_USING_MAPHYS is set to either "Yes" or "No", to
  # simplify subsequent checks
  FEMILARO_USING_MAPHYS:=No
endif

#---------------------------------------------------------
# MUMPS
#---------------------------------------------------------
FEMILARO_USING_MUMPS:=No
ifeq ($(strip $(FEMILARO_USING_MUMPS)),Yes)
  ifneq ($(FEMILARO_USING_MPI),Yes)
    $(error "To use MUMPS you need MPI.")
  endif
  FEMILARO_LIBS:=$(FEMILARO_LIBS) -lsmumps -ldmumps -lmumps_common -lpord \
     -lscalapack -lptlapack -lptf77blas -lptcblas -lm -latlas -lpthread
  ifeq ($(FEMILARO_FC_VENDOR),ifort)
    # Ifort prefers that we don't link lm
    FEMILARO_LIBS:=$(filter-out -lm,$(FEMILARO_LIBS))
    FEMILARO_LDFLAGS:=$(FEMILARO_LDFLAGS) \
      -L/home/marre/PROGI/src/MUMPS_4.10.0-ifort/lib
    FEMILARO_MUMPS_INCLUDE:= \
      -I/home/marre/PROGI/src/MUMPS_4.10.0-ifort/include
  else ifeq ($(FEMILARO_FC_VENDOR),gfortran)
    FEMILARO_LDFLAGS:=$(FEMILARO_LDFLAGS) \
      -L/home/marre/PROGI/src/MUMPS_4.10.0-gfortran/lib
    FEMILARO_MUMPS_INCLUDE:= \
      -I/home/marre/PROGI/src/MUMPS_4.10.0-gfortran/include
  else
    $(error "Can not find the corresponding MUMPS directory.")
  endif
else
  # make sure FEMILARO_USING_MUMPS is set to either "Yes" or "No", to
  # simplify subsequent checks
  FEMILARO_USING_MUMPS:=No
endif

#---------------------------------------------------------
# PaStiX
#---------------------------------------------------------
FEMILARO_USING_PASTIX:=No
ifeq ($(strip $(FEMILARO_USING_PASTIX)),Yes)
  ifneq ($(FEMILARO_USING_MPI),Yes)
    $(error "To use PaStiX you need MPI.")
  endif
  FEMILARO_LIBS:=$(FEMILARO_LIBS) -lpastix_murge -lpastix \
   -lptscotch -lscotch -lhwloc \
   `pkg-config --libs blas` -lpthread
  FEMILARO_LDFLAGS:=$(FEMILARO_LDFLAGS) -L/home/marre/PROGI/lib
  FEMILARO_PASTIX_INCLUDE:= \
    -I/home/marre/PROGI/include
else
  # make sure FEMILARO_USING_PASTIX is set to either "Yes" or "No", to
  # simplify subsequent checks
  FEMILARO_USING_PASTIX:=No
endif

#-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-
#X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X
#
# End of configuration section
#
#X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X
#-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-


# Subdirectories
FEMILARO_SRCDIR  :=$(FEMILARO_DIR)/src
FEMILARO_BUILDDIR:=$(FEMILARO_DIR)/build
FEMILARO_LIBDIR  :=$(FEMILARO_DIR)/lib
FEMILARO_BINDIR  :=$(FEMILARO_DIR)/bin

