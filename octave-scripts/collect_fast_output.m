function data = collect_fast_output(infile)
% data = collect_fast_output(infile)
%
% Read a fast output file infile written either by dg-comp or by
% cg-ns. The output variable data contains the following fields:
%
% data.ie: element indexes
% data.times: time levels
% data.field1: data for the first output field
% data.field2: data for the second output field
% ...
%
% The data structure can be passed directly to interp_fast_output to
% interpolate the time history to a spacific point.

 % We assume that the octave part of the input file contains a first
 % field with the element indexes and subsequent fields with the name
 % of the variable as keyword and the per time-step variable dimension
 % as value. Notice that the per time-step variable dimension must be
 % an array with three elements, specifying:
 %  ( number-of-components , number-of-dof , number-of-elements )
 % For scalar variables, one will have number-of-components = 1.
 description = load(infile);
 varnames = fieldnames(description);

 % Initialize the work array tmp_data with one entry for each variable
 nvars = size(varnames,1)-1;
 for i=1:nvars
   tmp_data{i,1} = [];
   ndvars(:,i) = getfield(description,varnames{i+1})';
   nnvars(i,1) = prod(ndvars(:,i));
 end

 fid = fopen(infile,'r');

 % locate the data section
 delimiter = '#-/DELIMITER\-#';
 delsize = length(delimiter);
 found = 0;
 while(~found)
   line = fgets(fid);
   if(length(line)>=delsize)
     if(strcmp(line(1:delsize),delimiter))
       found = 1;
     end
   end
 end

 
%ALLOCARE I VETTORI DELLA DIMENSIONE GIUSTA!
 for j=1:nvars
   tmp_data{j,1} = zeros(ndvars(:,j)); % initialize the right shape
 end
 i = 0;
 while(line~=-1)
   line = fgets(fid);
   if(line~=-1)
     i = i+1;
     times(i) = sscanf(line,'%f');
     for j=1:nvars
       line = fgets(fid);
       tmp_data{j,1}(:,:,:,i) = reshape( ...
        sscanf(line,'%f',nnvars(j)) , ndvars(:,j) );
     end
   end
 end
 
 fclose(fid);

 % Copy and reshape
 data.ie = getfield(description,varnames{1});
 data.times = times;
 for i=1:nvars
   if(ndvars(1,i)==1) % scalar field
     data = setfield(data,varnames{i+1,1}, ...
              permute(tmp_data{i,1},[2 3 4 1]));
   else
     data = setfield(data,varnames{i+1,1},tmp_data{i,1});
   end
 end

return
