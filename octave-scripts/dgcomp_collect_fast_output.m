function data = dgcomp_collect_fast_output(infile)
% data = dgcomp_collect_fast_output(infile)

 error('This function is out of date: use collect_fast_output');

 description = load(infile);

 fid = fopen(infile,'r');

 % locate the data section
 delimiter = '#-/DELIMITER\-#';
 delsize = length(delimiter);
 found = 0;
 while(~found)
   line = fgets(fid);
   if(length(line)>=delsize)
     if(strcmp(line(1:delsize),delimiter))
       found = 1;
     end
   end
 end

 i = 0;
 while(line~=-1)
   line = fgets(fid);
   if(line~=-1)
     i = i+1;
     times(i) = sscanf(line,'%f');
     line = fgets(fid);
     uuu(:,:,:,i) = reshape( ...
        sscanf(line,'%f',prod(description.DS)) , description.DS);
   end
 end
 
 fclose(fid);

 data.ie = description.ie_fop;
 data.times = times;
 data.uuu = uuu;

return
