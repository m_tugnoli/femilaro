function [p,e,t] = grid_gmsh2oct(d,gridfile,varargin)
% [p,e,t] = grid_gmsh2oct(d,gridfile,renumber)
%
% This function reads a .msh gmsh grid from file "gridfile" and
% reformats it in the usual [p,e,t] "Matlab-like" format.
%
% For 2D meshes, the third coordinate is ignored.
%
% renumber is optional and, if present, it defines a renumbering for
% the boundary labels in e. The length of renumber must be (at least)
% equal to the maximum boundary label used in gridfile. Consider that:
% a) setting renumber(i)<0 the sides with boundary label i are
%   discarded (this is useful, for instance, to drop internal
%   boundaries)
% b) if it's known that an intermediate label i is not used, the
%   corresponding element renumber(i) should be NaN, in order to
%   detect any error.
%
% LIMITATIONS: 
% - only one label for the domains is recognized
% - the curvilinear abscissa in e is not set

% Note: some details about the msh format can be found here:
% http://www.geuz.org/gmsh/doc/texinfo/gmsh.html#MSH-ASCII-file-format

% General idea: gmsh saves many entities which we do not need, such as
% points or faces which do not belong to any element. This means that
% we need
% 1) eliminate the undesired entities
% 2) renumber the remaining ones to avoid gaps
%
% We do this by first reading all the entities defined by gmsh and
% then keeping only those which are connected to at least one element.
% While eliminating the undesired entities we also drop those sides
% which are eliminated by the renumbering optional input.


 % 1) Read all the GMSH entities ---------------------------------------

 fid = fopen(gridfile,'r');

 % Vertices
 fseek(fid,0,SEEK_SET);
 line = '';
 pos = [];
 while(isempty(pos))
   line = fgets(fid);
   pos = strfind(line,'$Nodes');
 end
 nv = fscanf(fid,'%d',1);
 % vertex coordinates
 p_gmsh = fscanf(fid,'%d %f %f %f',[4,nv]);
 p_gmsh = p_gmsh(2:1+d,:); % skip the vertex index: first row

 % Elements and sides

 % preliminary: decide what are the elements and what are the sides
 switch d
  case 2
   telm = 2; % triangle
   tsde = 1; % line segment
  case 3
   telm = 4; % tetrahedron
   tsde = 2; % triangle
 endswitch

 % now get the entities
 fseek(fid,0,SEEK_SET);
 line = '';
 pos = [];
 while(isempty(pos))
   line = fgets(fid);
   pos = strfind(line,'$Elements');
 end
 nes = fscanf(fid,'%d',1); line = fgets(fid); % get also the newline
 t_gmsh = zeros(d+2,nes); % we still don't know how many el. and sides
 e_gmsh = zeros(d+3,nes);
 ns = 0;
 ne = 0;
 for i=1:nes
   line = fgets(fid);
   ijk = str2num(line);
   switch ijk(2)
    case tsde
     ns = ns+1;
     % side vertexes
     e_gmsh(1:d,ns) = ijk( 4+ijk(3) : 4+ijk(3)+(d-1) );
     e_gmsh(d+3,ns) = ijk(    5     );
    case telm
     ne = ne+1;
     t_gmsh(1:d+1,ne) = ijk( 4+ijk(3) : 4+ijk(3)+d );
    % no need for otherwise: other entities are ignored
   endswitch
 end

 fclose(fid);

 % free some memory
 t_gmsh = t_gmsh(:,1:ne);
 e_gmsh = e_gmsh(:,1:ns);


 % 2) Drop all the undesired entities ----------------------------------

 e_keep = -1*ones(1,ns);

 % generate a list of the valid sides
 ii = nchoosek([1:d+1],d); % element sides (index combinations)
 for i=1:d+1 % element sides
   valid_s(i,:) = side_id( t_gmsh(ii(i,:),:) ,nv);
 end
 valid_s = unique( valid_s(:) );

 % drop the selected sides
 if(nargin>2)
   new_sm = varargin{1}; % new side markers
   keep_s = -1*ones(1,ns);
   for is=1:ns
     if( isnan(new_sm(e_gmsh(d+3,is))) )
       warning(['NaN label for side lable ',num2str(is)])
     else
       new_m = new_sm(e_gmsh(d+3,is));
       if( new_m>0 ) % keep the side
         keep_s(is) = 1;
         e_gmsh(d+3,is) = new_m;
       end % nothing to to for sides to discard
     end
   end
   e_gmsh = e_gmsh(:, find(keep_s==1) );
 end

 % generate a list of the gmsh sides
 gmsh_s = side_id( e_gmsh(1:d,:) ,nv);

 % select the meaningful sides
 m_sides = ismember( gmsh_s , valid_s );
 if( ~isempty(find(m_sides==0)) )
   warning(["\nSome GMSH sides do not belong to any element and ", ...
            "will be eliminated.\nThis could lead to missing ", ...
            "boundary sides in the computation.\n", ...
            "These sides have the following marker(s): ",...
            num2str(unique(e_gmsh(d+3,find(m_sides==0)))),"\n"]);
 end
 e_gmsh = e_gmsh( : , find(m_sides) );

 % drop the vertexes
 v_keep = -1*ones(1,nv);
 v_keep( unique(t_gmsh(1:d+1,:)(:)) ) = 1;
 nv = 0;
 for iv=1:length(v_keep)
   if(v_keep(iv)==1)
     nv = nv+1;
     v_keep(iv) = nv;
   end
 end

 p = p_gmsh(1:d, find( v_keep>0 ) );

 t = [ v_keep(t_gmsh( 1:d+1 ,:)) ; t_gmsh(d+2:end,:) ];

 e = [ v_keep(e_gmsh(  1:d  ,:)) ; e_gmsh(d+1:end,:) ];

return


% Map a column side to an integer
function sid = side_id(s,nv)

 s_s = sort(s,"descend"); % sort works by columns

 sid = uint64( s_s(1,:) );
 for i=2:size(s_s,1)
   sid += uint64( s_s(i,:) ) * uint64( nv+1 )^(i-1);
 end
  
return

