function [p,e,t] = grid_h2d_gmsh2oct(gridfile,varargin)
% [p,e,t] = grid_h2d_gmsh2oct(gridfile,varargin)
%
% Similar to grid_gmsh2oct but for a 2D hybrid grid.

 fid = fopen(gridfile,'r');

 % Vertices
 fseek(fid,0,SEEK_SET);
 line = '';
 pos = [];
 while(isempty(pos))
   line = fgets(fid);
   pos = strfind(line,'$Nodes');
 end
 nv = fscanf(fid,'%d',1);
 % vertex coordinates
 xyz = fscanf(fid,'%d %f %f %f',[4,nv]);
 % It is possible that additional vertexes are included in the mesh
 % file which do not belong to any element. We thus check which are
 % the vertexes really belonging to the mesh while defining the
 % elements, and define p afterwards.
 vo_tmp = zeros(1,nv);
 vn_tmp = vo_tmp;
 nv = 0; % reset the counter

 % Elements and sides
 fseek(fid,0,SEEK_SET);
 line = '';
 pos = [];
 while(isempty(pos))
   line = fgets(fid);
   pos = strfind(line,'$Elements');
 end
 nes = fscanf(fid,'%d',1);
 line = fgets(fid); % skip an empty line
 ns = 0;
 ne = zeros(1,4);
 elpoint = ftell(fid);
 % Count the elements of different shapes
 for i=1:nes
   line = fgets(fid);
   ijk = str2num(line);
   % the second number identifies the MSH type:
   % 1 -> line segment
   % 2 -> triangle
   % 3 -> quadrilateral
   % 4 -> tetrahedron
   switch ijk(2)
    case 1
     ns = ns+1;
    case 2
     ne(3) = ne(3)+1;
    case 3
     ne(4) = ne(4)+1;
    otherwise
     % nothing to do
   endswitch
 end
 t{1}.ie = zeros(1,ne(3)); % triangles
 t{1}.it = zeros(3,ne(3));
 t{2}.ie = zeros(1,ne(4)); % quadrilaterals
 t{2}.it = zeros(4,ne(4));
 e = zeros(5,ns);
 ns = 0;
 ne(:) = 0;
 fseek(fid,elpoint,SEEK_SET);
 for i=1:nes
   line = fgets(fid);
   ijk = str2num(line);
   switch ijk(2)
    case 1
     ns = ns+1;
     % side vertexes
     e(1:2,ns) = ijk( 4+ijk(3) : 4+ijk(3)+(2-1) );
     e(2+3,ns) = ijk(    5     );
    case 2
     ne(3) = ne(3)+1;
     t{1}.ie(ne(3)) = sum(ne);
     t{1}.it(:,ne(3))  = ijk( 4+ijk(3) : 4+ijk(3)+2 );
     % renumber the vertexes
     for j=0:2
       jj = 4+ijk(3)+j;
       if(vo_tmp(ijk(jj))==0)
         nv = nv+1;
	 vo_tmp(ijk(jj)) = nv;
	 vn_tmp(nv) = ijk(jj);
       endif
     end
    case 3
     ne(4) = ne(4)+1;
     t{2}.ie(ne(4)) = sum(ne);
     t{2}.it(:,ne(4))  = ijk( 4+ijk(3) : 4+ijk(3)+3 );
     % renumber the vertexes
     for j=0:3
       jj = 4+ijk(3)+j;
       if(vo_tmp(ijk(jj))==0)
         nv = nv+1;
	 vo_tmp(ijk(jj)) = nv;
	 vn_tmp(nv) = ijk(jj);
       endif
     end
   endswitch
 end

 fclose(fid);

 % Now we can renumber the vertexes.
 % discard the label (and possibly the z coordinate)
 p = xyz(1+1:1+2,vn_tmp(1:nv));
 % use the new node order
 for i=1:length(t)
   t{i}.it = vo_tmp(t{i}.it);
 end
 e(1:2,:) = vo_tmp(e(1:2,:));

 if(nargin>1)
   newl = varargin{1};
   side_del = [];
   for i=1:ns
     old_l = e(2+3,i);
     new_l = newl(old_l);
     if(new_l<0)
       side_del = [side_del i];
     else
       if(isnan(newl(e(2+3,i))))
         warning(['NaN label for side lable ',num2str(old_l)])
       endif
       e(2+3,i) = newl(e(2+3,i));
     end
   end
   e(:,side_del) = [];
 end

return

