function [grids,p_fixed,iv_ren,p_m2slave] = ...
                                    grid_partition(p,e,t,idx,varargin)
%
% grids = grid_partition(p,e,t,idx)
% grids = grid_partition(p,e,t,idx,periodic_faces)
% grids = grid_partition(p,e,t,idx,periodic_faces,data_names,data)
% [grids,p_fixed] = grid_partition(...)
% [grids,p_fixed,iv_ren,p_m2slave] = grid_partition(...)
%
% Partition the mesh (p,e,t) into length(idx) subgrids using the
% indexes in idx.
%
% Consider that idx can also be a function handle, in which case it is
% supposed to receive the coordinates of the element vertexes and
% return the partition index idx>=0.
%
% Notes:
%
% A) The index vector idx starts from 0: this is consistent with both
% METIS and MPI.
%
% B) The general procedure for partitioning a mesh is:
% 1) write the complete mesh in a form that is readable by mpmetis,
%   that is
%     Nel
%     e1v1 e1v2 e1v3 [e1v4]
%     e2v1 e2v2 e2v3 [e2v4]
%     ...
%   where Nel is the number of elements and the following rows are the
%   nodes of the elements (vertex numbering starts from 1)
% 2) partition the mesh with  "mpmetis -gtype=dual input-file N"
% 3) use this function with the idx provided by mpmetis (i.e. idx is
%    the array obtained loading the *.epart.* metis output file)
% 4) save each mesh returned in grids as a separate mesh file.
%
% C) One could also use "-gtype=nodal", however, since we distribute
% the elements and duplicate nodes and sides, the communications are
% minimized using "-gtype=dual".
%
% D) The decomposed grid files can be written as follows:
% fname = 'xyz';
% d = 23;
% for i=1:size(grids,2)
%   p=grids(i).p; t=grids(i).t; e=grids(i).e; ivl2ivn=grids(i).ivl2ivn;
%   vmap_l2g = grids(i).vmap_l2g; % only for postprocessing
%   save([fname,'.',num2str(i-1,'%.3d')],'d','p','e','t','ivl2ivn','vmap_l2g');
% end
%
% E) The return value grids includes a field ivl2ivn, which is an
% array with one element for each subgrid node. Each element is a
% struct with the following fields (assuming iv is the index of the
% node):
%  ivl2ivn(iv).nd -> number of neighbouring grids sharing this vertex
%  ivl2ivn(iv).id(:) -> indexes of the neighbouring grids
%  ivl2ivn(iv).iv(:) -> local indexes in the neighbouring grids
%
% F) The return value grids also includes two fields vmap_l2g,emap_l2g
% which can be used to map the local vertex/element indexing on the
% partitioned grids to the one used in the input grid. For instance,
% given some field f defined on the vertexes of each partitioned grid,
% a global field gf can be recovered by
%   for i=1:size(grids,2)
%     gf(grids(i).vmap_l2g) = f{i};
%   end
% Another useful example is distributing element data defined for the
% global grid to the partitioned grid, which can be done as
%   for i=1:size(grids,2)
%     local_edata = global_edata(grids(i).emap_l2g);
%   end
% This can be useful, for instance, for the pre/postprocessing. Notice
% however that these fields are meaningful only when working with both
% the partitioned grids *and* the original grid; a code which works on
% the sole partitioned grids therefore should not use vmap_l2g nor
% emap_l2g. Notice also that, since this is a correspondence with the
% local indexes on the partitioned grids, it is independent from any
% periodic boundary condition.
%
% G) The optional argument periodic_faces is a list of pairs of faces
% which have periodic boundary conditions. The list is in the form
%    periodic_faces = [ mf1 mf2 ... ]
%                     [ sf1 sf2 ... ]
%                     [ t11 t12 ... ]
%                     [ t21 t22 ... ]
%                     [ ... ... ... ]
% where each column corresponds to a master/slave pair and mf and sf
% are the indexes of two faces (master and slave) and [t1 t2 ...] is
% the translation vector which maps sf on mf.
%
% H) The output argument p_fixed is useful when using periodic
% boundary conditions on a mesh which is not exactly periodic: this is
% in fact a copy of the input value of p where the nodes on the slave
% boundaries are moved so that they correspond to the position of the
% master ones. If one wishes to use this corrected nodes, one should
% call the function a second time using p_fixed instead of p.
%
% I) The optional arguments data_names and data provide nodal data
% which are then partitioned together with the grid and stored in
% additional fields of the return value grids. data_fields must be a
% list of strings with the data names (used to name the corresponding
% additional fields in grids), while data must be an array where each
% column corresponds to a data field. To pass nodal data without
% specifying periodicity information, use
%   grids = grid_partition(p,e,t,idx,[],data_names,data)
%
% L) The two output arguments iv_ren and p_m2slave can be useful to
% see the details of the grid partitioning. More specifically:
% iv_ren: this arrays provides the global to local node renumbering;
%         it has one row for each subgrid and one column for each
%         global node, and lists the local index that each node
%         receives on each subdomain
% p_m2slave: whenever using periodic boundary conditions, this array
%            shows the master to slave correspondence for the nodes
%            collocated on the periodic faces, using the global node
%            numbering

 %---------------------------------------------------------------------
 % Check the input arguments

 % periodicity
 if((nargin>4)&&(~isempty(varargin{1})))
   sideper = varargin{1};
   for i=1:size(sideper,2)
     VM{i} = [];
     VS{i} = [];
   end
   toll = 1e-10;
 else
   sideper = zeros(2,0);
 end

 % nodal data
 have_noda_data = 0;
 if(nargin>5)
   have_noda_data = 1;
   node_data_names = varargin{2};
   node_data       = varargin{3};
   if(numel(node_data_names)~=columns(node_data))
     error(['The number of data names is not consistent ' ...
            'with the number of data fields']);
   end
 end

 % partitioning could be provided as a function handle
 if(is_function_handle(idx))
   fh = idx; % idx will be redefined as an array
   idx = zeros(size(t,2),1);
   dim = size(p,1); % space dimension
   for ie=1:size(t,2)
     idx(ie) = fh( p(:,t(1:dim+1,ie)) );
   end
 end
 %---------------------------------------------------------------------

 % initialize working arrays
 N = max(idx)+1;  % number of subgrids (idx starts from 0)
 dim = size(p,1); % space dimension
 iv_ren = zeros(N,size(p,2)); % local renumbering for each subgrid
 nv = zeros(N,1); % number of nodes for each subgrid
 nt = zeros(N,1); % number of elements for each subgrid

 % loop over the elements to count the local nodes and elements
 for ie=1:size(t,2)
   isg = idx(ie)+1; % index of the subgrid (starting from 1)
   nt(isg) = nt(isg) + 1;
   for l=1:dim+1 % loop on the nodes
     iv = t(l,ie);
     if(iv_ren(isg,iv)==0) % new local vertex
       nv(isg) = nv(isg) + 1;
       iv_ren(isg,iv) = nv(isg);
     end
   end
 end

 % check that all the nodes have been included at least in one domain
 if( min( max( iv_ren , 1 ) ) < 1 )
   error(['The following vertexes have not been assigned to any ', ...
          'processor: ',num2str(find( max( iv_ren , 1 ) == 0 ))]);
 end

 % allocate the local grids
 for isg=1:N
   grids(isg) = struct( ...
     'p', zeros(dim,nv(isg)), ...
     'e', zeros(size(e)), ... % will be reshaped
     't', zeros(dim+1,nt(isg)), ...
     'ivl2ivn', [] , ...
     'vmap_l2g', zeros(1,nv(isg)) , ...
     'emap_l2g', zeros(1,nt(isg)) );
   grids(isg).ivl2ivn(1:nv(isg)) = struct( ...
     'nd', 0, ...
     'id', [], ...
     'iv', [] );
   % since iv_ren will not change, vmap_l2g can be already computed
   iv_g_on_i = find(iv_ren(isg,:)!=0); % global verts on local grid
   grids(isg).vmap_l2g(iv_ren(isg,iv_g_on_i)) = iv_g_on_i;
 end

 % fill grids.p
 for iv=1:size(p,2)
   for isg=1:N
     if(iv_ren(isg,iv)!=0) % vertex iv belongs to subgrid isg
       grids(isg).p(:,iv_ren(isg,iv)) = p(:,iv);
     end
   end
 end

 % fill grids.t
 nt(:) = 0; % reset the counter
 for ie=1:size(t,2)
   isg = idx(ie)+1; % index of the subgrid
   nt(isg) = nt(isg) + 1;

   % keep track of the original elements
   grids(isg).emap_l2g(:,nt(isg)) = ie;

   % insert the nodes
   iv = t(1:dim+1,ie);                       % global indexes
   grids(isg).t(:,nt(isg)) = iv_ren(isg,iv); % local indexes
 end

 % fill grids.ivl2ivn (periodicity information will be added later)
 for iv=1:size(p,2) % loop on the vertex of the full grid
   nsg = sum( (iv_ren(:,iv)!=0) ); % iv belongs to nsg subgrids
   if(nsg>1) % the node belongs to more than one grid
     isg = find( (iv_ren(:,iv)!=0) )'; % subgrids which the node belongs to
     for i=1:length(isg)
       ign = setdiff(isg,isg(i)); % neighboring subgrids
       grids(isg(i)).ivl2ivn(iv_ren(isg(i),iv)) = struct( ...
         'nd', nsg-1, ... % number of subgrids sharing the node
         'id', ign-1, ... % subdomains are numbered starting from 0
         'iv', iv_ren(ign,iv)' ); % local indexes on neigh. subgrids
     end
   end
 end

 % partition e and include periodicity
 % This is done in the following steps:
 %  1) identify the boundary nodes
 %  2) define a vertex to elements pointer for the boundary nodes
 %  3) assign the boundary sides to the subgrids
 v2bv = zeros(size(p,2),1); % vertex to boundary vertex
 nbv  = 0;                  % number of boundary vertexes
 for ibs=1:size(e,2)
   % Check whether the face to which the boundary side belongs is
   % included in the list of master/slave periodic faces. We assume
   % that a face can appear in sideper only once.
   % i_ms: row index of sideper (i_ms=1->master, i_ms=2->slave)
   % i_sp: column index of sideper (index of side pair)
   [i_ms,i_sp] = find(sideper(1:2,:)==e(dim+2+1,ibs));
   if(isempty(i_ms)) % side is not periodic
     for ivl=1:dim
       iv = e(ivl,ibs);
       if(v2bv(iv)==0) % found a new boundary vertex
         nbv = nbv + 1;
         v2bv(iv) = nbv;
       end
     end
   else % side is periodic: add the connectivity information
     if(i_ms==1) % side belongs to the master face
       VM{i_sp} = [VM{i_sp},e(1:dim,ibs)']; % list of nodes (master)
     else % side belongs to the slave face
       VS{i_sp} = [VS{i_sp},e(1:dim,ibs)']; % list of nodes (slave)
     end
     % mark the side, so that it will be no longer considered a
     % boundary side
     e(1,ibs) = -1;
   end
 end
 if(nbv>0)
   bv2e{nbv} = []; % boundary vertex -> connected elements
 end
 for ie=1:size(t,2)
   for ivl=1:dim+1
     ibv = v2bv(t(ivl,ie));
     if(ibv!=0)
       bv2e{ibv} = [bv2e{ibv} , ie];
     end
   end
 end
 nbs = zeros(N,1);
 for ibs=1:size(e,2)
   if(e(1,ibs)!=-1) % side is not marked
     % identify the connected element
     ie = bv2e{v2bv(e(1,ibs))};
     for ivl=2:dim
       ie = intersect(ie,bv2e{v2bv(e(ivl,ibs))});
     end
     if(length(ie)>1) % we should never get here
       error(['The boundary side ',num2str(ibs), ...
              ' belongs to more than one element: ',num2str(ie)]);
     end
     if(length(ie)==0) % we should never get here
       error(['The boundary side ',num2str(ibs), ...
              ' does not belong to any element.']);
     end
     % now we know that the boundary side belongs to ie
     bs = e(:,ibs);
     isg = idx(ie) + 1; % subgrid index
     bs(1:dim,1) = iv_ren(isg,bs(1:dim,1)); % local node numbering
     nbs(isg) = nbs(isg) + 1;
     grids(isg).e(:,nbs(isg)) = bs; % assign the boundary side to isg
   end
 end
 for isg=1:N
   grids(isg).e = grids(isg).e(:,1:nbs(isg)); % trim grids.e
 end

 % fix the periodicity information
 p_fixed = p;
 if(nargout>3)
   p_m2slave = -1*ones(1,size(p,2));
 end
 for i=1:size(sideper,2)  % loop on the master faces
   vm = unique(VM{i}); % master nodes for this face (global numbering)
   vs = unique(VS{i}); % slave nodes for this face (global numbering)
   found_slave = zeros(size(vs));
   for ivm=1:length(vm)   % loop on the master nodes
     xvm = p(:,vm(ivm)) - sideper(3:end,i); % periodic translation
     found_master = 0; dist = inf;
     for ivs=1:length(vs) % loop on the slave nodes
       distn = norm(p(:,vs(ivs))-xvm);
       if(distn<=toll) % found
         found_master = 1;
         found_slave(ivs) = 1;

         if(nargout>3)
           p_m2slave(vm(ivm)) = vs(ivs);
         end

         % domains on the master side
         dom_ms = find(iv_ren(:,vm(ivm))!=0); % subdomain(s)
         num_ms = iv_ren(dom_ms,vm(ivm));     % local indexes

         % domains on the slave side
         dom_ss = find(iv_ren(:,vs(ivs))!=0); % subdomain(s)
         num_ss = iv_ren(dom_ss,vs(ivs));     % local indexes

         % add data
         for iv=1:length(dom_ms)
           master_ivl2ivn = grids(dom_ms(iv)).ivl2ivn(num_ms(iv));
           master_ivl2ivn.nd =  master_ivl2ivn.nd + length(dom_ss);
           master_ivl2ivn.id = [master_ivl2ivn.id , dom_ss'-1];
           master_ivl2ivn.iv = [master_ivl2ivn.iv , num_ss'  ];
           % Periodicity must be updated recursively (one level)
           for k=1:length(dom_ss)
             slave_ivl2ivn = grids(dom_ss(k)).ivl2ivn(num_ss(k));
             for kk=1:slave_ivl2ivn.nd
               % The point now is avoiding:
               % a) adding a vertex to itself
               % b) adding the same neighbour twice
               new_id = slave_ivl2ivn.id(kk);
               new_iv = slave_ivl2ivn.iv(kk);
               skip_new = 0;
               if( and( new_id == dom_ms(iv)-1 , new_iv == num_ms(iv) ) )
                 skip_new = 1;     % trying to add a vertex to itself
               end
               for hh=1:master_ivl2ivn.nd
                 if( and( new_id == master_ivl2ivn.id(hh), ...
                          new_iv == master_ivl2ivn.iv(hh) ) )
                   skip_new = 1;   % the neighbour is already present
                 end
               end
               if( not(skip_new) ) % found a new neigbour
                 master_ivl2ivn.nd = master_ivl2ivn.nd + 1;
                 master_ivl2ivn.id(1,end+1) = slave_ivl2ivn.id(kk);
                 master_ivl2ivn.iv(1,end+1) = slave_ivl2ivn.iv(kk);
               end
             end
           end
           grids(dom_ms(iv)).ivl2ivn(num_ms(iv)) = master_ivl2ivn;
         end
         for iv=1:length(dom_ss)
           slave_ivl2ivn = grids(dom_ss(iv)).ivl2ivn(num_ss(iv));
           slave_ivl2ivn.nd =  slave_ivl2ivn.nd + length(dom_ms);
           slave_ivl2ivn.id = [slave_ivl2ivn.id , dom_ms'-1];
           slave_ivl2ivn.iv = [slave_ivl2ivn.iv , num_ms'  ];
           for k=1:length(dom_ms)
             master_ivl2ivn = grids(dom_ms(k)).ivl2ivn(num_ms(k));
             for kk=1:master_ivl2ivn.nd
               new_id = master_ivl2ivn.id(kk);
               new_iv = master_ivl2ivn.iv(kk);
               skip_new = 0;
               if( and( new_id == dom_ss(iv)-1 , new_iv == num_ss(iv) ) )
                 skip_new = 1;     % trying to add a vertex to itself
               end
               for hh=1:slave_ivl2ivn.nd
                 if( and( new_id == slave_ivl2ivn.id(hh), ...
                          new_iv == slave_ivl2ivn.iv(hh) ) )
                   skip_new = 1;   % the neighbour is already present
                 end
               end
               if( not(skip_new) ) % found a new neigbour
                 slave_ivl2ivn.nd = slave_ivl2ivn.nd + 1;
                 slave_ivl2ivn.id(1,end+1) = master_ivl2ivn.id(kk);
                 slave_ivl2ivn.iv(1,end+1) = master_ivl2ivn.iv(kk);
               end
             end
           end
           grids(dom_ss(iv)).ivl2ivn(num_ss(iv)) = slave_ivl2ivn;
         end

         break
       else
         if(distn<dist)
           dist = distn;
           i_nearest = vs(ivs);
         end
       end
     end
     if(~found_master)
       warning([ ...
         'Unable to find a corresponding node for iv=', ...
                                              num2str(vm(ivm)), ...
         ', belonging to boundary face ',num2str(sideper(1,i)), ...
         ', periodic with boundary face ',num2str(sideper(2,i)),...
         '; nearest node: ',num2str(i_nearest), ...
         ', distance: ',num2str(dist)]);
       p_fixed(:,i_nearest) = p(:,vm(ivm)) - sideper(3:end,i);
     end
   end
   if(~isempty(find(found_slave==0)))
     warning([ ...
     'Unable to find a corresponding node for the following nodes: ',...
     num2str(vs(find(found_slave==0))), ...
     ', belonging to boundary face ',num2str(sideper(2,i)), ...
     ', periodic with boundary face ',num2str(sideper(1,i)) ]);
   end
 end

 % partition nodal data
 if(have_noda_data)
   for isg=1:N % loop on subgrids
     for id=1:numel(node_data_names) % loop on data fields
       [~,globa_idx,local_idx] = find(iv_ren(isg,:));
       grids(isg).(node_data_names{id})(local_idx) = ...
                              node_data(globa_idx,id);
     end
   end
 end

return

