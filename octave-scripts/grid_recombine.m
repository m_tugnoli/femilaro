function [d,p,e,t] = grid_recombine(fname,N)
% [d,p,e,t] = grid_recombine(fname,N)
%
% This can be considered the inverse function of grid_partition: given
% a sequence of partitioned grids, recombine them into a unique grid,
% typically to make a different partitioning.
%
% This function relies on the variable vmap_l2g in the grid file to
% recover the global vertex indexes.
%
% The ordering of the elements and sides will not be the same as the
% original grid, since there is no way to recover this information. In
% any case, this should not matter.

 p = [];
 e = [];
 t = [];

 for i=0:N % loop on the grid files
   gi = load([fname,'.',num2str(i,'%.4d')],'d','p','e','t','vmap_l2g');

   % nodes; shared vertexes are overwritten, should be fine
   p( : , gi.vmap_l2g ) = gi.p;

   % switch to global vertex indexes
   gi.e(1:gi.d  ,:) = gi.vmap_l2g( gi.e(1:gi.d  ,:) );
   gi.t(1:gi.d+1,:) = gi.vmap_l2g( gi.t(1:gi.d+1,:) );

   % build the global arrays
   e = [e , gi.e];
   t = [t , gi.t];

 end

 d = gi.d;

return

