function pack_MPI_files(output_file,input_file,itimes,iMPI)
%
% Collects all the files of an MPI run and pack them into: one file
% for the grid/base and one file for each time level.
%
% The MPI subdomains are simply collected into one-dimensional arrays
% (rows).
%
% The files are saved in -mat format, which can be directly loaded
% from python.
%
% output_file, input_file: these are names (string) of the files to
%   process; botroh input_file and output_file must contain a '%' to
%   indicate the subrodomain number and a '$' to indicate the suffix
%   'grid', 'base' or 'reros-XXXX'.
% itimes: an integer array with the time stamps to be processed
%   iMPI: the number of MPI partitions (it is understood that all the
%   files corresponding to the partitions [0:iMPI] will be processed)
%
% As an example, suppose that the want to process the following files:
%
%  test-ABC-P000-grid.octxt
%  test-ABC-P000-base.octxt
%  test-ABC-P000-res-0000.octxt
%    ...
%  test-ABC-P000-res-0050.octxt
%    ...
%  test-ABC-P009-grid.octxt
%  test-ABC-P009-base.octxt
%  test-ABC-P009-res-0000.octxt
%    ...
%  test-ABC-P009-res-0050.octxt
%
% i.e. the results obtained with 10 processors and 51 output files for
% each processor, and suppose that we are interested in the output
% files for time stamps [0 10 20 30 40 50]. The we would use
%
% pack_MPI_files("test-ABC-$.mat","test-ABC-P%-$.octxt",[0:10:50],9)

 part_format = '%.3i'; % how many digits for the MPI partition
 time_format = '%.4i'; % how many digits for the time stamp
 %mat_format = "-text"; % default ASCII format
 mat_format = "-V7";

 skip_fh = strcmp(mat_format,"-V7");

 i_xx = find(input_file =='$');
 if(isempty(i_xx))
   error("input_file must contain a '$' character")
 end
 o_xx = find(output_file=='$');
 if(isempty(o_xx))
   error("output_file must contain a '$' character")
 end

 % Read the grid files
 fname = [input_file(1:i_xx-1), 'grid', input_file(i_xx+1:end)];
 id_pos = find(fname=='%');
 for id=0:iMPI
   fname_id = [fname(       1:id_pos-1),num2str(id,part_format), ...
               fname(id_pos+1:end     )];
   vars = load(fname_id); % all the grid variables
   for [val,key]=vars % loop over the variables found in input_file
     if(skip_fh)
       VARS.(key)(id+1) = eliminate_function_handles(val);
     else
       VARS.(key)(id+1) = val;
     end
   end
 end

 % Read the base files
 fname = [input_file(1:i_xx-1), 'base', input_file(i_xx+1:end)];
 id_pos = find(fname=='%');
 for id=0:iMPI
   fname_id = [fname(       1:id_pos-1),num2str(id,part_format), ...
               fname(id_pos+1:end     )];
   vars = load(fname_id); % all the grid variables
   for [val,key]=vars % loop over the variables found in input_file
     if(skip_fh)
       VARS.(key)(id+1) = eliminate_function_handles(val);
     else
       VARS.(key)(id+1) = val;
     end
   end
 end

 % Write all these variables in a "grid" file
 fname = [output_file(1:o_xx-1), 'grid', output_file(o_xx+1:end)];
 if(exist("VARS"))
   save(mat_format,fname,"-struct","VARS");
   clear VARS
 end

 % Now the results
 for i=1:length(itimes)
   it = itimes(i);
   its = num2str(it,time_format);
   fname = [input_file(1:i_xx-1), ...
            'res-',its, ... % time stamp
            input_file(i_xx+1:end)];
   id_pos = find(fname=='%');
   for id=0:iMPI
     fname_id = [fname(       1:id_pos-1),num2str(id,part_format), ...
                 fname(id_pos+1:end     )];
     % Notice: the results are arrays with different dimension for
     % each subdomain; we have to pack them into a struct which we
     % call res. For this reason, this part is slightly different from
     % the grid/base file.
     res(id+1) = load(fname_id);
   end
   fname = [output_file(1:o_xx-1), ...
            'res-',its, ...
            output_file(o_xx+1:end)];
   save(mat_format,fname,"res");
   clear res
 end

return


% Octave has a problem saving function handle fields in mat format.
% This function recursively checks the fields of a struct to eliminate
% function handles.
function s2 = eliminate_function_handles(s1)

 s2 = s1;

 for i=1:length(s2(:))
   if(or( strcmp(typeinfo(s2(i)),"struct") , ...
          strcmp(typeinfo(s2(i)),"scalar struct") ))
     for [val,key]=s2(i) % loop over the fields
       if(or( strcmp(typeinfo(val),"struct") , ...
              strcmp(typeinfo(val),"scalar struct") ))
         s2(i).(key) = eliminate_function_handles(s2(i).(key));
       elseif(strcmp(typeinfo(val),"function handle"))
         s2(i).(key) = [];
       endif
     end
   end
 end

return

