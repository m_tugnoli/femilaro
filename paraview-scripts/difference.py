"""
Compute the difference between two data sets. The data sets can
include an arbitrary number of fields and/or partitions. Some
diagnostics are also printed.
"""

# Get the keys from the first block of the first data set
keys = inputs[0].__iter__().next().PointData.keys()
# Initialize the maximum value dictionary
dmax = dict(zip(keys,[-1.0]*len(keys)))
vmax = dmax.copy()
# Loop on the blocks of the two input data sets
for BLOCK in zip(inputs[0],inputs[1],output):
  b_in = BLOCK[0:2] # block inputs
  b_out = BLOCK[2]  # block output
  # Loop on the variables
  for k in b_in[0].PointData.keys(): # field loop
    delta = b_in[0].PointData[k] - b_in[1].PointData[k]
    # delta is either a 1d array (scalar filed) or a 2d one
    dm = max(max(abs(delta)))
    # compute also a normalization constant
    vm = max(max(abs(b_in[0].PointData[k])))
    if(dm>dmax[k]):
      dmax[k] = dm
    if(vm>vmax[k]):
      vmax[k] = vm
    b_out.PointData.append(delta,'Delta_'+k)

print('Maximum differences:')
print(dmax)
print('Maximum relative differences:')
for k in vmax.keys():
  vmax[k] = dmax[k]/vmax[k]
print(vmax)

