"""
Test various time integrators and perturbation expansions for the
computation of the motion of a charged particle in crossed electric
and magnetic fields.

The complete equation of motion is (in 2D)

 m du/dt = E + u x B

where B is uniform and pointing outward from the motion plane and

 E = -grad(Phi),  Phi = -sqrt(x^2 + y^2)  (uniform, radial field)

"""


import numpy as np
import scipy as sp
import pylab as pp
import matplotlib.pyplot as plt


#----------------------------------------------------------------------
# Define some general constants and functions

B0 = 3.0
E0 = 0.2
x0 = np.array(( 1.0 , 0.0 ))
v0 = np.array(( 0.0 , 1.5 ))
#M = 0.00125
M = 0.125
#Tf = 25.0
Tf = 50.0

def E(xy):
  """
  Electric field
  """
  #return E0*np.array((0.0,1.0))
  r = sp.math.sqrt( xy[0]**2 + xy[1]**2 )
  return E0/r * np.array( xy ) # make sure we return a np.array

def gradE(xy):
  r = sp.math.sqrt( xy[0]**2 + xy[1]**2 )
  return (1.0/r**3)*np.array( [       \
    [    xy[1]**2  , -xy[0]*xy[1] ] , \
    [ -xy[0]*xy[1] ,    xy[0]**2  ] ] )

def acc(xy,vv):
  """
  Differential equation
  """
  return 1/M*( E(xy) + B0*np.array( (vv[1],-vv[0]) ) )

def pet1(xy):
  """
  First perturbative expansion: computes vv
  """
  Exy = E(xy)
  return 1/B0*np.array(( Exy[1] , -Exy[0] ))

def pet2(xy):
  """
  Second perturbative expansion (assumes dE/dt=0)

  u = vE + M/B^2 * vE.grad(E)
  """
  Exy = E(xy)
  gE = gradE(xy)
  vE = 1/B0*np.array(( Exy[1] , -Exy[0] ))
  return vE + (M/B0**2)*np.array( ( \
     vE[0]*gE[0,0] + vE[1]*gE[0,1] ,
     vE[0]*gE[1,0] + vE[1]*gE[1,1] ) )

#----------------------------------------------------------------------

#----------------------------------------------------------------------
# Compute some quantities

V = np.math.sqrt( v0[0]**2 + v0[1]**2 )
rho = M * V / B0
Om = 1.0/(2.0*np.pi*rho/V)  #  1/T
print("V = %e, rho = %e, Om = %e" % (V,rho,Om))

#----------------------------------------------------------------------

# compute a reference solution, using the Heun method
dt = 1.0/(1000*Om)
N = int( np.ceil(Tf/dt) )
t_ref  = np.zeros( (  N+1) )
xv_ref = np.zeros( (4,N+1) )

t_ref[0] = 0
xv_ref[:2,0] = x0
xv_ref[2:,0] = v0
for i in range(N):
  t_ref[i+1] = (i+1)*dt

  xs = xv_ref[:2,i] + dt/2.0*xv_ref[2:,i]
  vs = xv_ref[2:,i] + dt/2.0*acc( xv_ref[:2,i] , xv_ref[2:,i] )

  xv_ref[:2,i+1] = xv_ref[:2,i] + dt*vs
  xv_ref[2:,i+1] = xv_ref[2:,i] + dt*acc( xs , vs )

# compute another reference solution, enforcing well balance
t_ref2  = np.zeros( (  N+1) )
xv_ref2 = np.zeros( (4,N+1) )

t_ref2[0] = 0
xv_ref2[:2,0] = x0
xv_ref2[2:,0] = pet2(x0)
for i in range(N):
  t_ref2[i+1] = (i+1)*dt

  xs = xv_ref2[:2,i] + dt/2.0*xv_ref2[2:,i]
  vs = xv_ref2[2:,i] + dt/2.0*acc( xv_ref2[:2,i] , xv_ref2[2:,i] )

  xv_ref2[:2,i+1] = xv_ref2[:2,i] + dt*vs
  xv_ref2[2:,i+1] = xv_ref2[2:,i] + dt*acc( xs , vs )


# now the evolution given by the first perturbative term
dt = 1.0/(100*Om)
N = int( np.ceil(Tf/dt) )
t_pe1  = np.zeros( (  N+1) )
xv_pe1 = np.zeros( (4,N+1) )

t_pe1[0] = 0
xv_pe1[:2,0] = x0
xv_pe1[2:,0] = pet1(xv_pe1[:2,0])
for i in range(N):
  t_pe1[i+1] = (i+1)*dt

  xs = xv_pe1[:2,i] + dt/2.0*pet1(xv_pe1[:2,i])

  xv_pe1[:2,i+1] = xv_pe1[:2,i] + dt*pet1(xs)
  xv_pe1[2:,i+1] = pet1(xv_pe1[:2,i+1])

# same for the second perturbative term
t_pe2  = np.zeros( (  N+1) )
xv_pe2 = np.zeros( (4,N+1) )

t_pe2[0] = 0
xv_pe2[:2,0] = x0
xv_pe2[2:,0] = pet2(xv_pe2[:2,0])
for i in range(N):
  t_pe2[i+1] = (i+1)*dt

  xs = xv_pe2[:2,i] + dt/2.0*pet2(xv_pe2[:2,i])

  xv_pe2[:2,i+1] = xv_pe2[:2,i] + dt*pet2(xs)
  xv_pe2[2:,i+1] = pet2(xv_pe2[:2,i+1])



# Now the time discretization of the complete eq. with implicit Euler

def ImpEul(dt,Tf,x0,v0):
  """
  Implicit Euler discretization

  The electric field term is linearized, so that the time step reads

  Delta_x - dt*v^{n+1} = 0
  -dt/M grad(E) Delta_x + (I-dt/M Bx)v^{n+1} = v^n + dt/M E(x^n)

  The first equation can be substitited in the second one, so that
  one is left with a 2x2 system

  ( I - dt/M Bx - dt^2/M grad(E) ) v^{n+1} = v^n + dt/M E(x^n)

  """

  N = int( np.ceil(Tf/dt) )
  t  = np.zeros( (  N+1) )
  xv = np.zeros( (4,N+1) )

  t[0] = 0
  xv[:2,0] = x0
  xv[2:,0] = v0
  for i in range(N):
    t[i+1] = (i+1)*dt
    Ex =     E(xv[:2,i])
    gE = gradE(xv[:2,i])

    # build the 2x2 matrix
    m11 = 1.0          -dt**2/M * gE[0,0]
    m12 =     -dt/M*B0 -dt**2/M * gE[0,1]
    m21 =      dt/M*B0 -dt**2/M * gE[1,0]
    m22 = 1.0          -dt**2/M * gE[1,1]

    # build the rhs and solve the system
    rhs1 = xv[2,i] + dt/M * Ex[0]
    rhs2 = xv[3,i] + dt/M * Ex[1]

    det = m11*m22 - m12*m21
    xv[2,i+1] = 1.0/det * (  m22*rhs1 - m12*rhs2 )
    xv[3,i+1] = 1.0/det * ( -m21*rhs1 + m11*rhs2 )

    # update also the position
    xv[:2,i+1] = xv[:2,i] + dt*xv[2:,i+1] 

  return t,xv

#dt = 1.0/(1000*Om)
#t_ie1,xv_ie1 = ImpEul( dt,Tf , x0,v0 )
#dt = 1.0/(100*Om)
#t_ie2,xv_ie2 = ImpEul( dt,Tf , x0,v0 )
#dt = 1.0/(10*Om)
#t_ie3,xv_ie3 = ImpEul( dt,Tf , x0,v0 )
#dt = 1.0/(1*Om)
#t_ie4,xv_ie4 = ImpEul( dt,Tf , x0,v0 )
#dt = 1.0/(0.1*Om)
#t_ie5,xv_ie5 = ImpEul( dt,Tf , x0,v0 )





# plot the backgroung electric field
N = 20
x = np.linspace(-2.0,2.0,N)
y = np.linspace(-2.0,2.0,N)
Exy = np.zeros( (2,x.size,y.size) )
for j in range(y.size):
  for i in range(x.size):
    Exy[:,i,j] = E( ( x[i] , y[j] ) )

plt.quiver(x,y,Exy[0,:,:].T,Exy[1,:,:].T,alpha=0.3,zorder=0)
plt.axis('equal')

# plot the reference trajectory
#plt.plot(xv_ref[0,:],xv_ref[1,:],zorder=1,color=[0,0,0])
plt.plot(xv_ref2[0,:],xv_ref2[1,:],zorder=1,color=[0,0,0],lw=1.5)

# plot the perturbative expansions
plt.plot(xv_pe1[0,:],xv_pe1[1,:],zorder=1.1,color=[0,0,1],lw=2.5,marker="o")
plt.plot(xv_pe2[0,:],xv_pe2[1,:],zorder=1.2,color=[0,1,0],lw=2.5,marker="o")

# plot the implicit Euler solutions
#plt.plot(xv_ie1[0,:],xv_ie1[1,:],zorder=1.2,color=[1.0,0,0])
#plt.plot(xv_ie2[0,:],xv_ie2[1,:],zorder=1.2,color=[0.8,0,0])
#plt.plot(xv_ie3[0,:],xv_ie3[1,:],zorder=1.2,color=[0.6,0,0])
#plt.plot(xv_ie4[0,:],xv_ie4[1,:],zorder=1.2,color=[0.4,0,0])
#plt.plot(xv_ie5[0,:],xv_ie5[1,:],zorder=1.2,color=[0.2,0,0])


plt.figure(2)
#plt.plot(t_ref,xv_ref[3,:],color=[0,0,0])
plt.plot(t_ref2,xv_ref2[3,:],color=[0,0,0])
plt.plot(t_pe1 ,xv_pe1[3,:],color=[0,0,1])
plt.plot(t_pe2 ,xv_pe2[3,:],color=[0,1,0])


plt.show()

