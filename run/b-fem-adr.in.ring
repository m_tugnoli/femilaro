&input

! output file name
basename = 'test-ring-40x40'

! test case, method and grid
!grid_file = './../GRIDS/2d/hybrid-ring-20x20-02584tri-00722quad-P4.octxt'
!grid_file = './../GRIDS/2d/hybrid-ring-40x40-10766tri-03042quad-P4.octxt'
!grid_file = './../GRIDS/2d/hybrid-ring-80x80-44924tri-12482quad-P4.octxt'
!grid_file = './../GRIDS/2d/hybrid-ring-160x160-180386tri-50562quad-P4.octxt'
!grid_file = './../GRIDS/2d/hybrid-ring-unif-20x20-0002496tri-0000722quad-P4.octxt'
!grid_file = './../GRIDS/2d/hybrid-ring-unif-20x20-0010348tri-0003042quad-P4.octxt'
!grid_file = './../GRIDS/2d/hybrid-ring-unif-20x20-0043350tri-0012482quad-P4.octxt'
grid_file = './../GRIDS/2d/hybrid-ring-unif-20x20-0174982tri-0050562quad-P4.octxt'

write_grid = .true.

! FE space
gq_deg = 4

! Boundary conditions
nbound = 2
! side ind:  1   2   
cbc_type = ' 1 , 2 ,
             1 , 1  '

! linear system
write_sys = .false.

! Problem setup
n_ions = 1

! time stepping
integrator = "bdf3"
tt_sta = 0.0
tt_end = 1.0
!dt = 0.0025 ! for the 20x20 grid
!dt = 0.00125
!dt = 0.000625
dt = 0.0003125
!dt = 1.5625e-04

!nout1 = 100 ! for the 20x20 grid
!nout1 = 200
!nout1 = 400
nout1 = 800
nout2 = 1000000

/


! 0.013490989258374454,   0.048000915252791376, 0.55405688039682999
! 0.0031586161562341536,  0.014287359333529282, 0.19780826806149565
! 0.00041947029506934685, 0.00212221407223518,  0.037039744300731647



!          L1                    L2                    Linf
! 0.012146669022069786,   0.0440797277292733,    0.51648449776474159
! 0.0026482912681855598,  0.01228359327854056,   0.16793838619685486
! 0.00036783181576660747, 0.0018405866199312376, 0.035703959862575463
! 
! 
! 
! (0.0060702984944960821,  0.024838470068820433,  0.24381984119244848)
! (0.0013244654620205227,  0.005866810268443752,  0.065787064656282368)
! (0.00044189013717519033, 0.0025241149867548072, 0.072625628981042953)


!   eL1         eL2         eLi
! 0.013573    0.048953    0.57379
! 0.0033461   0.015249    0.21135
! 4.8699e-04  0.0024721   0.042329




 0.0069836  0.028796  0.37199
 0.0015775  0.0074506 0.11035
 3.3717e-04 0.0015909 0.043640

