#!/bin/bash


#--------------------------CONFIGURATIONS-----------------------------

# Name of the executable
execroot="./../bin"
executable="cg-ns"
#executable="dg-comp"

# MPI set
#use_MPI="yes"
use_MPI="no"

# Number of cores and threads per core (for MPI)
num_cores=1
num_threads=1
# Number of OpenMP threads
num_OpenMP_thr=8

# Name specifier (used to identify different experiments)
name_spec="cavity-cnt-2"

# Input file
#infile=""
infile=${executable}.in

# Wtime
wtime="240:00:00"

#------------------------END CONFIGURATIONS---------------------------


#-----------------------------MAIN OUTPUT-----------------------------

# PBS script to submit the job
job_file="qsub_script_to_run_${executable}.sub"

# job qualifier
job_qual="${num_cores}x${num_threads}x${num_OpenMP_thr}"

# job name
job_name="${executable}_${job_qual}"

# stdout files: the first one collects the queue manager output
# (including a copy of the present file), the second one is the output
# of the executable.
job_out="out-${executable}-${name_spec}-N${job_qual}"
job_out1="${job_out}.txt"
job_out2="${job_out}-qsub.txt"

#---------------------------END MAIN OUTPUT---------------------------


# We make a copy so that the original file can be changed even while
# the job is waiting in the queue.
input_file=""
if [ -n "${infile}" ]; # notice that we need "" in the test!
 then
  input_file="temp-${job_name}-${RANDOM}"
  cp ${infile} "${input_file}"
fi
nodes_file="mpd.nodes-${RANDOM}"

# MPICH2 root
MPICH2=/usr/local/mpich2_1.2.1p1/bin

# Total number of MPI threads
np_MPI=`echo ${num_cores} ${num_threads} | awk '{ N = \$1*\$2 ; print N }'`

# Total number of threads per node
nth_node=`echo ${num_threads} ${num_OpenMP_thr} | awk '{ N = \$1*\$2 ; print N }'`

# Write the PBS input file
cat > ${job_file} << EOF
#!/bin/bash
#PBS -S /bin/bash

# Number of cores (and select maximum runtime)
#PBS -l nodes=${num_cores}:ppn=${nth_node},walltime=${wtime}

# Set the job name
#PBS -N ${job_name}

# Set the output file and merge it to the sterr
#PBS -o ${job_out2}
#PBS -j oe

# Start the job in the current directory (PBS starts in the home folder)
cd \${PBS_O_WORKDIR}

# Echo the PBS input file (this file)
cat ${job_file}
echo
echo
echo "#><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>#"
echo
echo

# Preliminaries
hostname
ulimit -s unlimited # make sure we can put big arrays on the stack
date

if [ "${use_MPI}" = "yes" ]
 then

#---------------------------------------------------------------------#
# Now set up the mpich2 ring on the assigned processors (this follows
# section 5.8.1 of the MPICH2 User's Guide, version November 18,
# 2009).

# 1) First we need to translate the file \${PBS_NODEFILE}, used by the
# PBS environment, into something that can be read by mpiexec.
cat \${PBS_NODEFILE} | uniq | \\
  awk '{ printf("%s:${num_threads}\\n", \$1); }' > ${nodes_file}
echo ""
echo "Selected nodes:"
cat ${nodes_file}
echo "---------------"
echo ""

# This is an alternative command to count the number of threads on
# each core
#   sort \${PBS_NODEFILE} | uniq -c | \\
#     awk '{ printf("%s:%s\\n", \$2, \$1); }' > ${nodes_file}

# 2) Now we set up (and test) the ring
${MPICH2}/mpdboot --verbose -f ${nodes_file} -n ${num_cores}
${MPICH2}/mpdtrace

# 3) Run the executable
${MPICH2}/mpiexec -machinefile ${nodes_file} -l -n ${np_MPI} \\
  -env OMP_NUM_THREADS ${num_OpenMP_thr} \\
  ${execroot}/${executable} ${input_file} > ${job_out1}

# 4) Clean up
${MPICH2}/mpdallexit
rm ${nodes_file}

#---------------------------------------------------------------------#

else

export OMP_NUM_THREADS=${num_OpenMP_thr}
${execroot}/${executable} ${input_file} > ${job_out1}

fi

date
EOF

# qsub appends the output, which can be confusing
if [ -e "${job_out2}" ]
 then
  rm ${job_out2}
fi

qsub ${job_file}

