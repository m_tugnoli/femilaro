program b_adr

 use mod_utils, only: &
   t_realtime, my_second

 use mod_messages, only: &
   mod_messages_constructor, &
   mod_messages_destructor,  &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_constructor, &
   mod_kinds_destructor,  &
   wp

 use mod_mpi_utils, only: &
   mod_mpi_utils_constructor, &
   mod_mpi_utils_destructor,  &
   mpi_comm_world, &
   mpi_init_thread, mpi_thread_single, mpi_thread_multiple, &
   mpi_finalize, mpi_comm_size, mpi_comm_rank

 !$ use mod_omp_utils, only: &
 !$   mod_omp_utils_constructor, &
 !$   mod_omp_utils_destructor,  &
 !$   detailed_timing_omp, &
 !$   omput_push_key,    &
 !$   omput_pop_key,     &
 !$   omput_start_timer, &
 !$   omput_close_timer, &
 !$   omput_write_time

 use mod_fu_manager, only: &
   mod_fu_manager_constructor, &
   mod_fu_manager_destructor,  &
   new_file_unit

 use mod_octave_io, only: &
   mod_octave_io_constructor, &
   mod_octave_io_destructor,  &
   real_format,    &
   write_octave,   &
   read_octave,    &
   read_octave_al, &
   locate_var
 
 use mod_linal, only: &
   mod_linal_constructor, &
   mod_linal_destructor

 use mod_perms, only: &
   mod_perms_constructor, &
   mod_perms_destructor

 use mod_octave_io_perms, only: &
   mod_octave_io_perms_constructor, &
   mod_octave_io_perms_destructor

 use mod_sympoly, only: &
   mod_sympoly_constructor, &
   mod_sympoly_destructor

 use mod_octave_io_sympoly, only: &
   mod_octave_io_sympoly_constructor, &
   mod_octave_io_sympoly_destructor

 use mod_numquad, only: &
   mod_numquad_constructor, &
   mod_numquad_destructor

 use mod_h2d_master_el, only: &
   mod_h2d_master_el_constructor, &
   mod_h2d_master_el_destructor

 use mod_h2d_grid, only: &
   mod_h2d_grid_constructor, &
   mod_h2d_grid_destructor,  &
   t_h2d_grid, new_base_and_grid, clear, &
   t_ddc_h2d_grid, &
   write_octave, read_octave_al

 use mod_h2d_base, only: &
   mod_h2d_base_constructor, &
   mod_h2d_base_destructor,  &
   t_h2d_base, new_base, clear, &
   write_octave

 use mod_h2d_bcs, only: &
   mod_h2d_bcs_constructor, &
   mod_h2d_bcs_destructor,  &
   t_h2d_bcs, &
   new_bcs, clear, &
   write_octave
 
 use mod_sparse, only: &
   mod_sparse_constructor, &
   mod_sparse_destructor, &
   transpose

 use mod_octave_io_sparse, only: &
   mod_octave_io_sparse_constructor, &
   mod_octave_io_sparse_destructor, &
   write_octave

 use mod_state_vars, only: &
   mod_state_vars_constructor, &
   mod_state_vars_destructor,  &
   c_stv

 use mod_output_control, only: &
   mod_output_control_constructor, &
   mod_output_control_destructor,  &
   elapsed_format, &
   base_name

 use mod_time_integrators, only: &
   mod_time_integrators_constructor, &
   mod_time_integrators_destructor,  &
   c_ode, c_ods, c_tint, t_ti_init_data, t_ti_step_diag, &
   t_ee, t_hm, t_rk4, t_ssprk54, &
   t_thetam, t_bdf1, t_bdf2, t_bdf3, t_bdf2ex, &
   t_erb2kry, t_erb2lej, &
   t_pcexpo
 
 use mod_linsolver, only: &
   mod_linsolver_constructor, &
   mod_linsolver_destructor

 use mod_sol_state, only: &
   mod_sol_state_constructor, &
   mod_sol_state_destructor,  &
   t_sol_state, new_sol_state, &
   clear, write_octave

 use mod_scalar_adr_ode, only: &
   mod_scalar_adr_ode_constructor, &
   mod_scalar_adr_ode_destructor,  &
   t_scalar_adr_ode, new_scalar_adr_ode, clear, &
   ! these can be useful to write the system matrix
   mmmt, const_mmmt, aaat, &
   out_file_sys_suff, out_file_sys_name


 implicit none

 ! Define some general parameters
 integer, parameter :: max_char_len = 10000
 character(len=*), parameter :: this_prog_name = 'b_adr'

 ! IO variables
 character(len=*), parameter :: input_file_name_def = 'b-adr.in'
 character(len=max_char_len) :: input_file_name
 character(len=*), parameter :: out_file_nml_suff = '-nml.octxt'
 character(len=max_char_len+len(out_file_nml_suff)):: out_file_nml_name
 character(len=max_char_len) :: basename
 logical :: write_grid, write_sys
 character(len=*), parameter :: out_file_grid_suff = '-grid.octxt'
 character(len=max_char_len+len(out_file_grid_suff))::out_file_grid_name
 character(len=*), parameter :: out_file_base_suff = '-base.octxt'
 character(len=max_char_len+len(out_file_base_suff))::out_file_base_name
 integer :: fu, ierr

 ! Grid
 type(t_h2d_grid), target :: grid
 type(t_ddc_h2d_grid) :: ddc_grid
 character(len=max_char_len) :: grid_file

 ! Base
 type(t_h2d_base) :: base
 integer :: gq_deg

 ! BCS
 integer :: nbound ! # of boundary regions
 character(len=max_char_len) :: cbc_type
 type(t_h2d_bcs), target :: bcs
 integer, allocatable :: bc_type_t(:,:)

 ! Test case
 character(len=max_char_len) :: testname
 character(len=max_char_len) :: integrator

 ! Unknowns
 integer :: n_ions
 type(t_sol_state), pointer :: &
  ! using pointers to simplify the updating after the time step
  uuu0 => null(), &
  uuun => null(), &
  uuuo => null()

 ! Initial condition
 integer :: shape_in(2), shape_out(2), ie, ix, iv, i
 real(wp), allocatable :: tmp_init(:,:)
 !real(wp), allocatable :: xe(:,:,:), ve(:,:,:)

 ! Time integration
 integer :: nstep, n, n0, n_out
 real(wp) :: dt, tt_sta, tt_end, t_nm1, t_n
 logical :: l_output1, l_output2
 integer :: nout1, nout2
 type(t_scalar_adr_ode) :: sol_ode
 type(c_ods) :: ods
 type(t_ti_init_data) :: ti_init_data
 type(t_ti_step_diag) :: ti_step_diag
 class(c_tint), allocatable :: tint

 character(len=max_char_len) :: message(1)

 ! Timing
 real(t_realtime) :: t00, t0, t1, t0step

 ! MPI variables
 logical :: master_proc
 integer :: mpi_nd, mpi_id

 ! Input namelist
 namelist /input/ &
   ! test case
   !testname, &
   ! output base name
   basename, &
   ! grid
   write_grid, &
   grid_file,  &
   ! base
   gq_deg, & ! accuracy of the Gaussian quadrature rule
   ! linear system
   write_sys, &
   ! problm size
   n_ions, &
   ! boundary conditions
   nbound, cbc_type, &
   ! time stepping
   tt_sta, tt_end, dt, &
   integrator, &
   nout1, nout2  ! output frequency


 call mod_messages_constructor()

 call mod_kinds_constructor()

 call mpi_init_thread(mpi_thread_multiple,i,ierr)
 call mod_mpi_utils_constructor()
 call mpi_comm_size(mpi_comm_world,mpi_nd,ierr)
 call mpi_comm_rank(mpi_comm_world,mpi_id,ierr)
 master_proc = mpi_id.eq.0

 !$ if(detailed_timing_omp) call mod_omp_utils_constructor()
 
 call mod_fu_manager_constructor()

 call mod_octave_io_constructor()

 call mod_linal_constructor()

 call mod_perms_constructor()
 call mod_octave_io_perms_constructor()

 call mod_sympoly_constructor()
 call mod_octave_io_sympoly_constructor()

 call mod_state_vars_constructor()

 !----------------------------------------------------------------------
 ! Read input file
 if(command_argument_count().gt.0) then
   call get_command_argument(1,value=input_file_name)
 else
   input_file_name = input_file_name_def
 endif
 call new_file_unit(fu,ierr)
 open(fu,file=trim(input_file_name), &
      status='old',action='read',form='formatted')
 read(fu,input)
 close(fu)
 ! If there are more processes, each of them needs its own basename
 ! and grid
 if(mpi_nd.gt.1) then
   write(basename, '(a,a,i3.3)') trim(basename),'-P',mpi_id
   write(grid_file,'(a,a,i3.3)') trim(grid_file),'.',mpi_id
 endif
 ! echo the input namelist
 out_file_nml_name = trim(basename)//out_file_nml_suff
 open(fu,file=trim(out_file_nml_name), &
      status='replace',action='write',form='formatted')
 write(fu,input)
 close(fu)

 allocate(bc_type_t(nbound,2))
 read(cbc_type,*) bc_type_t ! transposed, for simplicity
 !----------------------------------------------------------------------

 call mod_output_control_constructor(basename)

 !----------------------------------------------------------------------
 ! Define the grid and the base
 t0 = my_second()

 call mod_numquad_constructor()

 call mod_h2d_master_el_constructor()

 call mod_h2d_base_constructor()

 call mod_h2d_grid_constructor()

 call new_base_and_grid( base,grid , grid_file,gq_deg , &
                         ddc_grid, mpi_comm_world )

 ! write the octave output
 out_file_base_name = trim(base_name)//out_file_base_suff
 call new_file_unit(fu,ierr)
 open(fu,file=trim(out_file_base_name), &
      status='replace',action='write',form='formatted')
 call write_octave(base,'base',fu)
 close(fu)
 if(write_grid) then
   out_file_grid_name = trim(base_name)//out_file_grid_suff
   call new_file_unit(fu,ierr)
   open(fu,file=trim(out_file_grid_name), &
        status='replace',action='write',form='formatted')
   call write_octave(grid,'grid',fu)
   call write_octave(ddc_grid,'ddc_grid',fu)
   close(fu)
 endif

 t1 = my_second()
 write(message(1),elapsed_format) &
   'Completed base and grid construction: elapsed time ',t1-t0,'s.'
 if(master_proc) &
   call info(this_prog_name,'',message(1))
 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! Define the boundary conditions

 call mod_h2d_bcs_constructor()
 call new_bcs(bcs,grid,transpose(bc_type_t) , ddc_grid,mpi_comm_world)
 deallocate( bc_type_t )

 if(write_grid) then
   call new_file_unit(fu,ierr)
   open(fu,file=trim(out_file_grid_name), status='old',action='write', &
        form='formatted',position='append')
    call write_octave(bcs,'bcs',fu)
   close(fu)
 endif
 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! Linear solver
 call mod_sparse_constructor()
 call mod_octave_io_sparse_constructor()
 call mod_linsolver_constructor()
 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! Allocations and initial condition
 t0 = my_second()

 call mod_time_integrators_constructor()

 call mod_sol_state_constructor()
 allocate( uuu0 , uuun )
 call new_sol_state(uuu0 , grid,base,bcs,n_ions,2)
 call new_sol_state(uuun , grid,base,bcs,n_ions,2)

 call mod_scalar_adr_ode_constructor(grid,ddc_grid,base,write_sys)
 call new_scalar_adr_ode( sol_ode ,grid,base,bcs)

! call mod_testcases_constructor(testname,(/grid%gx%d,grid%gv%d/))

 ! set the initial condition
 !uuu0%ni(1)%s = exp( -( ((grid%v%x(1)-0.3_wp)/0.05_wp)**2 &
 !                      +((grid%v%x(2)-0.5_wp)/0.15_wp)**2 ) )
 !uuu0%ni(1)%s = max( 1.0_wp-sqrt( ((grid%v%x(1)-0.3_wp)/0.05_wp)**2 &
 !                      +((grid%v%x(2)-0.5_wp)/0.15_wp)**2 ) ,0.0_wp)
 !uuu0%vi(1)%v(1)%s = 0.6_wp
 !uuu0%vi(1)%v(2)%s = 0.0_wp
 !uuu0%ni(1)%s = exp( -( ((grid%v%x(1)-0.25_wp)/0.15_wp)**2 &
 !                      +((grid%v%x(2)-0.50_wp)/0.15_wp)**2 ) )
 !uuu0%vi(1)%v(1)%s = -1.2_wp*grid%v%x(2)
 !uuu0%vi(1)%v(2)%s = 1.2_wp*(grid%v%x(1)-1.65_wp)

 !uuu0%ni(1)%s = exp( -( ((grid%v%x(1)-0.0_wp)/0.25_wp)**2 &
 !                      +((grid%v%x(2)+1.0_wp)/0.025_wp)**2 ) )

 ! Uniform x advection
 !allocate(tmp_init(grid%nv,1))
 !tmp_init(:,1) = sqrt((grid%v%x(1)-0.5)**2 + (2*grid%v%x(2))**2)      ! r
 !!uuu0%ni(1)%s = exp( -( (tmp_init(:,1)/0.25_wp)**2 ) )
 !uuu0%ni(1)%s = 0.0_wp; where(tmp_init(:,1).le.0.30_wp) uuu0%ni(1)%s = 1.0_wp

 !uuu0%vi(1)%v(1)%s = 1.2_wp
 !uuu0%vi(1)%v(2)%s = 0.0_wp
 !deallocate(tmp_init)

 ! Solid body rotation
 allocate(tmp_init(grid%nv,2))
 tmp_init(:,1) = sqrt(grid%v%x(1)**2 + grid%v%x(2)**2)      ! r
 tmp_init(:,2) = atan2(grid%v%x(1),-grid%v%x(2)) ! theta
 uuu0%ni(1)%s = exp( -( (tmp_init(:,2)/0.25_wp)**2 &
                       +((tmp_init(:,1)-1.0_wp)/0.025_wp)**2 ) )

 uuu0%vi(1)%v(1)%s = -6.2832_wp*grid%v%x(2)
 uuu0%vi(1)%v(2)%s =  6.2832_wp*grid%v%x(1)
 deallocate(tmp_init)

! call write_octave(test_name       ,'test_name'       ,fu)
! call write_octave(test_description,'test_description',fu)

 t1 = my_second()
 write(message(1),elapsed_format) &
   'Completed start-up phase: elapsed time ',t1-t0,'s.'
 if(master_proc) &
   call info(this_prog_name,'',message(1))
 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! Time integration
 !
 !   0        1                 n             nstep
 !   |--------|--------|--------|--------|------|
 ! tt_sta            t_nm1     t_n            tt_end
 !                     | step_n |
 !

 ! Set time integration method
 time_int_case: select case(trim(integrator))
  case('bdf1')
   allocate(t_bdf1::tint)
  case('bdf2')
   allocate(t_bdf2::tint)
  case('bdf3')
   allocate(t_bdf3::tint)
  case default
   call error(this_prog_name, '',&
    'Unknown integration method "'//trim(integrator)//'".')
 end select time_int_case
 
 nstep = ceiling(tt_end/dt)
 n_out = 0
! call write_out2(grid,base,tt_sta,uuu0,e_field,.true.,ti_step_diag)
! ! the next call also updates the electric field
! call write_out2(grid,base,tt_sta,uuu0,e_field,.false.,ti_step_diag)
 call write_out1(0,tt_sta,n_out,uuu0,ti_step_diag)

 call tint%init( sol_ode,dt,0.0_wp,uuu0,ods,            &
        init_data=ti_init_data , step_diag=ti_step_diag )
 t0 = my_second()
 n0 = ti_step_diag%bootstrap_steps + 1
 time_loop: do n=n0,nstep

   t_nm1 = tt_sta + real(n-1,wp)*dt
   t_n   = t_nm1 + dt
   !$ if(detailed_timing_omp) then
   !$   call omput_push_key("time_step")
   !$   call omput_start_timer()
   !$ endif

   call tint%step(uuun,sol_ode,t_nm1,uuu0,ods,ti_step_diag)
   uuuo => uuu0
   uuu0 => uuun
   uuun => uuuo

   ! Select how often the full distribution is written
   l_output1 = (mod(n,nout1).eq.0).or.(n.eq.n0).or.(n.eq.nstep)
   if(l_output1) then
     write(*,*) 'Step ',n,' of ',nstep
     n_out = n_out + 1
     call write_out1(n,t_n,n_out,uuu0,ti_step_diag)
   endif

!   ! Select how often the integrated diagnostics are written
!   l_output2 = (mod(n,nout2).eq.0).or.(n.eq.1).or.(n.eq.nstep)
!   if(l_output2) &
!     call write_out2(grid,base,t_n,uuu0,e_field,.false.,ti_step_diag)
!
!   !$ if(detailed_timing_omp) then
!   !$   call omput_write_time()
!   !$   call omput_close_timer()
!   !$   call omput_pop_key()
!   !$ endif

 enddo time_loop
 t1 = my_second()
 write(message(1),elapsed_format) &
   'Completed time loop: elapsed time ',t1-t0,'s.'
 if(master_proc) &
   call info(this_prog_name,'',message(1))

 call tint%clean(sol_ode,ti_step_diag)
 deallocate(tint)

! call mod_testcases_destructor()

 call clear(sol_ode)
 call mod_scalar_adr_ode_destructor()

 call clear(uuu0)
 call clear(uuun)
 deallocate( uuu0 , uuun )
 call mod_sol_state_destructor()

 call mod_time_integrators_destructor()

 call mod_linsolver_destructor()
 call mod_octave_io_sparse_destructor()
 call mod_sparse_destructor()

 call clear(bcs)
 call mod_h2d_bcs_destructor()

 call clear(ddc_grid)
 call clear(grid)
 call mod_h2d_grid_destructor()

 call clear(base)
 call mod_h2d_base_destructor()

 call mod_h2d_master_el_destructor()

 call mod_numquad_destructor()

 call mod_output_control_destructor()

 call mod_state_vars_destructor()

 call mod_octave_io_sympoly_destructor()
 call mod_sympoly_destructor()

 call mod_perms_destructor()
 call mod_octave_io_perms_destructor()

 call mod_linal_destructor()

 call mod_octave_io_destructor()

 call mod_fu_manager_destructor()

 !$ if(detailed_timing_omp) call mod_omp_utils_destructor()

 call mod_mpi_utils_destructor()
 call mpi_finalize(ierr)

 call mod_kinds_destructor()

 call mod_messages_destructor()


contains


 subroutine write_out1(n,t,n_out,uuu,sd)
  integer, intent(in) :: n, n_out
  real(wp), intent(in) :: t
  type(t_sol_state), intent(in) :: uuu
  type(t_ti_step_diag), intent(in) :: sd

  integer :: fu, ierr
  character(len=*), parameter :: time_stamp_format = '(i4.4)'
  character(len=4) :: time_stamp
  character(len=*), parameter :: suff1 = '-res-'
  character(len=*), parameter :: suff2 = '.octxt'
  character(len= len_trim(base_name) + len(suff1) + 4 + len(suff2)) :: &
     out_file_res_name

   call new_file_unit(fu,ierr)
   write(time_stamp,time_stamp_format) n_out
   out_file_res_name = trim(base_name)//suff1//time_stamp//suff2
   open(fu,file=out_file_res_name, &
        status='replace',action='write',form='formatted')
    call write_octave(n  ,'n'  ,fu)
    call write_octave(t  ,'t'  ,fu)
    call write_octave(uuu%ni(1)%s,'c','n1',fu)
    call write_octave(uuu%vi(1)%v(1)%s,'c','vx',fu)
    call write_octave(uuu%vi(1)%v(2)%s,'c','vy',fu)
    if(write_sys) then
      call write_octave(transpose(mmmt(1)%m(1,1)),'mmm',fu)
      call write_octave(transpose(aaat(1)%m(1,1)),'aaa',fu)
    endif
   close(fu)

 end subroutine write_out1

end program b_adr
