!> \file
!! Minimal model for the SOL.
!!
!! \n
!!
!! To run this file:
!! <ul>
!!  <li> generate a grid following the instructions in GRIDS/README
!!  <li> set the input file <tt>b-sol-minimal.in</tt> (this is the
!!  input file read by default, an alternative file can be passed as
!!  command line argument)
!!  <li> run this program
!!  <li> the program produces various ASCII files which can be loaded
!!  in octave; very important, since some of these ASCII files contain
!!  function handles to methods defined in
!!  <tt>src/fem/octave-methods</tt>, before loading these files in
!!  octave one needs <tt>addpath src/fem/octave-methods</tt>
!!  <li> an octave script \c pack_MPI_files is available to gather all
!!  the output produced by different processors in a parallel run
!!  (since this script has to load the files, <tt>addpath
!!  src/fem/octave-methods</tt> is required before running it).
!! </ul>
program b_sol_minimal

 use mod_utils, only: &
   t_realtime, my_second

 use mod_messages, only: &
   mod_messages_constructor, &
   mod_messages_destructor,  &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_constructor, &
   mod_kinds_destructor,  &
   wp

 use mod_mpi_utils, only: &
   mod_mpi_utils_constructor, &
   mod_mpi_utils_destructor,  &
   mpi_logical, mpi_integer, wp_mpi, &
   mpi_comm_world, &
   mpi_init_thread, mpi_thread_single, mpi_thread_multiple, &
   mpi_finalize, &
   mpi_comm_size, mpi_comm_rank, &
   mpi_bcast, mpi_gather

 !$ use mod_omp_utils, only: &
 !$   mod_omp_utils_constructor, &
 !$   mod_omp_utils_destructor,  &
 !$   detailed_timing_omp, &
 !$   omput_push_key,    &
 !$   omput_pop_key,     &
 !$   omput_start_timer, &
 !$   omput_close_timer, &
 !$   omput_write_time

 use mod_fu_manager, only: &
   mod_fu_manager_constructor, &
   mod_fu_manager_destructor,  &
   new_file_unit

 use mod_octave_io, only: &
   mod_octave_io_constructor, &
   mod_octave_io_destructor,  &
   real_format,    &
   write_octave,   &
   read_octave,    &
   read_octave_al, &
   locate_var
 
 use mod_linal, only: &
   mod_linal_constructor, &
   mod_linal_destructor

 use mod_perms, only: &
   mod_perms_constructor, &
   mod_perms_destructor

 use mod_octave_io_perms, only: &
   mod_octave_io_perms_constructor, &
   mod_octave_io_perms_destructor

 use mod_sympoly, only: &
   mod_sympoly_constructor, &
   mod_sympoly_destructor

 use mod_octave_io_sympoly, only: &
   mod_octave_io_sympoly_constructor, &
   mod_octave_io_sympoly_destructor

 use mod_numquad, only: &
   mod_numquad_constructor, &
   mod_numquad_destructor

 use mod_state_vars, only: &
   mod_state_vars_constructor, &
   mod_state_vars_destructor,  &
   c_stv

 use mod_sparse, only: &
   mod_sparse_constructor, &
   mod_sparse_destructor, &
   transpose

 use mod_octave_io_sparse, only: &
   mod_octave_io_sparse_constructor, &
   mod_octave_io_sparse_destructor, &
   write_octave

 use mod_output_control, only: &
   mod_output_control_constructor, &
   mod_output_control_destructor,  &
   elapsed_format, &
   base_name

 use mod_h2d_master_el, only: &
   mod_h2d_master_el_constructor, &
   mod_h2d_master_el_destructor

 use mod_h2d_grid, only: &
   mod_h2d_grid_constructor, &
   mod_h2d_grid_destructor,  &
   t_h2d_grid, new_base_and_grid, clear, &
   t_ddc_h2d_grid, &
   write_octave, read_octave_al

 use mod_h2d_base, only: &
   mod_h2d_base_constructor, &
   mod_h2d_base_destructor,  &
   t_h2d_base, clear, &
   write_octave

 use mod_h2d_bcs, only: &
   mod_h2d_bcs_constructor, &
   mod_h2d_bcs_destructor,  &
   t_h2d_bcs, &
   new_bcs, clear, &
   write_octave
 
 use mod_linsolver, only: &
   mod_linsolver_constructor, &
   mod_linsolver_destructor

 use mod_time_integrators, only: &
   mod_time_integrators_constructor, &
   mod_time_integrators_destructor,  &
   c_ode, c_ods, c_tint, t_ti_init_data, t_ti_step_diag, &
   t_ee, t_hm, t_rk4, t_ssprk54, &
   t_thetam, t_bdf1, t_bdf2, t_bdf3, t_bdf2ex, &
   t_erb2kry, t_erb2lej, &
   t_pcexpo
 
 use mod_em_physical_constants, only: &
   mod_em_physical_constants_constructor, &
   mod_em_physical_constants_destructor, &
   write_octave

 use mod_magnetic_geometry, only: &
   mod_magnetic_geometry_constructor, &
   mod_magnetic_geometry_destructor

 use mod_vect_operators, only: &
   mod_vect_operators_constructor, &
   mod_vect_operators_destructor

 use mod_sol_state, only: &
   mod_sol_state_constructor, &
   mod_sol_state_destructor,  &
   t_sol_state, new_sol_state, &
   clear, write_octave

 use mod_sol_testcases, only: &
   mod_sol_testcases_constructor, &
   mod_sol_testcases_destructor, &
   tc

 use mod_sol_locmat, only: &
   mod_sol_locmat_constructor, &
   mod_sol_locmat_destructor

 use mod_sol_minimal_ode, only: &
   mod_sol_minimal_ode_constructor, &
   mod_sol_minimal_ode_destructor,  &
   t_sol_minimal_ode, new_sol_minimal_ode, clear, &
   t_sol_minimal_diags, new_sol_minimal_diags, &
   update_sol_diagnostics, write_octave, &
   ! these can be useful to write the system matrix
   mmmt, const_mmmt, aaat, psnt, psn_rhs, psn_rhs_contr, &
   out_file_sys_suff, out_file_sys_name, &
   sol_data, picard_res


 implicit none

 ! Define some general parameters
 integer, parameter :: max_char_len = 10000
 character(len=*), parameter :: this_prog_name = 'b_sol_minimal'

 ! IO variables
 character(len=*), parameter :: input_file_name_def = 'b-sol-minimal.in'
 character(len=max_char_len) :: input_file_name
 character(len=*), parameter :: out_file_nml_suff = '-nml.octxt'
 character(len=max_char_len+len(out_file_nml_suff)):: out_file_nml_name
 character(len=max_char_len) :: basename
 logical :: write_grid, write_sys
 character(len=*), parameter :: out_file_grid_suff = '-grid.octxt'
 character(len=max_char_len+len(out_file_grid_suff))::out_file_grid_name
 character(len=*), parameter :: out_file_base_suff = '-base.octxt'
 character(len=max_char_len+len(out_file_base_suff))::out_file_base_name
 integer :: fu, ierr
 ! additional output at selected times
 integer :: n_sot, nn_sot ! counter and number of special output times
 real(wp), allocatable :: selected_output_times(:)

 ! Grid
 type(t_h2d_grid), target :: grid
 type(t_ddc_h2d_grid) :: ddc_grid
 character(len=max_char_len) :: grid_file

 ! Base
 type(t_h2d_base) :: base
 integer :: gq_deg

 ! BCS
 integer :: nbound ! # of boundary regions
 character(len=max_char_len) :: cbc_type
 type(t_h2d_bcs), target :: bcs
 integer, allocatable :: bc_type_t(:,:)

 ! Test case
 character(len=max_char_len) :: testname
 character(len=max_char_len) :: integrator

 ! Unknowns
 integer :: n_ions
 type(t_sol_state), allocatable :: uuu0, uuun

 ! Initial condition
 integer :: shape_in(2), shape_out(2), ie, ix, iv, i
 !real(wp), allocatable :: xe(:,:,:), ve(:,:,:)

 ! Time integration
 integer :: nstep, n, n0, n_out
 real(wp) :: dt, tt_sta, tt_end, t_nm1, t_n
 logical :: l_checkpoint, l_output
 real(wp) :: time_last_out, dt_out, time_last_check, dt_check
 type(t_sol_minimal_ode) :: sol_ode
 type(t_sol_minimal_diags) :: sol_ods
 type(t_ti_init_data) :: ti_init_data
 type(t_ti_step_diag) :: ti_step_diag
 class(c_tint), allocatable :: tint

 character(len=max_char_len) :: message(1)

 ! Timing
 real(t_realtime) :: t00, t0, t1, t0step

 ! MPI variables
 logical :: master_proc
 integer :: mpi_nd, mpi_id

 ! Input namelist
 namelist /input/ &
   ! test case
   testname, &
   ! output base name
   basename, &
   ! grid
   write_grid, &
   grid_file,  &
   ! base
   gq_deg, & ! accuracy of the Gaussian quadrature rule
   ! linear system
   write_sys, &
   ! problm size
   n_ions, &
   ! boundary conditions
   nbound, cbc_type, &
   ! time stepping
   tt_sta, tt_end, dt, &
   integrator, &
   ! output
   dt_out, &
   ! chepoints
   dt_check, &
   ! special output times
   nn_sot ! number of times in selected_output_times (possibly 0)

 ! Auxiliary namelist (to make the allocation)
 namelist /out_details/ &
   selected_output_times


 !---------------------------------------------------------------------
 ! Initializations and startup
 t00 = my_second()

 call mod_messages_constructor()

 call mod_kinds_constructor()

 call mpi_init_thread(mpi_thread_multiple,i,ierr)
 call mod_mpi_utils_constructor()
 call mpi_comm_size(mpi_comm_world,mpi_nd,ierr)
 call mpi_comm_rank(mpi_comm_world,mpi_id,ierr)
 master_proc = mpi_id.eq.0

 !$ if(detailed_timing_omp) call mod_omp_utils_constructor()
 
 call mod_fu_manager_constructor()

 call mod_octave_io_constructor()

 call mod_linal_constructor()

 call mod_perms_constructor()
 call mod_octave_io_perms_constructor()

 call mod_sympoly_constructor()
 call mod_octave_io_sympoly_constructor()

 call mod_numquad_constructor()

 call mod_state_vars_constructor()

 !----------------------------------------------------------------------
 ! Read input file
 if(command_argument_count().gt.0) then
   call get_command_argument(1,value=input_file_name)
 else
   input_file_name = input_file_name_def
 endif

 call new_file_unit(fu,ierr)
 open(fu,file=trim(input_file_name), &
      status='old',action='read',form='formatted',iostat=ierr)
 read(fu,input)
 close(fu,iostat=ierr)
 ! If there are more processes, each of them needs its own basename
 ! and grid
 if(mpi_nd.gt.1) then
   write(basename, '(a,a,i3.3)') trim(basename),'-P',mpi_id
   write(grid_file,'(a,a,i3.3)') trim(grid_file),'.',mpi_id
 endif
 ! echo the input namelist
 out_file_nml_name = trim(basename)//out_file_nml_suff
 open(fu,file=trim(out_file_nml_name), &
      status='replace',action='write',form='formatted',iostat=ierr)
 write(fu,input)
 close(fu,iostat=ierr)

 allocate(bc_type_t(nbound,2))
 read(cbc_type,*) bc_type_t ! transposed, for simplicity
 !----------------------------------------------------------------------

 call mod_output_control_constructor(basename)

 !----------------------------------------------------------------------
 ! Define the grid and the base
 t0 = my_second()

 call mod_h2d_master_el_constructor()

 call mod_h2d_base_constructor()

 call mod_h2d_grid_constructor()

 call new_base_and_grid( base,grid , grid_file,gq_deg , &
                         ddc_grid, mpi_comm_world )

 ! write the octave output
 out_file_base_name = trim(base_name)//out_file_base_suff
 call new_file_unit(fu,ierr)
 open(fu,file=trim(out_file_base_name), &
      status='replace',action='write',form='formatted',iostat=ierr)
 call write_octave(base,'base',fu)
 close(fu,iostat=ierr)
 if(write_grid) then
   out_file_grid_name = trim(base_name)//out_file_grid_suff
   call new_file_unit(fu,ierr)
   open(fu,file=trim(out_file_grid_name), &
        status='replace',action='write',form='formatted',iostat=ierr)
   call write_octave(grid,'grid',fu)
   call write_octave(ddc_grid,'ddc_grid',fu)
   close(fu,iostat=ierr)
 endif

 t1 = my_second()
 write(message(1),elapsed_format) &
   'Completed base and grid construction: elapsed time ',t1-t0,'s.'
 if(master_proc) &
   call info(this_prog_name,'',message(1))
 !----------------------------------------------------------------------

 !---------------------------------------------------------------------
 ! Read the remaining input namelist(s)
 if(nn_sot.eq.0) then ! no additional times are required
   ! This is the simplest way to avoid any special casing
   nn_sot = 1
   allocate(selected_output_times(1))
   selected_output_times = 2.0_wp*tt_end ! beyond the final time
 else ! read the additional namelist
   call new_file_unit(fu,ierr)
   open(fu,file=trim(input_file_name), &
        status='old',action='read',form='formatted',iostat=ierr)
    allocate(selected_output_times(nn_sot))
    read(fu,out_details)
   close(fu,iostat=ierr)
   ! echo the input namelist
   open(fu,file=trim(out_file_nml_name), status='old', & 
     action='readwrite',form='formatted',position='append',iostat=ierr)
    write(fu,out_details)
   close(fu,iostat=ierr)
 endif
 !---------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! Define the boundary conditions

 call mod_h2d_bcs_constructor()
 call new_bcs(bcs,grid,transpose(bc_type_t) , ddc_grid,mpi_comm_world)
 deallocate( bc_type_t )

 if(write_grid) then
   call new_file_unit(fu,ierr)
   open(fu,file=trim(out_file_grid_name), status='old',action='write', &
        form='formatted',position='append',iostat=ierr)
    call write_octave(bcs,'bcs',fu)
   close(fu,iostat=ierr)
 endif
 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! Linear solver
 call mod_sparse_constructor()
 call mod_octave_io_sparse_constructor()
 call mod_linsolver_constructor()
 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! Allocations and initial condition
 t0 = my_second()

 call mod_time_integrators_constructor()

 call mod_sol_state_constructor()
 allocate( uuu0 , uuun )
 call new_sol_state(uuu0 , grid,base,bcs,n_ions,1)
 call new_sol_state(uuun , grid,base,bcs,n_ions,1)

 ! set-up the system geometry and the initial condition
 call mod_em_physical_constants_constructor()

 call mod_magnetic_geometry_constructor()

 call mod_vect_operators_constructor()
 
 ! set-up the test-case
 call mod_sol_testcases_constructor(trim(testname))

 do iv=1,grid%nv
   ! density
   uuu0%ni(1)%s(iv) = tc%dens_profile( grid%v(iv)%x )
   ! parallel velocity
   uuu0%vi(1)%v(1)%s(iv) = 0.0_wp
 enddo

 ! set-up the ODE
 call mod_sol_locmat_constructor()

 call mod_sol_minimal_ode_constructor(grid,ddc_grid,bcs, &
                                      base,write_sys)
 call new_sol_minimal_ode( sol_ode ,grid,ddc_grid,base,bcs)
 call new_sol_minimal_diags( sol_ods ,grid,base)
 if(write_grid) then ! include the ode data in the grid file
   call new_file_unit(fu,ierr)
   open(fu,file=trim(out_file_grid_name), status='old',action='write', &
        form='formatted',position='append',iostat=ierr)
    call write_octave(tc%phc,'phc',fu)
    call write_octave(sol_data,'sol_data',fu)
   close(fu,iostat=ierr)
 endif


! call write_octave(test_name       ,'test_name'       ,fu)
! call write_octave(test_description,'test_description',fu)

 t1 = my_second()
 write(message(1),elapsed_format) &
   'Completed start-up phase: elapsed time ',t1-t0,'s.'
 if(master_proc) &
   call info(this_prog_name,'',message(1))
 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! Time integration
 !
 !   0        1                 n             nstep
 !   |--------|--------|--------|--------|------|
 ! tt_sta            t_nm1     t_n            tt_end
 !                     | step_n |
 !

 ! Set time integration method
 time_int_case: select case(trim(integrator))
  case('bdf1')
   allocate(t_bdf1::tint)
  case('bdf2')
   allocate(t_bdf2::tint)
  case default
   call error(this_prog_name, '',&
    'Unknown integration method "'//trim(integrator)//'".')
 end select time_int_case
 
 call init_time_stepping()

 ! the next call also updates the diagnostics
 call write_out1(0,tt_sta,n_out,uuu0,sol_ods,ti_step_diag)

 call tint%init( sol_ode,dt,0.0_wp,uuu0,sol_ods,        &
        init_data=ti_init_data , step_diag=ti_step_diag )
 ! Consider the bootstrap steps
 n0 = ti_step_diag%bootstrap_steps
 t_n             = t_n             + n0*dt
 time_last_check = time_last_check + n0*dt
 time_last_out   = time_last_out   + n0*dt

 t0 = my_second()
 time_loop: do n=n0,nstep
    
   call init_time_step() ! setup and synchronization
   
   call tint%step(uuun,sol_ode,t_nm1,uuu0,sol_ods,ti_step_diag)
   call swap_sol_state( uuu0 , uuun )
   
   ! Checkpoint
   if(l_checkpoint) then
     !t1 = my_second()
     !call check_step(t1-t0step , grid , ddc_grid , base , dt , &
     !                uuu0%u , ti_step_diag)
   endif

   ! Select how often the full distribution is written
   if(l_output) then
     write(*,*) 'Step ',n,' of ',nstep
     call write_out1(n,t_n,n_out,uuu0,sol_ods,ti_step_diag)
   endif

!   ! Select how often the integrated diagnostics are written
!   l_output2 = (mod(n,nout2).eq.0).or.(n.eq.1).or.(n.eq.nstep)
!   if(l_output2) &
!     call write_out2(grid,base,t_n,uuu0,e_field,.false.,ti_step_diag)

 enddo time_loop
 t1 = my_second()
 write(message(1),elapsed_format) &
   'Completed time loop: elapsed time ',t1-t0,'s.'
 if(master_proc) &
   call info(this_prog_name,'',message(1))

 call tint%clean(sol_ode,ti_step_diag)
 deallocate(tint)

 call clear(sol_ods)
 call clear(sol_ode)
 call mod_sol_minimal_ode_destructor()

 call mod_sol_locmat_destructor()

 call mod_sol_testcases_destructor()

 call mod_vect_operators_destructor()

 call mod_magnetic_geometry_destructor()

 call mod_em_physical_constants_destructor()

 call clear(uuu0)
 call clear(uuun)
 deallocate( uuu0 , uuun )
 call mod_sol_state_destructor()

 call mod_time_integrators_destructor()

 call mod_linsolver_destructor()
 call mod_octave_io_sparse_destructor()
 call mod_sparse_destructor()

 call clear(bcs)
 call mod_h2d_bcs_destructor()

 call clear(ddc_grid)
 call clear(grid)
 call mod_h2d_grid_destructor()

 call clear(base)
 call mod_h2d_base_destructor()

 call mod_h2d_master_el_destructor()

 call mod_output_control_destructor()

 call mod_state_vars_destructor()

 call mod_numquad_destructor()

 call mod_octave_io_sympoly_destructor()
 call mod_sympoly_destructor()

 call mod_perms_destructor()
 call mod_octave_io_perms_destructor()

 call mod_linal_destructor()

 call mod_octave_io_destructor()

 call mod_fu_manager_destructor()

 !$ if(detailed_timing_omp) call mod_omp_utils_destructor()

 call mod_mpi_utils_destructor()
 call mpi_finalize(ierr)

 call mod_kinds_destructor()

 t1 = my_second()
 write(message(1),elapsed_format) &
   'Done: elapsed time ',t1-t00,'s.'
 if(mpi_id.eq.0) &
   call info(this_prog_name,'',message(1))

 call mod_messages_destructor()


contains

!-----------------------------------------------------------------------

 subroutine init_time_stepping()
 ! Set the main time stepping variables in a consistent way on all the
 ! processes.
 
  integer, parameter :: &
   n_ibuff = 3, &
   n_rbuff = 2
  integer ::  intgbuff(n_ibuff)
  real(wp) :: realbuff(n_rbuff)

   if(mpi_id.eq.0) then ! master process
     nstep = ceiling((tt_end-tt_sta)/dt)
     n_out = 0
     n_sot = 1
     t_n             = tt_sta
     time_last_out   = 0.0_wp
     time_last_check = 0.0_wp
   endif

   if(mpi_nd.le.1) return

   if(mpi_id.eq.0) then ! master process

     intgbuff = (/ nstep , n_out , n_sot /)
     realbuff = (/ dt , t_n /)
     call mpi_bcast(intgbuff,n_ibuff,mpi_integer,0,mpi_comm_world,ierr)
     call mpi_bcast(realbuff,n_rbuff,wp_mpi     ,0,mpi_comm_world,ierr)

   else

     call mpi_bcast(intgbuff,n_ibuff,mpi_integer,0,mpi_comm_world,ierr)
     call mpi_bcast(realbuff,n_rbuff,wp_mpi     ,0,mpi_comm_world,ierr)
     nstep = intgbuff(1)
     n_out = intgbuff(2)
     n_sot = intgbuff(3)
     dt              = realbuff(1)
     t_n             = realbuff(2)

   endif
   
 end subroutine init_time_stepping
 
!-----------------------------------------------------------------------

 subroutine init_time_step()
 ! Sets the main time step variables in a consistent way on all the
 ! processes.

  integer, parameter :: &
   n_lbuff = 2, &
   n_ibuff = 2, &
   n_rbuff = 3
  logical ::  logcbuff(n_lbuff)
  integer ::  intgbuff(n_ibuff)
  real(wp) :: realbuff(n_rbuff)

   if(mpi_id.eq.0) then ! master process

     ! main time step variables
     t_nm1 = t_n                  ! t^{n-1}
     t_n = tt_sta + real(n,wp)*dt ! t^n
     if(n.eq.nstep) then ! fix the last time step
       t_n = tt_end
       dt = t_n-t_nm1
       time_last_out = huge(time_last_out)/100.0_wp
     endif

     ! checkpoint
     time_last_check = time_last_check + dt
     if(time_last_check.ge.dt_check) then
       l_checkpoint = .true.
       time_last_check = time_last_check - dt_check
     else
       l_checkpoint = .false.
     endif

     ! output
     time_last_out = time_last_out + dt
     if( (time_last_out.ge.dt_out) .or. &
         (t_n.ge.selected_output_times(n_sot)) ) then
       l_output = .true.
       n_out = n_out+1
       if(time_last_out.ge.dt_out) &
         time_last_out = time_last_out - dt_out
       if(t_n.ge.selected_output_times(n_sot)) then
         if(n_sot.lt.nn_sot) then
           n_sot = n_sot + 1
         else ! last selected time: redefine it to avoid other outputs
           selected_output_times(n_sot) = 2.0_wp*tt_end
         endif
       endif
     else
       l_output = .false.
     endif

     ! communication
     if(mpi_nd.gt.0) then
       logcbuff = (/ l_checkpoint , l_output /)
       intgbuff = (/ n_out , n_sot /)
       realbuff = (/ t_nm1 , t_n , dt /)
       call mpi_bcast(logcbuff,n_lbuff,mpi_logical,0,mpi_comm_world, &
             ierr)
       call mpi_bcast(intgbuff,n_ibuff,mpi_integer,0,mpi_comm_world, &
             ierr)
       call mpi_bcast(realbuff,n_rbuff,wp_mpi     ,0,mpi_comm_world, &
             ierr)
     endif

   else

     call mpi_bcast(logcbuff,n_lbuff,mpi_logical,0,mpi_comm_world,ierr)
     call mpi_bcast(intgbuff,n_ibuff,mpi_integer,0,mpi_comm_world,ierr)
     call mpi_bcast(realbuff,n_rbuff,wp_mpi     ,0,mpi_comm_world,ierr)
     l_checkpoint = logcbuff(2)
     l_output     = logcbuff(3)
     n_out = intgbuff(1)
     n_sot = intgbuff(2)
     t_nm1                        = realbuff(1)
     t_n                          = realbuff(2)
     dt                           = realbuff(3)

   endif

 end subroutine init_time_step

!-----------------------------------------------------------------------

 subroutine swap_sol_state( x , y )
  type(t_sol_state), allocatable, intent(inout) :: x, y
  type(t_sol_state), allocatable :: w
  
   call move_alloc( to=w , from=x )
   call move_alloc( to=x , from=y )
   call move_alloc( to=y , from=w )
 end subroutine swap_sol_state

!-----------------------------------------------------------------------

 subroutine write_out1(n,t,n_out,uuu,ods,sd)
  integer, intent(in) :: n, n_out
  real(wp), intent(in) :: t
  type(t_sol_state), intent(in) :: uuu
  type(t_sol_minimal_diags), intent(inout) :: ods ! recomputed
  type(t_ti_step_diag), intent(in) :: sd

  integer :: fu, ierr
  character(len=*), parameter :: time_stamp_format = '(i4.4)'
  character(len=4) :: time_stamp
  character(len=*), parameter :: suff1 = '-res-'
  character(len=*), parameter :: suff2 = '.octxt'
  character(len= len_trim(base_name) + len(suff1) + 4 + len(suff2)) :: &
     out_file_res_name

   ! Update the diagnostics (using grid etc. from host association)
   call update_sol_diagnostics( ods , grid,base,bcs , uuu )

   call new_file_unit(fu,ierr)
   write(time_stamp,time_stamp_format) n_out
   out_file_res_name = trim(base_name)//suff1//time_stamp//suff2
   open(fu,file=out_file_res_name, &
        status='replace',action='write',form='formatted',iostat=ierr)
    call write_octave(trim(tc%test_name)       ,'test_name'       ,fu)
    associate( c => tc%test_description ) ! trim the lines
    call write_octave(c(:)(:maxval(len_trim(c))),'test_description',fu)
    end associate
    call write_octave(n  ,'n'  ,fu)
    call write_octave(t  ,'t'  ,fu)
    call write_octave(uuu%ni(1)%s     ,'c','ni'   ,fu)
    call write_octave(uuu%vi(1)%v(1)%s,'c','v_par',fu)
    call write_octave(ods,'diags',fu)
    if(allocated(picard_res)) &
      call write_octave(picard_res,'r','picard_res',fu)
    if(write_sys) then
      ! matrix skeletons
      call write_octave(          mmmt(1)        ,'mass_mmm_sk'  ,fu)
      call write_octave(          aaat(1)        ,'mass_aaa_sk'  ,fu)
      ! matrices
      call write_octave(transpose(mmmt(1)%m(1,1)),'mass_mmm'     ,fu)
      call write_octave(transpose(aaat(1)%m(1,1)),'mass_aaa'     ,fu)
      ! This is the easiest way to write the constant mass matrix
      mmmt(1)%m(1,1)%ax = const_mmmt
      call write_octave(transpose(mmmt(1)%m(1,1)),'const_mass_mmm',fu)
      call write_octave(transpose(   psnt%m(1,1)),'psnt'         ,fu)
      call write_octave(             psn_rhs,'c' ,'psn_rhs'      ,fu)
    endif
    call write_octave(            psn_rhs_contr,'psn_rhs_contr',fu)
   close(fu,iostat=ierr)

 end subroutine write_out1

!-----------------------------------------------------------------------

end program b_sol_minimal

