!> \file
!!
!! Solve a simplified Braginsky model
!!
!! \n
!!
!! The simplified Braginsky equation is solved directly, without
!! introducing the drift formulation.
program br1

 use mod_utils, only: &
   t_realtime, my_second

 use mod_messages, only: &
   mod_messages_constructor, &
   mod_messages_destructor,  &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_constructor, &
   mod_kinds_destructor,  &
   wp

 use mod_mpi_utils, only: &
   mod_mpi_utils_constructor, &
   mod_mpi_utils_destructor,  &
   mpi_logical, mpi_integer, wp_mpi, &
   mpi_comm_world, &
   mpi_init_thread, mpi_thread_single, mpi_thread_multiple, &
   mpi_finalize, &
   mpi_comm_size, mpi_comm_rank, &
   mpi_bcast, mpi_gather

 use mod_fu_manager, only: &
   mod_fu_manager_constructor, &
   mod_fu_manager_destructor,  &
   new_file_unit

 use mod_octave_io, only: &
   mod_octave_io_constructor, &
   mod_octave_io_destructor,  &
   real_format,    &
   write_octave,   &
   read_octave,    &
   read_octave_al
 
 use mod_linal, only: &
   mod_linal_constructor, &
   mod_linal_destructor

 use mod_perms, only: &
   mod_perms_constructor, &
   mod_perms_destructor

 use mod_octave_io_perms, only: &
   mod_octave_io_perms_constructor, &
   mod_octave_io_perms_destructor

 use mod_sympoly, only: &
   mod_sympoly_constructor, &
   mod_sympoly_destructor

 use mod_octave_io_sympoly, only: &
   mod_octave_io_sympoly_constructor, &
   mod_octave_io_sympoly_destructor

 use mod_numquad, only: &
   mod_numquad_constructor, &
   mod_numquad_destructor

 use mod_state_vars, only: &
   mod_state_vars_constructor, &
   mod_state_vars_destructor,  &
   c_stv

 use mod_sparse, only: &
   mod_sparse_constructor, &
   mod_sparse_destructor

 use mod_octave_io_sparse, only: &
   mod_octave_io_sparse_constructor, &
   mod_octave_io_sparse_destructor, &
   write_octave

 use mod_output_control, only: &
   mod_output_control_constructor, &
   mod_output_control_destructor,  &
   elapsed_format, &
   base_name

 use mod_h2d_master_el, only: &
   mod_h2d_master_el_constructor, &
   mod_h2d_master_el_destructor

 use mod_h2d_grid, only: &
   mod_h2d_grid_constructor, &
   mod_h2d_grid_destructor,  &
   t_h2d_grid, t_ddc_h2d_grid, new_base_and_grid, clear, &
   write_octave

 use mod_h2d_base, only: &
   mod_h2d_base_constructor, &
   mod_h2d_base_destructor,  &
   t_h2d_base, clear, &
   write_octave

 use mod_h2d_bcs, only: &
   mod_h2d_bcs_constructor, &
   mod_h2d_bcs_destructor,  &
   t_h2d_bcs, &
   new_bcs, clear, &
   write_octave
 
 use mod_linsolver, only: &
   mod_linsolver_constructor, &
   mod_linsolver_destructor

 use mod_time_integrators, only: &
   mod_time_integrators_constructor, &
   mod_time_integrators_destructor,  &
   c_ode, c_ods, c_tint, t_ti_init_data, t_ti_step_diag, &
   t_ee, t_hm, t_rk4, t_ssprk54, &
   t_thetam, t_bdf1, t_bdf2, t_bdf3, t_bdf2ex, &
   t_erb2kry, t_erb2lej, &
   t_pcexpo
 
 use mod_em_physical_constants, only: &
   mod_em_physical_constants_constructor, &
   mod_em_physical_constants_destructor, &
   write_octave

 use mod_magnetic_geometry, only: &
   mod_magnetic_geometry_constructor, &
   mod_magnetic_geometry_destructor

 use mod_sol_state, only: &
   mod_sol_state_constructor, &
   mod_sol_state_destructor,  &
   t_sol_state, new_sol_state, &
   clear, write_octave

 
 implicit none

 ! Define some general parameters
 integer, parameter :: max_char_len = 10000
 character(len=*), parameter :: this_prog_name = 'br1'

 ! IO variables
 character(len=*), parameter :: input_file_name_def = 'br1.in'
 character(len=max_char_len) :: input_file_name
 character(len=*), parameter :: out_file_nml_suff = '-nml.octxt'
 character(len=max_char_len+len(out_file_nml_suff)):: out_file_nml_name
 character(len=max_char_len) :: basename
 logical :: write_grid, write_sys
 character(len=*), parameter :: out_file_grid_suff = '-grid.octxt'
 character(len=max_char_len+len(out_file_grid_suff))::out_file_grid_name
 character(len=*), parameter :: out_file_base_suff = '-base.octxt'
 character(len=max_char_len+len(out_file_base_suff))::out_file_base_name
 integer :: fu, ierr
 ! additional output at selected times
 integer :: n_sot, nn_sot ! counter and number of special output times
 real(wp), allocatable :: selected_output_times(:)

 ! Grid
 type(t_h2d_grid), target :: grid
 type(t_ddc_h2d_grid) :: ddc_grid
 character(len=max_char_len) :: grid_file

 ! Base
 type(t_h2d_base) :: base
 integer :: gq_deg

 ! BCS
 integer :: nbound ! # of boundary regions
 character(len=max_char_len) :: cbc_type
 type(t_h2d_bcs), target :: bcs
 integer, allocatable :: bc_type_t(:,:)

 ! Test case
 character(len=max_char_len) :: testname
 character(len=max_char_len) :: integrator

 ! Prognostic variables
 type(t_sol_state), allocatable :: uuu0, uuun

 ! Time integration
 integer :: nstep, n, n0, n_out
 real(wp) :: dt, tt_sta, tt_end, t_nm1, t_n
 logical :: l_checkpoint, l_output
 real(wp) :: time_last_out, dt_out, time_last_check, dt_check
 !type(??? t_sol_minimal_ode) :: br_ode
 !type(??? t_sol_minimal_diags) :: br_ods
 !type(t_ti_init_data) :: ti_init_data
 !type(t_ti_step_diag) :: ti_step_diag
 !class(c_tint), allocatable :: tint

 character(len=max_char_len) :: message(1)

 ! Timing
 real(t_realtime) :: t00, t0, t1, t0step

 ! MPI variables
 logical :: master_proc
 integer :: mpi_nd, mpi_id

 ! Input namelist
 namelist /input/ &
!   ! test case
!   testname, &
   ! output base name
   basename, &
   ! grid
   write_grid, &
   grid_file,  &
   ! base
   gq_deg, & ! accuracy of the Gaussian quadrature rule
!   ! linear system
!   write_sys, &
   ! boundary conditions
   nbound, cbc_type, &
   ! time stepping
   integrator, &
   tt_sta, tt_end, dt, &
   ! output
   dt_out, &
   ! chepoints
   dt_check, &
   ! special output times
   nn_sot ! number of times in selected_output_times (possibly 0)

 ! Auxiliary namelist (to make the allocation)
 namelist /out_details/ &
   selected_output_times

 !---------------------------------------------------------------------
 ! Initializations and startup
 t00 = my_second()

 call mod_messages_constructor()

 call mod_kinds_constructor()

 call mpi_init_thread(mpi_thread_multiple,n,ierr) ! using n as tmp
 call mod_mpi_utils_constructor()
 call mpi_comm_size(mpi_comm_world,mpi_nd,ierr)
 call mpi_comm_rank(mpi_comm_world,mpi_id,ierr)
 master_proc = mpi_id.eq.0

 call mod_fu_manager_constructor()

 call mod_octave_io_constructor()

 call mod_linal_constructor()

 call mod_perms_constructor()
 call mod_octave_io_perms_constructor()

 call mod_sympoly_constructor()
 call mod_octave_io_sympoly_constructor()

 call mod_numquad_constructor()

 call mod_state_vars_constructor()

 !----------------------------------------------------------------------
 ! Read input file
 if(command_argument_count().gt.0) then
   call get_command_argument(1,value=input_file_name)
 else
   input_file_name = input_file_name_def
 endif

 call new_file_unit(fu,ierr)
 open(fu,file=trim(input_file_name), &
      status='old',action='read',form='formatted',iostat=ierr)
 read(fu,input)
 close(fu,iostat=ierr)
 ! add the partition index for parallel runs
 if(mpi_nd.gt.1) then
   write(basename, '(a,a,i3.3)') trim(basename),'-P',mpi_id
   write(grid_file,'(a,a,i3.3)') trim(grid_file),'.',mpi_id
 endif
 ! echo the input namelist
 out_file_nml_name = trim(basename)//out_file_nml_suff
 open(fu,file=trim(out_file_nml_name), &
      status='replace',action='write',form='formatted',iostat=ierr)
 write(fu,input)
 close(fu,iostat=ierr)

 allocate(bc_type_t(nbound,2))
 read(cbc_type,*) bc_type_t ! transposed, for simplicity
 !----------------------------------------------------------------------

 call mod_output_control_constructor(basename)

 !----------------------------------------------------------------------
 ! Define the grid and the base
 t0 = my_second()

 call mod_h2d_master_el_constructor()

 call mod_h2d_base_constructor()

 call mod_h2d_grid_constructor()

 call new_base_and_grid( base,grid , grid_file,gq_deg , &
                         ddc_grid, mpi_comm_world )

 ! write the octave output
 out_file_base_name = trim(base_name)//out_file_base_suff
 call new_file_unit(fu,ierr)
 open(fu,file=trim(out_file_base_name), &
      status='replace',action='write',form='formatted',iostat=ierr)
 call write_octave(base,'base',fu)
 close(fu,iostat=ierr)
 if(write_grid) then
   out_file_grid_name = trim(base_name)//out_file_grid_suff
   call new_file_unit(fu,ierr)
   open(fu,file=trim(out_file_grid_name), &
        status='replace',action='write',form='formatted',iostat=ierr)
   call write_octave(grid,'grid',fu)
   call write_octave(ddc_grid,'ddc_grid',fu)
   close(fu,iostat=ierr)
 endif

 t1 = my_second()
 write(message(1),elapsed_format) &
   'Completed base and grid construction: elapsed time ',t1-t0,'s.'
 if(master_proc) &
   call info(this_prog_name,'',message(1))
 !----------------------------------------------------------------------

 !---------------------------------------------------------------------
 ! Read the remaining input namelist(s)
 if(nn_sot.eq.0) then ! no additional times are required
   ! This is the simplest way to avoid any special casing
   nn_sot = 1
   allocate(selected_output_times(1))
   selected_output_times = 2.0_wp*tt_end ! beyond the final time
 else ! read the additional namelist
   call new_file_unit(fu,ierr)
   open(fu,file=trim(input_file_name), &
        status='old',action='read',form='formatted',iostat=ierr)
    allocate(selected_output_times(nn_sot))
    read(fu,out_details)
   close(fu,iostat=ierr)
   ! echo the input namelist
   open(fu,file=trim(out_file_nml_name), status='old', & 
     action='readwrite',form='formatted',position='append',iostat=ierr)
    write(fu,out_details)
   close(fu,iostat=ierr)
 endif
 !---------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! Define the boundary conditions

 call mod_h2d_bcs_constructor()
 call new_bcs(bcs,grid,transpose(bc_type_t) , ddc_grid,mpi_comm_world)
 deallocate( bc_type_t )

 if(write_grid) then
   call new_file_unit(fu,ierr)
   open(fu,file=trim(out_file_grid_name), status='old',action='write', &
        form='formatted',position='append',iostat=ierr)
    call write_octave(bcs,'bcs',fu)
   close(fu,iostat=ierr)
 endif
 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! Linear solver

 call mod_sparse_constructor()

 call mod_octave_io_sparse_constructor()

 call mod_linsolver_constructor()
 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! Allocations and initial condition
 t0 = my_second()

 call mod_time_integrators_constructor()

 call mod_sol_state_constructor()
 allocate( uuu0 , uuun )
 call new_sol_state( uuu0 , grid,base,bcs , n_ions=1 , d=3 )
 call new_sol_state( uuun , grid,base,bcs , n_ions=1 , d=3 )

 ! set-up the system geometry and the initial condition
 call mod_em_physical_constants_constructor()

 call mod_magnetic_geometry_constructor()

 ! ! set-up the test-case
 ! call mod_sol_testcases_constructor(trim(testname))

 ! do iv=1,grid%nv
 !   ! density
 !   uuu0%ni(1)%s(iv) = tc%dens_profile( grid%v(iv)%x )
 !   ! parallel velocity
 !   uuu0%vi(1)%v(1)%s(iv) = 0.0_wp
 ! enddo

 ! ! set-up the ODE
 ! call mod_sol_locmat_constructor()

mod_sol_br1_data -> t_ods with ve and phi
                 -> the three unit vectors at the quad points
                 -> B at the quad points

mod_sol_br1_ode -> GMRES resolution of the precond. linear system

loc_mat -> ??? make the computations, maybe the best thing is having a
module with all these computations: mod_sol_br1_locmat

 ! call mod_sol_minimal_ode_constructor(grid,ddc_grid,bcs, &
 !                                      base,write_sys)
 ! call new_sol_minimal_ode( sol_ode ,grid,ddc_grid,base,bcs)
 ! call new_sol_minimal_diags( sol_ods ,grid,base)
 ! if(write_grid) then ! include the ode data in the grid file
 !   call new_file_unit(fu,ierr)
 !   open(fu,file=trim(out_file_grid_name), status='old',action='write', &
 !        form='formatted',position='append',iostat=ierr)
 !    call write_octave(tc%phc,'phc',fu)
 !    call write_octave(sol_data,'sol_data',fu)
 !   close(fu,iostat=ierr)
 ! endif


! call write_octave(test_name       ,'test_name'       ,fu)
! call write_octave(test_description,'test_description',fu)

 t1 = my_second()
 write(message(1),elapsed_format) &
   'Completed start-up phase: elapsed time ',t1-t0,'s.'
 if(master_proc) &
   call info(this_prog_name,'',message(1))
 !----------------------------------------------------------------------




 call mod_magnetic_geometry_destructor()

 call mod_em_physical_constants_destructor()

 call clear( uuu0 )
 call clear( uuun )
 deallocate( uuu0 , uuun )
 call mod_sol_state_destructor()

 call mod_time_integrators_destructor()

 call mod_linsolver_destructor()

 call mod_octave_io_sparse_destructor()
 call mod_sparse_destructor()

 call clear(bcs)
 call mod_h2d_bcs_destructor()

 call clear(ddc_grid)
 call clear(grid)
 call mod_h2d_grid_destructor()

 call clear(base)
 call mod_h2d_base_destructor()

 call mod_h2d_master_el_destructor()

 call mod_output_control_destructor()

 call mod_state_vars_destructor()

 call mod_numquad_destructor()

 call mod_sympoly_destructor()
 call mod_octave_io_sympoly_destructor()

 call mod_perms_destructor()
 call mod_octave_io_perms_destructor()

 call mod_linal_destructor()

 call mod_octave_io_destructor()

 call mod_fu_manager_destructor()

 call mpi_finalize(ierr)
 call mod_mpi_utils_destructor()

 call mod_messages_destructor()

 call mod_kinds_destructor()

end program br1

