!>\brief
!!
!! Collect some physical constants for electromagnetic problems
!!
!! \n
!!
!! This module should not be used directly, but only through a
!! testcase object, so that each test case is free to redefine the
!! physical constants.
!<----------------------------------------------------------------------
module mod_em_physical_constants

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_octave_io, only: &
   mod_octave_io_initialized, &
   write_octave

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_em_physical_constants_constructor, &
   mod_em_physical_constants_destructor,  &
   mod_em_physical_constants_initialized, &
   t_phc, write_octave

 private

!-----------------------------------------------------------------------

! Module types and parameters

 ! private members
 real(wp), parameter :: &
   !> light speed
   c  = 2.9979e10_wp,  & ! cm/s
   !> electron charge
   e  = 4.8032e-10_wp, & ! statcoulomb
   !> electron mass
   me = 9.1094e-28_wp, & ! g
   sqrtme = sqrt(me),  &
   !> Coulomb logarithm
   lambda = 16.0_wp,   &
   !> atomic number (defined as real to simplify the implementation)
   z  = 1.0_wp,        &
   !> reference internal and external densities
   nmax = 1.0e13_wp,   &
   nmin = nmax/1.0e4_wp, &
   !> density anomalous diffusions
   adiff_n = 0.1e4_wp, & ! cm^2/s
   adiff_p = 0.0_wp,   &
   !> reference internal temperature
   t0 = (1.6022e-12_wp*1.0e3_wp) * 1.0_wp, & ! erg/keV * keV
   !> anomalous conductivity as a fraction of the perpendicular one
   sigma_an_eps = 1.0_wp, & ! dimensionless
   !> \f$\pi\f$
   pi = 3.14159265358979323846264338327950_wp, &
   !> \f$\sqrt{2\pi}\f$
   sqrt2pi = 2.50662827463100050241576528481105_wp

 ! public members

 !> physical constant collection
 type :: t_phc
  ! fundamental constants
  real(wp) :: c       = c
  real(wp) :: e       = e
  real(wp) :: me      = me
  real(wp) :: lambda  = lambda
  ! some useful quantities
  real(wp) :: tau_e   = 3.0_wp*sqrtme / (4.0_wp*sqrt2pi*lambda*e**4)
  ! reference values
  real(wp) :: nmax    = nmax
  real(wp) :: nmin    = nmin
  real(wp) :: t0      = t0
  real(wp) :: z       = z
  ! empirical coefficients
  real(wp) :: adiff_n = adiff_n
  real(wp) :: adiff_p = adiff_p
  real(wp) :: sigma_an_eps = sigma_an_eps
  ! pi
  real(wp) :: pi      = pi
  real(wp) :: sqrt2pi = sqrt2pi
 end type t_phc

 interface write_octave
   module procedure write_phc_struct
 end interface 

! Module variables

 ! public members
 logical, protected :: &
   mod_em_physical_constants_initialized = .false.
 ! private members
 character(len=*), parameter :: &
   this_mod_name = 'mod_em_physical_constants'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_em_physical_constants_constructor()
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false.) .or. &
          (mod_kinds_initialized.eqv..false.) .or. &
      (mod_octave_io_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_em_physical_constants_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   mod_em_physical_constants_initialized = .true.
 end subroutine mod_em_physical_constants_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_em_physical_constants_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_em_physical_constants_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_em_physical_constants_initialized = .false.
 end subroutine mod_em_physical_constants_destructor

!-----------------------------------------------------------------------

 subroutine write_phc_struct(phc,var_name,fu)
  integer, intent(in) :: fu
  type(t_phc), intent(in) :: phc
  character(len=*), intent(in) :: var_name
 
  character(len=*), parameter :: &
    this_sub_name = 'write_phc_struct'
 
   write(fu,'(a,a)')    '# name: ',var_name
   write(fu,'(a)')      '# type: struct'
   write(fu,'(a)')      '# length: 14' ! number of fields

   ! field 01 : c
   write(fu,'(a)')      '# name: c'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(phc%c,'<cell-element>',fu)

   ! field 02 : e
   write(fu,'(a)')      '# name: e'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(phc%e,'<cell-element>',fu)

   ! field 03 : me
   write(fu,'(a)')      '# name: me'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(phc%me,'<cell-element>',fu)

   ! field 04 : lambda
   write(fu,'(a)')      '# name: lambda'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(phc%lambda,'<cell-element>',fu)

   ! field 05 : tau_e
   write(fu,'(a)')      '# name: tau_e'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(phc%tau_e,'<cell-element>',fu)

   ! field 06 : nmax
   write(fu,'(a)')      '# name: nmax'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(phc%nmax,'<cell-element>',fu)

   ! field 07 : nmin
   write(fu,'(a)')      '# name: nmin'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(phc%nmin,'<cell-element>',fu)

   ! field 08 : t0
   write(fu,'(a)')      '# name: t0'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(phc%t0,'<cell-element>',fu)

   ! field 09 : z
   write(fu,'(a)')      '# name: z'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(phc%z,'<cell-element>',fu)

   ! field 10 : adiff_n
   write(fu,'(a)')      '# name: adiff_n'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(phc%adiff_n,'<cell-element>',fu)

   ! field 11 : adiff_p
   write(fu,'(a)')      '# name: adiff_p'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(phc%adiff_p,'<cell-element>',fu)

   ! field 12 : sigma_an_eps
   write(fu,'(a)')      '# name: sigma_an_eps'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(phc%sigma_an_eps,'<cell-element>',fu)

   ! field 13 : pi
   write(fu,'(a)')      '# name: pi'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(phc%pi,'<cell-element>',fu)

   ! field 14 : sqrt2pi
   write(fu,'(a)')      '# name: sqrt2pi'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(phc%sqrt2pi,'<cell-element>',fu)

 end subroutine write_phc_struct

!-----------------------------------------------------------------------

end module mod_em_physical_constants

