!>\brief
!!
!! Module to describe the magnetic geometry and some profiles
!!
!! \n
!!
!! \section magn_geom_geometrical_setup Geometrical setup
!!
!! Let us first introduce a three dimensional reference using
!! cylindrical coordinates, namely \f$[R\,,\varphi\,,z]\f$. The
!! toroidal coordinate \f$\varphi\f$ is the homogeneity coordinate.
!! The relevant differential operators in such coordinates are
!! \f{displaymath}{
!!  \nabla f \mapsto
!!  \left[\partial_Rf\,,R^{-1}\partial_\varphi f\,,\partial_z
!!  f\right]^T ,\qquad
!!  \nabla\cdot{\bf a} \mapsto 
!!  R^{-1}\partial_R(Ra_R) + R^{-1}\partial_{\varphi}a_\varphi +
!!  \partial_za_z.
!! \f}
!! Another useful coordinate system is defined aligning one coordinate
!! with the magnetic field, which direction is defined by the unit
!! vector \f${\bf b}\f$. We thus introduce the two bases
!! \f{displaymath}{
!!  \left[
!!   \begin{array}{ccc}
!!    {\bf e}_1 &
!!    {\bf e}_2 &
!!    {\bf e}_3 
!!   \end{array}
!!  \right] = 
!!  \left[
!!   \begin{array}{ccc}
!!    1 & 0 & b_r \\
!!    0 & 0 & b_{\varphi} \\
!!    0 & 1 & b_z
!!   \end{array}
!!  \right], \qquad
!!  \left[
!!   \begin{array}{ccc}
!!    {\bf e}^1 &
!!    {\bf e}^2 &
!!    {\bf e}^3 
!!   \end{array}
!!  \right] = 
!!  \left[
!!   \begin{array}{ccc}
!!    1 & 0 & 0 \\
!!    -\frac{b_r}{b_{\varphi}} & -\frac{b_z}{b_{\varphi}} &
!!    \frac{1}{b_{\varphi}} \\
!!    0 & 1 & 0
!!   \end{array}
!!  \right],
!! \f}
!! so that
!! \f{displaymath}{
!!  {\bf B} \parallel {\bf e}_3, \quad {\bf B}\perp {\bf e}^{1,2},
!!  \qquad 
!!  \nabla \parallel {\bf e}_{1,2}, \quad \nabla\perp {\bf e}^3.
!! \f}
!!
!! \subsection magn_geom_codet More about the cylindrical coordinates
!!
!! The cylindrical coordinates defined above induce the covariant
!! basis
!! \f{displaymath}{
!!  \hat{\bf e}_R = \frac{\partial}{\partial R}{\bf x}, \quad
!!  \hat{\bf e}_{\varphi} = \frac{\partial}{\partial \varphi}{\bf x},
!!  \quad
!!  \hat{\bf e}_z = \frac{\partial}{\partial z}{\bf x},
!! \f}
!! with
!! \f{displaymath}{
!!  {\bf x} = \left[
!!  \begin{array}{c}
!!   R\cos\varphi \\ R\sin\varphi \\ z
!!  \end{array}
!!  \right].
!! \f}
!! These three vectors are mutually orthogonal and we have
!! \f$|\hat{\bf e}_R| = |\hat{\bf e}_z| = 1\f$ and
!! \f$|\hat{\bf e}_{\varphi}| = R\f$. Due to the fact that 
!! \f$\hat{\bf e}_{\varphi}\f$ is not normalized, the contravariant
!! basis does not coincide with the covariant one, and in fact one has
!! \f{displaymath}{
!!  \hat{\bf e}^R = \hat{\bf e}_R, \quad
!!  \hat{\bf e}^{\varphi} = \frac{1}{R^2} \hat{\bf e}_{\varphi}, \quad
!!  \hat{\bf e}^z = \hat{\bf e}_z.
!! \f}
!! Typically, when working with cylindrical coordinates, a normalized
!! basis is preferred, i.e. one defines
!! \f{displaymath}{
!!  {\bf e}_R = \hat{\bf e}_R = \hat{\bf e}^R, \quad
!!  {\bf e}_{\varphi} = \frac{1}{R}\hat{\bf e}_{\varphi} = R \hat{\bf
!!  e}^{\varphi}, \quad
!!  {\bf e}_z = \hat{\bf e}_z = \hat{\bf e}^z;
!! \f}
!! for this normalized basis the primal and dual bases coincide, so
!! that there is no need to distinguish between subscript and
!! superscript indexes. Here, we follow this convention.
!! \note Since \f$\partial_{\varphi}{\bf e}_R\neq\partial_R{\bf
!! e}_{\varphi}\f$, the normalized basis is not induced by any
!! coordinate system.
!!
!! The choice of the basis is important when specializing the general
!! differential geometry definitions to our coordinates; for instance,
!! concerning the gradient we have
!! \f{displaymath}{
!!  \begin{array}{rcl}
!!   \nabla f & = & \partial_{u^i}f\,{\bf e}^i \\
!!   & = & \partial_Rf\,\hat{\bf e}^R + 
!!   \partial_{\varphi}f\,\hat{\bf e}^{\varphi} + 
!!   \partial_zf\,\hat{\bf e}^z \\
!!   & = & \partial_Rf\,{\bf e}_R + 
!!   R^{-1}\partial_{\varphi}f\,{\bf e}_{\varphi} + 
!!   \partial_zf\,{\bf e}_z.
!!  \end{array}
!! \f}
!! Let us now consider the gradient of a vector field: \f$\nabla{\bf
!! a}\f$. For a generic coordinate system, such a quantity has a
!! contravariant and a covariant representation, given respectively as
!! \f{displaymath}{
!!  a^i_{,j} = \frac{\partial a^i}{\partial u^j} +
!!  \hat{\Gamma}^i_{jk}a^k, \qquad
!!  a_{i,j} = \frac{\partial a_i}{\partial u^j} -
!!  \hat{\Gamma}^k_{ij}a_k
!! \f}
!! (this is in fact the covariant derivative defined in differential
!! geometry), which can be multiplied by the contravariant components
!! of a vector (a vector tangent to the displacement) to compute
!! \f$\Delta{\bf a} = {\bf a}({\bf x}+{\bf v}) - {\bf a}({\bf x})\f$
!! as
!! \f{displaymath}{
!!  (\Delta {\bf a})^i = a^i_{,j}v^j + o(v), \qquad
!!  (\Delta {\bf a})_i = a_{i,j}v^j + o(v).
!! \f}
!! For the cylindrical system, the only nonzero symbols are
!! \f{displaymath}{
!!  \hat{\Gamma}^{\varphi}_{R\varphi} =
!!  \hat{\Gamma}^{\varphi}_{\varphi R} = R^{-1}, \quad
!!  \hat{\Gamma}^{R}_{\varphi\varphi} = -R,
!! \f}
!! so that
!! \f{displaymath}{
!!  a^i_{,j} = \left[\begin{array}{ccc}
!!   \partial_R\hat{a}^R &
!!   \partial_{\varphi}\hat{a}^R-R\hat{a}^{\varphi} &
!!   \partial_z\hat{a}^R \\
!!   \partial_R\hat{a}^\varphi + R^{-1}\hat{a}^\varphi &
!!   \partial_{\varphi}\hat{a}^\varphi + R^{-1}\hat{a}^R &
!!   \partial_z\hat{a}^\varphi \\
!!   \partial_R\hat{a}^z & \partial_{\varphi}\hat{a}^z &
!!   \partial_z\hat{a}^z
!!   \end{array}\right]
!! \f}
!! and
!! \f{displaymath}{
!!  a_{i,j} = \left[\begin{array}{ccc}
!!   \partial_R\hat{a}_R &
!!   \partial_{\varphi}\hat{a}_R-R^{-1}\hat{a}_{\varphi} &
!!   \partial_z\hat{a}_R \\
!!   \partial_R\hat{a}_\varphi - R^{-1}\hat{a}_\varphi &
!!   \partial_{\varphi}\hat{a}_\varphi + R\hat{a}_R &
!!   \partial_z\hat{a}_\varphi \\
!!   \partial_R\hat{a}_z & \partial_{\varphi}\hat{a}_z &
!!   \partial_z\hat{a}_z
!!   \end{array}\right].
!! \f}
!! Any of these expressions can be used to compute the coordinates of
!! the gradient with respect to the normalized basis. To do this, we
!! have to substitute
!! \f{displaymath}{
!!  \hat{a}^R=\hat{a}_R=a_R, \quad
!!  R \hat{a}^\varphi=R^{-1} \hat{a}_\varphi=a_\varphi, \quad
!!  \hat{a}^z=\hat{a}_z=a_z
!! \f}
!! and scale the components according to the new basis, such as
!! \f{displaymath}{
!!  (\nabla{\bf a})_{\varphi R} = 
!!  \nabla{\bf a} : {\bf e}_\varphi\otimes{\bf e}_R = 
!!  \left\{\begin{array}{c}
!!   \displaystyle
!!   R^{-1} \nabla{\bf a} : \hat{\bf e}_\varphi\otimes\hat{\bf e}_R =
!!   R^{-1}a_{\varphi,R} =
!!   R^{-1}\left[\partial_R(Ra_\varphi)-a_\varphi\right] \\[4mm]
!!  \displaystyle
!!  R \nabla{\bf a} : \hat{\bf e}^\varphi\otimes\hat{\bf e}_R =
!!  Ra^\varphi_{,R} =
!!  R\left[\partial_R(R^{-1}a_\varphi)+R^{-2}a_\varphi\right] 
!! \end{array}\right\} = \partial_Ra_\varphi.
!! \f}
!! We arrive at
!! \f{displaymath}{
!!  \nabla{\bf a} = \left[\begin{array}{ccc} \partial_Ra_R &
!!  R^{-1}\left( \partial_{\varphi}a_R-a_{\varphi}\right) &
!!  \partial_za_R \\
!!   \partial_Ra_\varphi & R^{-1}\left( \partial_{\varphi}a_\varphi +
!!   a_R\right) & \partial_za_\varphi \\
!!   \partial_Ra_z & R^{-1}\partial_{\varphi}a_z & \partial_za_z
!!   \end{array}\right].
!! \f}
!!
!! A direct verification using the corresponding expressions shows
!! that, once \f$\nabla{\bf a}\f$ has been computed, the divergence
!! and curl of the vector field can be obtained as
!! \f{displaymath}{
!!  \nabla\cdot{\bf a} = {\rm tr}( \nabla{\bf a} ), \qquad
!!  \nabla\times{\bf a} = \nabla{\bf a} - \nabla{\bf a}^T = \left[
!!  \begin{array}{c}
!!   \nabla{\bf a}_{z\varphi} - \nabla{\bf a}_{\varphi z} \\
!!   \nabla{\bf a}_{Rz} - \nabla{\bf a}_{zR} \\
!!   \nabla{\bf a}_{\varphi R} - \nabla{\bf a}_{R\varphi}
!!  \end{array}
!!  \right].
!! \f}
!!
!! \subsection vector_implementation Vectors in the poloidal plane
!!
!! The most natural representation in the three-dimensional
!! cylindrical system would be
!! \f{displaymath}{
!!  {\bf a} = [a_R\,,a_{\varphi}\,,a_z]^T = a_R{\bf e}_R +
!!  a_{\varphi}{\bf e}_{\varphi} + a_z{\bf e}_z,
!! \f}
!! which uses the orthogonal, right oriented local basis. For a
!! toroidally symmetric problem, however, it is convenient to
!! represent the poloidal component of the vector using the first two
!! indexes. For this reason, all the three-dimensional vectors are
!! represented with respect to
!! \f{displaymath}{
!!  \tilde{\bf e}_R = {\bf e}_R, \qquad
!!  \tilde{\bf e}_z = {\bf e}_z, \qquad
!!  \tilde{\bf e}_{\varphi} = -{\bf e}_{\varphi},
!! \f}
!! which is still a right oriented local basis. A vector with positive
!! third component <tt>a(3).gt.0</tt>, therefore, points outward from
!! the poloidal plane.
!!
!! \section magn_geom_B_field Magnetic field and surfaces
!!
!! The magnetic field is prescribed as
!! \f{displaymath}{
!!  {\bf B} = -I\,\nabla\varphi - \nabla \psi \times \nabla\varphi,
!! \f}
!! where \f$I=I(\psi)\f$, \f$\varphi\f$ is the toroidal angle and
!! \f$2\pi\psi\f$ is the poloidal flux (see also equation (6.2.13) of
!! <a
!! href="http://www.springer.com/978-3-642-75597-2">[D'haeseleer,
!! Hitchon, Callen, Shohet, <em>Flux coordinates and magnetic field
!! structures</em>]</a>).
!!
!! \note In this module \f$\varphi\f$ always denotes the toroidal
!! angle, measured in counterclockwise direction when looking from
!! \f$z=+\infty\f$. This is different from what is done in other
!! references, where a minus sign is introduced. Hence,
!! \f{displaymath}{
!!  \nabla\varphi \parallel {\bf e}_{\varphi} = -\tilde{\bf
!!  e}_{\varphi}.
!! \f}
!!
!! On the poloidal plane, we can use the two coordinates \f$R,z\f$
!! defined \ref magn_geom_geometrical_setup "here" to express
!! \f$\psi=\psi(R,z)\f$.
!!
!! A simple configuration is obtained setting \f$I=B_0R_0\f$ and
!! \f{displaymath}{
!!  \psi = \frac{1}{2}B_paR_0\frac{ (R-R_0)^2 + z^2 }{a^2},
!! \f}
!! where \f$B_0\f$, \f$B_p\f$ and\f$R_0\f$ are constants and we assume
!! \f$R_0\geq 0\f$. Notice that we make no assumptions concerning the
!! sign of \f$B_0\f$ and \f$B_p\f$. A typical value would be
!! \f$B_p=0.1B_0\f$. In cylindrical coordinates we have
!! \f{displaymath}{
!!  \nabla\varphi = \frac{1}{R}{\bf e}_{\varphi}, \qquad
!!  \nabla\psi = B_p\frac{R_0}{a}\left( (R-R_0){\bf e}_R + z{\bf e}_z
!!  \right),
!! \f}
!! which yields the magnetic field
!! \f{displaymath}{
!!  {\bf B} = -\frac{R_0}{R}B_0{\bf e}_{\varphi} -
!!  \frac{R_0}{aR}B_p\left( (R-R_0){\bf e}_z - z{\bf e}_R \right).
!! \f}
!! To compute the gradient of the unit vector \f${\bf b}\f$, observe
!! that
!! \f{displaymath}{
!!  \nabla{\bf b} = \frac{1}{B}\nabla{\bf B} - \frac{1}{B^2}{\bf B}
!!  \otimes \nabla B
!! \f}
!! which provides, in cylindrical coordinates,
!! \f{displaymath}{
!!  B = \frac{R_0}{R}|B_0|\sqrt{1+\frac{(R-R_0)^2+z^2}{a^2}
!!  \frac{B_p^2}{B_0^2}},
!! \f}
!! \f{displaymath}{
!!  \nabla B = \frac{R_0}{R^2}|B_0|\left[1+\frac{(R-R_0)^2+z^2}{a^2}
!!  \frac{B_p^2}{B_0^2}\right]^{-\frac{1}{2}} \left[
!!   \begin{array}{c}
!!    - 1 - \frac{R_0^2-R_0R+z^2}{a^2}\frac{B_p^2}{B_0^2} \\[3mm]
!!    0 \\[3mm]
!!    \frac{Rz}{a^2}\frac{B_p^2}{B_0^2}
!!   \end{array}
!!  \right]
!! \f}
!! and
!! \f{displaymath}{
!!  \nabla{\bf B} = \frac{R_0}{R^2}
!!  \left[\begin{array}{ccc}
!!   -\frac{z}{a}B_p & B_0 & \frac{R}{a}B_p \\[2mm]
!!   B_0 & \frac{z}{a}B_p & 0 \\[2mm]
!!   -\frac{R_0}{a}B_p & 0 & 0
!!  \end{array}\right].
!! \f}
!! \note Since \f${\bf B}\f$ does not depend on \f$\varphi\f$, we have
!! \f$(\nabla{\bf b})_{z\varphi}=0\f$, while the remaining terms of
!! such gradient do not vanish, in general.
!!
!! To compute \f$\nabla\times\frac{\bf b}{B}\f$ we use
!! \f{displaymath}{
!!  \nabla\times\frac{\bf b}{B} = \frac{1}{B}\left( \nabla\times{\bf
!!  b} - \frac{1}{B}\nabla B\times{\bf b} \right).
!! \f}
!<----------------------------------------------------------------------
module mod_magnetic_geometry

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_magnetic_geometry_constructor, &
   mod_magnetic_geometry_destructor,  &
   mod_magnetic_geometry_initialized, &
   i_dx, setup_magnetic_geometry, &
   compute_toroidal_magnetic_geom, &
   compute_local_magnetic_geom, &
   sharp_radial_profile, &
   compute_polynoml_temperat_prof

 private

!-----------------------------------------------------------------------

! Module types and parameters

 !> Compute the volume element
 !!
 !! The functions to compute the volume element are private to this
 !! module; they should be used through the pointers returned by the
 !! functions which compute the magnetic geometry.
 abstract interface
  pure function i_dx(x) result(dx)
   import :: wp
   implicit none
   real(wp), intent(in) :: x(:,:)
   real(wp) :: dx(size(x,2))
  end function i_dx  
 end interface

 !> Select the geometry (see \c setup_magnetic_geometry)
 !!
 !! One of these variables should be set by \c
 !! setup_magnetic_geometry; if none is set, various module procedures
 !! will return error codes.
 logical :: &
   cart_geometry = .false. , &
   toro_geometry = .false.

 !> This variable is used in \c dx_cart to include the factor
 !! \f$R_0\f$ in the volume element; it shall be set by \c
 !! setup_magnetic_geometry.
 real(wp) :: dx_r0

 ! public members
 logical, protected ::               &
   mod_magnetic_geometry_initialized = .false.
 ! private members
 character(len=*), parameter :: &
   this_mod_name = 'mod_magnetic_geometry'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_magnetic_geometry_constructor()
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_kinds_initialized.eqv..false.) .or. &
    (mod_messages_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_magnetic_geometry_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   mod_magnetic_geometry_initialized = .true.
 end subroutine mod_magnetic_geometry_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_magnetic_geometry_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_magnetic_geometry_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_magnetic_geometry_initialized = .false.
 end subroutine mod_magnetic_geometry_destructor

!-----------------------------------------------------------------------

 !> Define some module variables which control the geometrical
 !! configuration
 !!
 !! In principle, this subroutine could be absorbed into the module
 !! constructor, however this would complicate calling this function
 !! from the test case constructor, which seems to be the most natural
 !! choice. So the present implementation assumes the following code
 !! flow:
 !!
 !! \code
 !!
 !!   call mod_magnetic_geometry_constructor()
 !!   call mod_sol_testcases_constructor()
 !!     call mod_<selected_test>_constructor()
 !!       call setup_magnetic_geometry() ! this function
 !!       call <other-procedures-in-this-module>()
 !!   ...
 !!   call mod_magnetic_geometry_destructor()
 !!
 !! \endcode
 !!
 !! No module allocations are done here, so this function does not
 !! need a corresponding clean-up subroutine.
 subroutine setup_magnetic_geometry(geom,r0 , dx)
  character(len=*), intent(in) :: geom
  real(wp), intent(in) :: r0
  procedure(i_dx), pointer, intent(out) :: dx

  character(len=*), parameter :: &
    this_sub_name = 'setup_magnetic_geometry'
 
   if( mod_magnetic_geometry_initialized .eqv. .false. )               &
     call error(this_sub_name,this_mod_name,                           &
       'Please, initialize this module before calling this subroutine.')

   geom_case: select case(trim(geom))

    case("toroidal")
     dx    => dx_tor
     toro_geometry = .true.

    case("Cartesian")
     dx_r0 = r0
     dx    => dx_cart
     cart_geometry = .true.

    case default
     call error(this_sub_name,this_mod_name,          &
                'Unknwon geometry "'//trim(geom)//'".')

   end select geom_case
  
 end subroutine  setup_magnetic_geometry

!-----------------------------------------------------------------------

 !> Volume element for the toroidal geometry: \f$dv = R\,dR\,dz\f$
 pure function dx_tor(x) result(dx)
  real(wp), intent(in) :: x(:,:)
  real(wp) :: dx(size(x,2))

   dx = x(1,:)
 end function dx_tor

!-----------------------------------------------------------------------

 !> Volume element for the Cartesian geometry
 pure function dx_cart(x) result(dx)
  real(wp), intent(in) :: x(:,:)
  real(wp) :: dx(size(x,2))

   dx = dx_r0
 end function dx_cart

!-----------------------------------------------------------------------

 !> Compute \f${\bf b}\f$, \f$B\f$ and \f$\nabla{\bf b}\f$ as
 !! described in the module documentation
 !!
 !! The vector basis is reordered as described \ref
 !! vector_implementation "here" in view of the reduction of the
 !! problem to the poloidal plane.
 pure subroutine compute_toroidal_magnetic_geom(            &
    b,gradb,divb , bb,gradbb , sbb,gradsbb , curl_bfb , x , &
                                                 r0,a,bp,b0 )
  real(wp), intent(in) :: x(:,:)
  real(wp), intent(out) :: b(:,:)
  real(wp), intent(out) :: gradb(:,:,:)
  real(wp), intent(out) :: divb(:)
  real(wp), intent(out) :: bb(:,:)
  real(wp), intent(out) :: gradbb(:,:,:)
  real(wp), intent(out) :: sbb(:)
  real(wp), intent(out) :: gradsbb(:,:)
  real(wp), intent(out) :: curl_bfb(:,:)
  real(wp), intent(in) :: r0, a
  real(wp), intent(in) :: b0, bp
 
  integer :: i, j, l
  real(wp) :: r0_r(size(x,2)), fsbb(size(x,2)), bp_ab02
  character(len=*), parameter :: &
    this_sub_name = 'compute_toroidal_magnetic_geom'
 
   ! Typically, this will happen if this subroutine is called before
   ! setup_magnetic_geometry, which is not allowed.
   if(all( (/ cart_geometry , toro_geometry /) .eqv. .false. )) then
     bb = -huge(bb); gradb = -huge(bb); divb = -huge(bb)
     bb = -huge(bb); gradbb = -huge(bb); sbb = -huge(bb);
     gradsbb = -huge(bb); curl_bfb = -huge(bb)
     return
   endif

   associate( r => x(1,:) , z => x(2,:) )

   if(cart_geometry) then
     r0_r = 1.0_wp
   elseif(toro_geometry) then
     r0_r = r0/r
   endif

   ! B vector
   bb(1,:) =  r0_r * bp *    z/a
   bb(2,:) = -r0_r * bp * (r - r0)/a
   bb(3,:) =  r0_r * b0 ! changing sign when reordering the components

   ! |B|
   sbb = sqrt( sum( bb**2 , 1 ) )
   fsbb = 1.0_wp/sbb

   ! b vector
   do l=1,size(x,2)
     b(:,l) = fsbb(l)*bb(:,l)
   enddo

   ! nabla |B|
   bp_ab02 = ( bp/(a*b0) )**2
   if(cart_geometry) then
     gradsbb(1,:) =                    r - r0         * bp_ab02
   elseif(toro_geometry) then
     gradsbb(1,:) = -( 1.0_wp + (r0**2 - r0*r + z**2) * bp_ab02 )/r
   endif
   gradsbb(2,:)   =                      z            * bp_ab02
   gradsbb(3,:)   = -0.0_wp ! changing sign
   do l=1,size(x,2)
     gradsbb(:,l) = r0_r(l)*abs(b0) / (                        &
         sqrt( 1.0_wp + ( (r(l)-r0)**2 + z(l)**2 ) * bp_ab02 ) &
                           ) * gradsbb(:,l)
   enddo

   ! nabla B (careful swapping the indexes and changing the signs)
   if(cart_geometry) then
     gradbb(1,1,:) =       0.0_wp   
     gradbb(1,3,:) =   -(  0.0_wp    )  ! change sign
     gradbb(1,2,:) =       bp/a

     gradbb(3,1,:) = -(    0.0_wp     ) ! change sign
     gradbb(3,3,:) = -(-(  0.0_wp    )) ! change sign (twice)
     gradbb(3,2,:) = -(    0.0_wp     ) ! change sign

     gradbb(2,1,:) =      -bp/a
     gradbb(2,3,:) =   -(  0.0_wp    )  ! change sign
     gradbb(2,2,:) =       0.0_wp
   elseif(toro_geometry) then
     gradbb(1,1,:) =      -bp/a * z
     gradbb(1,3,:) =   -(  b0        )  ! change sign
     gradbb(1,2,:) =       bp/a * r

     gradbb(3,1,:) = -(    b0         ) ! change sign
     gradbb(3,3,:) = -(-(  bp/a * z  )) ! change sign (twice)
     gradbb(3,2,:) = -(    0.0_wp     ) ! change sign

     gradbb(2,1,:) =      -bp/a * r0 
     gradbb(2,3,:) =   -(  0.0_wp    )  ! change sign
     gradbb(2,2,:) =       0.0_wp
     do l=1,size(x,2)
       gradbb(:,:,l) = (r0/r(l)**2) * gradbb(:,:,l)
     enddo
   endif

   ! nabla b
   do j=1,3
     do i=1,3
       gradb(i,j,:) = fsbb * ( gradbb(i,j,:) - b(i,:)*gradsbb(j,:) )
     enddo
   enddo

   ! div(b) (the diagonal elements do not need sign changes)
   divb = 0.0_wp
   do i=1,3
     divb = divb + gradb(i,i,:)
   enddo

   ! warning: coeff_c is not included here!

   ! curl(b)
   curl_bfb(1,:) = -gradb(2,3,:) + gradb(3,2,:)
   curl_bfb(3,:) = -gradb(1,2,:) + gradb(2,1,:)
   curl_bfb(2,:) = -gradb(3,1,:) + gradb(1,3,:)
   ! add -1/B grad(B) \times b
   curl_bfb(1,:) = curl_bfb(1,:) &
                - fsbb * ( gradsbb(2,:)*b(3,:) - gradsbb(3,:)*b(2,:) )
   curl_bfb(2,:) = curl_bfb(2,:) &
                - fsbb * ( gradsbb(3,:)*b(1,:) - gradsbb(1,:)*b(3,:) )
   curl_bfb(3,:) = curl_bfb(3,:) &
                - fsbb * ( gradsbb(1,:)*b(2,:) - gradsbb(2,:)*b(1,:) )
   ! divide by |B|
   do i=1,3
     curl_bfb(i,:) = fsbb * curl_bfb(i,:)
   enddo

   end associate

 end subroutine compute_toroidal_magnetic_geom
 
!-----------------------------------------------------------------------

 !> Compute an axisymmetric profile with a sharp transition
 !!
 !! The profile is expressed as a function of \f$\rho =
 !! \sqrt{(R-R_0)^2 + z^2}\f$ with a perturbation possibly depending
 !! on \f$\theta=\displaystyle{\rm atan}\frac{z}{R-R_0}\f$, namely
 !! \f{displaymath}{
 !!  f(\rho,\theta) = (1+\delta(\rho,\theta))
 !!   \left( f_{\min} + \frac{f_{\max}-f_{\min}}{2}
 !!   \left( 1 - \tanh\left( 6\sigma\frac{\rho-\overline{R}}
 !!     {\Delta R} \right) \right)\right)
 !! \f}
 !! where \f$\overline{R}=\frac{1}{2}(R_{\max}+R_{\min})\f$, \f$\Delta
 !! R = R_{\max}-R_{\min}\f$, \f$\sigma\f$ is the sharpness
 !! coefficient, and
 !! \f{displaymath}{
 !!  \delta(\rho,\theta) = \delta_{\rm pert}
 !!  \frac{4(\rho-R_{\min})(R_{\max}-\rho)}{\Delta R^2} 
 !!  \left( 1 + \cos\left( m_{\rm pert} \theta \right) \right).
 !! \f}
 pure function sharp_radial_profile( x , r0,rmin,rmax,sharpness, &
                fmin,fmax , pert_number,pert_amplitude ) result(f)
  real(wp), intent(in) :: x(:)
  real(wp), intent(in) :: r0, rmin, rmax
  !> transition sharpness, dimensionless, large values -> sharp trans.
  real(wp), intent(in) :: sharpness
  real(wp), intent(in) :: fmin, fmax
  real(wp), intent(in) :: pert_number
  real(wp), intent(in) :: pert_amplitude
  real(wp) :: f

  real(wp) :: r05, rd2, rho, pert

   r05 = 0.5_wp*(rmin+rmax)
   rd2 = 0.5_wp*(rmax-rmin)
   rho = sqrt( (x(1)-r0)**2 + x(2)**2 ) ! from the magnetic axis

   ! perturbation
   pert = pert_amplitude                                   &
         ! modulation in the radial direction
         *   4.0_wp*(rho-rmin)*(rmax-rho)/(rmax-rmin)**2   &
         ! parallel perturbation
         * ( 1.0_wp+cos(pert_number * atan2(x(2),x(1)-r0)) )
        
   f = (1.0_wp + pert) * ( fmin + (fmax-fmin)                         &
    * 0.5_wp*(1.0_wp-tanh( (rho-r05)/(rd2/3.0_wp * 1.0_wp/sharpness))))
   
   ! limit the profile to correct possible round-off negative values
   f = max( f , fmin/1000.0_wp )
  
 end function sharp_radial_profile

!-----------------------------------------------------------------------

 !> Define a simple polynomial temperature profile
 pure subroutine compute_polynoml_temperat_prof(ti,gradti,te,gradte,x, &
                                               r0,rmin,rmax, tmax,tmin )
  real(wp), intent(in) :: x(:,:)
  real(wp), intent(out) :: ti(:)
  real(wp), intent(out) :: gradti(:,:)
  real(wp), intent(out) :: te(:)
  real(wp), intent(out) :: gradte(:,:)
  real(wp), intent(in) :: r0, rmin, rmax
  real(wp), intent(in) :: tmax, tmin

  integer, parameter :: n = 2 ! profile exponent

  integer :: l
  real(wp) :: rho(size(x,2)), frho(size(x,2)), xi, dt
  character(len=*), parameter :: &
    this_sub_name = 'compute_polynoml_temperat_prof'
 
   associate( r => x(1,:) , z => x(2,:) )

   dt = tmax - tmin

   rho = sqrt( (r-r0)**2 + z**2 )
   frho = 1.0_wp/rho

   do l=1,size(x,2)

     if(rho(l).lt.rmin) then     ! inside the annulus

       ti(l) = tmax
       gradti(:,l) = 0.0_wp

     elseif(rho(l).gt.rmax) then ! outside the annulus

       ti(l) = tmin
       gradti(:,l) = 0.0_wp

     else                        ! annulus

       ! normalized radial coord.
       xi = (rho(l)-rmin)/(rmax-rmin)

       ! standard polynomial temperature profile
       ti(l) = (1.0_wp-xi**n) * dt + tmin

       gradti(1,l) = r(l)-r0
       gradti(2,l) =  z(l)
       gradti(:,l) = - dt * (real(n,wp)/(rmax-rmin))     &
                    * frho(l) * xi**(n-1) * gradti(:,l)

     endif

   enddo

   ! assume thermalized state
   te = ti
   gradte = gradti

   end associate

 end subroutine compute_polynoml_temperat_prof
 
!-----------------------------------------------------------------------

 !> Compute a simplified, local magnetic field.
 !!
 !! We describe here a local geometry, according to the setup depicted
 !! in the following figure.
 !! 
 !! \image html local-domain.png
 !! \image latex local-domain.png "" width=0.3\textwidth
 !!
 !! The magnetic field is
 !! \f{displaymath}{
 !!  {\bf B} = -\frac{R_0}{R}B_0{\bf e}_{\varphi},
 !! \f}
 !! so that
 !! \f{displaymath}{
 !!  B = \frac{R_0}{R}|B_0|, \qquad {\bf b} = -{\rm sign}(B_0)\,{\bf
 !!  e}_{\varphi}.
 !! \f}
 !! Notice that this definition ensures \f$\nabla\cdot{\bf B}=0\f$
 !! both in toroidal and Cartesian geometry.
 !!
 !! Assuming toroidal geometry, we then have
 !! \f{displaymath}{
 !!  \nabla B = -\frac{R_0|B_0|}{R^2}{\bf e}_R, \qquad 
 !!  \nabla {\bf B} = \frac{R_0}{R^2}B_0
 !!  \left[
 !!  \begin{array}{ccc}
 !!   0 & 1 & 0 \\
 !!   1 & 0 & 0 \\
 !!   0 & 0 & 0
 !!  \end{array}
 !!  \right],
 !! \f}
 !! from which all the required quantities can be computed as in
 !! \fref{mod_magnetic_geometry,compute_toroidal_magnetic_geom}.
 !!
 !! The situation is similar in Cartesian geometry, for which we have
 !! \f{displaymath}{
 !!  \nabla B = -\frac{R_0|B_0|}{R^2}{\bf e}_R, \qquad 
 !!  \nabla {\bf B} = \frac{R_0}{R^2}B_0
 !!  \left[
 !!  \begin{array}{ccc}
 !!   0 & 0 & 0 \\
 !!   1 & 0 & 0 \\
 !!   0 & 0 & 0
 !!  \end{array}
 !!  \right].
 !! \f}
 pure subroutine compute_local_magnetic_geom(               &
    b,gradb,divb , bb,gradbb , sbb,gradsbb , curl_bfb , x , &
                                                      r0,b0 )
  real(wp), intent(in) :: x(:,:)
  real(wp), intent(out) :: b(:,:)
  real(wp), intent(out) :: gradb(:,:,:)
  real(wp), intent(out) :: divb(:)
  real(wp), intent(out) :: bb(:,:)
  real(wp), intent(out) :: gradbb(:,:,:)
  real(wp), intent(out) :: sbb(:)
  real(wp), intent(out) :: gradsbb(:,:)
  real(wp), intent(out) :: curl_bfb(:,:)
  real(wp), intent(in) :: r0
  real(wp), intent(in) :: b0
 
  integer :: i, j, l
  real(wp) :: fsbb(size(x,2))
  character(len=*), parameter :: &
    this_sub_name = 'compute_local_magnetic_geom'

   ! Typically, this will happen if this subroutine is called before
   ! setup_magnetic_geometry, which is not allowed.
   if(all( (/ cart_geometry , toro_geometry /) .eqv. .false. )) then
     bb = -huge(bb); gradb = -huge(bb); divb = -huge(bb)
     bb = -huge(bb); gradbb = -huge(bb); sbb = -huge(bb);
     gradsbb = -huge(bb); curl_bfb = -huge(bb)
     return
   endif

   associate( r => x(1,:) )

   ! B vector
   bb(1,:) = 0.0_wp
   bb(2,:) = 0.0_wp
   bb(3,:) = r0/r * b0 ! changing sign when reordering the components

   ! |B|
   sbb = sqrt( sum( bb**2 , 1 ) )
   fsbb = 1.0_wp/sbb

   ! b vector
   do l=1,size(x,2)
     b(:,l) = fsbb(l)*bb(:,l)
   enddo

   ! nabla |B|
   gradsbb(1,:) = -(r0*abs(b0))/(r**2)
   gradsbb(2,:) =  0.0_wp
   gradsbb(3,:) = -0.0_wp ! changing sign

   ! nabla B (careful swapping the indexes and changing the signs)
   gradbb(1,1,:) =       0.0_wp
   if(cart_geometry) then
     gradbb(1,3,:) = -(  0.0_wp )  ! change sign
   elseif(toro_geometry) then
     gradbb(1,3,:) = -(  1.0_wp )  ! change sign
   endif
   gradbb(1,2,:) =       0.0_wp

   gradbb(3,1,:) = -(    1.0_wp  ) ! change sign
   gradbb(3,3,:) = -(-(  0.0_wp )) ! change sign (twice)
   gradbb(3,2,:) = -(    0.0_wp  ) ! change sign

   gradbb(2,1,:) =       0.0_wp
   gradbb(2,3,:) =   -(  0.0_wp )  ! change sign
   gradbb(2,2,:) =       0.0_wp
   do l=1,size(x,2)
     gradbb(:,:,l) = ((r0*b0)/r(l)**2) * gradbb(:,:,l)
   enddo

   ! nabla b
   do j=1,3
     do i=1,3
       gradb(i,j,:) = fsbb * ( gradbb(i,j,:) - b(i,:)*gradsbb(j,:) )
     enddo
   enddo

   ! div(b) (the diagonal elements do not need sign changes)
   divb = 0.0_wp
   do i=1,3
     divb = divb + gradb(i,i,:)
   enddo

   ! warning: coeff_c is not included here!

   ! curl(b)
   curl_bfb(1,:) = -gradb(2,3,:) + gradb(3,2,:)
   curl_bfb(3,:) = -gradb(1,2,:) + gradb(2,1,:)
   curl_bfb(2,:) = -gradb(3,1,:) + gradb(1,3,:)
   ! add -1/B grad(B) \times b
   curl_bfb(1,:) = curl_bfb(1,:) &
                - fsbb * ( gradsbb(2,:)*b(3,:) - gradsbb(3,:)*b(2,:) )
   curl_bfb(2,:) = curl_bfb(2,:) &
                - fsbb * ( gradsbb(3,:)*b(1,:) - gradsbb(1,:)*b(3,:) )
   curl_bfb(3,:) = curl_bfb(3,:) &
                - fsbb * ( gradsbb(1,:)*b(2,:) - gradsbb(2,:)*b(1,:) )
   ! divide by |B|
   do i=1,3
     curl_bfb(i,:) = fsbb * curl_bfb(i,:)
   enddo

   end associate

 end subroutine compute_local_magnetic_geom
 
!-----------------------------------------------------------------------

end module mod_magnetic_geometry

