!>\brief
!!
!! Building block for the SOL equations: the
!! advection-diffusion-reaction problem.
!!
!! \n
!!
!! This module provides the space discretization of a generic
!! advection-diffusion-reaction equation, which is one of the building
!! block for the approximation of the Braginskii equations.
!!
!! We consider here a scalar equation, where the unknown is <tt>
!! t_sol_state\%ni(i_sp)</tt> and the advection field is <tt>
!! t_sol_state\%vi(i_sp)</tt>, where \c i_sp is a module parameter set
!! to 1.
!!
!! The equations are discretized on a 2D grid of triangles and
!! quadrilaterals using \f$\mathbb{P}_1\f$ and \f$\mathbb{Q}_1\f$
!! local conformal finite element spaces, using the SUPG method to
!! stabilize advection.
!!
!! For the definition fo the SUPG method, we refer to <a
!! href="http://dx.doi.org/10.1016/j.cma.2004.01.026">[Bochev,
!! Gunzburger, Shadid, CMAME 2004]</a>. To clarify the procedure, let
!! us consider the following equation for \f$n^{\rm i}\f$, where we
!! also introduce an additional diffusion
!! \f$\underline{\underline{\varepsilon}}\f$ given by a semidefinite
!! tensor. The continuous problem is thus
!! \f{displaymath}{
!!  \partial_t n^{\rm i} 
!!  + \nabla\cdot\left( {\bf v}^{\rm i} n^{\rm i}\right)
!!  -\nabla\cdot\left( \underline{\underline{\varepsilon}}
!!                     \nabla n^{\rm i} \right)
!!  + \mu n^{\rm i} = S_{n^{\rm i}}.
!! \f}
!! The corresponding finite element discretization, including the SUPG
!! stabilization, can be obtained in two equivalent ways:
!! <ol>
!!  <li> build the usual Gelrkin weak form of the problem and add to
!!  the right-hand-side the
!!  (strongly consistent) stabilization term 
!!  \f{displaymath}{
!!   \sum_K \int_K \underbrace{\left[ S_{n^{\rm i}} -
!!    \partial_t n^{\rm i} 
!!  - \nabla\cdot\left( {\bf v}^{\rm i} n^{\rm i}\right)
!!  +\nabla\cdot\left( \underline{\underline{\varepsilon}}
!!                     \nabla n^{\rm i}\right)
!!  - \mu n^{\rm i} \right]}_{R(n^{\rm i})}
!!     W(\phi)\,dx,
!!  \f}
!!  where \f$W(\phi)\f$ is the weighting operator, typically defined
!!  as \f$\tau{\bf v}^{\rm i}\cdot\nabla\phi\f$
!!  <li> build the weak form of the problem using the test function
!!  \f$\phi+W(\phi)\f$ (at least formally; in practice one must be
!!  careful concerning integration by parts and terms which must be
!!  computed as sum of local integrals).
!! </ol>
!! The result is the following weak problem:
!! \f{displaymath}{
!!  \begin{array}{r}
!!   \displaystyle
!!   \int_\Omega \partial_t n^{\rm i} \phi\,dx
!!   - \int_\Omega \left( {\bf v}^{\rm i} n^{\rm i} 
!!          -\underline{\underline{\varepsilon}}\nabla n^{\rm i}
!!          \right) \cdot \nabla \phi\,dx
!!   + \int_{\partial\Omega} \left( {\bf v}^{\rm i} n^{\rm i}
!!          -\underline{\underline{\varepsilon}}\nabla n^{\rm i}
!!          \right) \cdot {\bf n}_{\partial\Omega}\phi\,d\sigma
!!   + \int_{\Omega} \mu n^{\rm i}\phi\,dx
!!     \\[4mm] \displaystyle
!!   + \sum_K\int_K \tau \left[ \partial_t n^{\rm i} 
!!     + {\bf v}^{\rm i}\cdot\nabla n^{\rm i}
!!     + \left( \mu + \nabla\cdot{\bf v}^{\rm i} \right) n^{\rm i}
!!     - \underline{\underline{\varepsilon}} : D^2n^{\rm i}
!!   \right]{\bf v}^{\rm i} \cdot\nabla\phi\,dx
!!      = \int_{\Omega} S_{n^{\rm i}} \left(\phi+W(\phi)\right)\,dx,
!!  \end{array}
!! \f}
!! where \f$\left[D^2n^{\rm i}\right]_{ij} = \partial_{x_ix_j}n^{\rm
!! i}\f$. Here, we assume that
!! \f$\underline{\underline{\varepsilon}}\f$ and \f$\mu\f$ are
!! constant on each element and \f${\bf v}^{\rm i}\f$ is piecewise
!! linear and continuous; one can verify that this is consistent with
!! the order \f$k=1\f$ of the finite element discretization.  The main
!! characteristics of the SUPG method now are:
!! <ul>
!!  <li> the stabilization vanishes for the exact solution, i.e. the
!!  method is strongly consistent
!!  <li> the term
!!   \f{displaymath}{
!!    \int_K \tau ({\bf v}^{\rm i}\cdot\nabla n^{\rm i}) ({\bf v}^{\rm
!!    i}\cdot\nabla\phi) \, dx = \int_K \tau {\bf v}^{\rm i}\otimes
!!    {\bf v}^{\rm i} \nabla n^{\rm i} \cdot \nabla\phi \, dx
!!   \f}
!!   provides an additional streamline diffusion
!! </ul>
!! We now make the following observations:
!! <ul>
!!  <li> the two advection and diffusion terms in the weak form of the
!!  original problem are integrated by parts, as usual when working
!!  with the <em>conservative</em> form of the equation
!!  <li> in the residual, where no integration by parts is performed,
!!  \f$\nabla\cdot({\bf v}^{\rm i}n^{\rm i})\f$ is expanded and yields
!!  two contributions: the streamline additional diffusion and a
!!  consistency term which is included in the zero-order term
!!  <li> the assumption that \f${\bf v}^{\rm i}\f$ is piecewise linear
!!  and globally continuous ensures that the term proportional to
!!  \f$\nabla\cdot{\bf v}^{\rm i}\f$ is treated consistently
!!  <li> since no integration by parts is performed in the
!!  stabilization terms, such a term does not affect the for of the
!!  boundary conditions (see \ref sol_bcs "later")
!!  <li> the diffusion coefficient is assumed to be piecewise
!!  constant, which significantly simplifies the form of the
!!  corresponding stabilization term without affecting the convergence
!!  of the scheme
!!  <li> in addition, observe that \f$D^2\phi\f$ is zero for
!!  triangular elements; in the general case it is 
!!  \f{displaymath}{
!!   \left[D^2\phi\right]_{ij} = \frac{\partial\xi_k}{\partial x_i}
!!   \frac{\partial\xi_h}{\partial x_j}
!!   \frac{\partial^2\phi}{\partial\xi_k\partial\xi_h}
!!   + \frac{\partial^2\xi_k}{\partial x_i\partial x_j}\frac{\partial
!!   n}{\partial\xi_k}
!!  \f}
!!  and it might be possible that it can be neglected for linear
!!  quadrilateral elements without major impact on the results (see
!!  also Remark 3, page 237 of 
!!  <a href="http://dx.doi.org/10.1016/0045-7825(82)90071-8">[Brooks,
!!  Hughes, CMAME 1982]</a>).
!! </ul>
!! Rearranging the terms and not considering for the moment the
!! boundary terms, the resulting formulation is
!! \f{displaymath}{
!!  \begin{array}{r}
!!   \displaystyle
!!   \int_\Omega \partial_tn^{\rm i} \left(\phi+W(\phi)\right)\,dx
!!   - \sum_K \int_K \left(1-\tau(\mu+\nabla\cdot {\bf v}^{\rm i})
!!   \right){\bf v}^{\rm i} n^{\rm i}\cdot\nabla \phi\,dx
!!   + \int_\Omega \left(\underline{\underline{\varepsilon}} +
!!      \tau {\bf v^{\rm i}}\otimes{\bf v^{\rm i}} \right)
!!     \nabla n^{\rm i}\cdot\nabla\phi\,dx 
!!     \\[4mm] \displaystyle
!!   + \int_{\Omega}\mu n^{\rm i}\phi\,dx
!!   + \int_{\partial \Omega}\left({\bf v^{\rm i}}n^{\rm i} -
!!     \underline{\underline{\varepsilon}} \nabla n^{\rm i}\right)
!!     \cdot {\bf n}_{\partial\Omega}\phi\,d\sigma
!!     = \int_{\Omega} S_{n^{\rm i}} \left(\phi+W(\phi)\right)\,dx.
!!  \end{array}
!! \f}
!! This shows that the SUPG method can be recast in the same form as
!! the standard finite element method with the following modified
!! terms:
!! <ul>
!!  <li> the mass matrix is \f$M_{ij} = \int_\Omega \phi_j\left(
!!  \phi_i + \tau{\bf v}^{\rm i}\cdot\nabla\phi_i\right)dx\f$ (notice
!!  that such a matrix is not symmetric anymore, as is also discussed
!!  in <a href="http://dx.doi.org/10.1016/j.cma.2004.01.026">[Bochev,
!!  Gunzburger, Shadid, CMAME 2004]</a>)
!!  <li> in the advection term, the modified velocity
!!  \f$\left(1-\tau(\mu+\nabla\cdot {\bf v}^{\rm i}) \right){\bf
!!  v}^{\rm i}\f$ is used; notice that this is the only place where we
!!  have to account explicitly for \f$\nabla\cdot{\bf v}^{\rm i} \neq
!!  0\f$
!!  <li> the viscosity coefficient includes the numerical viscosity
!!  <li> no changes are required for the boundary term: this also
!!  means that the stabilization does not affect the usual global
!!  conservation equation.
!! </ul>
!!
!! \section sol_bcs Boundary conditions
!!
!! The standard way to enforce boundary conditions would be in an
!! essential way for Dirichlet conditions and natural form for the
!! Neumann ones. However, while natural boundary conditions do not
!! pose any significant problem, essential ones require partitioning
!! the unknowns, and assuming that these conditions are different for
!! inflow and outflow boundaries, and since the velocity field is not
!! known a priori and can be different for different species, this
!! would result in significant complications.
!!
!! An alternative solution is enforcing Dirichlet boundary conditions
!! via a penalization term, following <a
!! href="http://dx.doi.org/10.1007/BF02995904">[Nitsche, Abh. Math.
!! Semin. Univ.  Hamb. 1971]</a>; as shown in <a
!! href="http://dx.doi.org/10.1007/s10915-011-9514-2">[Bernardi,
!! Chac&oacute;n Rebollo, Restelli, J. Sci. Comput. 2012]</a>, this
!! does not require knowing a priori where each condition will be
!! enforced, and both Dirichlet and Neumann conditions can be treated
!! in the same way.
!!
!! To clarify this, let us introduce the partition of the boundary
!! \f{displaymath}{
!!  \partial \Omega = \Gamma_D \cup \Gamma_N
!! \f}
!! and assume that the following quantities are prescribed:
!! \f{displaymath}{
!!  n^{\rm i} = n^{\rm i,b}\,\,{\rm on}\,\, \Gamma_D, \qquad
!!  - \underline{\underline{\varepsilon}} \nabla n^{\rm i}
!!     \cdot {\bf n}_{\partial\Omega} + v^{\rm i,-}_{\partial\Omega}
!!     n^{\rm i} = f^{n^{\rm i},{\rm b}}\,\,{\rm on}\,\, \Gamma_N,
!! \f}
!! with \f$v^{\rm i,-}_{\partial\Omega} = \frac{1}{2}\left( v^{\rm
!! i}_{\partial\Omega} - |v^{\rm i}_{\partial\Omega}| \right) \f$ and
!! \f$v^{\rm i}_{\partial\Omega} = {\bf v}^{\rm i}\cdot{\bf
!! n}_{\partial\Omega}\f$.
!!
!! The Nitsche penalization results in the discrete problem
!! \f{displaymath}{
!!  \begin{array}{r}
!!   \displaystyle
!!   \int_\Omega \partial_tn^{\rm i} \left(\phi+W(\phi)\right)\,dx -
!!   \sum_K \int_K \left(1-\tau(\mu+\nabla\cdot {\bf v}^{\rm i})
!!   \right){\bf v}^{\rm i} n^{\rm i}\cdot\nabla \phi\,dx
!!   + \int_\Omega \left(\underline{\underline{\varepsilon}} +
!!      \tau {\bf v^{\rm i}}\otimes{\bf v^{\rm i}} \right)
!!     \nabla n^{\rm i}\cdot\nabla\phi\,dx 
!!     \\[4mm] \displaystyle
!!   + \int_\Omega \mu n^{\rm i}\phi\,dx
!!   + \int_{\Gamma_D}\left({\bf v^{\rm i}}n^{\rm i}
!!     - \underline{\underline{\varepsilon}} \nabla n^{\rm i}\right)
!!     \cdot {\bf n}_{\partial\Omega}\phi\,d\sigma
!!   - \int_{\Gamma_D} \underline{\underline{\varepsilon}} \nabla \phi
!!     \cdot {\bf n}_{\partial\Omega}\left(n^{\rm i}-n^{\rm
!!     i,b}\right)d\sigma
!!   + \int_{\Gamma_D} \xi \left(n^{\rm i}-n^{\rm
!!     i,b}\right)\phi\,d\sigma
!!     \\[4mm] \displaystyle
!!   + \int_{\Gamma_N} f^{n^{\rm i},{\rm b}}\phi\,d\sigma
!!   + \int_{\Gamma_N}v^{\rm i,+}_{\partial\Omega}n^{\rm i}\phi\,d\sigma
!!     = \int_{\Omega} S_{n^{\rm i}} \left(\phi+W(\phi)\right)\,dx.
!!  \end{array}
!! \f}
!! In the above expression, symmetry of the diffusion contribution is
!! ensured introducing a term which vanishes for the exact solution,
!! \f$\xi=C h^{-1}\f$ is the penalization term to enforce the
!! Dirichlet condition, and
!! \f{displaymath}{
!!  v^{\rm i,+}_{\partial\Omega} = {\bf v}^{\rm i}\cdot{\bf
!!   n}_{\partial\Omega} - v^{\rm i,-}_{\partial\Omega} =
!!  \frac{1}{2}\left( v^{\rm i}_{\partial\Omega} + |v^{\rm
!!  i}_{\partial\Omega}| \right) .
!! \f}
!!
!! \section scalar_adr_noncons Nonconservative formulation
!!
!! Besides the discretized problem described above, one could consider
!! an alternative formulation in nonconservative form. The main reason
!! for doing this is probably the requirement to exactly preserve
!! constants fields.
!!
!! In order to obtain the nonconservative formulation, it suffices to
!! observe that, provided \f${\bf v}^{\rm i}\f$ has the required
!! regularity,
!! \f{displaymath}{
!!  \begin{array}{l}
!!   \displaystyle
!!  -\sum_K \int_K {\bf v}^{\rm i}n^{\rm i}\cdot\nabla\phi\,dx =
!!  - \int_\Omega {\bf v}^{\rm i}n^{\rm i}\cdot\nabla\phi\,dx \\
!!  \displaystyle
!!  = \int_\Omega {\bf v}^{\rm i}\cdot\nabla n^{\rm i}\,\phi\,dx 
!!  + \int_\Omega \nabla\cdot{\bf v}^{\rm i} \, n^{\rm i}\phi\,dx 
!!  - \int_{\partial \Omega} {\bf v}^{\rm i} n^{\rm i} \cdot {\bf
!!  n}_{\partial\Omega}\phi\,d\sigma.
!!  \end{array}
!! \f}
!! After some simple manipulations, the two formulations can be
!! written using the following unified form
!! \f{displaymath}{
!!  \begin{array}{r}
!!   \displaystyle
!!   \int_\Omega \partial_tn^{\rm i} \left(\phi+W(\phi)\right)\,dx -
!!   \sum_K \int_K \left(\kappa-\tau(\mu+\nabla\cdot {\bf v}^{\rm i})
!!   \right){\bf v}^{\rm i} n^{\rm i}\cdot\nabla \phi\,dx
!!   + \int_\Omega \left(\left(\underline{\underline{\varepsilon}} +
!!      \tau {\bf v^{\rm i}}\otimes{\bf v^{\rm i}} \right)^T
!!     \nabla \phi + (1-\kappa){\bf v}^{\rm i}\phi \right) \cdot\nabla
!!     n^{\rm i}\,dx \\[4mm] \displaystyle
!!   + \int_\Omega \left(\mu+(1-\kappa)\nabla\cdot{\bf v}^{\rm
!!     i}\right) n^{\rm i}\phi\,dx
!!   + \int_{\Gamma_D}\left(\kappa{\bf v^{\rm i}}n^{\rm i}
!!     - \underline{\underline{\varepsilon}} \nabla n^{\rm i}\right)
!!     \cdot {\bf n}_{\partial\Omega}\phi\,d\sigma
!!   - \int_{\Gamma_D} \underline{\underline{\varepsilon}} \nabla \phi
!!     \cdot {\bf n}_{\partial\Omega}\left(n^{\rm i}-n^{\rm
!!     i,b}\right)d\sigma
!!   + \int_{\Gamma_D} \xi \left(n^{\rm i}-n^{\rm
!!     i,b}\right)\phi\,d\sigma
!!     \\[4mm] \displaystyle
!!   + \int_{\Gamma_N} f^{n^{\rm i},{\rm b}}\phi\,d\sigma
!!   + \int_{\Gamma_N}\left( v^{\rm i,+}_{\partial\Omega}
!!   -(1-\kappa){\bf v}^{\rm i}\cdot{\bf n}_{\partial\Omega} \right)
!!   n^{\rm i}\phi\,d\sigma
!!     = \int_{\Omega} S_{n^{\rm i}} \left(\phi+W(\phi)\right)\,dx.
!!  \end{array}
!! \f}
!! where
!! <ul>
!!  <li> \f$\kappa=1\f$: conservative formulation
!!  <li> \f$\kappa=0\f$: nonconservative formulation.
!! </ul>
!!
!! \section mpi_parallelization MPI parallelization
!!
!! The code can be parallelized in two (logical) directions: the
!! computational domain can be partitioned, and different linear
!! problems can be solved by different processes, for instance for
!! different ion species.
!!
!! \subsection scalar_adr_ode_ddc Domain decomposition
!!
!! The grid partitioning is visible here in two ways:
!! <ul>
!!  <li> whenever a linear system is treated, the linear solver must
!!  know the proper local-to-global map of the degree of freedom; this
!!  is taken care of in \c mod_sol_linsys, during the definition of
!!  the \fref{mod_linsolver_base,c_linpb} objects
!!  <li> the boundary conditions must take into account the
!!  neighbouring elements; this is done in \c mod_sol_locmat, where
!!  indeed it turns out that there is not much to do.
!! </ul>
!! Apart from these two issues, there are not many other places where
!! the domain decomposition should show up.
!!
!! \note Once the local-to-global map has been provided to the linear
!! solver, the local (in the sense of this subdomain) matrices can be
!! dealt with without worrying about the grid partitioning.
!!
!! \subsection linsys_parallel Parallelization of the linear systems
!!
!! This is only relevant for the complete Braginskii equation.
!<----------------------------------------------------------------------
module mod_scalar_adr_ode

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_fu_manager, only: &
   mod_fu_manager_initialized, &
   new_file_unit

 use mod_state_vars, only: &
   mod_state_vars_initialized, &
   c_stv

 use mod_time_integrators, only: &
   mod_time_integrators_initialized, &
   c_ode, c_ods

 use mod_sparse, only: &
   mod_sparse_initialized, &
   ! sparse types
   t_intar,     &
   t_col,       &
   t_tri,       &
   t_pm_sk,     &
   ! construction of new objects
   new_col,     &
   new_tri,     &
   ! convertions
   col2tri,     &
   tri2col,     &
   tri2col_skeleton, &
   tri2col_skeleton_part, &
   ! overloaded operators
   operator(+), &
   operator(*), &
   sum,         &
   transpose,   &
   matmul,      &
   ! error codes
   wrong_n,     &
   wrong_m,     &
   wrong_nz,    &
   wrong_dim,   &
   ! other functions
   nnz_col,     &
   nz_col,      &
   nz_col_i,    &
   get,         &
   set,         &
   diag,        &
   spdiag,      &
   ! deallocate
   clear

 use mod_octave_io_sparse, only: &
   mod_octave_io_sparse_initialized, &
   write_octave

 use mod_output_control, only: &
   mod_output_control_initialized, &
   elapsed_format, &
   base_name

 use mod_linsolver, only: &
   mod_linsolver_initialized, &
   c_linpb, c_itpb, c_mumpspb, c_umfpackpb, &
   gmres

 use mod_h2d_master_el, only: &
   mod_h2d_master_el_initialized, &
   c_h2d_me_geom, c_h2d_me_base

 use mod_h2d_grid, only: &
   mod_h2d_grid_initialized, &
   t_h2d_grid, t_ddc_h2d_grid, &
   t_2dv, t_2ds, t_2de

 use mod_h2d_bcs, only: &
   mod_h2d_bcs_initialized, &
   t_h2d_bcs, &
   t_b_2dv, t_b_2ds, t_b_2de

 use mod_h2d_base, only: &
   mod_h2d_base_initialized, &
   t_h2d_base

 use mod_sol_state, only: & 
   mod_sol_state_initialized, &
   t_sol_state

 use mod_sol_locmat, only: &
   mod_sol_locmat_initialized, &
   t_loc_mat, c_s_coeff, c_v_coeff, c_t_coeff, &
   loc_mass_mat, loc_supg_mat

 use mod_sol_linsys, only: &
   mod_sol_linsys_constructor, &
   mod_sol_linsys_destructor,  &
   mmmt, const_mmmt, aaat, rhs, &
   sys_mat_t, sys_rhs, linpb, &
   out_file_sys_suff, out_file_sys_name

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_scalar_adr_ode_constructor, &
   mod_scalar_adr_ode_destructor,  &
   mod_scalar_adr_ode_initialized, &
   t_scalar_adr_ode, new_scalar_adr_ode, clear, &
   ! these can be useful to write the system matrix
   mmmt, const_mmmt, aaat, &
   out_file_sys_suff, out_file_sys_name

 private

!-----------------------------------------------------------------------

! Module types and parameters

 !> SOL ODE
 !!
 !! Here we provide the methods for both explicit and implicit time
 !! discretizations.
 type, extends(c_ode) :: t_scalar_adr_ode
  type(t_h2d_grid), pointer :: grid => null()
  type(t_h2d_base), pointer :: base => null()
  type(t_h2d_bcs),  pointer :: bcs  => null()
 contains
  procedure, pass(ode) :: rhs   => scalar_adr_rhs
  procedure, pass(ode) :: solve => scalar_adr_solve
 end type t_scalar_adr_ode
 
 interface clear
   module procedure clear_scalar_adr_ode
 end interface

! Module variables

 ! public members
 logical, protected :: &
   mod_scalar_adr_ode_initialized = .false.

 ! private members

 integer, parameter :: i_sp = 1 !< species index

 !> Volume element: standard 2D Cartesian geometry
 type, extends(c_s_coeff) :: t_dx
 contains
  procedure, pass(coeff) :: evg  => dx_evg
  procedure, pass(coeff) :: evgs => dx_evgs
 end type t_dx

 !> Velocity coefficient
 !!
 !! Here we assume that the velocity belongs to the finite element
 !! space (either \f$\left(\mathbb{P}^1(K)\right)^2\f$ or
 !! \f$\left(\mathbb{Q}^1(K)\right)^2\f$) and compute all the
 !! quantities from the nodal values.
 type, extends(c_v_coeff) :: t_v_coeff
  real(wp), allocatable :: v(:,:) !< nodal velocities
 contains
  procedure, pass(coeff) :: evg  => v_coeff_evg
  procedure, pass(coeff) :: divg => v_coeff_divg
  procedure, pass(coeff) :: vng  => v_coeff_vng
 end type t_v_coeff

 !> Diffusion coefficient
 type, extends(c_t_coeff) :: t_eps_coeff
 contains
  procedure, pass(coeff) :: evg  => eps_coeff_evg
  procedure, pass(coeff) :: evgs => eps_coeff_evgs
 end type t_eps_coeff

 !> \f$\mu\f$ coefficient
 type, extends(c_s_coeff) :: t_mu
 contains
  procedure, pass(coeff) :: evg  => mu_evg
  ! no need to evaluate this coefficient on the boundary
 end type t_mu

 !> Some working arrays to compute the local matrices
 type(t_loc_mat), allocatable :: mm(:), aa(:), bb(:)

 !> Problem coefficients
 type(t_dx),                   save :: dx_coeff
 type(t_v_coeff), allocatable, save :: v_coeff(:)
 type(t_eps_coeff),            save :: eps_coeff
 type(t_mu),                   save :: mu_coeff

 character(len=*), parameter :: &
   this_mod_name = 'mod_scalar_adr_ode'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_scalar_adr_ode_constructor(grid,ddc_grid,base,write_sys)
  type(t_h2d_grid), intent(in) :: grid
  type(t_ddc_h2d_grid), intent(in) :: ddc_grid
  type(t_h2d_base), intent(in) :: base
  logical, intent(in) :: write_sys

  integer :: i, j, ie, pos, fu, ierr
  integer, allocatable :: mmmi(:), mmmj(:)
  real(wp), allocatable :: mmmx(:)
  type(t_intar) :: idij(1)

  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_kinds_initialized.eqv..false.) .or. &
    (mod_messages_initialized.eqv..false.) .or. &
  (mod_fu_manager_initialized.eqv..false.) .or. &
  (mod_state_vars_initialized.eqv..false.) .or. &
(mod_time_integrators_initialized.eqv..false.) .or. &
      (mod_sparse_initialized.eqv..false.) .or. &
(mod_octave_io_sparse_initialized.eqv..false.) .or. &
(mod_output_control_initialized.eqv..false.) .or. &
   (mod_linsolver_initialized.eqv..false.) .or. &
(mod_h2d_master_el_initialized.eqv..false.) .or. &
    (mod_h2d_grid_initialized.eqv..false.) .or. &
    (mod_h2d_base_initialized.eqv..false.) .or. &
     (mod_h2d_bcs_initialized.eqv..false.) .or. &
   (mod_sol_state_initialized.eqv..false.) ) then  
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_scalar_adr_ode_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   ! 0) Some allocations
   allocate( mm(base%lb:base%ub), bb(base%lb:base%ub) )
   allocate( v_coeff(base%lb:base%ub) )
   do ie=base%lb,base%ub
     ! local matrices have the same dimension as the local dofs
     mm(ie)%d = base%e(ie)%me%pk
     allocate( mm(ie)%m( mm(ie)%d , mm(ie)%d ) )
     ! local vectors
     bb(ie)%d = base%e(ie)%me%pk
     allocate( bb(ie)%v( bb(ie)%d ) )
     ! problem coefficients
     allocate( v_coeff(ie)%v(2,base%e(ie)%me%pk) )
   enddo
   aa = mm ! using allocation on assignment

   ie = sum( grid%e%poly**2 ) ! used as temporary, vertex unknowns
   allocate( mmmi(ie) , mmmj(ie) , mmmx(ie) )

   ! 1) Build the mass matrix and compute the (not yet assembled)
   ! coefficients of the constant mass matrix

   ! 1.1) compute the matrix pattern
   pos = 0 ! access index in the matrix skeleton
   do ie=1,grid%ne
     associate(    e => grid%e(ie) ,      &
                poly => grid%e(ie)%poly   )
     associate(  bme => base%e(poly)%me , &
                lm_s => mm(poly)%d ,      &
                 lmm => mm(poly)%m        )

     call loc_mass_mat( lmm , e , bme , dx_coeff )

     do j=1,lm_s
       do i=1,lm_s
         pos = pos+1
         ! dofs are numbered directly after the vertexes
         mmmi(pos) = e%iv(i)
         mmmj(pos) = e%iv(j)
         mmmx(pos) = lmm(i,j)
       enddo
     enddo

     end associate
     end associate
   enddo

   ! 1.2) define the skeleton
   allocate(mmmt(1))
   associate( mt => mmmt(1) )
   allocate(idij(1)%i(grid%nv)); idij(1)%i = (/(i,i=1,grid%nv)/)
   ! notice transposition; also, switch to zero based indexing
   mmmi = mmmi-1; mmmj = mmmj-1; idij(1)%i = idij(1)%i-1
   call tri2col_skeleton_part(mt,idij,idij,         &
          new_tri(grid%nv,grid%nv,mmmj,mmmi,0.0_wp) )
   ! deallocations will be done later

   ! 1.3) assemble the constant mass matrix
   mt%m(1,1)%ax = 0.0_wp
   do pos=1,mt%n_in
     mt%t2c(pos)%p = mt%t2c(pos)%p + mmmx(pos)
   enddo
   deallocate( mmmx )
   allocate( const_mmmt(size(mt%m(1,1)%ax)) )
   const_mmmt = mt%m(1,1)%ax
   allocate( rhs(grid%nv,1) )
   end associate

   ! 2) Build the matrix A of the differential operator

   ! 2.1) this matrix has the same structure as the mass matrix;
   ! however, we can not simply copy such matrix because all the
   ! pointers would not be set correctly. Thus, we build another
   ! skeleton identical to the first one.
   allocate( aaat(1) )
   call tri2col_skeleton_part(aaat(1),idij,idij,    &
          new_tri(grid%nv,grid%nv,mmmj,mmmi,0.0_wp) )
   deallocate( idij(1)%i , mmmi , mmmj )

   ! 3) Build the linear system
   call mod_sol_linsys_constructor(grid,ddc_grid)

   ! 4) Write the octave output
   if(write_sys) then
     out_file_sys_name = trim(base_name)//out_file_sys_suff
     call new_file_unit(fu,ierr)
     open(fu,file=trim(out_file_sys_name), &
          status='replace',action='write',form='formatted')
     call write_octave(transpose(mmmt(1)%m(1,1)),'const_mmm',fu)
     close(fu)
   endif

   mod_scalar_adr_ode_initialized = .true.
 end subroutine mod_scalar_adr_ode_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_scalar_adr_ode_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_scalar_adr_ode_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   call mod_sol_linsys_destructor()

   deallocate(v_coeff)
   deallocate(mm,aa,bb)
   deallocate(const_mmmt)
   deallocate(rhs)
   call clear(aaat(1)); deallocate(aaat)
   call clear(mmmt(1)); deallocate(mmmt)

   mod_scalar_adr_ode_initialized = .false.
 end subroutine mod_scalar_adr_ode_destructor

!-----------------------------------------------------------------------

 subroutine new_scalar_adr_ode(obj,grid,base,bcs)
  type(t_h2d_grid), intent(in), target :: grid
  type(t_h2d_base), intent(in), target :: base
  type(t_h2d_bcs),  intent(in), target :: bcs
  type(t_scalar_adr_ode),  intent(out) :: obj

   obj%grid => grid
   obj%base => base
   obj%bcs  => bcs

 end subroutine new_scalar_adr_ode
 
!-----------------------------------------------------------------------

 pure subroutine clear_scalar_adr_ode(obj)
  type(t_scalar_adr_ode),  intent(inout) :: obj

   nullify( obj%grid )
   nullify( obj%base )
   nullify( obj%bcs  )

 end subroutine clear_scalar_adr_ode

!-----------------------------------------------------------------------

 subroutine scalar_adr_rhs(tnd,ode,t,uuu,ods,term)
  class(t_scalar_adr_ode), intent(in) :: ode !< ODE problem
  real(wp),     intent(in)    :: t   !< time level
  class(c_stv), intent(in)    :: uuu !< present state
  class(c_ods), intent(inout) :: ods !< scratch (diagnostic vars.)
  class(c_stv), intent(inout) :: tnd !< tendency
  !> term to be evaluated and number of terms in the integrator
  integer,      intent(in), optional :: term(2)

 end subroutine scalar_adr_rhs

!-----------------------------------------------------------------------

 !> Solve implicit problem for the SOL equation
 !!
 !! To see how this subroutine is implemented, let us consider the
 !! density equation, which can be written as
 !! \f{displaymath}{
 !!  \tilde{M}\,\dot{{\tt n}}^{\rm i} + A\,{\tt n}^{\rm i} = 
 !!   \tilde{M}\,{\tt s}_{n^{\rm i}} + {\tt b}_{n^{\rm i}}
 !! \f}
 !! where \f${\tt n}\f$ and \f${\tt s}\f$ are the nodal values of the
 !! ion density and source term, respectively, \f$\tilde{M}\f$ is the
 !! mass matrix including the SUPG correction, and \f${\tt b}_{n^{\rm
 !! i}}\f$ is the term associated with the boundary conditions. This
 !! problem can be recast in the general form defined in \c
 !! mod_time_integrators_base defining
 !! \f{displaymath}{
 !!  f({\tt n}^{\rm i}) = - \tilde{M}^{-1}A\,{\tt n}^{\rm i} + {\tt
 !!  s}_{n^{\rm i}} + \tilde{M}^{-1} {\tt b}_{n^{\rm i}},
 !! \f}
 !! so that the problem defined in
 !! \field_fref{mod_time_integrators_base,c_ode,solve} turns out to be
 !! \f{displaymath}{
 !!  \left( \tilde{M} + \sigma A\right){\tt n}^{\rm i} =
 !!  \tilde{M}\left( b + \sigma{\tt s}_{n^{\rm i}} \right) + \sigma{\tt
 !!  b}_{n^{\rm i}},
 !! \f}
 !! for two arbitrary \f$\sigma\f$, \f$b\f$. Due to the time
 !! dependence of \f${\bf v}^{\rm i}\f$, only the standard mass and
 !! diffusion matrices can be precomputed, while all the other terms
 !! must be recomputed at each call.
 subroutine scalar_adr_solve(x,ode,t,sigma,b,xl,ods)
  class(t_scalar_adr_ode), intent(in) :: ode
  real(wp),     intent(in)    :: t, sigma
  class(c_stv), intent(in)    :: b, xl
  class(c_ods), intent(inout) :: ods
  class(c_stv), intent(inout) :: x

  integer :: ie, pos, i, j

   select type(b ); type is(t_sol_state)
   select type(xl); type is(t_sol_state)
   select type(x ); type is(t_sol_state)
   associate( mt => mmmt(1) , at => aaat(1) )

   ! 1) Compute the system matrix and rhs
   mt%m(1,1)%ax = const_mmmt ! constant mass matrix term
   at%m(1,1)%ax = 0.0_wp
   rhs = 0.0_wp
   
   pos = 0
   do ie=1,ode%grid%ne
     associate(    e => ode%grid%e(ie) ,      &
                poly => ode%grid%e(ie)%poly , &
                  be => ode%bcs%b_e2be(ie)    )
     associate(  bme => ode%base%e(poly)%me , &
                lm_s => mm(poly)%d , &
                 lmm => mm(poly)%m , &
                 laa => aa(poly)%m , &
                  lb => bb(poly)%v , &
                  vv => v_coeff(poly) )

     ! local velocity (this looks ugly, but constructing the local
     ! velocity like this we don't have to worry about its
     ! dimension...)
     vv%v = reshape( (/                                                &
         (( xl%vi(i_sp)%v(i)%s( e%iv(j) ) , j=1,poly),i=1,xl%vi(1)%d ) &
                   /) , shape=(/xl%vi(1)%d,poly/) , order=(/2,1/) )

     call loc_supg_mat( lmm,laa,lb , e,bme,be , &
              dx_coeff, vv, eps_coeff, mu_coeff )
 
     do j=1,lm_s
       do i=1,lm_s
         pos = pos+1
         mt%t2c(pos)%p = mt%t2c(pos)%p + lmm(i,j)
         at%t2c(pos)%p = at%t2c(pos)%p + laa(i,j)
       enddo
     enddo
     rhs(e%iv,1) = rhs(e%iv,1) + lb

     end associate
     end associate
   enddo

   ! Combine the two matrices in the linear system one
   sys_mat_t%ax = mt%m(1,1)%ax + sigma*at%m(1,1)%ax

   ! Set the rhs of the linear system
   sys_rhs = matmul( b%ni(i_sp)%s , mt%m(1,1) ) &! + sigma*source ) &
            + sigma*rhs(:,1)

   ! v = const (as well as all the other fields except ni(i_sp) )
   call x%copy( b )

   ! Solve the linear system
   call linpb%factor('factorization')
   call linpb%solve( x%ni(i_sp) )

   end associate
   end select
   end select
   end select

 end subroutine scalar_adr_solve

!-----------------------------------------------------------------------

 pure function dx_evg(coeff,e,bme) result(s)
  class(t_dx),          intent(in) :: coeff
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  real(wp) :: s(bme%m)

   s = 1.0_wp
 end function dx_evg

 pure function dx_evgs(coeff,e,bme,isl,be) result(s)
  class(t_dx),          intent(in) :: coeff
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  integer,              intent(in) :: isl
  type(t_b_2de),        intent(in), optional :: be
  real(wp) :: s(bme%ms)

   s = 1.0_wp
 end function dx_evgs

!-----------------------------------------------------------------------

 pure function v_coeff_evg(coeff,e,bme) result(v)
  class(t_v_coeff),     intent(in) :: coeff
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  real(wp) :: v(2,bme%m)

   v = matmul( coeff%v , bme%p )
 end function v_coeff_evg

 pure function v_coeff_divg(coeff,e,bme,gradp_phy) result(div)
  class(t_v_coeff),     intent(in) :: coeff
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  real(wp),             intent(in) :: gradp_phy(:,:,:)
  real(wp) :: div(bme%m)

  integer :: l

   do l=1,bme%m
     div(l) = sum( coeff%v * gradp_phy(:,:,l) )
   enddo
 end function v_coeff_divg

 pure function v_coeff_vng(coeff,e,bme,isl,be) result(vn)
  class(t_v_coeff),     intent(in) :: coeff
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  integer,              intent(in) :: isl
  type(t_b_2de),        intent(in), optional :: be
  real(wp) :: vn(bme%ms)

   vn = matmul( e%n(:,isl) , matmul( coeff%v , bme%pb(:,:,isl) ) )
 end function v_coeff_vng

!-----------------------------------------------------------------------

 pure function eps_coeff_evg(coeff,e,bme) result(t)
  class(t_eps_coeff),   intent(in) :: coeff
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  real(wp) :: t(2,2,bme%m)

   !t(1,1,:) = 0.1_wp;  t(1,2,:) = 0.0_wp
   !t(2,1,:) = 0.0_wp;  t(2,2,:) = 0.1_wp

   t = 0.0_wp

 end function eps_coeff_evg

 pure function eps_coeff_evgs(coeff,e,bme,isl,be) result(t)
  class(t_eps_coeff),   intent(in) :: coeff
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  integer,              intent(in) :: isl
  type(t_b_2de),        intent(in), optional :: be
  real(wp) :: t(2,2,bme%ms)

   !t(1,1,:) = 0.1_wp;  t(1,2,:) = 0.0_wp
   !t(2,1,:) = 0.0_wp;  t(2,2,:) = 0.1_wp

   t = 0.0_wp

 end function eps_coeff_evgs

!-----------------------------------------------------------------------

 pure function mu_evg(coeff,e,bme) result(s)
  class(t_mu),          intent(in) :: coeff
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  real(wp) :: s(bme%m)

   s = 0.0_wp
 end function mu_evg

!-----------------------------------------------------------------------

end module mod_scalar_adr_ode

