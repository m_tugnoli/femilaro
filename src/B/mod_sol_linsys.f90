!>\brief
!!
!! SOL linear systems.
!!
!! \n
!!
!! The main reason for writing this module is to keep the various
!! <tt>mod_*_ode</tt> as simple as possible by moving here all the
!! details of the linear solvers, especially concerning the
!! initializations.
!!
!! Matrices are collected in arrays, because different models
!! use/reuse different linear systems. Notice that this could also
!! depend on the time integration strategy: is the linear problems are
!! treated in parallel, a matrix is required for each equation,
!! otherwise the same matrix can be recycled. Having more matrices
!! simplifies assembling the linear system, because one loop on the
!! elements is enough, but increase the memory footprint.
module mod_sol_linsys

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_mpi_utils, only: &
   mod_mpi_utils_initialized, &
   mpi_comm_world, mpi_comm_rank, &
   mpi_allreduce, mpi_in_place, wp_mpi, mpi_sum
 
 use mod_sparse, only: &
   mod_sparse_initialized, &
   ! sparse types
   t_tri,       &
   t_col,       &
   t_pm_sk,     &
   matmul,      &
   col2tri,     &
   clear

 use mod_state_vars, only: &
   mod_state_vars_initialized, &
   c_stv

 use mod_linsolver, only: &
   mod_linsolver_initialized, &
   c_linpb, c_itpb, c_mumpspb, c_pastixpb, c_umfpackpb, &
   gmres

 use mod_h2d_grid, only: &
   mod_h2d_grid_initialized, &
   t_h2d_grid, t_ddc_h2d_grid

 use mod_sol_state, only: &
   mod_sol_state_initialized, &
   t_scalar_field

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_sol_linsys_constructor, &
   mod_sol_linsys_destructor,  &
   mod_sol_linsys_initialized, &
   mmmt, const_mmmt, aaat, rhs, &
   psnt, psn_linpb, psn_rhs, &
   sys_mat_t, sys_rhs, linpb, &
   out_file_sys_suff, out_file_sys_name, &
   linsys_res

 private

!-----------------------------------------------------------------------

! Module tipes and variables

 !> Transposed mass matrix
 !!
 !! The mass matrix is represented as a \fref{mod_sparse,t_pm_sk}
 !! object, so that it is easy to add the time dependent
 !! contributions. Moreover, we define an array to store the
 !! coefficients, in compressed form, of the constant mass matrix.
 !!
 !! \note For grids where all the elements have the same number of
 !! local degrees of freedom, given one element it is obvious which
 !! are the corresponding entries in the skeleton. In the case of a
 !! hybrid grid, however, this is not true any more; this should not
 !! be a problem since the skeleton is always accessed sequentially.
 type(t_pm_sk), allocatable, target, save :: mmmt(:)
 real(wp), allocatable :: const_mmmt(:)
 !> transposed matrix of the differential operator
 type(t_pm_sk), allocatable, target, save :: aaat(:)
 !> rhs (source and boundary conditions)
 real(wp), allocatable :: rhs(:,:)
 !> Poisson problem for \f$\Phi\f$ (transposed matrix)
 !!
 !! For this matrix, one could also use a <tt>t_col</tt> object, since
 !! the matrix is not recomputed and the skeleton is used only once.
 type(t_pm_sk), target, save :: psnt

 !> Linear system, matrix, rhs and solver
 type(t_col), target, save :: sys_mat_t
 real(wp), allocatable, target :: sys_rhs(:)
 character(len=*), parameter :: linear_solver = 'mumps'
 class(c_linpb), allocatable :: linpb
 integer, allocatable, target :: gij(:)

 !> The Poisson matrix can be factorized in advance: we need a
 !! dedicated linear problem
 class(c_linpb), allocatable :: psn_linpb
 real(wp), allocatable, target :: psn_rhs(:)

 ! Solver inrefaces
 type, extends(c_umfpackpb) :: t_sol_umfpack
 contains
  procedure, nopass :: xassign => general_sol_xassign
 end type t_sol_umfpack

 type, extends(c_mumpspb) :: t_sol_mumps
 contains
  procedure, nopass :: xassign => general_sol_xassign
 end type t_sol_mumps

 type, extends(c_pastixpb) :: t_sol_pastix
 contains
  procedure, nopass :: xassign => general_sol_xassign
 end type t_sol_pastix

 ! Some variables used to write the system matrices
 integer, parameter :: max_char_len = 10000
 character(len=*), parameter :: out_file_sys_suff = '-sys.octxt'
 character(len=max_char_len+len(out_file_sys_suff)):: out_file_sys_name

 logical, protected ::               &
   mod_sol_linsys_initialized = .false.
 character(len=*), parameter :: &
   this_mod_name = 'mod_sol_linsys'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 !> Module constructor
 !!
 !! When calling this constructor, the public matrices defined in this
 !! module must have been already allocated, while this constructor
 !! takes care of the linear system objects.
 subroutine mod_sol_linsys_constructor(grid,ddc_grid)
  type(t_h2d_grid), intent(in) :: grid
  type(t_ddc_h2d_grid), intent(in) :: ddc_grid

  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_kinds_initialized.eqv..false.) .or. &
    (mod_messages_initialized.eqv..false.) .or. &
   (mod_mpi_utils_initialized.eqv..false.) .or. &
      (mod_sparse_initialized.eqv..false.) .or. &
  (mod_state_vars_initialized.eqv..false.) .or. &
   (mod_linsolver_initialized.eqv..false.) .or. &
    (mod_h2d_grid_initialized.eqv..false.) .or. &
   (mod_sol_state_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_sol_linsys_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   ! Build a generic linear problem for all the matrices that must be
   ! factorized at each time step, using the fact that they all share
   ! the same sparsity pattern. To get such sparsity pattern, we take
   ! it here from the first mass matrix.
   sys_mat_t = mmmt(1)%m(1,1)
   allocate( sys_rhs(grid%nv) )
   call setup_linsys( linpb , sys_mat_t , sys_rhs , grid,ddc_grid , &
                      trim(linear_solver) )
   ! The matrix structure is now defined: we can call the analysis
   call linpb%factor('analysis')

   ! Poisson problem
   allocate( psn_rhs(grid%nv) )
   call setup_linsys( psn_linpb , psnt%m(1,1),psn_rhs,grid,ddc_grid , &
                      trim(linear_solver) )
   ! Again, the matrix can be analyzed (factorization must be done
   ! after filling the coefficients)
   call psn_linpb%factor('analysis')

   mod_sol_linsys_initialized = .true.
 
 contains

  subroutine setup_linsys( pb , mat_t,rhs,grid,ddc_grid,solver )
   type(t_col), target,  intent(in) :: mat_t
   real(wp),    target,  intent(in) :: rhs(:)
   type(t_h2d_grid),     intent(in) :: grid
   type(t_ddc_h2d_grid), intent(in) :: ddc_grid
   character(len=*),     intent(in) :: solver
   class(c_linpb), allocatable, intent(out) :: pb

    select case(solver)
     case('gmres')
!      allocate(t_poisson_it::linpb)
!      select type(linpb); type is(t_poisson_it)
!       linpb%abstol    = .true.
!       linpb%tolerance = 1.0e-8_wp
!       linpb%nmax      = 50 ! size of the Krylov space
!       linpb%rmax      = 5  ! restarts
!       linpb%solver    => gmres
!       linpb%mpi_comm  = mpi_comm_world
!       call mpi_comm_rank(linpb%mpi_comm,linpb%mpi_id,i)
!      end select
     case('mumps')
      allocate(t_sol_mumps::pb)
      select type(pb); type is(t_sol_mumps)
       pb%distributed    = .true.
       !pb%poo           = <choose reordering>
       pb%transposed_mat = .true.
       pb%gn             = ddc_grid%ngv ! global # of vertexes
       pb%m              => mat_t
       pb%rhs            => rhs
       ! gij is common to all the matrices: we allocate it if it is not
       ! already allocated
       if(.not.allocated(gij)) then
         allocate(gij(grid%nv)); gij = ddc_grid%gv%gi-1 ! 0 based
       endif
       pb%gij            => gij
       pb%mpi_comm       = mpi_comm_world
      end select
     case('pastix')
      allocate(t_sol_pastix::pb)
      select type(pb); type is(t_sol_pastix)
       pb%transposed_mat = .true.
       pb%gn             = ddc_grid%ngv ! global # of vertexes
       pb%m              => mat_t
       pb%rhs            => rhs
       if(.not.allocated(gij)) then
         allocate(gij(grid%nv)); gij = ddc_grid%gv%gi-1 ! 0 based
       endif
       pb%gij            => gij
       pb%mpi_comm       = mpi_comm_world
      end select
     case('umfpack')
      allocate(t_sol_umfpack::pb)
      select type(pb); type is(t_sol_umfpack)
       pb%print_level    = 0
       pb%transposed_mat = .true.
       pb%m              => mat_t
       pb%rhs            => rhs
      end select
     case default
      call error(this_sub_name,this_mod_name, 'Unknown linear solver.' )
    end select

  end subroutine setup_linsys

 end subroutine mod_sol_linsys_constructor

!-----------------------------------------------------------------------
 
 !> Module destructor
 !!
 !! This destructor deallocates the objects allocated by the module
 !! constructors, while object allocated externally are supposed to be
 !! also deallocated externally after calling this subroutine.
 subroutine mod_sol_linsys_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_sol_linsys_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   call psn_linpb%clean()
   deallocate(psn_linpb)
   deallocate(psn_rhs)

   call linpb%clean()
   deallocate(linpb)
   deallocate(sys_rhs)
   call clear(sys_mat_t)

   if(allocated(gij)) deallocate(gij)

   mod_sol_linsys_initialized = .false.
 end subroutine mod_sol_linsys_destructor

!-----------------------------------------------------------------------

 subroutine general_sol_xassign(x,s,x_vec)
  real(wp),       intent(in)    :: x_vec(:)
  class(c_linpb), intent(inout) :: s
  class(c_stv),   intent(inout) :: x

   select type(x); type is(t_scalar_field)
    x%s = x_vec
   end select
 end subroutine general_sol_xassign

!-----------------------------------------------------------------------

 !> Compute the residual of the linear system \f$r = \|b-Ax\|\f$
 !!
 !! This operation would be trivial for a parallel execution, while it
 !! requires some care in the case of a distributed system. Here we
 !! assume that the matrix \f$A\f$ and the right-hand-side \f$b\f$ are
 !! distributed, so that duplicated entries must be added, while for
 !! \f$x\f$ we assume that duplicated entries are copies of the same
 !! value. This is the most natural setting when dealing with a linear
 !! system.
 !!
 !! \warning It is the caller's responsibility to ensure that \f$x\f$
 !! is consistent among different processors, i.e. duplicated values
 !! are the same on all the processrs.
 !!
 !! The algorithm is very simple:
 !! <ul>
 !!  <li> each processor computes the local residual
 !!  <li> the residuals are added on the overlapping entries
 !!  <li> each processor computes the local residual norm
 !!  <li> a global reduction is performed, and the result is returned
 !!  on all the processors.
 !! <ul>
 !!
 !! \note Check the implementation to select the desired norm.
 subroutine linsys_res(rnorm,a,x,b,transposed_mat , grid,ddc_grid)
  type(t_col), intent(in) :: a
  real(wp), intent(in) :: x(:), b(:)
  logical, intent(in) :: transposed_mat
  type(t_h2d_grid), intent(in) :: grid
  type(t_ddc_h2d_grid), intent(in) :: ddc_grid
  real(wp), intent(out) :: rnorm

  integer :: ierr
  real(wp), allocatable :: r(:), rg(:) ! avoid large stack variables
  character(len=*), parameter :: &
    this_sub_name = 'linsys_res'

   if(.not.transposed_mat) &
     call error(this_sub_name,this_mod_name, 'Not yet implemented' )

   allocate( r(grid%nv) , rg(ddc_grid%ngv) )

   ! 1) local residual
   r = b - matmul( x , a ) ! assuming that a is transposed

   ! 2) add overlapping entries (lazy, inefficient solution...)
   rg = 0.0_wp
   rg( ddc_grid%gv%gi ) = r

   call mpi_allreduce(mpi_in_place,rg,ddc_grid%ngv,wp_mpi,mpi_sum, &
                      mpi_comm_world, ierr)

   ! 3+4) directly computing the global norm
   rnorm = maxval(abs(rg))

   deallocate( r , rg )

 end subroutine linsys_res

!-----------------------------------------------------------------------

end module mod_sol_linsys

