!! Copyright (C) 2009,2010,2011,2012  Marco Restelli
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Marco Restelli                   <marco.restelli@gmail.com>

!> \brief
!!
!! Computation of the local mass matrices
!!
!! \n
!!
!! All the integrals are computed evaluating the coefficients at the
!! chosen quadrature nodes, using the methods of the
!! <tt>c_*_coeff</tt> objects. If one intends to use piecewise
!! constant coefficients, the appropriate value should be computed in
!! such methods.
!!
!! We provide here a type \c t_loc_mat which can be useful to define a
!! collection of working matrices for the various element shapes.
module mod_sol_locmat

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_h2d_master_el, only: &
   mod_h2d_master_el_initialized, &
   c_h2d_me_geom, c_h2d_me_base

 use mod_h2d_grid, only: &
   mod_h2d_grid_initialized, &
   t_2dv, t_2ds, t_2de

 use mod_h2d_bcs, only: &
   mod_h2d_bcs_initialized, &
   t_b_2dv,   t_b_2ds,   t_b_2de,   &
   p_t_b_2dv, p_t_b_2ds, p_t_b_2de, &
   b_dir,   b_neu,   b_ddc

 use mod_sol_testcases, only: &
   mod_sol_testcases_initialized, &
   t_phc, tc

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_sol_locmat_constructor, &
   mod_sol_locmat_destructor,  &
   mod_sol_locmat_initialized, &
   t_loc_mat, c_s_coeff, c_v_coeff, c_t_coeff, c_lbcs_coeff, &
   t_loc_poisson_rhs_data, &
   loc_mass_mat, loc_supg_mat, loc_poisson_mat, loc_poisson_rhs

 private

!-----------------------------------------------------------------------

! Module types and parameters

 type :: t_loc_mat
  integer :: d !< matrix size
  real(wp), allocatable :: v(:)   !< local vector
  real(wp), allocatable :: m(:,:) !< local matrix
 end type t_loc_mat

 !> Type to pass a scalar problem coefficient
 !!
 !! This module provides a dummy implementation for all the methods,
 !! so that the user needs to define only those which are effectively
 !! required.
 !!
 !! \note To ensure consistency, the results of the methods of a \c
 !! c_s_coeff object must be used only "as such", without additional
 !! processing. For instance, one could compute the values of a scalar
 !! field and then use them to compute its gradient; this however
 !! would not necessarily be consistent with the gradient provided by
 !! the \c c_s_coeff object itself, or this gradient could even not be
 !! defined at all for this particular field.
 !!
 !! \todo The concept of <em>problem coefficient</em> is a general
 !! one, which could be moved in a dedicated module in \c
 !! fml_general_utilities. However, it is difficult to provide enough
 !! methods for this class without compromising efficiency. Also, this
 !! should be done in a way which parallels the methods defined for
 !! the master elements. Currently, the master elements have rather
 !! unspecific methods, such as the one which scales arbitrary
 !! Gaussian weights, while these coefficients have more specified
 !! methods which work on all the Gaussian nodes of an element. An
 !! example could be the method to evaluate a coefficient:
 !! <ul>
 !!  <li> one possibility could be <tt>coeff(x)</tt>
 !!  <li> if the coefficient depends on the finite element basis,
 !!  however, it is convenient to know where we are evaluating it at
 !!  the Gaussian points: <tt>coeff_g()</tt>, <tt>coeff_gb(isl)</tt>
 !!  <li> finally, if more coefficients have to be computed at the
 !!  same location, combining the evaluations would typically be more
 !!  efficient.
 !! </ul>
 type, abstract :: c_s_coeff
 contains
  !> evaluate the field at the element quadrature points
  procedure, pass(coeff) :: evg  => s_coeff_evg
  !> evaluate the field at the side quadrature points
  !!
  !! \note This function could be written in two ways: either based on
  !! a side, or on an element and a local side. We choose the second
  !! option because it seems more natural for a continuous finite
  !! element method. This means that the sied quadrature nodes are
  !! ordered using the element-induced order.
  procedure, pass(coeff) :: evgs => s_coeff_evgs
 end type c_s_coeff

 !> Vector coefficient (see also \c c_s_coeff)
 type, abstract :: c_v_coeff
 contains
  procedure, pass(coeff) :: evg  => v_coeff_evg !< field at quad. nodes
  procedure, pass(coeff) :: divg => v_coeff_divg !< div at quad. nodes
  procedure, pass(coeff) :: vng  => v_coeff_vng !< v.n at side q. nodes
 end type c_v_coeff

 !> Tensor coefficient (see also \c c_s_coeff)
 type, abstract :: c_t_coeff
 contains
  procedure, pass(coeff) :: evg  => t_coeff_evg !< field at quad. nodes
  procedure, pass(coeff) :: evgs => t_coeff_evgs !< side quad. nodes
 end type c_t_coeff

 !> Local boundary condition
 !!
 !! This coefficient gives two kinds of information:
 !! <ul>
 !!  <li> the boundary condition: \c b_dir or \c b_neu, together with
 !!  the type of such condition, i.e. an arbitrary integer (see
 !!  \field_fref{mod_bcs,t_b_v,btype}
 !!  <li> the value of the boundary condition at the side quadrature
 !!  nodes.
 !! </ul>
 !! This type is introduced so that different boundary conditions can
 !! be specified for different fields, on the same mesh. This
 !! overrides the values provided in the two fields \c bc and \c btype
 !! defined in \c mod_bcs.
 type, abstract :: c_lbcs_coeff
 contains
  procedure(i_lbcs_coeff_bc), pass(coeff), deferred :: bc
  procedure, pass(coeff) :: cdir => lbcs_coeff_cdirneu !< Dirichlet
  procedure, pass(coeff) :: cneu => lbcs_coeff_cdirneu !> Neumann
 end type c_lbcs_coeff

 abstract interface
  pure subroutine i_lbcs_coeff_bc(bc,btype,coeff,breg)
   import :: c_lbcs_coeff
   class(c_lbcs_coeff), intent(in) :: coeff
   integer,             intent(in) :: breg
   integer,             intent(out) :: bc, btype
  end subroutine i_lbcs_coeff_bc
 end interface

 !> See \c t_loc_poisson_rhs_data
 type :: t_loc_poisson_rhs_data_es
  real(wp), pointer :: b(:,:) => null()
  real(wp), pointer :: c_bfb(:,:) => null()
  real(wp), pointer :: curl_c_bfb(:,:) => null()
  real(wp), pointer :: te(:) => null(), gte(:,:) => null()
  real(wp), pointer :: ti(:) => null(), gti(:,:) => null()
 end type t_loc_poisson_rhs_data_es

 !> A simple type to collect the data required in the computation of
 !! the Poisson rhs.
 !!
 !! The main reason for introducing this type is that passing the
 !! coefficients for the elements intersecting the domain boundary
 !! would otherwise be cumbersome. Notice that the side fields have to
 !! be set only for boundary elements, and in such case they should
 !! refer to the side ordering of the quadrature nodes.
 type :: t_loc_poisson_rhs_data
  !> element data
  type(t_loc_poisson_rhs_data_es) :: edata
  !> side data, for boundary elements
  type(t_loc_poisson_rhs_data_es), allocatable :: sdata(:)
  !> a function to compute \f$\sigma_\parallel\f$
  procedure(i_sigma_par), nopass, pointer :: sigma_par => null()
 end type t_loc_poisson_rhs_data

 abstract interface
  pure function i_sigma_par(te , phc) result(sp)
   import :: wp, t_phc
   real(wp), intent(in) :: te(:)
   type(t_phc), intent(in) :: phc
   real(wp) :: sp(size(te))
  end function i_sigma_par
 end interface

! Module variables

 ! public members
 logical, protected ::               &
   mod_sol_locmat_initialized = .false.
 ! private members
 character(len=*), parameter :: &
   this_mod_name = 'mod_sol_locmat'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_sol_locmat_constructor()
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false. ) .or. &
          (mod_kinds_initialized.eqv..false. ) .or. & 
  (mod_h2d_master_el_initialized.eqv..false. ) .or. & 
       (mod_h2d_grid_initialized.eqv..false. ) .or. &
        (mod_h2d_bcs_initialized.eqv..false. ) .or. &
  (mod_sol_testcases_initialized.eqv..false. ) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_sol_locmat_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   mod_sol_locmat_initialized = .true.
 end subroutine mod_sol_locmat_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_sol_locmat_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_sol_locmat_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_sol_locmat_initialized = .false.
 end subroutine mod_sol_locmat_destructor

!-----------------------------------------------------------------------
 
 !> Compute the local mass matrix (without SUPG stabilization)
 !!
 !! We compute here
 !! \f{displaymath}{
 !!   \int_K \phi_i \, \phi_j \,dx = 
 !!   \int_{\widehat{K}} \widehat{\phi}_i\, \widehat{\phi}_j|J|\,d\xi.
 !! \f}
 !! The possible presence of a metric factor is treated as discussed
 !! \ref locmat_generalization "here".
 pure subroutine loc_mass_mat(mmk,e,bme , dx)
  type(t_2de), intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme !< base master element
  real(wp), intent(out) :: mmk(:,:)
  class(c_s_coeff), intent(in) :: dx

  integer :: i, j, l
  real(wp) :: wga(bme%m), wgap
  character(len=*), parameter :: &
    this_sub_name = 'loc_mass_mat'

   ! scale the Gaussian weights, including the metric factor
   wga = dx%evg(e,bme) * e%me%scale_wg(bme%xig,bme%wg)

   mmk = 0.0_wp
   do l=1,bme%m
     do j=1,bme%pk
       wgap = wga(l) * bme%p(j,l)
       !  wga(l) * p(i,l)*p(j,l)
       do i=1,bme%pk
         mmk(i,j) = mmk(i,j) + wgap * bme%p(i,l)
       enddo
     enddo
   enddo

 end subroutine loc_mass_mat
 
!-----------------------------------------------------------------------

 !> Compute the local matrices
 !!
 !! To clarify the role of the coordinate transformation in the
 !! computation of the local matrices, let us start from the SUPG mass
 !! matrix, which can be expressed as
 !! \f{displaymath}{
 !!   \int_K\tau{\bf v}\cdot\nabla\phi_i \, \phi_j\,dx =
 !!   \int_{\widehat{K}}\tau\widehat{{\bf v}} \cdot 
 !!   \left( J^{-T}\widehat{\nabla}\widehat{\phi}_i\right)
 !!   \widehat{\phi}_j|J|\,d\xi =
 !!   \int_{\widehat{K}}\tau\left( J^{-1}\widehat{{\bf v}}\right) \cdot 
 !!   \widehat{\nabla}\widehat{\phi}_i \,
 !!   \widehat{\phi}_j|J|\,d\xi,
 !! \f}
 !! where \f$J=\frac{d{\bf x}}{d{\bf \xi}}\f$ and \f$\widehat{\bf
 !! v}({\bf \xi})={\bf v}({\bf x}({\bf \xi}))\f$. Here, in one case we
 !! apply the push forward transformation to the gradient 1-form,
 !! obtaining \f$J^{-T}\widehat{\nabla}\widehat{\phi}_i\f$, while in
 !! the other case we apply the pull back transform to the velocity
 !! vector, thus obtaining \f$J^{-1}\widehat{{\bf v}}\f$. The two
 !! expressions are equivalent, since by definition of the push
 !! forward/pull back operation the duality product is conserved,
 !! however from the computational viewpoint one can be more
 !! convenient that the other, since in one case we need to transform
 !! the velocity while in the other case we transform the basis
 !! functions. For instance, for a scalar problem, it is convenient to
 !! transform the velocity, which is one single vector field. However,
 !! if many matrices have to be computed using different vector
 !! fields, transforming the basis functions can be more efficient.
 !!
 !! The modified advection term has a very similar structure:
 !! \f{displaymath}{
 !!   -\int_K\left(1-\tau(\mu+\nabla\cdot{\bf v})\right){\bf
 !!   v}\cdot\nabla\phi_i \, \phi_j\,dx
 !! \f}
 !! so the only required modification is in the scalar coefficient
 !! \f$\tau \mapsto -\left(1-\tau(\mu+\nabla\cdot{\bf v})\right)\f$.
 !!
 !! The diffusion term can be treated in a similar way:
 !! \f{displaymath}{
 !!   \int_K \left(\underline{\underline{\varepsilon}}^T +
 !!      \tau {\bf v}\otimes{\bf v} \right)
 !!     \nabla \phi_i\cdot\nabla\phi_j\,dx 
 !! = \int_{\widehat{K}} \left(
 !! \widehat{\underline{\underline{\varepsilon}}}^T +
 !!      \tau \widehat{\bf v}\otimes\widehat{\bf v} \right)
 !!  \left( J^{-T}\widehat{\nabla}\widehat{\phi}_i\right) \cdot
 !!  \left( J^{-T}\widehat{\nabla}\widehat{\phi}_j\right)|J|\, d\xi,
 !! \f}
 !! where again \f$\widehat{\underline{\underline{\varepsilon}}} ({\bf
 !! \xi})= \underline{\underline{\varepsilon}}({\bf x}({\bf \xi}))\f$.
 !!
 !! Coming to the boundary terms, for the Dirichlet boundary
 !! conditions, letting \f$\partial K_D = \partial K\cap \Gamma_D\f$,
 !! we need to compute
 !! \f{displaymath}{
 !!   \int_{\partial K_D}\left\{
 !!   \left({\bf v}\cdot{\bf n}_{\partial K} + \xi \right)\phi_i\phi_j
 !!   - \underline{\underline{\varepsilon}}^T\,{\bf n}_{\partial K}
 !!   \cdot \left( \nabla\phi_i\,\phi_j + \phi_i\nabla\phi_j \right)
 !!   \right\}d\sigma
 !! \f}
 !! along with the corresponding forcing term. Assuming that \f${\bf
 !! v}\f$ and \f$\underline{\underline{\varepsilon}}\f$ are given in
 !! physical space, since the outward normal vector is typically
 !! known in physical space too, the easiest way to compute this
 !! integral is applying the push forward transformation to
 !! \f$\widehat{\nabla}\widehat{\phi}_i|_{\partial\widehat{K}}\f$,
 !! which is done exactly as for the interior points, and then
 !! evaluating the integrand in physical space.
 !!
 !! \section locmat_generalization Generalization of the problem
 !!
 !! This function is meant to be used for various specific SOL models;
 !! this requires some generalizations compared to the standard scalar
 !! advection-diffusion case.
 !!
 !! The first aspect is that the diffusion tensor could not be
 !! symmetric; this motivates the presence of the transposition in the
 !! expressions of the local matrices, which in fact are correct even
 !! for nonsymmetric \f$\underline{\underline{\varepsilon}}\f$.
 !!
 !! A second aspect concerns the way the velocity vector is provided,
 !! which can be either a piecewise linear function, defined by the
 !! nodal values, or a piecewise constant one. Moreover, different
 !! velocity components can be prescribed in different ways, such as
 !! in the drift models. To address this issue, the velocity is passed
 !! as a \c c_v_coeff object where the caller has the freedom to
 !! specify whatever coefficient is required.
 !!
 !! Another aspect is that the area element \f$dx\f$ and the surface
 !! element \f$d\sigma\f$ might include a metric factor; for instance,
 !! in cylindrical coordinates one has \f$dx^{\textrm{3D}} =
 !! R\,dx^{\textrm{2D}}\f$, and the same holds for the surface
 !! element, where \f$R\f$ is the radius (see also \ref n_reduct_2D
 !! "this section"). For this reason, an input argument \c dx is
 !! included.
 pure subroutine loc_supg_mat( supg_mmk,supg_aak,supg_bk , e,bme,be , &
                               dx,v,eps,mu,lbcs )
  type(t_2de),     intent(in) :: e  !< element
  class(c_h2d_me_base), intent(in) :: bme !< base master element
  type(p_t_b_2de), intent(in) :: be !< corresponding boundary element
  !> area element; the following methods are used:
  !! <ul>
  !!  <li> \field_fref{mod_sol_locmat,c_s_coeff,evg} to evaluate the
  !!  metric factor at the internal quadrature points
  !!  <li> \field_fref{mod_sol_locmat,c_s_coeff,evgs} to evaluate the
  !!  metric factor if the element intersects the boundary.
  !! </ul>
  class(c_s_coeff), intent(in) :: dx
  !> velocity; the following methods are used:
  !! <ul>
  !!  <li> \field_fref{mod_sol_locmat,c_v_coeff,evg} to evaluate the
  !!  velocity at the internal quadrature points
  !!  <li> \field_fref{mod_sol_locmat,c_v_coeff,divg} to evaluate the
  !!  velocity divergence at the internal quadrature points
  !!  <li> \field_fref{mod_sol_locmat,c_v_coeff,vng} to evaluate the
  !!  normal velocity if the element intersects the boundary.
  !! </ul>
  class(c_v_coeff), intent(in) :: v
  !> diffusion tensor; the following methods are used:
  !! <ul>
  !!  <li> \field_fref{mod_sol_locmat,c_t_coeff,evg} to evaluate the
  !!  tensor at the internal quadrature points
  !!  <li> \field_fref{mod_sol_locmat,c_t_coeff,evgs} to evaluate the
  !!  tensor if the element intersects the boundary.
  !! </ul>
  class(c_t_coeff), intent(in) :: eps
  !> zero order term \f$\mu\f$; the following methods are used:
  !! <ul>
  !!  <li> \field_fref{mod_sol_locmat,c_s_coeff,evg} to evaluate the
  !!  coefficient at the internal quadrature points.
  !! </ul>
  class(c_s_coeff),    intent(in), optional :: mu
  !> boundary conditions, only used if <tt>associated(be%p)</tt>
  class(c_lbcs_coeff), intent(in), optional :: lbcs
  real(wp), intent(out) :: supg_mmk(:,:), supg_aak(:,:), supg_bk(:)

  ! Select conservative vs. nonconservative form:
  ! kappa = 1  ->  conservative
  ! kappa = 0  ->  nonconservative
  real(wp), parameter :: &
    kappa =    1.0_wp      , &
    nkapp = 1.0_wp - kappa

  integer :: i, j, l
  real(wp) :: wga(bme%m), gradp_phy(2,bme%pk,bme%m), v_g(2,bme%m), &
    div_v(bme%m), o_t_mu_div_v_g(bme%m), eps_g(2,2,bme%m), mu_g(bme%m),&
    w_v_grad_phi, w_e_grad_phi(2), w_mu_phi, tau, tmp
  character(len=*), parameter :: &
    this_sub_name = 'loc_supg_mat'

   ! scale the Gaussian weights, including the metric factor
   wga = dx%evg(e,bme) * e%me%scale_wg(bme%xig,bme%wg)

   ! scale the basis derivatives to physical space
   gradp_phy = e%me%scale_gradp(bme%xig,bme%gradp)

   ! coefficients at the quadrature points
   v_g   = v%evg(  e,bme)
   eps_g = eps%evg(e,bme)
   if(present(mu)) then
     mu_g = mu%evg(e,bme)
   else
     mu_g = 0.0_wp
   endif

   ! velocity divergence
   div_v = v%divg(e,bme,gradp_phy)

   ! stabilization coefficient
   tau = set_tau(e,wga,v_g,eps_g) 
   ! other coefficients: 1-tau*(mu+div(v)) and the effective diffusion
   o_t_mu_div_v_g = kappa - tau*(mu_g + div_v)
   do l=1,bme%m
     ! complete diffusion tensor
     do j=1,2
       do i=1,2
         eps_g(i,j,l) = eps_g(i,j,l) + tau * v_g(i,l)*v_g(j,l)
       enddo
     enddo
   enddo

   supg_mmk = 0.0_wp
   supg_aak = 0.0_wp
   supg_bk  = 0.0_wp
   do l=1,bme%m
     do i=1,bme%pk

       ! compute    mu \phi_i   (with Gauss weight)
       w_mu_phi     = wga(l) * (mu_g(l)+nkapp*div_v(l)) * bme%p(i,l)

       ! compute   v \cdot \nabla\phi_i   (with Gauss weight)
       w_v_grad_phi = wga(l) * dot_product(v_g(:,l) , gradp_phy(:,i,l))

       ! compute  (eps^T + tau v\otimes v) \nabla\phi_i  (with Gauss w.)
       w_e_grad_phi = wga(l)*( matmul(gradp_phy(:,i,l) , eps_g(:,:,l)) &
                              + nkapp*bme%p(i,l)*v_g(:,l) )

       do j=1,bme%pk
         ! zero order term
         supg_aak(i,j) = supg_aak(i,j) + w_mu_phi * bme%p(j,l)
         ! first order terms
         tmp = w_v_grad_phi * bme%p(j,l)
         supg_mmk(i,j) = supg_mmk(i,j) + tau * tmp
         supg_aak(i,j) = supg_aak(i,j) - o_t_mu_div_v_g(l) * tmp
         ! second order term
         supg_aak(i,j) = supg_aak(i,j) +                              &
                             dot_product(w_e_grad_phi,gradp_phy(:,j,l))
       enddo

     enddo
   enddo

   ! boundary conditions
   if(associated(be%p)) &
     call loc_bcs( supg_aak,supg_bk, e,bme,be%p , kappa , dx,eps,lbcs,v )

 contains

  ! Notice that tau has the dimension of time, as it should be.
  pure function set_tau(e,wga,v_g,eps_g) result(tau)
   type(t_2de), intent(in) :: e
   real(wp), intent(in) :: wga(:), v_g(:,:), eps_g(:,:,:)
   real(wp) :: tau
   
   ! This constant is used to scale the value for tau obtained from
   ! dimensional analysis; in principle, it is related to the grid
   ! regularity, in practice it can be chosen empirically.
   real(wp), parameter :: tau_constant = 2.0_wp
   real(wp), parameter :: toll = 1000.0_wp*epsilon(1.0_wp)
   real(wp) :: h, vv, ee, pe, evol

    ! area (includes the metric factor and is different from e%vol)
    evol = sum(wga)

    ! velocity scale
    vv = dot_product( sqrt(sum(v_g**2,1)) , wga )/evol
    if(vv.le.toll) then ! inviscid problem
      tau = 0.0_wp
      return
    endif

    ! length scale
    h = sqrt(e%vol)

    ! diffusion scale: only the symmetric part is relevant
    ee = dot_product( sqrt( eps_g(1,1,:)**2 + eps_g(2,2,:)**2        &
                           + 0.5_wp*(eps_g(2,1,:)+eps_g(1,2,:))**2 ) &
                     , wga )/evol

    ! local Peclet number
    if(ee.gt.toll) then
      pe = vv*h/(2.0_wp*ee)
      if(pe.le.1.0_wp) then
        tau = h**2/(4.0_wp*ee) ! h/(2*vv)*Pe
      else
        tau = h/(2.0_wp*vv)
      endif
    else ! inviscid case
      tau = h/(2.0_wp*vv)
    endif

    tau = tau_constant * tau
    
    ! no stabilization
    !tau = 0.0_wp

  end function set_tau

 end subroutine loc_supg_mat
 
!-----------------------------------------------------------------------

 !> Compute the boundary terms: matrices and forcing terms
 !!
 !! If we call this function, it means that the element has at least
 !! one boundary side. Indeed, the number of boundary sides is
 !! <tt>be\%nbs</tt>. In this subroutine we loop on the boundary sides
 !! and add the corresponding terms.
 !!
 !! Notice that this function is independednt from the SUPG
 !! stabilization.
 pure subroutine loc_bcs( aak,bk , e,bme,be , kappa , dx,eps,lbcs,v )
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  type(t_b_2de),        intent(in) :: be
  class(c_s_coeff),     intent(in) :: dx
  real(wp),             intent(in) :: kappa !< cons./noncons. form
  class(c_v_coeff),     intent(in), optional :: v !< default: zero
  class(c_t_coeff),     intent(in) :: eps
  class(c_lbcs_coeff),  intent(in) :: lbcs
  real(wp), intent(inout) :: aak(:,:), bk(:)

  real(wp), parameter :: xi_scale = 1.0e5_wp

  integer :: isb, isl, l, i, j, breg, bc, btype
  real(wp) :: wga(bme%ms), v_g_n_xi(bme%ms), xi_coeff, &
    gradp_phy(2,bme%pk,bme%ms), eps_g(2,2,bme%ms),     &
    eps_n_grad_phy(bme%pk,bme%ms), bval(bme%ms)
  character(len=*), parameter :: &
    this_sub_name = 'loc_bcs'

   ! loop on the boundary sides, each of them is treated independently
   do isb=1,be%nbs
     isl = be%bs(isb)%p%s%isl(1) ! local side index on element e

     ! ddc boundary conditions are the same for all the fields;
     ! otherwise, we ask the coefficient, which overrides the value
     ! stored in the grid
     if(be%bs(isb)%p%bc.eq.b_ddc) then
       bc = b_ddc
     else
       breg = -e%s(isl)%p%ie(2) ! will be needed later
       call lbcs%bc( bc,btype , breg )
     endif
   
     btype_case: select case(bc)

      case(b_dir)

       ! Notice: all these computations use the node ordering induced
       ! by the elements; there is no need for permutation.

       ! scale the Gaussian weights: trivial for a segment
       wga = dx%evgs(e,bme,isl,be) * bme%wgs*e%s(isl)%p%a

       ! problem data and coefficients: boundary value
       bval = lbcs%cdir( breg , x=e%me%map( bme%xigb(:,:,isl) ) )

       ! diffusion coefficient
       eps_g = eps%evgs(e,bme,isl,be)
       
       ! normal velocity
       if(present(v)) then
         v_g_n_xi = v%vng(e,bme,isl,be)
       else
         v_g_n_xi = 0.0_wp
       endif

       ! penalization term: must be large compared to the coefficients
       xi_coeff = xi_scale/e%s(isl)%p%a                         &
                 * ( maxval(abs(eps_g)) + maxval(abs(v_g_n_xi)) )

       ! include the penalizazion coefficient xi
       v_g_n_xi = kappa*v_g_n_xi + xi_coeff

       ! diffusion terms
       gradp_phy = e%me%scale_gradp(                        &
                     bme%xigb(:,:,isl),bme%gradpb(:,:,:,isl))
       do l=1,bme%ms
         eps_n_grad_phy(:,l) = matmul(         &
           matmul(e%n(:,isl) , eps_g(:,:,l)) , & ! notice: transposition
           gradp_phy(:,:,l) )
       enddo

       do l=1,bme%ms
         do j=1,bme%pk
           do i=1,bme%pk
             ! first order terms
             aak(i,j) = aak(i,j)                                      &
                   + wga(l)*v_g_n_xi(l)*bme%pb(i,l,isl)*bme%pb(j,l,isl)
             ! second order term
             aak(i,j) = aak(i,j)                                   &
                   - wga(l)*(  eps_n_grad_phy(i,l)*bme%pb(j,l,isl) &
                             + eps_n_grad_phy(j,l)*bme%pb(i,l,isl) )
           enddo
         enddo
         ! boundary term
         bk(:) = bk(:) + wga(l)*(                                   &
           xi_coeff*bme%pb(:,l,isl) - eps_n_grad_phy(:,l) ) * bval(l)
       enddo

      case(b_neu)

       wga = dx%evgs(e,bme,isl,be) * bme%wgs*e%s(isl)%p%a
       ! prescribed boundary value
       bval = lbcs%cneu( breg , x=e%me%map( bme%xigb(:,:,isl) ) )
       ! normal velocity
       if(present(v)) then
         v_g_n_xi = v%vng(e,bme,isl,be)
       else
         v_g_n_xi = 0.0_wp
       endif
       ! compute v^+
       v_g_n_xi = 0.5_wp*( v_g_n_xi + abs(v_g_n_xi) ) &
                 - (1.0_wp-kappa)*v_g_n_xi
       do l=1,bme%ms
         do j=1,bme%pk
           do i=1,bme%pk
             ! same as the first order term for Dir. bcs
             aak(i,j) = aak(i,j)                                      &
                   + wga(l)*v_g_n_xi(l)*bme%pb(i,l,isl)*bme%pb(j,l,isl)
           enddo
         enddo
         ! boundary term
         bk(:) = bk(:) - wga(l)*bme%pb(:,l,isl)*bval(l)
       enddo

      case(b_ddc)
       ! nothing to do

      case default
       aak = -huge(aak)
     end select btype_case
   enddo

 end subroutine loc_bcs

!-----------------------------------------------------------------------

 !> Local matrices for the Poisson problem (left-hand-side)
 !!
 !! This function computes the local matrices for the Poisson problem,
 !! which is basically a subset of the one treated in \c loc_supg_mat.
 pure subroutine loc_poisson_mat( aak , e,bme,be , dx,sigma,lbcs )
  type(t_2de),      intent(in) :: e   !< element
  class(c_h2d_me_base), intent(in) :: bme !< base master element
  type(p_t_b_2de),  intent(in) :: be  !< corresponding boundary element
  class(c_s_coeff), intent(in) :: dx  !< area element
  class(c_t_coeff), intent(in) :: sigma !< diffusion tensor
  !> boundary conditions, only used if <tt>associated(be%p)</tt>
  class(c_lbcs_coeff), intent(in), optional :: lbcs
  real(wp), intent(out) :: aak(:,:)

  integer :: i, j, l
  real(wp) :: wga(bme%m), gradp_phy(2,bme%pk,bme%m), &
    sigma_g(2,2,bme%m), w_s_grad_phi(2)
  real(wp) :: tmp_bk(size(aak,1)) ! not used but required by loc_bcs
  character(len=*), parameter :: &
    this_sub_name = 'loc_poisson_mat'

   ! scale the Gaussian weights, including the metric factor
   wga = dx%evg(e,bme) * e%me%scale_wg(bme%xig,bme%wg)

   ! scale the basis derivatives to physical space
   gradp_phy = e%me%scale_gradp(bme%xig,bme%gradp)

   ! coefficients at the quadrature points
   sigma_g = sigma%evg(e,bme)

   aak = 0.0_wp
   do l=1,bme%m
     do i=1,bme%pk

       ! compute  sigma^T \nabla\phi_i  (with Gauss w.)
       w_s_grad_phi = wga(l) * matmul(gradp_phy(:,i,l) , sigma_g(:,:,l))

       do j=1,bme%pk
         ! second order term
         aak(i,j) = aak(i,j) +                                 &
                      dot_product(w_s_grad_phi,gradp_phy(:,j,l))
       enddo

     enddo
   enddo

   ! boundary conditions
   tmp_bk = 0.0_wp ! this will not be used anyway
   if(associated(be%p)) &
     ! notice: the value kappa is not relevant here, because the
     ! advective term vanishes anyway
     call loc_bcs( aak,tmp_bk , e,bme,be%p , 1.0_wp , dx,sigma,lbcs )

 end subroutine loc_poisson_mat

!-----------------------------------------------------------------------

 !> Computation of the right-hand-side of the Poisson problem
 !!
 !! The computation of the right-hand-side follows \ref potential_eq
 !! "this description". Due to the specific form of this term, using a
 !! "nice" implementation is not easy; instead, we use a very
 !! specialized one. The code follows \c loc_poisson_mat.
 pure subroutine loc_poisson_rhs( bk , e,bme,be , dx ,          &
                     const_e,const_z , ne , esdata , sigma,lbcs )
  type(t_2de),      intent(in) :: e   !< element
  class(c_h2d_me_base), intent(in) :: bme !< base master element
  type(p_t_b_2de),  intent(in) :: be  !< corresponding boundary element
  class(c_s_coeff), intent(in) :: dx  !< area element
  real(wp), intent(in) :: const_e, const_z
  !> density, finite element dofs values
  real(wp), intent(in) :: ne(:)
  !> element (and side) data, at the quadrature nodes
  type(t_loc_poisson_rhs_data), intent(in) :: esdata
  class(c_t_coeff), intent(in) :: sigma !< for the bcs
  class(c_lbcs_coeff), intent(in), optional :: lbcs !< for the bcs
  !> Poisson rhs
  !!
  !! The second index is used to separate the parallel and
  !! perpendicular contributions. Notice that this is only required
  !! for diagnostic reasons; the two contributions could be added
  !! directly in the contained function \c compute_f_phi.
  real(wp), intent(out) :: bk(:,:)

  ! This parameter controls whether the perpendicular term in the
  ! right-hand-side is treated expanding the divergence (which is the
  ! alternative implementation) or not.
  logical, parameter :: use_alternative_impl = .false.
  ! ifort bug: see https://software.intel.com/en-us/forums/topic/541418
  !integer, parameter :: &
  !  jend =  1*count( (/use_alternative_impl/) ) &
  !        + 2*count( (/.not.use_alternative_impl/) )
  ! workaround for the bug
  integer :: jend

  integer :: i, j, l, isb, isl
  real(wp) :: f_phi(2,bme%m,2), & ! the vector f^\Phi in the rhs
     wga(bme%m), gradp_phy(2,bme%pk,bme%m), nn(bme%m), gn(2,1,bme%m), &
     dne(bme%pk)
  ! same quantities, for the side values
  real(wp) :: f_phis(2,bme%ms,2), wgas(bme%ms), f_phin(bme%ms), &
     nns(bme%ms), gns(2,1,bme%ms)
  real(wp) :: tmp_aak(bme%pk,bme%pk) ! used to call loc_bcs

  character(len=*), parameter :: &
    this_sub_name = 'loc_poisson_rhs'

   ! workaroung for the ifort bug
   jend =  1*count( (/use_alternative_impl/) ) &
         + 2*count( (/.not.use_alternative_impl/) )

   !--------------------------------------------------------------------
   ! 1) Volume terms

   ! scale the Gaussian weights, including the metric factor
   wga = dx%evg(e,bme) * e%me%scale_wg(bme%xig,bme%wg)

   ! To reduce the roundoff errors, we compute the gradient in the
   ! reference element and then transform it in physical space. Also,
   ! we subtract the mean value of the density, which obviously does
   ! not alter the gradient.
   dne = ne - sum(ne)/real(bme%pk,wp) ! local variations
   do l=1,bme%m
     gn(:,1,l) = matmul( bme%gradp(:,:,l) , dne )
   enddo

   ! scale the gradients to physical space
   gradp_phy = e%me%scale_gradp( bme%xig , bme%gradp )
   gn        = e%me%scale_gradp( bme%xig , gn        )

   ! evaluate the finite element field
   nn = matmul( ne , bme%p )

   associate( ed => esdata%edata )

   f_phi = compute_f_phi( bme%m, nn,gn(:,1,:), const_e,const_z, &
               esdata%sigma_par(ed%te,tc%phc) , ed%b,ed%c_bfb , &
                     ed%te,ed%gte , ed%ti,ed%gti , ed%curl_c_bfb)

   end associate

   bk = 0.0_wp
   if(.not.use_alternative_impl) then
     do j=1,2 ! two contributions: parellel and perpendicular
       do l=1,bme%m
         do i=1,bme%pk
           bk(i,j) = bk(i,j) &
              + wga(l)*dot_product( f_phi(:,l,j) , gradp_phy(:,i,l) )
         enddo
       enddo
     enddo
   else
     do l=1,bme%m
       do i=1,bme%pk
         bk(i,1) = bk(i,1) & ! parallel term: same as before
            + wga(l)*dot_product( f_phi(:,l,1) , gradp_phy(:,i,l) )
         bk(i,2) = bk(i,2) & ! perpendicular term: no divergence
            + wga(l) * f_phi(1,l,2)*bme%p(i,l)
       enddo
     enddo
   endif

   !--------------------------------------------------------------------
   ! 2) Boundary terms, if present

   if(associated(be%p)) then
     do isb=1,be%p%nbs
       isl = be%p%bs(isb)%p%s%isl(1) ! local side index on element e
       if(be%p%bs(isb)%p%bc.ne.b_ddc) then ! nothing to do for ddc

         ! Notice: using the side node ordering
         associate( ll => bme%stab(e%pi(isl),:) )

         wgas = dx%evgs(e,bme,isl,be%p) * bme%wgs*e%s(isl)%p%a
         wgas = wgas(ll) ! dx%evgs uses the element ordering -> reorder

         do l=1,bme%ms
           gns(:,1,l) = matmul( bme%gradpb(:,:,ll(l),isl) , dne )
         enddo
         gns = e%me%scale_gradp( bme%xigb(:,ll,isl) , gns )

         nns = matmul( ne , bme%pb(:,ll,isl) )

         associate( es => esdata%sdata(isl) )

         f_phis = compute_f_phi( bme%ms , nns,gns(:,1,:) ,      &
             const_e,const_z , esdata%sigma_par(es%te,tc%phc) , &
                    es%b,es%c_bfb , es%te,es%gte , es%ti,es%gti )

         end associate

         ! compute the normal component
         do j=1,jend
           f_phin = matmul( e%n(:,isl) , f_phis(:,:,j) )

           do l=1,bme%ms
             do i=1,bme%pk
               bk(i,j) = bk(i,j) &
                        - wgas(l) * f_phin(l) * bme%pb(i,ll(l),isl)
             enddo
           enddo
         enddo

         end associate

       endif
     enddo

     ! Possible terms because of nonhomogeneous bcs: these are
     ! included in the first column, which always exists.
     !
     ! Note: tmp_aak is not used anymore (see loc_poisson_mat)
     tmp_aak = 0.0_wp
     call loc_bcs( tmp_aak,bk(:,1) , e,bme,be%p ,1.0_wp, dx,sigma,lbcs )
   endif

 contains
  
  ! This function computes f^\Phi in the right-hand-side and can be
  ! used both for the internal nodes and the boundary ones.
  pure function compute_f_phi(mm, nn,gn, const_e,const_z,              &
               sigma_p,b,c_bfb, te,gte,ti,gti, curl_c_bfb) result(f_phi)
   integer, intent(in) :: mm       ! number of points
   real(wp), intent(in) :: nn(:)   ! density
   real(wp), intent(in) :: gn(:,:) ! density gradient
   real(wp), intent(in) :: const_e, const_z
   real(wp), intent(in) :: sigma_p(:), b(:,:), c_bfb(:,:)
   real(wp), intent(in), optional :: curl_c_bfb(:,:)
   real(wp), intent(in) :: te(:), gte(:,:), ti(:), gti(:,:)
   real(wp) :: f_phi(2,mm,2) ! last index separates the contributions

   ! This value is used to avoid dividing by zero density
   real(wp), parameter :: neg_density_toll = 1.0e8_wp

   integer :: l
   real(wp) :: fnn(mm), gp(2,mm)

    ! 1) first the sigma_parallel term

    fnn = 1.0_wp/max( nn , neg_density_toll )

    do l=1,mm
      f_phi(:,l,1) = (1.0_wp/const_e) * sigma_p(l) * b(1:2,l)          &
                    * dot_product( b(1:2,l) ,                          &
                      (1.0_wp+0.71_wp)*gte(:,l) + te(l)*fnn(l)*gn(:,l) )
    enddo

    ! 2) then the Bx term

    ! total pressure gradient
    do l=1,mm
      gp(:,l) = nn(l) * (gti(:,l)+const_z*gte(:,l)) &
               +  (ti(l)+const_z*te(l)) * gn(:,l)
    enddo

    if(.not.use_alternative_impl) then
      do l=1,mm
        ! since we are only interested in the first two components, we
        ! compute the cross product explicitly, without using the
        ! predefined functions
        f_phi(1,l,2) = - c_bfb(3,l) * gp(2,l)
        f_phi(2,l,2) =   c_bfb(3,l) * gp(1,l)
      enddo
    else
      ! in this case, only the first row is significant
      f_phi(:,:,2) = -huge(1.0_wp)
      if(present(curl_c_bfb)) then
        do l=1,mm
          f_phi(1,l,2) = -dot_product( curl_c_bfb(1:2,l) , gp(:,l) )
        enddo
      endif
    endif

  end function compute_f_phi

 end subroutine loc_poisson_rhs

!-----------------------------------------------------------------------

 pure function s_coeff_evg(coeff,e,bme) result(s)
  class(c_s_coeff),     intent(in) :: coeff !< coefficient
  type(t_2de),          intent(in) :: e     !< element
  class(c_h2d_me_base), intent(in) :: bme   !< base master element
  real(wp) :: s(bme%m)

   ! this function must be overridden
   s = -huge(s)
 end function s_coeff_evg

 pure function s_coeff_evgs(coeff,e,bme,isl,be) result(s)
  class(c_s_coeff),     intent(in) :: coeff !< coefficient
  type(t_2de),          intent(in) :: e     !< element
  class(c_h2d_me_base), intent(in) :: bme   !< base master element
  integer,              intent(in) :: isl   !< local side index
  !> boundary element
  !!
  !! The reason why this argument is optional is that this functions
  !! makes sense both for internal sides and for boundary sides, but a
  !! \c t_b_2de object is defined only for boundary sides. When this
  !! function is called without this argument, one can assume that it
  !! is being called for an internal side.
  type(t_b_2de),        intent(in), optional :: be
  real(wp) :: s(bme%ms)

   ! this function must be overridden
   s = -huge(s)
 end function s_coeff_evgs

!-----------------------------------------------------------------------

 pure function v_coeff_evg(coeff,e,bme) result(v)
  class(c_v_coeff),     intent(in) :: coeff !< coefficient
  type(t_2de),          intent(in) :: e     !< element
  class(c_h2d_me_base), intent(in) :: bme   !< base master element
  real(wp) :: v(2,bme%m)

   ! this function must be overridden
   v = -huge(v)
 end function v_coeff_evg

 !> Divergence in physical space
 !!
 !! Since it can be useful, we include among the arguments the
 !! gradient of the basis functions, <em>assumed to be in physical
 !! space</em>. Typically, during the computation of the local FE
 !! matrices, this quantity is needed anyway, so it is better to reuse
 !! it.
 pure function v_coeff_divg(coeff,e,bme,gradp_phy) result(div)
  class(c_v_coeff),     intent(in) :: coeff !< coefficient
  type(t_2de),          intent(in) :: e     !< element
  class(c_h2d_me_base), intent(in) :: bme   !< base master element
  real(wp),             intent(in) :: gradp_phy(:,:,:)
  real(wp) :: div(bme%m)

   ! this function must be overridden
   div = -huge(div)
 end function v_coeff_divg

 pure function v_coeff_vng(coeff,e,bme,isl,be) result(vn)
  class(c_v_coeff),     intent(in) :: coeff !< coefficient
  type(t_2de),          intent(in) :: e     !< element
  class(c_h2d_me_base), intent(in) :: bme   !< base master element
  !> boundary element (see the comments in \c s_coeff_evgs)
  type(t_b_2de),        intent(in), optional :: be
  integer,              intent(in) :: isl   !< local side index
  real(wp) :: vn(bme%ms)

   ! this function must be overridden
   vn = -huge(vn)
 end function v_coeff_vng

!-----------------------------------------------------------------------

 pure function t_coeff_evg(coeff,e,bme) result(t)
  class(c_t_coeff),     intent(in) :: coeff !< coefficient
  type(t_2de),          intent(in) :: e     !< element
  class(c_h2d_me_base), intent(in) :: bme   !< base master element
  real(wp) :: t(2,2,bme%m)

   ! this function must be overridden
   t = -huge(t)
 end function t_coeff_evg

 !> See also \c s_coeff_evgs
 pure function t_coeff_evgs(coeff,e,bme,isl,be) result(t)
  class(c_t_coeff),     intent(in) :: coeff !< coefficient
  type(t_2de),          intent(in) :: e     !< element
  class(c_h2d_me_base), intent(in) :: bme   !< base master element
  integer,              intent(in) :: isl   !< local side index
  type(t_b_2de),        intent(in), optional :: be
  real(wp) :: t(2,2,bme%ms)

   ! this function must be overridden
   t = -huge(t)
 end function t_coeff_evgs

!-----------------------------------------------------------------------

 pure function lbcs_coeff_cdirneu(coeff,breg,x) result(c)
  class(c_lbcs_coeff), intent(in) :: coeff  !< coefficient
  integer,             intent(in) :: breg   !< boundary region marker
  real(wp),            intent(in) :: x(:,:) !< coordinates
  real(wp) :: c(size(x,2))

   ! this function must be overridden
   c = -huge(c)
 end function lbcs_coeff_cdirneu

!-----------------------------------------------------------------------

end module mod_sol_locmat

