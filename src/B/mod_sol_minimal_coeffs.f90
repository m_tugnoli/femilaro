!> Collect all the coefficients used in the minimal SOL model
!!
!! In principle, these coefficients could be part of \c
!! mod_sol_minimal_ode, however to avoid having a very large module
!! they are collected here.
!!
!! \todo Once it is supported, one could considering transforming this
!! module in a sumbmodule.
!!
!! \section sol_minimal_coeffs_comments About this module
!!
!! In the code, there are essentially two specifications of the
!! problem coefficients: \c mod_sol_testcases includes the
!! "physical" description of the problem, while \c mod_sol_locmat
!! describes the coefficients from the viewpoint of the numerical
!! discretization (elements, quadrature nodes and so on). The main
!! purpose of this module is translating from the "physical"
!! representation, provided by the user, to the numerical one.
!<----------------------------------------------------------------------
module mod_sol_minimal_coeffs

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_h2d_master_el, only: &
   mod_h2d_master_el_initialized, &
   c_h2d_me_geom, c_h2d_me_base

 use mod_h2d_grid, only: &
   mod_h2d_grid_initialized, &
   t_2dv, t_2ds, t_2de

 use mod_h2d_bcs, only: &
   mod_h2d_bcs_initialized, &
   t_b_2dv, t_b_2ds, t_b_2de

 use mod_sol_testcases, only: &
   mod_sol_testcases_initialized, &
   t_phc, tc

 use mod_sol_locmat, only: &
   mod_sol_locmat_initialized, &
   c_s_coeff, c_v_coeff, c_t_coeff, c_lbcs_coeff

 use mod_sol_minimal_data, only: &
   mod_sol_minimal_data_initialized, &
   t_sol_minimal_data, sol_data

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_sol_minimal_coeffs_constructor, &
   mod_sol_minimal_coeffs_destructor,  &
   mod_sol_minimal_coeffs_initialized, &
   t_dx, &
   t_dens_v_coeff, t_dens_eps_coeff, t_dens_lbcs_coeff, &
   t_epot_sigma_coeff, t_epot_lbcs_coeff, &
   epot_sigma_coeff_sigma_par

 private

!-----------------------------------------------------------------------

! Module types

 !> Volume element for the cylindrical coordinates
 !!
 !! When working in the poloidal plane, the two-dimensional area
 !! element must be corrected by \f$R\f$, see n_reduct_2D "here". This
 !! function simply returns the factor \f$R(x) = x_1\f$.
 type, extends(c_s_coeff) :: t_dx
 contains
  procedure, pass(coeff) :: evg   => dx_evg
  procedure, pass(coeff) :: evgs  => dx_evgs
 end type t_dx

 !-----------------------------------

 !> See \c t_dens_v_coeff
 type :: t_r_mat
  real(wp), allocatable :: m(:,:)
 end type t_r_mat

 !> Velocity coefficient, continuity equation
 !!
 !! Most of the required data are accessed with pointers to the fields
 !! of the SOL diagnostics.
 type, extends(c_v_coeff) :: t_dens_v_coeff
  real(wp), pointer :: vn(:,:)    => null()
  real(wp), pointer :: div_vn(:)  => null()
  !> Interpolation matrices to compute \f${\bf v}_n\cdot{\bf
  !! n}_{\partial\Omega}\f$.
  !!
  !! For the boundary sides, we precompute a matrix used to obtain the
  !! normal velocity from the values of the previous field
  !! \field_fref{mod_sol_minimal_ode,t_dens_v_coeff,vn}. This array
  !! must be indexed with the side index, and is defined only for the
  !! boundary sides: <tt>is.gt.ni</tt>.
  !!
  !! It can be verified that this interpolation amounts to
  !! \f{displaymath}{
  !!  {\bf n}_{\partial\Omega}^T
  !!  \left[ \left({\bf v}_n\right)_i(x_l) \right]
  !!  \, \underbrace{\left[ w_l\phi_i({\bf x}^G_l) \right]^T
  !!  M_K^{-T}
  !!     \left[ \phi_i({\bf x}_{\partial\Omega}) \right] }_{\tt
  !!  n\_vn\_interp},
  !! \f}
  !! where \f$M_K\f$ is the local mass matrix and \f${\bf
  !! x}_{\partial\Omega}\f$ are the points where the velocity must be
  !! interpolated, i.e. the quadrature nodes on the side.
  type(t_r_mat), allocatable :: n_vn_interp(:)
 contains
  procedure, pass(coeff) :: evg   => dens_v_coeff_evg
  procedure, pass(coeff) :: divg  => dens_v_coeff_divg
  procedure, pass(coeff) :: vng   => dens_v_coeff_vng
 end type t_dens_v_coeff

 !> Coefficient \f$\underline{\underline{\varepsilon}}_n\f$ 
 !!
 !! No need to make our life too difficult, here we directly retrieve
 !! the required information from the module variable \c sol_data.
 type, extends(c_t_coeff) :: t_dens_eps_coeff
 contains
  procedure, pass(coeff) :: evg  => dens_eps_coeff_evg
  procedure, pass(coeff) :: evgs => dens_eps_coeff_evgs
 end type t_dens_eps_coeff

 !> Boundary conditions for the density equation
 !!
 !! We use Dirichlet conditions on both internal and external
 !! boundaries, using the same values as for the initial condition.
 type, extends(c_lbcs_coeff) :: t_dens_lbcs_coeff
 contains
  procedure, pass(coeff) :: bc   => dens_lbcs_coeff_bc
  procedure, pass(coeff) :: cdir => dens_lbcs_coeff_cdirneu
  procedure, pass(coeff) :: cneu => dens_lbcs_coeff_cdirneu
 end type t_dens_lbcs_coeff

 !-----------------------------------

 !> Coefficient \f$\underline{\underline{\sigma}}\f$ 
 type, extends(c_t_coeff) :: t_epot_sigma_coeff
 contains
  procedure, pass(coeff) :: evg  => epot_sigma_coeff_evg
  procedure, pass(coeff) :: evgs => epot_sigma_coeff_evgs
 end type t_epot_sigma_coeff

 !> Boundary conditions for \f$\Phi\f$
 !!
 !! We use homogeneous Dirichlet on the internal boundary and
 !! homogeneous Neumann on the external one.
 type, extends(c_lbcs_coeff) :: t_epot_lbcs_coeff
 contains
  procedure, pass(coeff) :: bc   => epot_lbcs_coeff_bc
  procedure, pass(coeff) :: cdir => epot_lbcs_coeff_cdirneu
  procedure, pass(coeff) :: cneu => epot_lbcs_coeff_cdirneu
 end type t_epot_lbcs_coeff

 !----------------------------------------------------------------------

! Module variables

 integer, parameter :: &
   !> boundary index for the internal boundary
   !!
   !! \todo This is probably not the best place to define this, since
   !! it could be test-case dependent, however for the time being this
   !! seems the best solution without introducing useless
   !! complications in the test case modules.
   breg_internal = 1, &
   breg_external = 2

 ! public members
 logical, protected ::               &
   mod_sol_minimal_coeffs_initialized = .false.
 ! private members
 character(len=*), parameter :: &
   this_mod_name = 'mod_sol_minimal_coeffs'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_sol_minimal_coeffs_constructor()
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if(  (mod_kinds_initialized.eqv..false.) .or. &
     (mod_messages_initialized.eqv..false.) .or. &
(mod_h2d_master_el_initialized.eqv..false.) .or. &
     (mod_h2d_grid_initialized.eqv..false.) .or. &
      (mod_h2d_bcs_initialized.eqv..false.) .or. &
(mod_sol_testcases_initialized.eqv..false.) .or. &
   (mod_sol_locmat_initialized.eqv..false.) .or. &
(mod_sol_minimal_data_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_sol_minimal_coeffs_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   mod_sol_minimal_coeffs_initialized = .true.
 end subroutine mod_sol_minimal_coeffs_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_sol_minimal_coeffs_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_sol_minimal_coeffs_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_sol_minimal_coeffs_initialized = .false.
 end subroutine mod_sol_minimal_coeffs_destructor

!-----------------------------------------------------------------------

 pure function dx_evg(coeff,e,bme) result(s)
  class(t_dx),          intent(in) :: coeff
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  real(wp) :: s(bme%m)

   s = tc%dx( e%me%map( bme%xig ) )

 end function dx_evg

 pure function dx_evgs(coeff,e,bme,isl,be) result(s)
  class(t_dx),          intent(in) :: coeff
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  integer,              intent(in) :: isl
  type(t_b_2de),        intent(in), optional :: be
  real(wp) :: s(bme%ms)

   s = tc%dx( e%me%map( bme%xigb(:,:,isl) ) )

 end function dx_evgs

!-----------------------------------------------------------------------

 !> Poloidal velocity for the density equation
 !!
 !! Notice that this velocity includes the complete parallel velocity
 !! and the drift terms <em>not depending on \f$\nabla n\f$</em>; the
 !! drifts proportional to the density gradient are included in the
 !! diffusion term.
 pure function dens_v_coeff_evg(coeff,e,bme) result(v)
  class(t_dens_v_coeff), intent(in) :: coeff
  type(t_2de),           intent(in) :: e
  class(c_h2d_me_base),  intent(in) :: bme
  real(wp) :: v(2,bme%m)

   v = coeff%vn
 end function dens_v_coeff_evg

 !> Velocity on the element boundary
 !!
 !! See also
 !! \field_fref{mod_sol_minimal_ode,t_dens_v_coeff,n_vn_interp}. 
 pure function dens_v_coeff_vng(coeff,e,bme,isl,be) result(vn)
  class(t_dens_v_coeff), intent(in) :: coeff
  type(t_2de),           intent(in) :: e
  class(c_h2d_me_base),  intent(in) :: bme
  integer,               intent(in) :: isl
  type(t_b_2de),         intent(in), optional :: be
  real(wp) :: vn(bme%ms)

   ! due to the way n_vn_interp%m is computed, the quad. nodes are
   ! already ordered following the element ordering
   vn = matmul( matmul( e%n(:,isl) , coeff%vn ) , &
                coeff%n_vn_interp( e%is(isl) )%m )

 end function dens_v_coeff_vng

 !> Divergence of \f${\bf v}_n\f$
 pure function dens_v_coeff_divg(coeff,e,bme,gradp_phy) result(div)
  class(t_dens_v_coeff), intent(in) :: coeff
  type(t_2de),           intent(in) :: e
  class(c_h2d_me_base),  intent(in) :: bme
  real(wp),              intent(in) :: gradp_phy(:,:,:)
  real(wp) :: div(bme%m)

   div = coeff%div_vn
 end function dens_v_coeff_divg

!-----------------------------------------------------------------------

 !> Evaluate \f$\underline{\underline{\varepsilon}}_n\f$ 
 !!
 !! Notice that in the poloidal plane we have
 !! \f{displaymath}{
 !!  \mathcal{B}_{\times} = \left[\begin{array}{cc}
 !!   0 & B_{\varphi} \\ -B_{\varphi} & 0 \end{array}\right].
 !! \f}
 pure function dens_eps_coeff_evg(coeff,e,bme) result(t)
  class(t_dens_eps_coeff), intent(in) :: coeff
  type(t_2de),           intent(in) :: e
  class(c_h2d_me_base),  intent(in) :: bme
  real(wp) :: t(2,2,bme%m)

   associate( ed => sol_data%e_data(e%i) ,                  &
              adp => tc%phc%adiff_p , adn => tc%phc%adiff_n )

   call dens_eps_coeff_evgs_internals( t , ed%ti , ed%b , adp,adn )

   end associate

 end function dens_eps_coeff_evg

 pure function dens_eps_coeff_evgs(coeff,e,bme,isl,be) result(t)
  class(t_dens_eps_coeff), intent(in) :: coeff
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  integer,              intent(in) :: isl
  type(t_b_2de),        intent(in), optional :: be
  real(wp) :: t(2,2,bme%ms)

   associate( sd => sol_data%s_data(e%is(isl)) ,            &
              adp => tc%phc%adiff_p , adn => tc%phc%adiff_n )

   call dens_eps_coeff_evgs_internals( t , sd%ti , sd%b , adp,adn )

   end associate

   ! This uses the side ordering, we need a reorder
   t = t( :,: , bme%stab(e%pi(isl),:) )

 end function dens_eps_coeff_evgs

 pure subroutine dens_eps_coeff_evgs_internals(t,ti,b , adiff_p,adiff_n)
  real(wp), intent(in)  :: ti(:), b(:,:)
  real(wp), intent(in)  :: adiff_p, adiff_n
  real(wp), intent(out) :: t(:,:,:)

   ! Note: because of the way we store vectors, B_\phi is -b(3)

   t(1,1,:) = &
      ( adiff_p*ti + adiff_n ) * ( 1.0_wp -   b(1,:)**2  )

   t(2,1,:) = &
      ( adiff_p*ti + adiff_n ) * (        - b(1,:)*b(2,:))

   t(1,2,:) = t(2,1,:) ! symmetric tensor

   t(2,2,:) = &
      ( adiff_p*ti + adiff_n ) * ( 1.0_wp -   b(2,:)**2  )

 end subroutine dens_eps_coeff_evgs_internals

!-----------------------------------------------------------------------

 pure subroutine dens_lbcs_coeff_bc(bc,btype,coeff,breg)
  class(t_dens_lbcs_coeff), intent(in) :: coeff
  integer, intent(in)  :: breg
  integer, intent(out) :: bc, btype
   
   call tc%dens_bctp( bc,btype , breg )

 end subroutine dens_lbcs_coeff_bc

 pure function dens_lbcs_coeff_cdirneu(coeff,breg,x) result(c)
  class(t_dens_lbcs_coeff), intent(in) :: coeff  !< coefficient
  integer,  intent(in) :: breg   !< boundary region marker
  real(wp), intent(in) :: x(:,:) !< coordinates
  real(wp) :: c(size(x,2))

   c = tc%dens_bcvl(breg,x)

 end function dens_lbcs_coeff_cdirneu
 
!-----------------------------------------------------------------------

 !> Evaluate \f$\underline{\underline{\sigma}}\f$ 
 pure function epot_sigma_coeff_evg(coeff,e,bme) result(t)
  class(t_epot_sigma_coeff), intent(in) :: coeff
  type(t_2de),           intent(in) :: e
  class(c_h2d_me_base),  intent(in) :: bme
  real(wp) :: t(2,2,bme%m)

   associate( ed => sol_data%e_data(e%i) )

   call epot_sigma_coeff_evgs_internals( t , ed%te , ed%b , tc%phc )

   end associate

 end function epot_sigma_coeff_evg

 pure function epot_sigma_coeff_evgs(coeff,e,bme,isl,be) result(t)
  class(t_epot_sigma_coeff), intent(in) :: coeff
  type(t_2de),          intent(in) :: e
  class(c_h2d_me_base), intent(in) :: bme
  integer,              intent(in) :: isl
  type(t_b_2de),        intent(in), optional :: be
  real(wp) :: t(2,2,bme%ms)

   associate( sd => sol_data%s_data(e%is(isl)) )

   call epot_sigma_coeff_evgs_internals( t , sd%te , sd%b , tc%phc )

   end associate

   ! This uses the side ordering, we need a reorder
   t = t( :,: , bme%stab(e%pi(isl),:) )

 end function epot_sigma_coeff_evgs

 !> Compute the coefficients of the potential equation
 !!
 !! The definition of the coefficients \f$\sigma_{\parallel}\f$ and
 !! \f$\sigma_{\perp}\f$ is given interms of
 !! \f{displaymath}{
 !!  \tau_e = \frac{3\sqrt{m_e}}{4\sqrt{2\pi}\lambda e^4}
 !!  \frac{T_e^{3/2}}{n}
 !! \f}
 !! as
 !! \f{displaymath}{
 !!  \sigma_{\perp} = \frac{e^2}{m_e} n \tau_e, \qquad
 !!  \sigma_{\parallel} = 1.96\,\sigma_{\perp}.
 !! \f}
 !! Notice that \f$n\f$ can be simplified in the computation of the
 !! two coefficients. \f$\sigma_{\perp}\f$ is not used in the minimal
 !! SOL model, since it does not appear in the potential equation.
 !! Instead, the perpendicular conductivity is given by the
 !! <em>anomalous</em> value
 !! \f{displaymath}{
 !!  \sigma_{\rm an} = \epsilon \sigma_{\perp},
 !! \f}
 !! where \f$\epsilon\f$ is a test case specific coefficient.
 pure subroutine epot_sigma_coeff_evgs_internals(t,te,b,phc)
  real(wp), intent(in)  :: te(:)  ! electron temperature
  real(wp), intent(in)  :: b(:,:) ! unit vector b
  type(t_phc), intent(in) :: phc
  real(wp), intent(out) :: t(:,:,:)

  real(wp) :: sigma_an(size(b,2)), sigma_para(size(b,2))

   ! Sometimes, only sigma_parallel is required, so it is useful
   ! having a separate function which computes this term. Then we can
   ! reconstruct sigma_perp dividing by 1.96.
   sigma_para = epot_sigma_coeff_sigma_par(te,phc)

   ! Reconstruct the perpendicular conductivity dividing by 1.96
   sigma_an   = (1.0_wp/1.96_wp) * phc%sigma_an_eps * sigma_para

   ! Assemble the two contribution in the diffusion tensor
   t(1,1,:) = sigma_para*b(1,:)**2 + sigma_an*(1.0_wp-b(1,:)**2)
   t(2,1,:) = (sigma_para-sigma_an)*b(1,:)*b(2,:)
   t(1,2,:) = t(2,1,:) ! symmetric tensor
   t(2,2,:) = sigma_para*b(2,:)**2 + sigma_an*(1.0_wp-b(2,:)**2)

 end subroutine epot_sigma_coeff_evgs_internals

!-----------------------------------------------------------------------

 !> Compute \f$\sigma_{\parallel}\f$
 !!
 !! See \c epot_sigma_coeff_evgs_internals for a description of this
 !! function.
 pure function epot_sigma_coeff_sigma_par(te , phc) result(sp)
  real(wp),    intent(in) :: te(:)
  type(t_phc), intent(in) :: phc
  real(wp) :: sp(size(te))

   ! In this computation, the density n has been simplified.
   sp = 1.96_wp * (phc%e**2/phc%me) * phc%tau_e * sqrt(te**3)

 end function epot_sigma_coeff_sigma_par

!-----------------------------------------------------------------------

 pure subroutine epot_lbcs_coeff_bc(bc,btype,coeff,breg)
  class(t_epot_lbcs_coeff), intent(in) :: coeff
  integer, intent(in)  :: breg
  integer, intent(out) :: bc, btype
   
   call tc%epot_bctp( bc,btype , breg )

 end subroutine epot_lbcs_coeff_bc

 pure function epot_lbcs_coeff_cdirneu(coeff,breg,x) result(c)
  class(t_epot_lbcs_coeff), intent(in) :: coeff
  integer,  intent(in) :: breg
  real(wp), intent(in) :: x(:,:)
  real(wp) :: c(size(x,2))

   c = tc%epot_bcvl(breg,x)

 end function epot_lbcs_coeff_cdirneu

!-----------------------------------------------------------------------

end module mod_sol_minimal_coeffs

