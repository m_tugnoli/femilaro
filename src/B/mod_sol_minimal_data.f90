!> Collect all the data arrays used in the minimal SOL model
!!
!! The main reason for having these objects in a separate module is
!! making them available for all the modules describing the mininal
!! SOL model, such as \c mod_sol_minimal_coeffs.
!!
!! \todo Once it is supported, one could considering transforming this
!! module in a sumbmodule.
!<----------------------------------------------------------------------
module mod_sol_minimal_data

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_octave_io, only: &
   mod_octave_io_initialized, &
   write_octave

 use mod_h2d_grid, only: &
   mod_h2d_grid_initialized, &
   t_h2d_grid

 use mod_h2d_base, only: &
   mod_h2d_base_initialized, &
   t_h2d_base

 use mod_sol_state, only: & 
   mod_sol_state_initialized, &
   t_sol_state, t_sol_diags

 use mod_sol_testcases, only: &
   mod_sol_testcases_initialized, &
   t_phc, tc

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_sol_minimal_data_constructor, &
   mod_sol_minimal_data_destructor,  &
   mod_sol_minimal_data_initialized, &
   t_sol_minimal_data, sol_data, &
   t_sol_minimal_diags, new_sol_minimal_diags, &
   write_octave, clear
 
 private

!-----------------------------------------------------------------------

! Module types and parameters

 !-----------------------------------

 !> See \c t_sol_minimal_data
 type :: t_sol_se_data
  !> \f${\bf b}\f$ at the quadrature nodes, three components
  real(wp), allocatable :: b(:,:)
  !> \f$\nabla{\bf b}\f$ at the quadrature nodes, three components
  real(wp), allocatable :: gradb(:,:,:)
  !> \f$\nabla\cdot{\bf b}\f$ at the quadrature nodes
  real(wp), allocatable :: divb(:)
  !> \f${\bf B}\f$ at the quadrature nodes, three components
  real(wp), allocatable :: bb(:,:)
  !> \f$\nabla{\bf B}\f$ at the quadrature nodes, three components
  real(wp), allocatable :: gradbb(:,:,:)
  !> \f$B\f$ at the quadrature nodes
  real(wp), allocatable :: sbb(:)
  !> \f$\nabla B\f$ at the quadrature nodes, three components
  real(wp), allocatable :: gradsbb(:,:)
  !> \f$\frac{c}{B}{\bf b}\f$ at the quadrature nodes
  real(wp), allocatable :: c_bfb(:,:)
  !> \f$\nabla\times\left(\frac{c}{B}{\bf b}\right)\f$
  real(wp), allocatable :: curl_c_bfb(:,:)
  !> \f$T_{\rm i}\f$ at the quad. nodes
  real(wp), allocatable :: ti(:)
  !> \f$T_{\rm e}\f$ at the quad. nodes
  real(wp), allocatable :: te(:)
  !> \f$\nabla T_{\rm i}\f$ at the quad. nodes
  real(wp), allocatable :: gradti(:,:)
  !> \f$\nabla T_{\rm e}\f$ at the quad. nodes
  real(wp), allocatable :: gradte(:,:)
 end type t_sol_se_data

 !> SOL data
 !!
 !! Constant data which can be precomputed for the whole grid. The
 !! organization is similar to \c t_sol_minimal_diags; the reason for
 !! not including these fields in the diagnostics is that they are
 !! constant in time.
 type :: t_sol_minimal_data
  !> side data
  type(t_sol_se_data), allocatable :: s_data(:)
  !> element data
  type(t_sol_se_data), allocatable :: e_data(:)
 end type t_sol_minimal_data

 !-----------------------------------

 !> Element local diagnostics
 !!
 !! Some vectors have only one or two independent components, such as
 !! \f$v_{\parallel}{\bf b}\f$ and \f${\bf v}_d\f$, respectively.
 !! Nevertheless, we store here the three-dimensional representation
 !! with respect to \f$\tilde{\bf e}_R, \tilde{\bf e}_z, \tilde{\bf
 !! e}_{\varphi}\f$ (see \ref vector_implementation "here" for more
 !! details) because this is unique for all the vectors and it enters
 !! the computation of the material derivative.
 type :: t_sol_e_diags
  !> \f$v_{\parallel}\f$
  real(wp), allocatable :: vp(:,:)
  !> \f${\bf E} \times {\bf B}\f$ drift
  real(wp), allocatable :: exb(:,:)
  !> diamagnetic drift \f$c\frac{{\bf B}\times\nabla p_{\rm i}}{Zen
  !! B^2}\f$
  real(wp), allocatable :: diam_d(:,:)
  !> anomalous diffusion density flux
  !! \f$-\mathcal{D}^\perp_{d,n}\frac{\nabla n}{n} - 
  !! \mathcal{D}^\perp_{d,p}\frac{\nabla p}{n}\f$
  real(wp), allocatable :: nabla_d(:,:)
  !> \f$p_{\rm i}\f$ at the quad. nodes
  real(wp), allocatable :: pi(:)
  !> \f$p_{\rm e}\f$ at the quad. nodes
  real(wp), allocatable :: pe(:)
  !> \f${\bf v}_n\f$
  !!
  !! This is the velocity appearing in the continuity equation,
  !! without the terms proportional to the density gradients. Notice
  !! that we only need the poloidal components.
  real(wp), allocatable :: vn(:,:)
  !> \f$\nabla\cdot{\bf v}_n\f$, see \ref solmin_vdrift "here"
  !!
  !! This is the divergence of the velocity which appears in the
  !! continuity equation, represented as a piecewise constant field.
  !! This quantity is used in the SUPG stabilization of the continuity
  !! equation.
  real(wp), allocatable :: div_vn(:)
 end type t_sol_e_diags

 !> SOL diagnostics for the minimal model
 !!
 !! Element diagnostics are grouped in \c t_sol_el_diags, while nodal
 !! quantities are represented using global arrays.
 type, extends(t_sol_diags) :: t_sol_minimal_diags
  !> element values, at the quadrature nodes
  type(t_sol_e_diags), allocatable :: e_diags(:)
 end type t_sol_minimal_diags

 !-----------------------------------

 interface write_octave
   module procedure write_sol_minimal_diags_struct, & 
     write_sol_e_diags_struct, write_sol_minimal_se_data_struct, &
     write_sol_minimal_data_struct
 end interface 

 interface clear
   module procedure clear_sol_minimal_diags
 end interface

 ! Module variables

 !> SOL data
 !!
 !! We do not use for this variable the \c protected keyword because
 !! sometimes it is useful to point a pointer to it; nevertheless,
 !! nothing outside this module should change \c sol_data.
 type(t_sol_minimal_data), save, target :: sol_data

 logical, protected :: &
   mod_sol_minimal_data_initialized = .false.
 character(len=*), parameter :: &
   this_mod_name = 'mod_sol_minimal_data'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_sol_minimal_data_constructor(grid,base)
  type(t_h2d_grid), intent(in), target :: grid
  type(t_h2d_base), intent(in), target :: base

  integer :: ie, is
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if(  (mod_kinds_initialized.eqv..false.) .or. &
     (mod_messages_initialized.eqv..false.) .or. &
    (mod_octave_io_initialized.eqv..false.) .or. &
     (mod_h2d_grid_initialized.eqv..false.) .or. &
     (mod_h2d_base_initialized.eqv..false.) .or. &
    (mod_sol_state_initialized.eqv..false.) .or. &
(mod_sol_testcases_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_sol_minimal_data_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   ! 1) Define the problem data: side and elements
   allocate( sol_data%s_data(grid%ns) )
   do is=1,grid%ns
     ! To map the side quad. nodes we rely on the first element
     associate( e => grid%s(is)%e(1)%p , isl => grid%s(is)%isl(1) )
     call set_sol_se_data( sol_data%s_data(is) ,     &
         e%me%map( base%e(e%poly)%me%xigb(:,:,isl) ) )
     end associate
   enddo
   allocate( sol_data%e_data(grid%ne) )
   do ie=1,grid%ne
     associate( e => grid%e(ie) )
     call set_sol_se_data( sol_data%e_data(ie) ,     &
         e%me%map( base%e(e%poly)%me%xig           ) )
     end associate
   enddo

   mod_sol_minimal_data_initialized = .true.

 contains

  ! A small subroutine to handle both sides and elements
  pure subroutine set_sol_se_data(sd,x)
   real(wp), intent(in) :: x(:,:) ! quad nodes
   type(t_sol_se_data), intent(out) :: sd

   integer :: mms ! number of quad. nodes

    mms = size(x,2)

    allocate( sd%b(         3,  mms) )
    allocate( sd%gradb(     3,3,mms) )
    allocate( sd%divb(          mms) )
    allocate( sd%bb(        3,  mms) )
    allocate( sd%gradbb(    3,3,mms) )
    allocate( sd%sbb(           mms) )
    allocate( sd%gradsbb(   3,  mms) )
    allocate( sd%curl_c_bfb(3,  mms) )
    call tc%magn_profile( sd%b , sd%gradb , sd%divb ,             &
      sd%bb , sd%gradbb , sd%sbb , sd%gradsbb , sd%curl_c_bfb , x )
    ! include c, which is not included in compute_magnetic_geom
    sd%curl_c_bfb = tc%phc%c * sd%curl_c_bfb

    allocate( sd%c_bfb(3,  mms) )
    sd%c_bfb(1,:) = tc%phc%c/sd%sbb * sd%b(1,:)
    sd%c_bfb(2,:) = tc%phc%c/sd%sbb * sd%b(2,:)
    sd%c_bfb(3,:) = tc%phc%c/sd%sbb * sd%b(3,:)

    allocate( sd%ti(            mms) )
    allocate( sd%te(            mms) )
    allocate( sd%gradti(    2,  mms) )
    allocate( sd%gradte(    2,  mms) )
    call tc%temp_profile( sd%ti,sd%gradti , sd%te,sd%gradte , x )

  end subroutine set_sol_se_data

 end subroutine mod_sol_minimal_data_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_sol_minimal_data_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_sol_minimal_data_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   deallocate( sol_data%s_data )
   deallocate( sol_data%e_data )

   mod_sol_minimal_data_initialized = .false.
 end subroutine mod_sol_minimal_data_destructor

!-----------------------------------------------------------------------

 !> Build a diagnostic object
 subroutine new_sol_minimal_diags(obj,grid,base)
  type(t_h2d_grid), intent(in), target :: grid
  type(t_h2d_base), intent(in), target :: base
  type(t_sol_minimal_diags), intent(out) :: obj

  integer :: ie

   ! electric potential
   allocate( obj%phi%s(grid%nv) )

   ! element diagnostics
   allocate( obj%e_diags(grid%ne) )
   do ie=1,grid%ne
     associate(   ed => obj%e_diags(ie) , &
                poly => grid%e(ie)%poly )
     associate(    m => base%e(poly)%me%m )

     allocate( ed%vp(     3,m) )
     allocate( ed%exb(    3,m) )
     allocate( ed%diam_d( 3,m) )
     allocate( ed%nabla_d(3,m) )
     allocate( ed%pi(       m) )
     allocate( ed%pe(       m) )
     allocate( ed%vn(     2,m) )
     allocate( ed%div_vn(   m) )

     end associate
     end associate
   enddo

 end subroutine new_sol_minimal_diags
 
!-----------------------------------------------------------------------

 pure subroutine clear_sol_minimal_diags(obj)
  type(t_sol_minimal_diags),  intent(inout) :: obj

   deallocate( obj%phi%s , obj%e_diags )
 end subroutine clear_sol_minimal_diags

!-----------------------------------------------------------------------

 subroutine write_sol_minimal_diags_struct(diags,var_name,fu)
  integer, intent(in) :: fu
  type(t_sol_minimal_diags), intent(in) :: diags
  character(len=*), intent(in) :: var_name
 
  character(len=*), parameter :: &
    this_sub_name = 'write_sol_minimal_diags_struct'
 
   write(fu,'(a,a)')    '# name: ',var_name
   write(fu,'(a)')      '# type: struct'
   write(fu,'(a)')      '# length: 2' ! number of fields

   ! field 01 : phi
   write(fu,'(a)')      '# name: phi'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(diags%phi%s,'c','<cell-element>',fu)

   ! field 02 : e_diags
   write(fu,'(a)')      '# name: e_diags'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(diags%e_diags,'<cell-element>',fu)

 end subroutine write_sol_minimal_diags_struct

!-----------------------------------------------------------------------

 subroutine write_sol_e_diags_struct(e_diags,var_name,fu)
  integer, intent(in) :: fu
  type(t_sol_e_diags), intent(in) :: e_diags(:)
  character(len=*), intent(in) :: var_name
 
  integer :: ie
  character(len=*), parameter :: &
    this_sub_name = 'write_sol_e_diags_struct'
 
   write(fu,'(a,a)')    '# name: ',var_name
   write(fu,'(a)')      '# type: struct'
   call write_dims()
   write(fu,'(a)')      '# length: 8' ! number of fields

   ! field 01 : vp
   write(fu,'(a)')      '# name: vp'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do ie=1,size(e_diags)
     call write_octave(e_diags(ie)%vp,'<cell-element>',fu)
   enddo

   ! field 02 : exb
   write(fu,'(a)')      '# name: exb'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do ie=1,size(e_diags)
     call write_octave(e_diags(ie)%exb,'<cell-element>',fu)
   enddo

   ! field 03 : diam_d
   write(fu,'(a)')      '# name: diam_d'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do ie=1,size(e_diags)
     call write_octave(e_diags(ie)%diam_d,'<cell-element>',fu)
   enddo

   ! field 04 : nabla_d
   write(fu,'(a)')      '# name: nabla_d'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do ie=1,size(e_diags)
     call write_octave(e_diags(ie)%nabla_d,'<cell-element>',fu)
   enddo

   ! field 05 : pi
   write(fu,'(a)')      '# name: pi'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do ie=1,size(e_diags)
     call write_octave(e_diags(ie)%pi,'r','<cell-element>',fu)
   enddo

   ! field 06 : pe
   write(fu,'(a)')      '# name: pe'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do ie=1,size(e_diags)
     call write_octave(e_diags(ie)%pe,'r','<cell-element>',fu)
   enddo

   ! field 07 : vn
   write(fu,'(a)')      '# name: vn'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do ie=1,size(e_diags)
     call write_octave(e_diags(ie)%vn,'<cell-element>',fu)
   enddo

   ! field 08 : div_vn
   write(fu,'(a)')      '# name: div_vn'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do ie=1,size(e_diags)
     call write_octave(e_diags(ie)%div_vn,'r','<cell-element>',fu)
   enddo

 contains

  subroutine write_dims()
   write(fu,'(a)')      '# ndims: 2'
   write(fu,'(i0,a,i0)') 1," ",size(e_diags)
  end subroutine write_dims

 end subroutine write_sol_e_diags_struct

!-----------------------------------------------------------------------

 subroutine write_sol_minimal_data_struct(data,var_name,fu)
  integer, intent(in) :: fu
  type(t_sol_minimal_data), intent(in) :: data
  character(len=*), intent(in) :: var_name
 
  character(len=*), parameter :: &
    this_sub_name = 'write_sol_minimal_data_struct'
 
   write(fu,'(a,a)')    '# name: ',var_name
   write(fu,'(a)')      '# type: struct'
   write(fu,'(a)')      '# length: 2' ! number of fields

   ! field 01 : s_data
   write(fu,'(a)')      '# name: s_data'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(data%s_data,'<cell-element>',fu)

   ! field 02 : e_data
   write(fu,'(a)')      '# name: e_data'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(data%e_data,'<cell-element>',fu)

 end subroutine write_sol_minimal_data_struct

!-----------------------------------------------------------------------

 subroutine write_sol_minimal_se_data_struct(se_data,var_name,fu)
  integer, intent(in) :: fu
  type(t_sol_se_data), intent(in) :: se_data(:)
  character(len=*), intent(in) :: var_name
 
  integer :: i
  character(len=*), parameter :: &
    this_sub_name = 'write_sol_minimal_se_data_struct'
 
   write(fu,'(a,a)')    '# name: ',var_name
   write(fu,'(a)')      '# type: struct'
   call write_dims()
   write(fu,'(a)')      '# length: 13' ! number of fields

   ! field 01 : b
   write(fu,'(a)')      '# name: b'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do i=1,size(se_data)
     call write_octave(se_data(i)%b,'<cell-element>',fu)
   enddo

   ! field 02 : gradb
   write(fu,'(a)')      '# name: gradb'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do i=1,size(se_data)
     call write_octave(se_data(i)%gradb,'<cell-element>',fu)
   enddo

   ! field 03 : divb
   write(fu,'(a)')      '# name: divb'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do i=1,size(se_data)
     call write_octave(se_data(i)%divb,'r','<cell-element>',fu)
   enddo

   ! field 04 : bb
   write(fu,'(a)')      '# name: bb'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do i=1,size(se_data)
     call write_octave(se_data(i)%bb,'<cell-element>',fu)
   enddo

   ! field 05 : gradbb
   write(fu,'(a)')      '# name: gradbb'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do i=1,size(se_data)
     call write_octave(se_data(i)%gradbb,'<cell-element>',fu)
   enddo

   ! field 06 : sbb
   write(fu,'(a)')      '# name: sbb'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do i=1,size(se_data)
     call write_octave(se_data(i)%sbb,'r','<cell-element>',fu)
   enddo

   ! field 07 : gradsbb
   write(fu,'(a)')      '# name: gradsbb'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do i=1,size(se_data)
     call write_octave(se_data(i)%gradsbb,'<cell-element>',fu)
   enddo

   ! field 08 : c_bfb
   write(fu,'(a)')      '# name: c_bfb'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do i=1,size(se_data)
     call write_octave(se_data(i)%c_bfb,'<cell-element>',fu)
   enddo

   ! field 09 : curl_c_bfb
   write(fu,'(a)')      '# name: curl_c_bfb'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do i=1,size(se_data)
     call write_octave(se_data(i)%curl_c_bfb,'<cell-element>',fu)
   enddo

   ! field 10 : ti
   write(fu,'(a)')      '# name: ti'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do i=1,size(se_data)
     call write_octave(se_data(i)%ti,'r','<cell-element>',fu)
   enddo

   ! field 11 : te
   write(fu,'(a)')      '# name: te'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do i=1,size(se_data)
     call write_octave(se_data(i)%te,'r','<cell-element>',fu)
   enddo

   ! field 12 : gradti
   write(fu,'(a)')      '# name: gradti'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do i=1,size(se_data)
     call write_octave(se_data(i)%gradti,'<cell-element>',fu)
   enddo

   ! field 13 : gradte
   write(fu,'(a)')      '# name: gradte'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do i=1,size(se_data)
     call write_octave(se_data(i)%gradte,'<cell-element>',fu)
   enddo

 contains

  subroutine write_dims()
   write(fu,'(a)')      '# ndims: 2'
   write(fu,'(i0,a,i0)') 1," ",size(se_data)
  end subroutine write_dims

 end subroutine write_sol_minimal_se_data_struct

!-----------------------------------------------------------------------
end module mod_sol_minimal_data

