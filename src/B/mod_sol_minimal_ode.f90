!>\brief
!!
!! Minimal Braginskii model with three fields.
!!
!! \n
!!
!! We consider here a minimal version of the Braginskii equation with
!! two prognostic quantities, namely ion density and parallel
!! velocity, and one diagnostic, namely the electric potential.
!!
!! \section density_eq Density equation
!!
!! The density equation is, in three spatial dimensions,
!! \f{displaymath}{
!!  \partial_t n
!!  + \nabla\cdot\left( {\bf v} n\right) = S_{n}.
!! \f}
!! The three components of the velocity are then treated separately:
!! <ul>
!!  <li> the parallel component is retained as a problem unknown
!!  <li> the perpendicular component, on the other hand, is modeled
!!  making the drift approximation, so that it becomes a diagnostic
!!  quantity.
!! </ul>
!! The resulting expression is
!! \f{displaymath}{
!!  {\bf v} = v_{\parallel}{\bf b} + {\bf v}_d, \qquad
!!  {\bf v}_d\perp{\bf b}, \quad
!!  {\bf v}_d\parallel{\bf e}^{1,2},
!! \f}
!! so that \f$v_{\parallel} = {\bf v}\cdot{\bf b}\f$. The drift
!! approximation is given by
!! \f{displaymath}{
!!  {\bf v}_d = \underbrace{c\frac{ {\bf B}\times\nabla\Phi }{B^2}}_{
!!  {\bf v}_E} + c\frac{ {\bf B}\times\nabla p_{\rm i} }{ZenB^2}
!!  - \frac{ \mathcal{D}^{\perp}_{d,n} \nabla n }{n}
!!  - \frac{ \mathcal{D}^{\perp}_{d,p} \nabla p_{\rm i} }{n}.
!! \f}
!! Here, \f$p_{\rm i}=nT_{\rm i}\f$ and \f${\bf B}\f$, \f${\bf b}\f$,
!! \f$S_n\f$, \f$\mathcal{D}^{\perp}_{d,n}\f$,
!! \f$\mathcal{D}^{\perp}_{d,p}\f$ and \f$T_{\rm i}\f$ are prescribed
!! quantities, with
!! \f{displaymath}{
!!  \mathcal{D}^{\perp}_{d,n|p} = \tau_{d,n|p}\left( \mathcal{I} - {\bf
!!  b}\otimes{\bf b}\right).
!! \f}
!! The fact that the perpendicular velocity is modeled in terms of the
!! density gradient changes the character of the continuity equation,
!! from an advection problem to an advection-diffusion one (notice
!! that this also affects the way in which the boundary conditions
!! must be provided). In fact, such an equation can be recast as
!! \f{displaymath}{
!!  \partial_t n
!!  + \nabla\cdot\left( {\bf v}_n n \right)
!!  - \nabla\cdot\left( \underline{\underline{\varepsilon}}_n \nabla n
!!  \right) = S_{n},
!! \f}
!! where
!! \f{displaymath}{
!!  \begin{array}{l}
!!   \displaystyle
!!   {\bf v}_n = v_{\parallel}{\bf b} + {\bf v}_E 
!!   + \frac{1}{Ze}T_{\rm i}\nabla\times\left( c\frac{\bf b}{B} \right)
!!   - \mathcal{D}^{\perp}_{d,p} \nabla T_{\rm i}
!!    \\[3mm]
!!   \displaystyle
!!   \underline{\underline{\varepsilon}}_n = 
!!    T_{\rm i} \mathcal{D}^{\perp}_{d,p} +\mathcal{D}^{\perp}_{d,n}.
!!  \end{array}
!! \f}
!! Notice that the diamagnetic drift has been rewritten using \ref
!! mod_sol_minimal_epsn "these" vector identities, so that a term that
!! would initially appear like a diffusion term in \f$n\f$ (but with an
!! antisymmetric "diffusion" coefficient!) is rewritten as an
!! advection term.
!!
!! \note \f$\underline{\underline{\varepsilon}}_n\f$ is a
!! \f$3\times3\f$ tensor, while the gradient has only two nonvanishing
!! components. The toroidal component of
!! \f$\underline{\underline{\varepsilon}}_n\nabla n\f$ is not zero,
!! but by assumption it does not depend on \f$\varphi\f$, so its
!! contribution to the divergence vanishes. This means that we can
!! substitute \f$\underline{\underline{\varepsilon}}_n\f$ with its
!! \f$2\times2\f$ poloidal component.
!!
!! \subsection mod_sol_minimal_epsn The asymmetric "diffusion" tensor
!!
!! The diamagnetic drift in the density equation leads to the
!! following term:
!! \f{displaymath}{
!!  \frac{1}{Ze}\nabla\cdot\left[ c\frac{\bf b}{B}\times\nabla
!!  \underbrace{(T_{\rm i}n)}_{p_{\rm i}}\right] =
!!  \nabla\left( \mathcal{S}\nabla p_{\rm i}\right),
!! \f}
!! where \f$\mathcal{S}\f$ is a skew-symmetric tensor, formally
!! appearing as a diffusion tensor. In fact, this term can be shown to
!! have the effect of a transport term as follows.
!!
!! Consider \f$\nabla\cdot(\mathcal{S}\nabla n)\f$ for a generic
!! skew-symmetric tensor \f$\mathcal{S}\f$. We have
!! \f{displaymath}{
!!  \nabla\cdot(\mathcal{S}\nabla n) = \sum_{i,j} \left[
!!  \frac{\partial \mathcal{S}_{ij}}{\partial x_i} \frac{\partial
!!  n}{\partial x_j} + \mathcal{S}_{ij}\frac{ \partial^2n}{\partial
!!  x_i\partial x_j}\right] = -(\nabla\cdot\mathcal{S}) \cdot \nabla n
!!  = - \nabla\cdot(\nabla\cdot \mathcal{S}\,n),
!! \f}
!! where in the last equality we have used that
!! \f$\nabla\cdot(\nabla\cdot \mathcal{S})=0\f$.
!!
!! Now let
!! \f{displaymath}{
!!  \mathcal{S} = \left[\begin{array}{ccc}
!!   0 & -S_3 & S_2 \\ S_3 & 0 & -S_1 \\ -S_2 & S_1 & 0
!!  \end{array}\right], \qquad
!!  \mathcal{S}{\bf v} = {\bf S}\times{\bf v}.
!! \f}
!! One can verify that \f$\nabla\cdot\mathcal{S} = -\nabla\times{\bf
!! S}\f$, so that the previous identity can be recast as
!! \f{displaymath}{
!!  \nabla\cdot({\bf S}\times\nabla n) = \nabla\times{\bf S} \cdot
!!  \nabla n = \nabla\cdot(\nabla\times{\bf S}\,n).
!! \f}
!!
!! Concerning the numerical discretization, any of these expressions
!! can be used. Treating such a term as an advection term with
!! velocity \f$\nabla\times {\bf S}\f$ however has the advantage that
!! it allows combining it with other advection terms and defining a
!! stabilization which accounts for all the noncoercive terms in the
!! equation.
!!
!! \subsection n_reduct_2D Reduction to a 2D problem
!!
!! Using the fact that the problem is independent from the toroidal
!! angle, the three-dimensional equation can be reduced to a
!! two-dimensional one. This is better done working directly on the
!! weak form of the problem.
!!
!! Upon writing the 3D weak form of the problem, we obtain an equation
!! which is formally identical to the one given \ref sol_bcs "here".
!! Now observe that, for the computations of such integrals, we are
!! free to choose any coordinate system; the most natural choice to
!! exploit the symmetry of the problem is thus using cylindrical
!! coordinates \f$R\f$, \f$\varphi\f$ and \f$z\f$, so that all the
!! quantities (including the computational grid and the test and shape
!! functions) do not depend of \f$\varphi\f$. The next two steps are:
!! <ul>
!!  <li> express the integrands in terms of these coordinates
!!  <li> compute the integrals, using \f$d\underline{x} =
!!  R\,dR\,d\varphi\,dz\f$.
!! </ul>
!! Concerning the computation of the integrals, since the integrands
!! are constant with respect to \f$\varphi\f$, noting that
!! \f{displaymath}{
!!  \int_0^{2\pi} R\,d\varphi = 2\pi R,
!! \f}
!! we simply have to substitute the volume and area elements
!! \f{displaymath}{
!!  d\underline{x} \mapsto 2\pi R\,dR\,dz, \qquad
!!  d\underline{\sigma} \mapsto 2\pi R\,d\sigma;
!! \f}
!! clearly, the constant \f$2\pi\f$ can then be simplified. This leads
!! to a two-dimensional integration domain. Concerning then the
!! integrands, the differential operators in terms of the cylindrical
!! coordinates are provided \ref magn_geom_geometrical_setup "here".
!! It can be observed that the gradient reduces to a two-dimensional,
!! Cartesian gradient in the poloidal plane, while the divergence must
!! be computed as
!! \f{displaymath}{
!!  \nabla\cdot{\bf a} = R^{-1}\partial_R(Ra_R) + \partial_za_z.
!! \f}
!!
!! \subsection solmin_vdrift The drift velocity: practical aspects
!!
!! As discussed above, the two velocity components are treated
!! differently, which poses some complications at the discretization
!! level. Typically, in the SUPG discretization, both \f${\bf v}\f$
!! and \f$\nabla\cdot{\bf v}\f$ are required and should have some
!! regularity. This is not a problem for the parallel component, since
!! \f$v_{\parallel}\f$ is taken in the continuous, piecewise polynomial
!! finite element space (see \ref momentum_eq "later"), while it is
!! less obvious for the perpendicular component, since the gradients
!! of the finite element functions are not globally continuous.
!! Fortunately, however, the divergence of \f${\bf v}_d\f$ depends
!! only on the <em>first</em> derivatives of \f$\Phi\f$, \f$p_{\rm
!! i}\f$ and \f$n\f$. This can be realized observing that (see also
!! \ref mod_sol_minimal_epsn "this section")
!! \f{displaymath}{
!!  \nabla\cdot\left({\bf a}\times\nabla w\right) =
!!  \nabla\times{\bf a}\cdot\nabla w.
!! \f}
!! The resulting relation is
!! \f{displaymath}{
!!  \nabla\cdot{\bf v} = \nabla v_{\parallel}\cdot{\bf b} +
!!  v_{\parallel}\nabla\cdot{\bf b} +\nabla\times\left(c\frac{\bf
!!  b}{B}\right)\cdot\left( \nabla\Phi + \frac{1}{Ze}\nabla T_{\rm i}
!!  \right) -\nabla\cdot\left( \mathcal{D}^{\perp}_{d,p}\nabla T_{\rm
!!  i} \right)
!! \f}
!! This allows us to define a drift velocity \f${\bf v}_{n,h}\f$ such
!! that
!! <ul>
!!  <li> \f${\bf v}_{n,h}\in H({\rm div},\Omega)\f$
!!  <li> \f${\bf v}_{n,h}\to{\bf v}_n\f$ in \f$H({\rm div},\Omega)\f$,
!! </ul>
!! so that we can consistently approximate both the velocity and its
!! divergence in the SUPG discretization. In fact, we proceed as
!! follows. First of all, define \f$\tilde{\bf v}_n\f$ and
!! \f$\tilde{d}_{{\bf v}_n}\f$ as the point-wise approximations to
!! \f${\bf v}\f$ and \f$\nabla\cdot{\bf v}\f$ obtained with the finite
!! element discretization. Then let \f$b_s^1\f$ denote the bubble
!! function associated with side \f$s\f$ and normalized so that
!! \f$\int_s b_s^1\,d\sigma = 1\f$. Now compute the normal fluxes
!! \f{displaymath}{
!!  \Psi_s = \int_{K^-_s}\left[ \tilde{d}_{{\bf v}_n}b^1_s +
!!  \tilde{\bf v}_n\cdot\nabla b^1_s \right]dx
!!  - \int_{K^+_s}\left[ \tilde{d}_{{\bf v}_n}b^1_s +
!!  \tilde{\bf v}_n\cdot\nabla b^1_s \right]dx.
!! \f}
!! Finally, let \f${\bf v}_{n,h}\in\mathbb{RT}_0(\Omega)\f$ be the
!! functions uniquely defined by the normal fluxes \f$\Psi_s\f$.
!!
!! \section momentum_eq Momentum equation
!!
!! We can start off writing the momentum equation as (in fact, we will
!! see in the following that, upon introducing the drift velocity,
!! only one scalar component of this equation survives)
!! \f{displaymath}{
!!  \partial_t \left( mn{\bf v}\right) + \nabla\cdot\left[ mn{\bf v}
!!  \otimes {\bf v} + \mathcal{I}p \right] - \nabla\cdot\left(
!!  mn\nu\nabla^S {\bf v} \right) = {\bf S}_M + {\bf f}^\perp,
!! \f}
!! where \f$p = p_{\rm i} + p_{\rm e} = \left(T_{\rm i}+ZT_{\rm
!! e}\right)n\f$, \f$\nabla^S {\bf v} = \frac{1}{2}\left( \nabla {\bf
!! v} + \nabla {\bf v}^T\right)\f$, and \f${\bf f}^{\perp}\f$ is a
!! term orthogonal to \f${\bf b}\f$ which is not relevant here, as it
!! will be clear in the following.
!!
!! At this point, we introduce the drift velocity, i.e. we let
!! \f{displaymath}{
!!  {\bf v} = v_{\parallel}{\bf b} + {\bf v}_d = {\bf v}_n -
!!  \frac{1}{n}\underline{\underline{\varepsilon}}_n\nabla n.
!! \f}
!!
!! If \f${\bf v}_n\f$ were an unknown of the problem, this equation
!! would be analogous to some models considered in mass flow problems,
!! such as <a
!! href="http://dx.doi.org/10.1016/S0309-1708(00)00073-7">[Franchi,
!! Straughan, Adv. Water Res., 2001]</a>, <a
!! href="http://dx.doi.org/10.1016/S1631-073X(02)02593-1">[Bresch,
!! Essoufi, Sy, C. R. Acad. Sci. Paris, 2002]</a>, <a
!! href="http://dx.doi.org/10.1090/S0025-5718-08-02099-1">[Guill&eacute;n-Gonz&aacute;lez,
!! Guti&eacute;rrez-Santacreu, Math. Comp., 2008]</a>, <a
!! href="http://dx.doi.org/10.1137/07067951X">[Guill&eacute;n-Gonz&aacute;lez,
!! Guti&eacute;rrez-Santacreu, SIAM J. Numer. Anal., 2008]</a> and <a
!! href="http://dx.doi.org/10.1090/S0025-5718-2011-02491-9">[Guti&eacute;rrez-Santacreu,
!! Rojas-Medar, Math. Comp., 2012]</a>, and one could recover a
!! prognostic equation for \f${\bf v}_n\f$ substituting the expression
!! of \f${\bf v}\f$, either in conservation or in nonconservation
!! form, where additional terms appear due to the diffusive
!! correction.
!!
!! Here, however, the perpendicular component of \f${\bf
!! v}_n\f$ is completely specified in terms of the other variables,
!! thereby becoming a diagnostic quantity, and the only remaining
!! prognostic variable is the scalar \f$v_{\parallel}\f$, so that the
!! only component of the momentum balance which can be retained is the
!! parallel one; the perpendicular momentum balance is dropped and
!! substituted by the introduction of the diagnostic quantity \f${\bf
!! v}_d\f$ (which in turns defines the perpendicular component of
!! \f${\bf v}_n\f$).
!!
!! In order to derive an equation for \f$v_{\parallel}\f$, we proceed
!! as follows:
!! <ul>
!!  <li> recast the equation in nonconservative form
!!  <li> write the weak form of the momentum equation
!!  <li> include the SUPG stabilization terms as well as the boundary
!!  terms (see \c mod_scalar_adr_ode)
!!  <li> select one scalar equation, namely the equation for the
!!  parallel velocity, by taking the test function
!!  \f{displaymath}{
!!   \boldsymbol{\phi} = \phi{\bf b}
!!  \f}
!!  <li> substitute the expression for \f${\bf v}_d\f$ and rearrange
!!  the resulting terms
!!  <li> use the hypothesis of cylindrical symmetry to recover a
!!  two-dimensional problem.
!! </ul>
!!
!! Using the continuity equation, the momentum equation can be recast
!! in nonconservative form as
!! \f{displaymath}{
!!  mn\partial_t {\bf v} + mn {\bf v} \cdot
!!  \nabla{\bf v} + \nabla p- \nabla\cdot\left( \nu\nabla^S {\bf v}
!!  \right) = {\bf S}_M + {\bf f}^\perp - mS_n{\bf v}.
!! \f}
!! The weak formulation is
!! readily obtained, observing that, since we use the nonconservative
!! form, integration by parts is required only for the pressure
!! gradient and the viscous term. This yields
!! \f{displaymath}{
!!  \begin{array}{l}
!!  \displaystyle
!!  \int_{\Omega} mn\partial_t {\bf v}\cdot\boldsymbol{\phi}\,dx
!!   + \int_{\Omega} mn \left( {\bf v} \cdot \nabla{\bf
!!   v}\right)\cdot\boldsymbol{\phi}\,dx 
!!   -\int_{\Omega} p\nabla\cdot\boldsymbol{\phi}\,dx
!!   +\int_{\Gamma} p{\bf
!!   n}_{\partial\Omega}\cdot\boldsymbol{\phi}\,d\sigma \\[3mm]
!!  \displaystyle
!!  +\int_{\Omega} \nu\nabla^S {\bf v} : \nabla^S\boldsymbol{\phi}\,dx
!!  -\int_{\Gamma}\nu\nabla^S{\bf v}\,{\bf n}_{\partial\Omega}\cdot
!!  \boldsymbol{\phi}\,d\sigma
!!  + \int_{\Omega}mS_n{\bf v}\cdot\boldsymbol{\phi}\,dx =
!!  \int_{\Omega} \left({\bf S}_M+{\bf f}^\perp\right)
!!  \cdot\boldsymbol{\phi}\,dx,
!!  \end{array}
!! \f}
!! where we have used that \f$A:B = A:B^S\f$ if \f$A\f$ is symmetric.
!!
!! To see now how the SUPG stabilization can be introduced, let us
!! first observe that a numerical diffusion term is any term of the
!! form (assuming summation for repeated indexes)
!! \f{displaymath}{
!!  \int_{\Omega} \mathcal{D}_{ijrs} (\nabla{\bf v})_{ij}
!!  (\nabla\boldsymbol{\phi})_{rs}dx,
!! \f}
!! where \f$\mathcal{D}_{ijrs}\f$ is a positive semidefinite,
!! fourth-order tensor symmetric in \f$i,r\f$ and \f$j,s\f$, i.e.
!! \f$\mathcal{D}_{ijrs}=\mathcal{D}_{rsij}\f$ (major symmetry); in
!! fact, the resulting bilinear form is symmetric, positive
!! semidefinite and introduces a dissipation proportional to
!! \f$\nabla{\bf v}\f$. If in addition to the major symmetry we also
!! have \f$\mathcal{D}_{ijrs}=\mathcal{D}_{jirs}=\mathcal{D}_{ijsr}\f$
!! the numerical diffusion term can be rewritten using the symmetric
!! gradient \f$\nabla^S\f$ instead of \f$\nabla\f$, which results in a
!! term formally identical to the viscous term of the compressible
!! momentum equation.
!!
!! The most general framework to apply the SUPG stabilization to a
!! compressible fluid equation can be found in <a
!! href="http://dx.doi.org/10.1007/s10915-008-9233-5">[Hughes,
!! Scovazzi, Tezduyar, J. Sci. Comput., 2010]</a>, where the
!! stabilization is constructed using the transposed flux Jacobian. A
!! simpler version is obtained following
!! <a
!! href="http://dx.doi.org/10.1016/0045-7825(82)90071-8">[Brooks,
!! Hughes, CMAME 1982]</a>, equation (5.2.8), where each component of
!! the advected vector is stabilized as a scalar quantity. The
!! resulting stabilization term is
!! \f{displaymath}{
!!  \int_{\Omega} \left({\bf v}\cdot\nabla{\bf v}\right) \cdot
!!  \left(\tau{\bf v}\cdot\nabla\boldsymbol{\phi}\right) dx,
!! \f}
!! which corresponds to \f$\mathcal{D}_{ijrs} = \delta_{ir}v_jv_s\f$.
!! Here, the minor symmetry is not verified; a possibility to recover
!! it, and thus obtain an expression involving the symmetric
!! gradients, is
!! \f{displaymath}{
!!  \int_{\Omega} \left({\bf v}\cdot\nabla{\bf v}\right) \cdot
!!  \left(\tau{\bf e}_{\bf v}\otimes{\bf e}_{\bf v}\,
!!  {\bf v}\cdot\nabla\boldsymbol{\phi}\right) dx,
!! \f}
!! where \f${\bf e}_{\bf v}=\frac{1}{v}{\bf v}\f$, so that
!! \f$\mathcal{D}_{ijrs} =\frac{1}{v^2} v_iv_jv_rv_s\f$. Both of these
!! choices, however, are problematic at the time of projecting the
!! equation along \f${\bf b}\f$, since the resulting scalar equation
!! would also involve the time derivative of \f${\bf v}_d\f$; in fact,
!! taking \f$\boldsymbol{\phi}={\bf b}\phi\f$ in the two cases yields
!! terms containing
!! \f{displaymath}{
!!  \partial_t{\bf v}\cdot\left(\nabla{\bf b}\,{\bf v}\phi\right), \qquad
!!  \partial_t{\bf v}\cdot\left({\bf e}_{\bf v}\otimes{\bf e}_{\bf
!!  v}\,\nabla\left({\bf b}\phi\right){\bf v}\phi\right),
!! \f}
!! respectively. The problem can be avoided using the following form
!! of the stabilization term
!! \f{displaymath}{
!!  \int_{\Omega} \left({\bf v}\cdot\nabla{\bf v}\right) \cdot
!!  \left(\tau{\bf b}\otimes{\bf b}\,
!!  {\bf v}\cdot\nabla\boldsymbol{\phi}\right) dx,
!! \f}
!! corresponding to the tensor \f$\mathcal{D}_{ijrs} =
!! b_iv_jb_rv_s\f$. As it will become clear shortly, this yields the
!! same discrete problem for \f$v_\parallel\f$ which would be obtained
!! taking first the projection along \f${\bf b}\f$ and then
!! stabilizing the resulting equation, regarded as a scalar
!! advection-reaction problem.
!!
!! Summarizing, upon introducing \f${\bf W}_{\bf b}(\boldsymbol{\phi})
!! = \tau {\bf b}\otimes{\bf b}\,{\bf
!! v}\cdot\nabla\boldsymbol{\phi}\f$, we arrive at the stabilized
!! problem
!! \f{displaymath}{
!!  \begin{array}{r}
!!  \displaystyle
!!  \int_{\Omega} mn\partial_t {\bf v}\cdot\left( \boldsymbol{\phi} +
!!  {\bf W}_{\bf b}(\boldsymbol{\phi}) \right) dx
!!   + \int_{\Omega} mn \left( {\bf v} \cdot \nabla{\bf
!!   v}\right)\cdot\boldsymbol{\phi}\,dx 
!!   -\int_{\Omega} p\nabla\cdot\boldsymbol{\phi}\,dx
!!   +\int_{\Gamma} p{\bf
!!   n}_{\partial\Omega}\cdot\boldsymbol{\phi}\,d\sigma \\[2mm]
!!  \displaystyle
!!  +\int_{\Omega} \nu\nabla^S {\bf v} : \nabla^S\boldsymbol{\phi}\,dx
!!  +\int_{\Omega}\tau mn\left({\bf v}\cdot\nabla{\bf v}\right)
!!   \cdot {\bf b}\otimes{\bf b}\left( {\bf v}\cdot\nabla
!!   \boldsymbol{\phi}\right) dx
!!  -\int_{\Gamma}\nu\nabla^S{\bf v}\,{\bf n}_{\partial\Omega}\cdot
!!  \boldsymbol{\phi}\,d\sigma \\[2mm]
!!  \displaystyle
!!  + \int_{\Omega}mS_n{\bf v}\cdot\left(\boldsymbol{\phi} +
!!  {\bf W}_{\bf b}(\boldsymbol{\phi}) \right)dx =
!!  \int_{\Omega} \left({\bf S}_M+{\bf f}^{\perp}\right)
!!  \cdot\left(\boldsymbol{\phi}+ {\bf W}_{\bf b}(\boldsymbol{\phi})
!!  \right)dx,
!!  \end{array}
!! \f}
!! where we have neglected the stabilization term associated with
!! \f$\nabla p\f$, which vanishes when \f$p\f$ is piecewise constant;
!! this should be a reasonable approximation for finite element spaces
!! of degree one.
!!
!! The boundary terms can be included as done \ref sol_bcs "here".
!!
!! We then select the evolution equation for the parallel component by
!! taking \f$\boldsymbol{\phi}={\bf b}\phi\f$; this provides the
!! following prognostic equation for \f$v_\parallel\f$
!! \f{displaymath}{
!!  \begin{array}{r}
!!  \displaystyle
!!  \int_{\Omega} mn\partial_t v_{\parallel}\left( \phi + W(\phi)
!!  \right) dx
!!   + \int_{\Omega} mn\,{\bf b}\otimes{\bf v}:\nabla{\bf v}\,\phi\,dx 
!!   -\int_{\Omega} p\left( \nabla\cdot{\bf b}\,\phi + {\bf
!!   b}\cdot\nabla\phi \right)dx
!!   +\int_{\Gamma} b_np\phi\,d\sigma \\[2mm]
!!  \displaystyle
!!  +\int_{\Omega} \nu\nabla^S {\bf v} :
!! \left( \nabla^S{\bf b}\,\phi + \frac{1}{2}\left( {\bf
!! b}\otimes\nabla\phi + \nabla\phi\otimes{\bf b} \right) \right) dx
!!  +\int_{\Omega}\tau mn\left({\bf b}\otimes{\bf v}:\nabla{\bf
!!  v}\right)
!!  \left( {\bf v}\cdot\nabla \phi\right) dx
!!  -\int_{\Gamma}\nu\nabla^S{\bf v}\,{\bf n}_{\partial\Omega}\cdot
!!  {\bf b}\phi\,d\sigma \\[2mm]
!!  \displaystyle
!!  + \int_{\Omega}mS_nv_{\parallel}\left(\phi + W(\phi) \right)dx =
!!  \int_{\Omega} S_{M_\parallel} \left(\phi+  W(\phi)
!!  \right)dx,
!!  \end{array}
!! \f}
!! where \f$W(\phi)=\tau{\bf v}\cdot\nabla\phi\f$, as usual,
!! \f$b_n = {\bf b}\cdot{\bf n}_{\partial\Omega}\f$, and
!! and we
!! have used \f$\nabla({\bf b}\phi)=\nabla{\bf b}\,\phi + {\bf
!! b}\otimes\nabla\phi\f$ and \f$\nabla{\bf b}^T{\bf b}=0\f$.
!!
!! The next step is now making the substitution \f${\bf
!! v}=v_{\parallel}{\bf b}+{\bf v}_d\f$. This is useful to identify
!! those terms that can be treated implicitly when solving for
!! \f$v_{\parallel}\f$. The result is the discrete problem
!! \f{displaymath}{
!!  \begin{array}{r}
!!  \displaystyle
!!  \int_{\Omega} mn\partial_t v_{\parallel}\left( \phi + W(\phi)
!!  \right) dx
!!   + \int_{\Omega} mn\,{\bf v}\cdot\nabla v_{\parallel}\,\phi\,dx 
!!   + \int_{\Omega} mn\,{\bf b}\otimes{\bf v}:\nabla{\bf v}_d\,\phi\,dx 
!!   \\[2mm]\displaystyle
!!   -\int_{\Omega} p\left( \nabla\cdot{\bf b}\,\phi + {\bf
!!   b}\cdot\nabla\phi \right)dx
!!   +\int_{\Gamma} b_np\phi\,d\sigma \\[2mm]
!!  \displaystyle
!!  +\int_{\Omega} \nu\left[ |\nabla^S {\bf b}|^2v_{\parallel}\phi
!!  + \left(\nabla{\bf b}\,{\bf
!!  b}\right)\cdot\left(\nabla\phi\,v_{\parallel} + \nabla
!!  v_{\parallel}\,\phi\right) + \frac{1}{2}\nabla
!!  v_{\parallel}^T\left(\mathcal{I}+{\bf b}\otimes{\bf
!!  b}\right)\nabla\phi
!!  \right]dx
!!   \\[2mm]\displaystyle
!!  +\int_{\Omega} \nu\nabla^S {\bf v}_d :
!! \left( \nabla^S{\bf b}\,\phi + \frac{1}{2}\left( {\bf
!! b}\otimes\nabla\phi + \nabla\phi\otimes{\bf b} \right) \right) dx
!!   \\[2mm]\displaystyle
!!  -\frac{1}{2}\int_{\Gamma}\nu \left(
!!   \nabla{\bf b}\,{\bf b}\,v_{\parallel} + \left( \mathcal{I}  + {\bf
!!   b}\otimes{\bf b} \right) \nabla v_{\parallel}
!!   \right) \cdot {\bf n}_{\partial\Omega}\phi\, d\sigma
!!  -\int_{\Gamma}\nu\nabla^S{\bf v}_d\,{\bf n}_{\partial\Omega}\cdot
!!  {\bf b}\phi\,d\sigma
!!   \\[2mm]\displaystyle
!!  +\int_{\Omega}\tau mn \,\nabla v_{\parallel}^T {\bf v}\otimes{\bf
!!  v}\nabla\phi\, dx 
!!  +\int_{\Omega}\tau mn\left({\bf b}\otimes{\bf v}:\nabla{\bf
!!  v}_d\right)
!!  \left( {\bf v}\cdot\nabla \phi\right) dx \\[2mm]
!!  \displaystyle
!!  + \int_{\Omega}mS_nv_{\parallel}\left(\phi + W(\phi) \right)dx =
!!  \int_{\Omega} S_{M_\parallel} \left(\phi+  W(\phi)
!!  \right)dx.
!!  \end{array}
!! \f}
!!
!! Finally, including the hypothesis of cylindrical symmetry can be
!! done as for the continuity equation.
!!
!! \subsection simplified_momentum Simplified momentum equation
!!
!! The terms proportional to \f$\nu\f$ in the momentum equation
!! are rather complicated; more important, since the perpendicular
!! velocity is prescribed, some of them, which are not symmetric,
!! could lead to stability problems. If we consider that the precise
!! form of the diffusion term in the three-dimensional original
!! problem is not physically motivated, a reasonable alternative could
!! be to substitute all of them with a simple diffusive term for
!! \f$v_{\parallel}\f$. This results in the equation
!! \f{displaymath}{
!!  \begin{array}{r}
!!  \displaystyle
!!  \int_{\Omega} mn\partial_t v_{\parallel}\left( \phi + W(\phi)
!!  \right) dx
!!  + \int_{\Omega} mn\,{\bf v}\cdot\nabla v_{\parallel}\,\phi\,dx 
!!  -\int_{\Omega} p\left( \nabla\cdot{\bf b}\,\phi + {\bf
!!  b}\cdot\nabla\phi \right)dx
!!  +\int_{\Gamma} b_np\phi\,d\sigma
!!   \\[2mm]\displaystyle
!!  +\int_{\Omega} \nabla v_{\parallel}^T \left( \nu\mathcal{I} + \tau
!!  mn \,{\bf v}\otimes{\bf v}\right)\nabla\phi\, dx 
!!  -\int_{\Gamma} \nu\nabla v_{\parallel}\cdot{\bf n}_{\partial\Omega}
!!  \phi\,d\sigma
!!  + \int_{\Omega}mS_nv_{\parallel}\left(\phi + W(\phi) \right)dx
!!   \\[2mm]\displaystyle
!!  = \int_{\Omega} \left( S_{M_\parallel} + mn\,\nabla{\bf b} : {\bf
!!  v}_d\otimes{\bf v} \right) \left(\phi+  W(\phi)
!!  \right)dx,
!!  \end{array}
!! \f}
!! where we have used
!! \f{displaymath}{
!!  {\bf b}\otimes{\bf v}:\nabla{\bf v}_d = {\bf
!!  v}\cdot\nabla\underbrace{({\bf b}\cdot{\bf v}_d)}_{=0} -
!!  \nabla{\bf b}:{\bf v}_d\otimes{\bf v}
!! \f}
!! to avoid computing derivatives of \f${\bf v}_d\f$. Notice that this
!! relation indicates that, although \f${\bf v}_d\f$ has in general
!! little regularity, because of the orthogonality constraint and the
!! regularity of \f${\bf b}\f$, its variations in the parallel
!! direction must be smooth.
!!
!! The resulting equation equation is very similar to a scalar
!! advection-diffusion equation with SUPG stabilization, with two
!! modifications:
!! <ul>
!!  <li> the presence of the \f${\bf b}\f$-corrected pressure gradient
!!  <li> the additional forcing term proportional to \f${\bf v}_d\f$.
!! </ul>
!!
!! \subsection conservative_formulation Conservative formulation
!!
!! Besides the nonconservative form of the momentum equation derived
!! above, one could consider the conservative one. Since the pressure
!! and viscosity terms are the same in the two formulations, we sketch
!! here the derivation of the conservative on ignoring those terms.
!! The weak form in three dimensions is
!! \f{displaymath}{
!!  \begin{array}{l}
!!  \displaystyle
!!  \int_{\Omega} \partial_t (mn{\bf v})\cdot\boldsymbol{\phi}\,dx -
!!  \int_{\Omega} mn {\bf v}\otimes{\bf v}:\nabla\boldsymbol{\phi}\,dx
!!  + \int_{\Gamma} \left( mn {\bf v}\otimes{\bf v}\,{\bf
!!  n}_{\partial\Omega} \right)\cdot\boldsymbol{\phi}\,d\sigma =
!!  \int_{\Omega} \left({\bf S}_M+{\bf f}^\perp\right)
!!  \cdot\boldsymbol{\phi}\,dx.
!!  \end{array}
!! \f}
!! The SUPG stabilization can be done as in the nonconservative case,
!! arriving at
!! \f{displaymath}{
!!  \begin{array}{l}
!!  \displaystyle
!!  \int_{\Omega} \partial_t (mn{\bf v})\cdot\left( \boldsymbol{\phi}
!!  + {\bf W}_{\bf b}(\boldsymbol{\phi})\right)dx -
!!  \int_{\Omega} mn {\bf v}\otimes{\bf v}:\nabla\boldsymbol{\phi}\,dx
!!  + \int_{\Gamma} \left( mn {\bf v}\otimes{\bf v}\,{\bf
!!  n}_{\partial\Omega} \right)\cdot\boldsymbol{\phi}\,d\sigma 
!!   \\[2mm]\displaystyle
!!  +\int_{\Omega}\tau \left[ mn\left({\bf v}\cdot\nabla{\bf v}\right)
!!  + \nabla\cdot\left(mn{\bf v}\right){\bf v}
!!   \right]
!!   \cdot {\bf b}\otimes{\bf b}\left( {\bf v}\cdot\nabla
!!   \boldsymbol{\phi}\right) dx
!!  = \int_{\Omega} \left({\bf S}_M+{\bf f}^\perp\right)
!!  \cdot\left( \boldsymbol{\phi}
!!  + {\bf W}_{\bf b}(\boldsymbol{\phi})\right)dx,
!!  \end{array}
!! \f}
!! which can be rewritten as
!! \f{displaymath}{
!!  \begin{array}{r}
!!  \displaystyle
!!  \int_{\Omega} \partial_t (mn{\bf v})\cdot\left( \boldsymbol{\phi}
!!  + {\bf W}_{\bf b}(\boldsymbol{\phi})\right)dx -
!!  \int_{\Omega} \left( mn\mathcal{I} - \tau\nabla\cdot(mn{\bf
!!  v}){\bf b}\otimes{\bf b} \right){\bf v}\otimes{\bf v} :
!!  \nabla\boldsymbol{\phi}\,dx
!!  + \int_{\Gamma} \left( mn {\bf v}\otimes{\bf v}\,{\bf
!!  n}_{\partial\Omega} \right)\cdot\boldsymbol{\phi}\,d\sigma 
!!   \\[3mm]\displaystyle
!!  +\int_{\Omega}\tau mn\left({\bf v}\cdot\nabla{\bf v}\right)
!!   \cdot {\bf b}\otimes{\bf b}\left( {\bf v}\cdot\nabla
!!   \boldsymbol{\phi}\right) dx
!!  = \int_{\Omega} \left({\bf S}_M+{\bf f}^\perp\right)
!!  \cdot\left( \boldsymbol{\phi}
!!  + {\bf W}_{\bf b}(\boldsymbol{\phi})\right)dx.
!!  \end{array}
!! \f}
!! Here, the stabilization term is the same as in the weak form, while
!! the velocity must be corrected to preserve consistency when the
!! flux divergence does not vanish. This structure is very similar to
!! the one obtained for the \ref sol_bcs "scalar case".
!!
!! Taking \f$\boldsymbol{\phi}={\bf b}\phi\f$, the only terms which
!! differ from the nonconservative case are the second and the third
!! ones. They can be handled as follows
!! \f{displaymath}{
!!  \begin{array}{rcl}
!!  \displaystyle
!!  \int_{\Omega} \left( mn\mathcal{I} - \tau\nabla\cdot(mn{\bf
!!  v}){\bf b}\otimes{\bf b} \right){\bf v}\otimes{\bf v} :
!!  \nabla\boldsymbol{\phi}\,dx & = & \displaystyle
!!  \int_{\Omega} \left( mn - \tau\nabla\cdot(mn{\bf
!!  v}) \right){\bf v}v_{\parallel}\cdot\nabla\phi\,dx
!!  + \int_{\Omega} mn \nabla{\bf b}:{\bf v}_d\otimes{\bf v}\,\phi\,dx
!!   \\[3mm]\displaystyle
!!  \int_{\Gamma} \left( mn {\bf v}\otimes{\bf v}\,{\bf
!!  n}_{\partial\Omega} \right)\cdot\boldsymbol{\phi}\,d\sigma 
!!  & = & \displaystyle \int_{\Gamma} mn v_nv_{\parallel}\phi\,d\sigma.
!!  \end{array}
!! \f}
!! The result is the following equation
!! \f{displaymath}{
!!  \begin{array}{r}
!!  \displaystyle
!!  \int_{\Omega} \partial_t (mnv_{\parallel})\left( \phi + W(\phi)
!!  \right) dx
!!  - \int_{\Omega} \left( mn-\tau\nabla\cdot(mn{\bf v}) \right) {\bf
!!  v}v_{\parallel}\cdot\nabla \phi\,dx 
!!   \\[3mm]\displaystyle
!!  -\int_{\Omega} p\left( \nabla\cdot{\bf b}\,\phi + {\bf
!!  b}\cdot\nabla\phi \right)dx
!!  +\int_{\Gamma} \left( mnv_nv_{\parallel} + b_np\right)\phi\,d\sigma
!!   \\[3mm]\displaystyle
!!  +\int_{\Omega} \nabla v_{\parallel}^T \left( \nu\mathcal{I} + \tau
!!  mn \,{\bf v}\otimes{\bf v}\right)\nabla\phi\, dx 
!!  -\int_{\Gamma} \nu\nabla v_{\parallel}\cdot{\bf n}_{\partial\Omega}
!!  \phi\,d\sigma
!!   \\[3mm]\displaystyle
!!  = \int_{\Omega} \left( S_{M_\parallel} + mn\,\nabla{\bf b} : {\bf
!!  v}_d\otimes{\bf v} \right) \left(\phi+  W(\phi)
!!  \right)dx.
!!  \end{array}
!! \f}
!!
!! \section potential_eq Potential equation
!!
!! The equation for the electric potential is 
!! \f{displaymath}{
!!  \nabla\cdot(\underline{\underline{\sigma}}\nabla\Phi) =
!!  \nabla\cdot{\bf f}^{\Phi},
!! \f}
!! for a prescribed function \f${\bf f}^{\Phi}(n)\f$ and a constant
!! diffusion tensor
!! \f{displaymath}{
!!  \underline{\underline{\sigma}} = \sigma_{\parallel}{\bf
!!  b}\otimes{\bf b} + \sigma_{\rm an}(\mathcal{I}-{\bf b}\otimes{\bf
!!  b}).
!! \f}
!! This is a standard Poisson equation which does not pose any special
!! difficulty. The only nonstandard aspect is the form of the
!! right-hand-side: being prescribed in form of a divergence, it is
!! natural to perform integration by parts also on this term. We
!! obtain the equation
!! \f{displaymath}{
!!  \int_{\Omega} (\underline{\underline{\sigma}} \nabla\Phi) \cdot
!!  \nabla\phi\,dx - \int_{\Gamma} (\underline{\underline{\sigma}}
!!  \nabla\Phi)\cdot{\bf n}_{\partial\Omega}\phi\,d\sigma =
!!  \int_{\Omega} {\bf f}^{\Phi} \cdot \nabla\phi\,dx - \int_{\Gamma}
!!  {\bf f}^{\Phi}\cdot{\bf n}_{\partial\Omega}\phi\,d\sigma.
!! \f}
!! The complete expression of \f${\bf f}^{\Phi}\f$ is
!! \f{displaymath}{
!!  \begin{array}{rcl}
!!   \displaystyle
!!  {\bf f}^{\Phi} & = & \displaystyle
!!  \sigma_{\parallel}\left( \frac{1}{Zen}{\bf b}\cdot\nabla p_e +
!!  0.71\frac{1}{e}{\bf b}\cdot\nabla T_e\right) {\bf b} +
!!  \frac{c}{B^2}{\bf B}\times\nabla p
!!   \\[3mm]\displaystyle
!!   & = & \displaystyle
!!  \sigma_{\parallel} {\bf b}\otimes{\bf b}\left(
!!  \frac{1+0.71}{e}\nabla T_e +\frac{T_e}{en}\nabla n \right)
!!  +\frac{c}{B^2}{\bf B}\times\left( n \nabla(T_i+ZT_e) +
!!  (T_i+ZT_e)\nabla n \right).
!!  \end{array}
!! \f}
!!
!! \section implementation_issues Implementation issues
!!
!! The implementation is complicated by the large number of diagnostic
!! quantities with different representations. The idea is to
!! distinguish among the following objects:
!! <ul>
!!  <li> problem data, such as the temperature profiles and the
!!  magnetic field: these are collected in \c t_sol_minimal_data
!!  <li> diagnostic quantities of general interest: these are
!!  collected in \c t_sol_minimal_diags
!!  <li> diagnostic quantities which are required to build the local
!!  matrices, but do not need to be stored globally: these are
!!  included in the <tt>*_coeff_*</tt> types.
!! </ul>
!! Each of these type could include specific "local" types to
!! distinguish side and element data; this is necessary because the
!! shape of the elements is different, which prevents using global
!! arrays.
!!
!! \subsection sparse_matrices Sparse matrices
!!
!! All the matrices appearing in the problem have the same sparsity
!! pattern, since all the unknowns share the same FE space. Here, this
!! pattern, together with the corresponding access skeleton, are
!! defined in the module constructor while building the constant mass
!! matrix. The local matrices are are collocated one after another
!! following the element order, and each matrix is unrolled in column
!! major order. Dofs are obviously numbered following the vertex
!! numbering. All this is done in the module constructor.
!<----------------------------------------------------------------------
module mod_sol_minimal_ode

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_mpi_utils, only: &
   mod_mpi_utils_initialized, &
   mpi_integer, wp_mpi,  &
   mpi_status_size,      &
   mpi_isend, mpi_irecv, &
   mpi_waitall

 use mod_fu_manager, only: &
   mod_fu_manager_initialized, &
   new_file_unit

 use mod_state_vars, only: &
   mod_state_vars_initialized, &
   c_stv

 use mod_time_integrators, only: &
   mod_time_integrators_initialized, &
   c_ode, c_ods

 use mod_sparse, only: &
   mod_sparse_initialized, &
   ! sparse types
   t_intar,     &
   t_col,       &
   t_tri,       &
   t_pm_sk,     &
   ! construction of new objects
   new_col,     &
   new_tri,     &
   ! convertions
   col2tri,     &
   tri2col,     &
   tri2col_skeleton, &
   tri2col_skeleton_part, &
   ! overloaded operators
   operator(+), &
   operator(*), &
   sum,         &
   transpose,   &
   matmul,      &
   ! error codes
   wrong_n,     &
   wrong_m,     &
   wrong_nz,    &
   wrong_dim,   &
   ! other functions
   nnz_col,     &
   nz_col,      &
   nz_col_i,    &
   get,         &
   set,         &
   diag,        &
   spdiag,      &
   ! deallocate
   clear

 use mod_octave_io_sparse, only: &
   mod_octave_io_sparse_initialized, &
   write_octave

 use mod_perms, only: &
   mod_perms_initialized, &
   operator(*),     &
   operator(**),    &
   idx

 use mod_linal, only: &
   mod_linal_initialized, &
   invmat_chol

 use mod_output_control, only: &
   mod_output_control_initialized, &
   elapsed_format, &
   base_name

 use mod_linsolver, only: &
   mod_linsolver_initialized, &
   c_linpb, c_itpb, c_mumpspb, c_umfpackpb, &
   gmres

 use mod_h2d_master_el, only: &
   mod_h2d_master_el_initialized, &
   c_h2d_me_geom, c_h2d_me_base

 use mod_h2d_grid, only: &
   mod_h2d_grid_initialized, &
   t_h2d_grid, t_ddc_h2d_grid, &
   t_2dv, t_2ds, t_2de

 use mod_h2d_base, only: &
   mod_h2d_base_initialized, &
   t_h2d_base

 use mod_h2d_bcs, only: &
   mod_h2d_bcs_initialized, &
   t_h2d_bcs

 use mod_vect_operators, only: &
   mod_vect_operators_initialized, &
   cross_3eq3x3, cross_3eq3x2, cross_2eq3x2, &
   perp_proj, perp_proj_3eq2

 use mod_sol_state, only: & 
   mod_sol_state_initialized, &
   t_sol_state, t_sol_diags

 use mod_sol_testcases, only: &
   mod_sol_testcases_initialized, &
   t_phc, tc

 use mod_sol_locmat, only: &
   mod_sol_locmat_initialized, &
   t_loc_mat, t_loc_poisson_rhs_data, &
   loc_mass_mat, loc_supg_mat, loc_poisson_mat, loc_poisson_rhs

 use mod_sol_linsys, only: &
   mod_sol_linsys_constructor, &
   mod_sol_linsys_destructor,  &
   mmmt, const_mmmt, aaat, rhs, &
   sys_mat_t, sys_rhs, linpb, &
   psnt, psn_linpb, psn_rhs, &
   out_file_sys_suff, out_file_sys_name, &
   linsys_res
 
 ! the next two modules are candidate for becoming submodules
 use mod_sol_minimal_data, only: &
   mod_sol_minimal_data_constructor, &
   mod_sol_minimal_data_destructor,  &
   t_sol_minimal_data, sol_data, &
   t_sol_minimal_diags, new_sol_minimal_diags, &
   write_octave, clear

 use mod_sol_minimal_coeffs, only: &
   mod_sol_minimal_coeffs_constructor, &
   mod_sol_minimal_coeffs_destructor,  &
   t_dx, &
   t_dens_v_coeff, t_dens_eps_coeff, t_dens_lbcs_coeff, &
   t_epot_sigma_coeff, t_epot_lbcs_coeff, &
   epot_sigma_coeff_sigma_par

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_sol_minimal_ode_constructor, &
   mod_sol_minimal_ode_destructor,  &
   mod_sol_minimal_ode_initialized, &
   t_sol_minimal_ode, new_sol_minimal_ode, clear, &
   t_sol_minimal_diags, new_sol_minimal_diags, &
   update_sol_diagnostics, write_octave, &
   ! these can be useful to write the system matrix
   mmmt, const_mmmt, aaat, psnt, psn_rhs, psn_rhs_contr, &
   out_file_sys_suff, out_file_sys_name, &
   ! other objects which might be useful
   sol_data, picard_res

 private

!-----------------------------------------------------------------------

! Module types and parameters

 !----------------------------------------------------------------------

 !> SOL minimal ODE
 type, extends(c_ode) :: t_sol_minimal_ode
  type(t_h2d_grid),     pointer :: grid     => null()
  type(t_ddc_h2d_grid), pointer :: ddc_grid => null()
  type(t_h2d_base),     pointer :: base     => null()
  type(t_h2d_bcs),      pointer :: bcs      => null()
 contains
  procedure, pass(ode) :: rhs   => sol_minimal_rhs
  procedure, pass(ode) :: solve => sol_minimal_solve
 end type t_sol_minimal_ode
 
 !----------------------------------------------------------------------

 interface clear
   module procedure clear_sol_minimal_ode
 end interface

! Module variables

 type(t_dx), save :: dx_coeff
 type(t_dens_v_coeff),     save :: dens_v_coeff
 type(t_dens_eps_coeff),   save :: dens_eps_coeff
 type(t_dens_lbcs_coeff),  save :: dens_lbcs_coeff
 type(t_epot_sigma_coeff), save :: epot_sigma_coeff
 type(t_epot_lbcs_coeff),  save :: epot_lbcs_coeff

 !> Various contributions which are then added in the rhs of the
 !! Poisson problem. 
 !!
 !! \note This variable is provided only for diagnostic purposes; the
 !! only term which is required for the computation is the sum of all
 !! the contributions.
 real(wp), allocatable :: psn_rhs_contr(:,:)

 !> Residuals of the Picard iterations
 !!
 !! \warning Different time integrators might require a different
 !! number of iterations, due to the equivalent time step (the
 !! argument \c sigma passed to \c sol_minimal_solve). This means that
 !! there is no easy way to group the residuals. In any case, this
 !! array contains the residuals corresponding to the last call of \c
 !! sol_minimal_solve, whichever it is.
 real(wp), allocatable :: picard_res(:)

 logical, protected :: &
   mod_sol_minimal_ode_initialized = .false.

 ! private members

 !> Some working arrays to compute the local matrices
 type(t_loc_mat), allocatable :: mm(:), aa(:), bb(:), b2(:)

 character(len=*), parameter :: &
   this_mod_name = 'mod_sol_minimal_ode'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_sol_minimal_ode_constructor(grid,ddc_grid,bcs, &
                                            base,write_sys)
  type(t_h2d_grid),     intent(in), target :: grid
  type(t_ddc_h2d_grid), intent(in), target :: ddc_grid
  type(t_h2d_bcs),      intent(in) :: bcs
  type(t_h2d_base), intent(in) :: base
  logical, intent(in) :: write_sys

  integer :: ie, i, j, ie_pos, pos, fu, ierr
  integer, allocatable :: mmmi(:), mmmj(:)
  type(t_intar) :: idij(1)
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_kinds_initialized.eqv..false.) .or. &
    (mod_messages_initialized.eqv..false.) .or. &
   (mod_mpi_utils_initialized.eqv..false.) .or. &
  (mod_fu_manager_initialized.eqv..false.) .or. &
  (mod_state_vars_initialized.eqv..false.) .or. &
(mod_time_integrators_initialized.eqv..false.) .or. &
      (mod_sparse_initialized.eqv..false.) .or. &
(mod_octave_io_sparse_initialized.eqv..false.) .or. &
       (mod_perms_initialized.eqv..false.) .or. &
       (mod_linal_initialized.eqv..false.) .or. &
(mod_output_control_initialized.eqv..false.) .or. &
   (mod_linsolver_initialized.eqv..false.) .or. &
(mod_h2d_master_el_initialized.eqv..false.) .or. &
    (mod_h2d_grid_initialized.eqv..false.) .or. &
    (mod_h2d_base_initialized.eqv..false.) .or. &
     (mod_h2d_bcs_initialized.eqv..false.) .or. &
(mod_vect_operators_initialized.eqv..false.) .or. &
   (mod_sol_state_initialized.eqv..false.) .or. &
(mod_sol_testcases_initialized.eqv..false.) .or. &
  (mod_sol_locmat_initialized.eqv..false.) ) then  
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_sol_minimal_ode_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   !--------------------------------------------------------------------
   ! 1) Data and module work arrays

   ! 1.0) Constructors of the "submodules"
   call mod_sol_minimal_data_constructor(grid,base)
   call mod_sol_minimal_coeffs_constructor()

   ! 1.1) Prepare some working arrays
   allocate( mm(base%lb:base%ub) , &
             bb(base%lb:base%ub) , &
             b2(base%lb:base%ub) )
   do ie=base%lb,base%ub
     ! local matrices have the same dimension as the local dofs
     mm(ie)%d = base%e(ie)%me%pk
     allocate( mm(ie)%m( mm(ie)%d , mm(ie)%d ) )
     ! local vectors
     bb(ie)%d = base%e(ie)%me%pk
     allocate( bb(ie)%v( bb(ie)%d ) )
     b2(ie)%d = base%e(ie)%me%pk
     allocate( b2(ie)%m( b2(ie)%d , 2 ) )
   enddo
   aa = mm ! using allocation on assignment

   !--------------------------------------------------------------------
   ! 2) Allocate the various sparse matrices
   !
   ! We build here the sparsity patter of all the required matrices.
   ! Some of them, which are constant in time, will the be filled
   ! later inside this constructor, while those which depend on time
   ! must be recomputed during the time integration.

   ! 2.1) Compute the local matrices and determine the matrix pattern
   ie = sum( grid%e%poly**2 ) ! used as temporary, local nnz-s
   allocate( mmmi(ie) , mmmj(ie) )

   pos = 0 ! access index in the matrix skeleton
   do ie=1,grid%ne
     associate(    e => grid%e(ie) ,       &
                poly => grid%e(ie)%poly    )
     associate(   pk => base%e(poly)%me%pk )

     ! The mass matrix coefficients will be computed later and iserted
     ! using the matrix skeleton. Here, we only define the sparsity
     ! pattern.

     do j=1,pk
       do i=1,pk
         pos = pos+1
         ! dofs are numbered directly after the vertexes
         mmmi(pos) = e%iv(i)
         mmmj(pos) = e%iv(j)
       enddo
     enddo

     end associate
     end associate
   enddo
   
   ! 2.2) Define the matrix skeletons
   allocate(mmmt(2),aaat(2))
   associate( dens_mt => mmmt(1) , dens_at => aaat(1) )

   ! 2.2.1) density mass matrix
   allocate(idij(1)%i(grid%nv)); idij(1)%i = (/(i,i=1,grid%nv)/)
   ! notice transposition; also, switch to zero based indexing
   mmmi = mmmi-1; mmmj = mmmj-1; idij(1)%i = idij(1)%i-1
   call tri2col_skeleton_part(dens_mt,idij,idij,    &
          new_tri(grid%nv,grid%nv,mmmj,mmmi,0.0_wp) )
   ! deallocations will be done later

   ! 2.2.2) define some other variables related to the linear systems
   allocate( const_mmmt(size(dens_mt%m(1,1)%ax)) ) ! will be set later
   allocate( rhs(grid%nv,2) )
   ! Note: this matrix has the same structure as the mass matrix;
   ! however, we can not simply copy such matrix because all the
   ! pointers would not be set correctly. Thus, we build another
   ! skeleton identical to the first one.
   call tri2col_skeleton_part(dens_at,idij,idij,    &
          new_tri(grid%nv,grid%nv,mmmj,mmmi,0.0_wp) )

   ! Poisson problem for the electric potential
   call tri2col_skeleton_part(psnt,idij,idij,       &
          new_tri(grid%nv,grid%nv,mmmj,mmmi,0.0_wp) )

   deallocate( idij(1)%i , mmmi , mmmj )

   ! 2.3) Build the linear system
   call mod_sol_linsys_constructor(grid,ddc_grid)

   ! 2.4) Write the octave output
   if(write_sys) then
     out_file_sys_name = trim(base_name)//out_file_sys_suff
     call new_file_unit(fu,ierr)
     open(fu,file=trim(out_file_sys_name), &
          status='replace',action='write',form='formatted',iostat=ierr)
     call write_octave(transpose(dens_mt%m(1,1)),'const_mmm',fu)
     close(fu,iostat=ierr)
   endif

   end associate
   
   !--------------------------------------------------------------------
   ! 3) Compute the constant-in-time matrices

   allocate( dens_v_coeff%n_vn_interp( grid%ni+1:grid%ns ) )
   mmmt(1)%m(1,1)%ax = 0.0_wp
      psnt%m(1,1)%ax = 0.0_wp

   ie_pos = 0; pos = 0
   do ie=1,grid%ne
     associate(    e => grid%e(ie)      , &
                poly => grid%e(ie)%poly , &
                  be => bcs%b_e2be(ie)    )
     associate(  bme => base%e(poly)%me ,    &
                  pk => base%e(poly)%me%pk , &
                   m => base%e(poly)%me%m  )

     ! 3.1) Local mass matrix
     call loc_mass_mat( mm(poly)%m , e , bme , dx_coeff )

     ! Fill the global, constant mass matrix
     pos = ie_pos
     do j=1,pk
       do i=1,pk
         pos = pos+1
         mmmt(1)%t2c(pos)%p = mmmt(1)%t2c(pos)%p + mm(poly)%m(i,j)
       enddo
     enddo

     ! 3.2) Poisson problem for the electric potential
     call loc_poisson_mat( aa(poly)%m , e,bme,be,    &
           dx_coeff,epot_sigma_coeff,epot_lbcs_coeff )
     pos = ie_pos
     do j=1,pk
       do i=1,pk
         pos = pos+1
         psnt%t2c(pos)%p = psnt%t2c(pos)%p + aa(poly)%m(i,j)
       enddo
     enddo

     ! 3.3) While we have the mass matrix, compute  n_vn_interp
     if(associated(be%p)) then ! there is at least one boundary side
       call invmat_chol( mm(poly)%m , aa(poly)%m ) ! reusing aa as tmp.
       do i=1,be%p%nbs
         associate( is => be%p%bs(i)%p%s%i ,      & ! side index
                   isl => be%p%bs(i)%p%s%isl(1) , & ! local side index
                    ms => base%e(poly)%me%ms ,    & 
                     p => base%e(poly)%me%p ,     &
                    pb => base%e(poly)%me%pb )
         allocate( dens_v_coeff%n_vn_interp(is)%m( m , ms ) )
         dens_v_coeff%n_vn_interp(is)%m = &
           ! let the compiler figure out the temporaries...
           matmul(                                                    &
             ! first matrix
             transpose(                                               &
   spread( dx_coeff%evg(e,bme) * e%me%scale_wg(bme%xig,bme%wg) ,1,pk) &
                                       * p                            &
             ) ,                                                      &
             ! second and third matrices
             matmul( transpose(aa(poly)%m) , pb(:,:,isl) )            &
           )
         end associate
       enddo
     endif

     ie_pos = ie_pos + pk**2
     end associate
     end associate
   enddo

   ! 3.4) Other operations on the constant matrices
   const_mmmt = mmmt(1)%m(1,1)%ax
   call psn_linpb%factor('factorization')

   allocate( psn_rhs_contr(grid%nv,2) )


   mod_sol_minimal_ode_initialized = .true.
 end subroutine mod_sol_minimal_ode_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_sol_minimal_ode_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
  integer :: i

   !Consistency checks ---------------------------
   if(mod_sol_minimal_ode_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   if(allocated(picard_res)) deallocate(picard_res)

   deallocate( psn_rhs_contr )

   deallocate( dens_v_coeff%n_vn_interp )

   call mod_sol_linsys_destructor()

   do i=1,size(mmmt); call clear(mmmt(i)); enddo; deallocate(mmmt)
   do i=1,size(aaat); call clear(aaat(i)); enddo; deallocate(aaat)
   deallocate(const_mmmt,rhs)
   call clear(psnt)

   deallocate( mm , aa , bb , b2 )

   call mod_sol_minimal_coeffs_destructor()
   call mod_sol_minimal_data_destructor()

   mod_sol_minimal_ode_initialized = .false.
 end subroutine mod_sol_minimal_ode_destructor

!-----------------------------------------------------------------------

 subroutine new_sol_minimal_ode(obj,grid,ddc_grid,base,bcs)
  type(t_h2d_grid),     intent(in), target :: grid
  type(t_ddc_h2d_grid), intent(in), target :: ddc_grid
  type(t_h2d_base),     intent(in), target :: base
  type(t_h2d_bcs),      intent(in), target :: bcs
  type(t_sol_minimal_ode),  intent(out) :: obj

   obj%grid     => grid
   obj%ddc_grid => ddc_grid
   obj%base     => base
   obj%bcs      => bcs

 end subroutine new_sol_minimal_ode
 
!-----------------------------------------------------------------------

 pure subroutine clear_sol_minimal_ode(obj)
  type(t_sol_minimal_ode),  intent(inout) :: obj

   nullify( obj%grid , obj%ddc_grid )
   nullify( obj%base )
   nullify( obj%bcs  )

 end subroutine clear_sol_minimal_ode

!-----------------------------------------------------------------------

 subroutine sol_minimal_rhs(tnd,ode,t,uuu,ods,term)
  class(t_sol_minimal_ode), intent(in) :: ode !< ODE problem
  real(wp),     intent(in)    :: t   !< time level
  class(c_stv), intent(in)    :: uuu !< present state
  class(c_ods), intent(inout) :: ods !< scratch (diagnostic vars.)
  class(c_stv), intent(inout) :: tnd !< tendency
  !> term to be evaluated and number of terms in the integrator
  integer,      intent(in), optional :: term(2)

 end subroutine sol_minimal_rhs

!-----------------------------------------------------------------------

 !> Solve the implicit problem.
 !!
 !! The general structure of the problem is the same as in
 !! \fref{mod_scalar_adr_ode,scalar_adr_solve}; in addition, we
 !! consider here two possible formulations of the implicit problem:
 !! linearly implicit and nonlinearly implicit.
 !!
 !! The linearly implicit case is the same as discussed in
 !! \fref{mod_scalar_adr_ode,scalar_adr_solve}.
 !!
 !! For the nonlinearly implicit case, the algorithm is the following:
 !! <ul>
 !!  <li> set \f$x^{(0)} \mapsfrom x_l\f$ (see
 !!  \field_fref{mod_time_integrators_base,c_ode,solve})
 !!  <li> compute all the diagnostics (in particular, \f$\Phi\f$) as
 !!  functions of \f$x^{(0)}\f$
 !!  <li> compute the matrices of the linear system
 !!  <li> compute the residual of \f$x^{(0)}\f$ as solution of the
 !!  linear system
 !!  <li> if the residual is larger than a prescribed tolerance, solve
 !!  the linear system computing \f$x^{(1)}\f$ and repeat the loop.
 !! </ul>
 !! \note The implementation ensures that at least one iteration is
 !! executed, irrespectively of the value of the residual.
 subroutine sol_minimal_solve(x,ode,t,sigma,b,xl,ods)
  class(t_sol_minimal_ode), intent(in) :: ode
  real(wp),     intent(in)    :: t, sigma
  class(c_stv), intent(in)    :: b, xl
  class(c_ods), intent(inout) :: ods
  class(c_stv), intent(inout) :: x

   ! The computation of the local matrices is easier if we can point
   ! some pointers to the fields of the diagnostics ods. However, this
   ! function must respect the general interface defined for the time
   ! integrators, so we can not include the TARGET attribute for the
   ! ods dummy argument. This can be circumvented having a call to
   ! another function, which is not constrained by the time integrator
   ! interface.
   !
   ! Note that this is both standard conforming and safe: no tricks!

   call sol_minimal_solve_impl(x,ode,t,sigma,b,xl,ods)

 end subroutine sol_minimal_solve


 subroutine sol_minimal_solve_impl(x,ode,t,sigma,b,xl,ods)
  class(t_sol_minimal_ode), intent(in) :: ode
  real(wp),     intent(in)    :: t, sigma
  class(c_stv), intent(in)    :: b, xl
  class(c_ods), intent(inout), target :: ods
  class(c_stv), intent(inout) :: x

  logical,  parameter :: linimplicit = .false.
  integer,  parameter :: picard_nmx = 100
  real(wp), parameter :: picard_tol = 1.0e5_wp

  integer :: ie, ie_pos, pos, i, j, niter
  real(wp) :: nres

   if(.not.allocated(picard_res)) allocate(picard_res(picard_nmx))
   picard_res = -1.0_wp

   select type(b  ); type is(t_sol_state)
   select type(xl ); type is(t_sol_state)
   select type(x  ); type is(t_sol_state)
   select type(ods); type is(t_sol_minimal_diags)

   ! 0) initial guess
   call x%copy( xl )

   associate( dens_mt=>mmmt(1) , dens_at=>aaat(1) , dens_rhs=>rhs(:,1) )

   niter = 0
   picard_do: do 
     niter = niter + 1


     ! 1) Update the diagnostics, including the electric field
     call update_sol_diagnostics( ods , ode%grid,ode%base,ode%bcs , x )


     ! 2) Compute the local and global matrices
     dens_mt%m(1,1)%ax = const_mmmt ! constant mass matrix term
     dens_at%m(1,1)%ax = 0.0_wp
     dens_rhs = 0.0_wp
     
     ie_pos = 0; pos = 0
     do ie=1,ode%grid%ne
       associate(    e => ode%grid%e(ie) ,      &
                  poly => ode%grid%e(ie)%poly , &
                    be => ode%bcs%b_e2be(ie)    )
       associate(  bme => ode%base%e(poly)%me ,    &
                    pk => ode%base%e(poly)%me%pk , &
                   lmm =>         mm(poly)%m ,     &
                   laa =>         aa(poly)%m ,     &
                    lb =>         bb(poly)%v       )

       ! 2.1) continuity equation
       ! 2.1.1) set the coefficient for the element
       dens_v_coeff%vn     => ods%e_diags(ie)%vn
       dens_v_coeff%div_vn => ods%e_diags(ie)%div_vn

       ! 2.1.2) compute and assemble the local matrices
       call loc_supg_mat( lmm,laa,lb , e,bme,be ,                  &
         dx_coeff,dens_v_coeff,dens_eps_coeff,lbcs=dens_lbcs_coeff )
       pos = ie_pos
       do j=1,pk
         do i=1,pk
           pos = pos+1
           dens_mt%t2c(pos)%p = dens_mt%t2c(pos)%p + lmm(i,j)
           dens_at%t2c(pos)%p = dens_at%t2c(pos)%p + laa(i,j)
         enddo
       enddo
       dens_rhs(e%iv) = dens_rhs(e%iv) + lb

       ie_pos = pos
       end associate
       end associate
     enddo

   
     ! 3) Compute the residual

     ! 3.1) continuity equation

     ! We solve for the increment Dx = x-b, which is more stable than
     ! solving directly for x
     sys_mat_t%ax = dens_mt%m(1,1)%ax + sigma*dens_at%m(1,1)%ax
     sys_rhs = sigma*( dens_rhs - matmul( b%ni(1)%s , dens_at%m(1,1) ) )

     call linsys_res( nres , sys_mat_t , x%ni(1)%s-b%ni(1)%s , &
                      sys_rhs , .true. , ode%grid,ode%ddc_grid )
     picard_res(niter) = nres

     ! 4) Solve the linear system

     if( (niter.ne.1) .and. &
        ( (nres.lt.picard_tol) .or. (niter.eq.picard_nmx) ) ) then

       exit picard_do

     else

       call linpb%factor('factorization')
       call linpb%solve( x%ni(1) ) ! here x stores  Dx = x-b

       x%ni(1)%s = x%ni(1)%s + b%ni(1)%s ! recover the complete x

       if(linimplicit) exit picard_do
     endif

   enddo picard_do


   ! X) momentum equation
   call x%vi(1)%copy( b%vi(1) ) ! todo

   end associate

   end select
   end select
   end select
   end select

 end subroutine sol_minimal_solve_impl

!-----------------------------------------------------------------------

 !> Compute all the diagnostics
 !!
 !! Update the diagnostic variables. These variables are then
 !! available for the evaluation of the nonlinear terms as well as for
 !! the output. Notice that here we deal with the <em>global</em>
 !! diagnostic quantities; additional local diagnostics can be
 !! computed using \c update_sol_loc_diagnostics.
 !!
 !! In particular, the velocity divergence is computed as discussed
 !! \ref solmin_vdrift "here".
 subroutine update_sol_diagnostics(ods,grid,base,bcs,xl)
  type(t_h2d_grid),     intent(in), target :: grid
  type(t_h2d_base),     intent(in), target :: base
  type(t_h2d_bcs),      intent(in) :: bcs
  type(t_sol_state),    intent(in) :: xl !< state used for linearization
  type(t_sol_minimal_diags), intent(inout) :: ods

  ! work array type
  type :: t_w
   ! intermediate results
   real(wp), allocatable :: ne(:), vpe(:), phie(:)
   real(wp), allocatable :: gradp_phy(:,:,:)
   real(wp), allocatable :: nn(:), fnn(:), vp(:)
   real(wp), allocatable :: gn(:,:), gphi(:,:), gpi_fn(:,:)
  end type t_w
  type(t_w), allocatable :: w(:)
  ! work array for the Poisson problem
  type(t_loc_poisson_rhs_data) :: wpsn

  integer :: ie, i, l, is
  character(len=*), parameter :: &
    this_sub_name = 'update_sol_diagnostics'

   !--------------------------------------------------------------------
   ! 1) First of all we solve the elliptic problem for Phi

   allocate( wpsn%sdata(base%ub) )

   psn_rhs_contr = 0.0_wp
   phi_elem_do: do ie=1,grid%ne
     associate(    e => grid%e(ie) ,      &
                poly => grid%e(ie)%poly , &
                  be => bcs%b_e2be(ie)    )
     associate(  bme => base%e(poly)%me , &
                  lb =>     b2(poly)%m    )

     ! Notice: the matrix is constant, we only need the RHS

     wpsn%edata%b     => sol_data%e_data(ie)%b
     wpsn%edata%c_bfb => sol_data%e_data(ie)%c_bfb
     wpsn%edata%curl_c_bfb => sol_data%e_data(ie)%curl_c_bfb
     wpsn%edata%te    => sol_data%e_data(ie)%te
     wpsn%edata%gte   => sol_data%e_data(ie)%gradte
     wpsn%edata%ti    => sol_data%e_data(ie)%ti
     wpsn%edata%gti   => sol_data%e_data(ie)%gradti
     wpsn%sigma_par => epot_sigma_coeff_sigma_par

     ! for boundary elements, the side data are also required
     if(associated(be%p)) then
       do i=1,be%p%nbs
         l = be%p%bs(i)%p%s%isl(1) ! local side index on element e
         is = e%is(l)              ! global side index

         wpsn%sdata(l)%b     => sol_data%s_data(is)%b
         wpsn%sdata(l)%c_bfb => sol_data%s_data(is)%c_bfb
         wpsn%sdata(l)%te    => sol_data%s_data(is)%te
         wpsn%sdata(l)%gte   => sol_data%s_data(is)%gradte
         wpsn%sdata(l)%ti    => sol_data%s_data(is)%ti
         wpsn%sdata(l)%gti   => sol_data%s_data(is)%gradti
       enddo
     endif

     call loc_poisson_rhs( lb , e,bme,be , dx_coeff ,             &
                  tc%phc%e,tc%phc%z , xl%ni(1)%s( e%iv ) , wpsn , &
                  epot_sigma_coeff,epot_lbcs_coeff )

     psn_rhs_contr( e%iv , : ) = psn_rhs_contr( e%iv , : ) + lb

     end associate
     end associate
   enddo phi_elem_do

   deallocate( wpsn%sdata )

   ! solve the Poisson linear system
   psn_rhs = sum( psn_rhs_contr , 2 )
   call psn_linpb%solve( ods%phi )

   !--------------------------------------------------------------------
   ! 2) Now we can compute the local diagnostics

   ! 2.0) we need to allocate the working arrays
   allocate( w(base%lb:base%ub) )
   do ie=base%lb,base%ub
     ! note: within this loop "ie" coincides with the number of sides
     associate( pk => base%e(ie)%me%pk , &
                 m => base%e(ie)%me%m  )

     allocate( w(ie)%ne(pk) , w(ie)%vpe(pk) , w(ie)%phie(pk) )
     allocate( w(ie)%gradp_phy(2,pk,m) )
     allocate( w(ie)%nn(m) , w(ie)%fnn(m) , w(ie)%vp(m) )
     allocate( w(ie)%gn(2,m) , w(ie)%gphi(2,m) , w(ie)%gpi_fn(2,m) )

     end associate
   enddo

   ! 2.1) element loop
   elem_do: do ie=1,grid%ne

     associate(    e => grid%e(ie) ,                &
                poly => grid%e(ie)%poly ,           &
               diags => ods%e_diags(ie) ,           &
                   b => sol_data%e_data(ie)%b ,     &
                divb => sol_data%e_data(ie)%divb ,  &
               c_bfb => sol_data%e_data(ie)%c_bfb , &
          curl_c_bfb => sol_data%e_data(ie)%curl_c_bfb , &
                  ti => sol_data%e_data(ie)%ti ,    &
                  te => sol_data%e_data(ie)%te ,    &
                 gti => sol_data%e_data(ie)%gradti )
     associate(  bme => base%e(poly)%me )
     associate(   ne => w(poly)%ne ,        &
                 vpe => w(poly)%vpe ,       &
                phie => w(poly)%phie ,      &
           gradp_phy => w(poly)%gradp_phy , &
                  nn => w(poly)%nn ,        &
                 fnn => w(poly)%fnn ,       &
                  vp => w(poly)%vp ,        &
                  gn => w(poly)%gn ,        &
                gphi => w(poly)%gphi ,      &
              gpi_fn => w(poly)%gpi_fn )

     ! get element nodal values
     ne  = xl%ni(1)%s(      e%iv ) ! density, element nodal values
     vpe = xl%vi(1)%v(1)%s( e%iv ) ! v_\parallel, el. nodal values
     phie = ods%phi%s(      e%iv ) ! Phi, element nodal values

     ! element basis functions: gradients in physical space
     gradp_phy = e%me%scale_gradp( bme%xig , bme%gradp )

     ! density and inverse density: values at quad. points
     !   -> consider limiting here in case of vanishing density
     nn  = matmul( ne , bme%p )
     fnn = 1.0_wp/nn
     ! other density related quantities
     do l=1,bme%m
       ! logarithmic density gradient (physical space)
       gn(  :,l) = fnn(l)*matmul( gradp_phy(:,:,l) , ne )
       ! electric potential: gradient (physical space)
       gphi(:,l) = matmul( gradp_phy(:,:,l) , phie )
       ! ionic pressure gradient (divided by n)
       gpi_fn(:,l) = gti(:,l) + ti(l)*gn(:,l)
     enddo

     ! ExB drift
     diags%exb = cross_3eq3x2( c_bfb , gphi )

     ! diamagnetic drift
     diags%diam_d = (1.0_wp/(tc%phc%z*tc%phc%e))  &
                   * cross_3eq3x2( c_bfb , gpi_fn )

     ! diffusion drift
     diags%nabla_d = - tc%phc%adiff_n*perp_proj_3eq2( b , gn     ) &
                     - tc%phc%adiff_p*perp_proj_3eq2( b , gpi_fn )

     ! ionic and electronic pressures
     diags%pi = ti*nn
     diags%pe = te*nn

     ! vn: velocity in the density equation
     vp = matmul( vpe , bme%p ) ! v_\parallel, quad. points
     do l=1,bme%m
       diags%vp(:,l) = b(:,l)*vp(l) ! add the unit vector b
     enddo
     diags%vn =                                                        &
         diags%vp(1:2,:)                                               &
       + diags%exb(1:2,:)                                              &
       + (1.0_wp/(tc%phc%z*tc%phc%e))*curl_c_bfb(1:2,:)*spread(ti,1,2) &
       - tc%phc%adiff_p * perp_proj( b(1:2,:) , gti )

     ! div(vn)
     do l=1,bme%m
       diags%div_vn(l) = &
         ! v_parallel contributions
         dot_product( matmul( b(1:2,l) , gradp_phy(:,:,l) ) , vpe ) &
         + vp(l) * divb(l)                                          &
         ! grad(Phi) and grad(Ti) contributions
         + dot_product( curl_c_bfb(1:2,l) ,                         &
                gphi(:,l) + (1.0_wp/(tc%phc%z*tc%phc%e))*gti(:,l) )
     enddo

     end associate
     end associate
     end associate
     
   enddo elem_do

   deallocate( w )
 
 end subroutine update_sol_diagnostics

!-----------------------------------------------------------------------

 !> Local diagnostics
 !!
 !! Besides the diagnostic quantities computed in \c
 !! update_sol_diagnostics, some additional quantities are required
 !! for each element. Typically, these quantities are then accessed by
 !! the <tt>*_coeff_*</tt> methods to provide the problem coefficients
 !! for the computation of the local matrices.
 subroutine update_sol_loc_diagnostics()
!  integer, intent(in) :: &
!  real, intent(in) ::    &
 
  character(len=*), parameter :: &
    this_sub_name = 'update_sol_loc_diagnostics'
 
 end subroutine update_sol_loc_diagnostics
 
!-----------------------------------------------------------------------

end module mod_sol_minimal_ode

