!! Copyright (C) 2009,2010,2011,2012  Marco Restelli
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Marco Restelli                   <marco.restelli@gmail.com>

!> Representation of <em>SOL state</em>
!!
!! \n
!!
!! We use here superscripts "e" and "i" to denote electrons and ions,
!! respectively. Then we need the following variables:
!! <ul>
!!  <li> prognostic variables
!!   <ul>
!!    <li> \f$n^{\rm i}\f$: ion density
!!    <li> \f${\bf v}^{\rm i}\f$: ion velocity (???)
!!    <li> \f$p^{\rm i}\f$: ion pressure (???)
!!    <li> \f$p^{\rm e}\f$: electron pressure (???)
!!   </ul>
!!  <li> diagnostic variables
!!   <ul>
!!    <li> \f$\Phi\f$: electric potential
!!   </ul>
!! </ul>
!! Many of these quantities are computed solving a linear system,
!! which requires a type extending \fref{mod_state_vars,c_stv}. For
!! this purpose, we introduce a type \c t_scalar_field which represents
!! a generic scalar finite element field through its nodal values.
!! Another type \c t_vector_field, built on top of \c t_scalar_field,
!! is introduced for vector quantities. Notice that this choice allows
!! one to treat as unknown in the implicit problems either the
!! separate components of the vector fields or the complete field.
!<----------------------------------------------------------------------
module mod_sol_state

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_octave_io, only: &
   mod_octave_io_initialized, &
   write_octave

 use mod_state_vars, only: &
   mod_state_vars_initialized, &
   c_stv

 use mod_time_integrators, only: &
   mod_time_integrators_initialized, &
   c_ode, c_ods

 use mod_h2d_grid, only: &
   mod_h2d_grid_initialized, &
   t_h2d_grid

 use mod_h2d_base, only: &
   mod_h2d_base_initialized, &
   t_h2d_base

 use mod_h2d_bcs, only: &
   mod_h2d_bcs_initialized, &
   t_h2d_bcs

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_sol_state_constructor, &
   mod_sol_state_destructor,  &
   mod_sol_state_initialized, &
   t_sol_state, new_sol_state, &
   t_sol_diags, &
   clear, write_octave, &
   ! other lower level types 
   t_scalar_field, t_vector_field

 private

!-----------------------------------------------------------------------

! Module types and parameters

 ! public members

 !> Generic scalar finite element field
 type, extends(c_stv) :: t_scalar_field
  type(t_h2d_grid), pointer :: grid => null()
  type(t_h2d_base), pointer :: base => null()
  type(t_h2d_bcs),  pointer :: bcs  => null()
  !> nodal scalar values
  real(wp), allocatable :: s(:)
 contains
  procedure, pass(x) :: incr => sf_incr
  procedure, pass(x) :: tims => sf_tims
  procedure, pass(z) :: copy => sf_copy
  procedure, pass(x) :: inlt => sf_inlt
  !procedure, pass(z) :: alt  => sf_alt
  procedure, pass(z) :: mlt  => sf_mlt
  !procedure, pass(x) :: scal => sf_scal
  !procedure, pass(x) :: show => sf_show
 end type t_scalar_field

 !> Generic vector field
 !!
 !! This type uses, for each scalar component, the operations defined
 !! in \c t_scalar_field.
 type, extends(c_stv) :: t_vector_field
  type(t_h2d_grid), pointer :: grid => null()
  type(t_h2d_base), pointer :: base => null()
  type(t_h2d_bcs),  pointer :: bcs  => null()
  integer :: d !< number of components
  !> nodal scalar values
  type(t_scalar_field), allocatable :: v(:)
 contains
  procedure, pass(x) :: incr => vf_incr
  procedure, pass(x) :: tims => vf_tims
  procedure, pass(z) :: copy => vf_copy
  procedure, pass(x) :: inlt => vf_inlt
  !procedure, pass(z) :: alt  => vf_alt
  procedure, pass(z) :: mlt  => vf_mlt
  !procedure, pass(x) :: scal => vf_scal
  !procedure, pass(x) :: show => vf_show
 end type t_vector_field

 !> State information for SOL
 type, extends(c_stv) :: t_sol_state
  type(t_h2d_grid), pointer :: grid => null()
  type(t_h2d_base), pointer :: base => null()
  type(t_h2d_bcs),  pointer :: bcs  => null()
  integer :: n_ions !< number of ion species
  !> ion densities
  type(t_scalar_field), allocatable :: ni(:)
  !> ion velocities
  type(t_vector_field), allocatable :: vi(:)
 contains
  procedure, pass(x) :: incr => sol_incr
  procedure, pass(x) :: tims => sol_tims
  procedure, pass(z) :: copy => sol_copy
  procedure, pass(x) :: inlt => sol_inlt
  !procedure, pass(z) :: alt  => sol_alt
  procedure, pass(z) :: mlt  => sol_mlt
  !procedure, pass(x) :: scal => sol_scal
  !procedure, pass(x) :: show => sol_show
 end type t_sol_state

 !> Diagnostics: electric field and potential
 type, extends(c_ods) :: t_sol_diags
  !> electric potential
  type(t_scalar_field) :: phi
  ! other fields are model specific and should be defined extending
  ! this type in the corresponding ode modules.
 end type t_sol_diags

 interface clear
   module procedure clear_sol_state
 end interface

 interface write_octave
   module procedure write_sol_state
 end interface write_octave

! Module variables

 ! public members
 logical, protected ::               &
   mod_sol_state_initialized = .false.
 ! private members
 character(len=*), parameter :: &
   this_mod_name = 'mod_sol_state'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_sol_state_constructor()
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_kinds_initialized.eqv..false.) .or. &
    (mod_messages_initialized.eqv..false.) .or. &
   (mod_octave_io_initialized.eqv..false.) .or. &
  (mod_state_vars_initialized.eqv..false.) .or. &
(mod_time_integrators_initialized.eqv..false.) .or. &
    (mod_h2d_grid_initialized.eqv..false.) .or. &
    (mod_h2d_base_initialized.eqv..false.) .or. &
     (mod_h2d_bcs_initialized.eqv..false.) ) then  
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_sol_state_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   mod_sol_state_initialized = .true.
 end subroutine mod_sol_state_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_sol_state_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_sol_state_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_sol_state_initialized = .false.
 end subroutine mod_sol_state_destructor

!-----------------------------------------------------------------------
 
 subroutine new_sol_state(obj,grid,base,bcs,n_ions,d)
  type(t_h2d_grid), intent(in), target :: grid
  type(t_h2d_base), intent(in), target :: base
  type(t_h2d_bcs),  intent(in), target :: bcs
  integer,          intent(in) :: n_ions, d
  type(t_sol_state), intent(out) :: obj

  integer :: i, id

   obj%grid => grid
   obj%base => base
   obj%bcs  => bcs
   obj%n_ions = n_ions

   ! ion density
   allocate( obj%ni(obj%n_ions) )
   do i=1,n_ions
     associate( ni=>obj%ni(i) )
     ni%grid => grid
     ni%base => base
     ni%bcs  => bcs
     allocate( ni%s( grid%nv ) )
     end associate
   enddo

   ! ion velocity
   allocate( obj%vi(obj%n_ions) )
   do i=1,n_ions
     associate( vi=>obj%vi(i) )
     vi%d = d
     vi%grid => grid
     vi%base => base
     vi%bcs  => bcs
     allocate( vi%v( vi%d ) )
     do id=1,vi%d
       associate( vi_id=>vi%v(id) )
       vi_id%grid => grid
       vi_id%base => base
       vi_id%bcs  => bcs
       allocate( vi_id%s( grid%nv ) )
       end associate
     enddo
     end associate
   enddo

 end subroutine new_sol_state
 
!-----------------------------------------------------------------------
 
 pure subroutine clear_sol_state(obj)
  type(t_sol_state), intent(out) :: obj

   nullify( obj%grid )
   nullify( obj%base )
   nullify( obj%bcs  )
   ! allocatable components are implicitly deallocated
 
 end subroutine clear_sol_state
 
!-----------------------------------------------------------------------

 subroutine sf_incr(x,y)
  class(c_stv), intent(in) :: y
  class(t_scalar_field), intent(inout) :: x

   select type(y); type is(t_scalar_field)

   x%s = x%s + y%s

   end select
 end subroutine sf_incr

 subroutine vf_incr(x,y)
  class(c_stv), intent(in) :: y
  class(t_vector_field), intent(inout) :: x

  integer :: i

   select type(y); type is(t_vector_field)

   do i=1,y%d
     call x%v(i)%incr(y%v(i))
     ! Possibly more efficient:
     ! x%v(i)%s = x%v(i)%s + y%v(i)%s
   enddo

   end select
 end subroutine vf_incr

 subroutine sol_incr(x,y)
  class(c_stv), intent(in) :: y
  class(t_sol_state), intent(inout) :: x

  integer :: i

   select type(y); type is(t_sol_state)

   do i=1,y%n_ions
     call x%ni(i)%incr( y%ni(i) )
     call x%vi(i)%incr( y%vi(i) )
   enddo

   end select
 end subroutine sol_incr

!-----------------------------------------------------------------------

 subroutine sf_tims(x,r)
  real(wp), intent(in) :: r
  class(t_scalar_field), intent(inout) :: x

   x%s = r*x%s

 end subroutine sf_tims

 subroutine vf_tims(x,r)
  real(wp), intent(in) :: r
  class(t_vector_field), intent(inout) :: x

  integer :: i

   do i=1,x%d
     call x%v(i)%tims(r)
     ! Possibly more efficient:
     ! x%v(i)%s = r*x%v(i)%s
   enddo
  
 end subroutine vf_tims

 subroutine sol_tims(x,r)
  real(wp), intent(in) :: r
  class(t_sol_state), intent(inout) :: x

  integer :: i

   do i=1,x%n_ions
     call x%ni(i)%tims( r )
     call x%vi(i)%tims( r )
   enddo

 end subroutine sol_tims

!-----------------------------------------------------------------------

 subroutine sf_copy(z,x)
  class(c_stv), intent(in) :: x
  class(t_scalar_field), intent(inout) :: z

   select type(x); type is(t_scalar_field)

   z%s = x%s

   end select
 end subroutine sf_copy

 subroutine vf_copy(z,x)
  class(c_stv), intent(in) :: x
  class(t_vector_field), intent(inout) :: z

  integer :: i

   select type(x); type is(t_vector_field)

   do i=1,x%d
     call z%v(i)%copy(x%v(i))
     ! Possibly more efficient:
     ! z%v(i)%s = x%v(i)%s
   enddo

   end select
 end subroutine vf_copy

 subroutine sol_copy(z,x)
  class(c_stv), intent(in) :: x
  class(t_sol_state), intent(inout) :: z

  integer :: i

   select type(x); type is(t_sol_state)

   do i=1,x%n_ions
     call z%ni(i)%copy( x%ni(i) )
     call z%vi(i)%copy( x%vi(i) )
   enddo

   end select
 end subroutine sol_copy

!-----------------------------------------------------------------------

 subroutine sf_inlt(x,r,y)
  real(wp), intent(in) :: r
  class(c_stv), intent(in) :: y
  class(t_scalar_field), intent(inout) :: x

   select type(y); type is(t_scalar_field)

   x%s = x%s + r*y%s

   end select
 end subroutine sf_inlt

 subroutine vf_inlt(x,r,y)
  real(wp), intent(in) :: r
  class(c_stv), intent(in) :: y
  class(t_vector_field), intent(inout) :: x

  integer :: i

   select type(y); type is(t_vector_field)

   do i=1,y%d
     call x%v(i)%inlt(r,y%v(i))
   enddo

   end select
 end subroutine vf_inlt

 subroutine sol_inlt(x,r,y)
  real(wp), intent(in) :: r
  class(c_stv), intent(in) :: y
  class(t_sol_state), intent(inout) :: x

  integer :: i

   select type(y); type is(t_sol_state)

   do i=1,y%n_ions
     call x%ni(i)%inlt( r , y%ni(i) )
     call x%vi(i)%inlt( r , y%vi(i) )
   enddo

   end select
 end subroutine sol_inlt

!-----------------------------------------------------------------------

 subroutine sf_mlt(z,r,x)
  real(wp), intent(in) :: r
  class(c_stv), intent(in) :: x
  class(t_scalar_field), intent(inout) :: z

   select type(x); type is(t_scalar_field)

   z%s = r*x%s

   end select
 end subroutine sf_mlt

 subroutine vf_mlt(z,r,x)
  real(wp), intent(in) :: r
  class(c_stv), intent(in) :: x
  class(t_vector_field), intent(inout) :: z

  integer :: i

   select type(x); type is(t_vector_field)

   do i=1,x%d
     call z%v(i)%mlt(r,x%v(i))
   enddo

   end select
 end subroutine vf_mlt

 subroutine sol_mlt(z,r,x)
  real(wp), intent(in) :: r
  class(c_stv), intent(in) :: x
  class(t_sol_state), intent(inout) :: z

  integer :: i

   select type(x); type is(t_sol_state)

   do i=1,x%n_ions
     call z%ni(i)%mlt( r , x%ni(i) )
     call z%vi(i)%mlt( r , x%vi(i) )
   enddo

   end select
 end subroutine sol_mlt

!-----------------------------------------------------------------------

!!-----------------------------------------------------------------------
!
! subroutine p_inlt(x,r,y)
!  real(wp),     intent(in) :: r
!  class(c_stv), intent(in) :: y
!  class(t_p_state), intent(inout) :: x
!
!   select type(y); type is(t_p_state)
!
!   x%pl = x%pl + r*y%pl
!
!   end select
! end subroutine p_inlt
!
!!-----------------------------------------------------------------------
!
! subroutine f_inlt(x,r,y)
!  real(wp),     intent(in) :: r
!  class(c_stv), intent(in) :: y
!  class(t_f_state), intent(inout) :: x
!
!  integer :: i,j,k, ni,nj,nk
!
!   select type(y); type is(t_f_state)
!
!   ! This seems to require a temporary with ifort, the loop is better
!   !x%f = x%f + r*y%f
!   nk = ubound(x%f,3)
!   nj = ubound(x%f,2)
!   ni = ubound(x%f,1)
!   do k=1,nk
!     do j=1,nj
!       do i=1,ni
!         x%f(i,j,k) = x%f(i,j,k) + r*y%f(i,j,k)
!       enddo
!     enddo
!   enddo
!
!   end select
! end subroutine f_inlt
!
!!-----------------------------------------------------------------------
!
! subroutine p_alt(z,x,r,y)
!  real(wp),     intent(in) :: r
!  class(c_stv), intent(in) :: x, y
!  class(t_p_state), intent(inout) :: z
!
!   select type(x); type is(t_p_state)
!    select type(y); type is(t_p_state)
!
!   z%pl = x%pl + r*y%pl
!
!    end select
!   end select
! end subroutine p_alt
!
!!-----------------------------------------------------------------------
!
! !> Overloaded for efficiency
! subroutine f_alt(z,x,r,y)
!  real(wp),     intent(in) :: r
!  class(c_stv), intent(in) :: x, y
!  class(t_f_state), intent(inout) :: z
!
!   select type(x); type is(t_f_state)
!    select type(y); type is(t_f_state)
!
!   z%f = x%f + r*y%f
!
!    end select
!   end select
! end subroutine f_alt
!
!!-----------------------------------------------------------------------
!
! subroutine p_mlt(z,r,x)
!  real(wp),     intent(in) :: r
!  class(c_stv), intent(in) :: x
!  class(t_p_state), intent(inout) :: z
!
!   select type(x); type is(t_p_state)
!
!   z%pl = r*x%pl
!
!   end select
! end subroutine p_mlt
!
!!-----------------------------------------------------------------------
!
! !> Overloaded for efficiency
! subroutine f_mlt(z,r,x)
!  real(wp),     intent(in) :: r
!  class(c_stv), intent(in) :: x
!  class(t_f_state), intent(inout) :: z
!
!   select type(x); type is(t_f_state)
!
!   z%f = r*x%f
!
!   end select
! end subroutine f_mlt
!
!!-----------------------------------------------------------------------
!
! subroutine p_copy(z,x)
!  class(c_stv),     intent(in)    :: x
!  class(t_p_state), intent(inout) :: z
!
!   select type(x); type is(t_p_state)
!
!   z%pl = x%pl
!
!   end select
! end subroutine p_copy
!
!!-----------------------------------------------------------------------
!
! subroutine f_copy(z,x)
!  class(c_stv),     intent(in)    :: x
!  class(t_f_state), intent(inout) :: z
!
!   select type(x); type is(t_f_state)
!
!   z%f  = x%f
!
!   end select
! end subroutine f_copy
!
!!-----------------------------------------------------------------------
!
! function p_scal(x,y) result(s)
!  class(t_p_state), intent(in) :: x
!  class(c_stv), intent(in) :: y
!  real(wp) :: s
!   
!   select type(y); type is(t_p_state)
!    s = dot_product(x%pl,y%pl)
!   end select
!
! end function p_scal
!
!!-----------------------------------------------------------------------
!
! function f_scal(x,y) result(s)
!  class(t_f_state), intent(in) :: x
!  class(c_stv), intent(in) :: y
!  real(wp) :: s
!
!  integer :: i,j,k, ni,nj,nk
!   
!   select type(y); type is(t_f_state)
!
!   !s = sum(x%f*y%f)
!   s = 0.0_wp
!   nk = ubound(x%f,3)
!   nj = ubound(x%f,2)
!   ni = ubound(x%f,1)
!   do k=1,nk
!     do j=1,nj
!       do i=1,ni
!         s = s + x%f(i,j,k)*y%f(i,j,k)
!       enddo
!     enddo
!   enddo
!
!   end select
!
! end function f_scal
!
!!-----------------------------------------------------------------------
!
! subroutine p_source(y,x)
!  class(t_p_state), intent(in) :: x
!  class(c_stv), allocatable, intent(out) :: y
!
!   allocate(t_p_state::y)
! 
!   select type(y); type is(t_p_state)
!
!   allocate(y%pl(size(x%pl)))
!
!   end select
! end subroutine p_source
! subroutine p_source_vect(y,x,m)
!  integer, intent(in) :: m
!  class(t_p_state), intent(in)  :: x
!  class(c_stv), allocatable, intent(out) :: y(:)
!
!  integer :: i
!
!   allocate(t_p_state::y(m))
!
!   select type(y); type is(t_p_state)
!
!   do i=1,m
!     allocate(y(i)%pl(size(x%pl)))
!   enddo
!
!   end select
! end subroutine p_source_vect
!
!!-----------------------------------------------------------------------
!
! subroutine p_show(x)
!  class(t_p_state), intent(in) :: x
!   write(*,'(a)') '------- Show a t_p_state object -------'
!   write(*,'(*(e12.3))') x%pl
!   write(*,'(a)') '---------------------------------------'
! end subroutine p_show
!
!!-----------------------------------------------------------------------
!
! subroutine f_show(x)
!  class(t_f_state), intent(in) :: x
!   write(*,'(a)') '------- Show a t_f_state object -------'
!   write(*,'(*(e12.3))') maxval(abs(x%f))
!   write(*,'(a)') '---------------------------------------'
! end subroutine f_show

!-----------------------------------------------------------------------

 subroutine write_sol_state(sol,var_name,fu)
  integer, intent(in) :: fu
  type(t_sol_state), intent(in) :: sol
  character(len=*), intent(in) :: var_name
 
  integer :: i, id, d
  real(wp), allocatable :: tmp2(:,:), tmp3(:,:,:)
  character(len=*), parameter :: &
    this_sub_name = 'write_sol_state'

   write(fu,'(a,a)') '# name: ',var_name
   write(fu,'(a)')   '# type: struct'
   write(fu,'(a)')   '# length: 3' ! number of fields

   ! field 01 : n_ions
   write(fu,'(a)')   '# name: n_ions'
   write(fu,'(a)')   '# type: cell'
   write(fu,'(a)')   '# rows: 1'
   write(fu,'(a)')   '# columns: 1'
   call write_octave(sol%n_ions,'<cell-element>',fu)

   ! field 02 : ni
   write(fu,'(a)')   '# name: ni'
   write(fu,'(a)')   '# type: cell'
   write(fu,'(a)')   '# rows: 1'
   write(fu,'(a)')   '# columns: 1'
   allocate( tmp2( sol%grid%nv , sol%n_ions ) )
   do i=1,sol%n_ions
     tmp2(:,i) = sol%ni(i)%s
   enddo
   call write_octave(tmp2,'<cell-element>',fu)
   deallocate( tmp2 )

   ! field 03 : vi
   write(fu,'(a)')   '# name: vi'
   write(fu,'(a)')   '# type: cell'
   write(fu,'(a)')   '# rows: 1'
   write(fu,'(a)')   '# columns: 1'
   if(sol%n_ions.gt.0) then
     d = sol%vi( lbound(sol%vi,1) )%d ! take the dim from the first ion
   else
     d = 0 ! for very weired cases
   endif
   allocate( tmp3( d , sol%grid%nv , sol%n_ions ) )
   do i=1,sol%n_ions
     do id=1,d
       tmp3(id,:,i) = sol%vi(i)%v(id)%s
     enddo
   enddo
   call write_octave(tmp3,'<cell-element>',fu)
   deallocate( tmp3 )

 end subroutine write_sol_state

!-----------------------------------------------------------------------

end module mod_sol_state

