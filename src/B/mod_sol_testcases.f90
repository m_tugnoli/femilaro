!> General interface to all the test-cases
!!
!! This module provides the interface used by all the rest of the
!! program to access the definition of the various test-cases.
!!
!! The main object made available by this module is \c tc, which
!! summarizes the test-case. No other variables of this type should
!! ever be introduced in the code, to ensure overall consistency.
!! Whenever it is required, one can use pointers to access the
!! variable provided here, for instance to make it accessible to the
!! methods of the problem coefficients.
!!
!! \c tc is declared \c protected: the test case constructors (and
!! destructors), which need to modify this variable, receive it as \c
!! out argument from the constructor of this module.
!!
!! All in all, the structure is very similar to the one used for other
!! applications; a detailed description, including a check list to
!! define a new test case, can be found in \c mod_dgcomp_testcases.
module mod_sol_testcases

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp
 
 use mod_sol_testcases_base, only: &
   mod_sol_testcases_base_constructor, &
   mod_sol_testcases_base_destructor,  &
   t_phc, t_sol_testcase

 use mod_simple_test, only: &
   mod_simple_test_constructor, &
   mod_simple_test_destructor,  &
   simple_test_name => test_name

 use mod_slab_analytic_test, only: &
   mod_slab_analytic_test_constructor, &
   mod_slab_analytic_test_destructor,  &
   slab_analytic_test_name => test_name

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_sol_testcases_constructor, &
   mod_sol_testcases_destructor,  &
   mod_sol_testcases_initialized, &
   t_phc, tc

 private

!-----------------------------------------------------------------------

! Module types and parameters

 !> Specification of the test case
 type(t_sol_testcase), save, target, protected :: tc

 logical, protected :: &
   mod_sol_testcases_initialized = .false.
 character(len=*), parameter :: &
   this_mod_name = 'mod_sol_testcases'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_sol_testcases_constructor(test_name)
  character(len=*), intent(in) :: test_name

  character(len=10000) :: message
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false.) .or. &
          (mod_kinds_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_sol_testcases_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   call mod_sol_testcases_base_constructor()

   casename: select case(trim(test_name))

    case(simple_test_name)
     call mod_simple_test_constructor(tc)

    case(slab_analytic_test_name)
     call mod_slab_analytic_test_constructor(tc)

    case default
     write(message,'(a,a,a)') 'Unknown test case "',trim(test_name),'"'
     call error(this_sub_name,this_mod_name,message)
   end select casename

   write(message,'(a,a,a)') 'Test case: "',trim(tc%test_name),'"'
   call info(this_sub_name,this_mod_name,message)

   mod_sol_testcases_initialized = .true.
 end subroutine mod_sol_testcases_constructor

!-----------------------------------------------------------------------

 subroutine mod_sol_testcases_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_sol_testcases_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   casename: select case(trim(tc%test_name))
    case(simple_test_name)
     call mod_simple_test_destructor(tc)
    case(slab_analytic_test_name)
     call mod_slab_analytic_test_destructor(tc)
   end select casename

   call mod_sol_testcases_base_destructor()

   mod_sol_testcases_initialized = .false.
 end subroutine mod_sol_testcases_destructor

!-----------------------------------------------------------------------

end module mod_sol_testcases

