!> Base module for \c mod_sol_testcases
module mod_sol_testcases_base

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_h2d_bcs, only: &
   mod_h2d_bcs_initialized, &
   t_b_2dv, t_b_2ds, t_b_2de, &
   b_dir,   b_neu,   b_ddc,   b_null

 use mod_em_physical_constants, only: &
   mod_em_physical_constants_initialized, &
   t_phc

 use mod_magnetic_geometry, only: &
   mod_magnetic_geometry_initialized, &
   i_dx, setup_magnetic_geometry, &
   compute_toroidal_magnetic_geom, &
   compute_local_magnetic_geom, &
   sharp_radial_profile, &
   compute_polynoml_temperat_prof

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_sol_testcases_base_constructor, &
   mod_sol_testcases_base_destructor,  &
   mod_sol_testcases_base_initialized, &
   t_b_2dv, t_b_2ds, t_b_2de, &
   b_dir,   b_neu,   b_ddc,   b_null, &
   t_phc, t_sol_testcase, &
   ! utilities for mod_magnetic_geometry
   setup_magnetic_geometry, &
   compute_toroidal_magnetic_geom, &
   compute_local_magnetic_geom, &
   sharp_radial_profile, &
   compute_polynoml_temperat_prof

 private

!-----------------------------------------------------------------------

! Module types and parameters

 ! public members

 !> Summarizes the test-case parameters and coefficients
 type :: t_sol_testcase
  !> physical constants
  type(t_phc) :: phc
  !> copy of the test name
  character(len=10000) :: test_name
  !> short description of the test-case
  character(len=10000), allocatable :: test_description(:)
  !> volume element
  procedure(i_dx),           pointer, nopass   :: dx => null()
  !> magnetic geometry
  procedure(i_magn_profile), pointer, pass(tc) :: magn_profile => null()
  !> initial density profile
  procedure(i_dens_profile), pointer, pass(tc) :: dens_profile => null()
  !> initial temperature profile
  procedure(i_temp_profile), pointer, pass(tc) :: temp_profile => null()
  !> density boundary conditions: type and value
  procedure(i_bctp),         pointer, pass(tc) :: dens_bctp    => null()
  !> \todo One could separate Dirichlet and Neumann/Robian bcs,
  !! especially since the Robin bcs require more data. Alternatively,
  !! optional arguments can be introduced for Robin bcs (this would
  !! work, since the caller knows the number of arguments given the bc
  !! type).
  procedure(i_bcvl),         pointer, pass(tc) :: dens_bcvl    => null()
  !> electric potential boundary conditions: type and value
  procedure(i_bctp),         pointer, pass(tc) :: epot_bctp    => null()
  procedure(i_bcvl),         pointer, pass(tc) :: epot_bcvl    => null()
 end type t_sol_testcase

 abstract interface
  pure subroutine i_magn_profile( b,gradb,divb , bb,gradbb ,    &
                                  sbb,gradsbb , curl_bfb , tc,x )
   import :: wp, t_sol_testcase
   implicit none
   class(t_sol_testcase), intent(in) :: tc
   real(wp), intent(in) :: x(:,:)
   real(wp), intent(out) :: b(:,:)
   real(wp), intent(out) :: gradb(:,:,:)
   real(wp), intent(out) :: divb(:)
   real(wp), intent(out) :: bb(:,:)
   real(wp), intent(out) :: gradbb(:,:,:)
   real(wp), intent(out) :: sbb(:)
   real(wp), intent(out) :: gradsbb(:,:)
   real(wp), intent(out) :: curl_bfb(:,:)
  end subroutine i_magn_profile  
 end interface

 abstract interface
  pure function i_dens_profile(tc,x) result(n)
   import :: wp, t_sol_testcase
   implicit none
   class(t_sol_testcase), intent(in) :: tc
   real(wp), intent(in) :: x(:)
   real(wp) :: n
  end function i_dens_profile
 end interface

 abstract interface
  pure subroutine i_temp_profile( ti,gradti , te,gradte , tc,x )
   import :: wp, t_sol_testcase
   implicit none
   class(t_sol_testcase), intent(in) :: tc
   real(wp), intent(in) :: x(:,:)
   real(wp), intent(out) :: ti(:)
   real(wp), intent(out) :: gradti(:,:)
   real(wp), intent(out) :: te(:)
   real(wp), intent(out) :: gradte(:,:)
  end subroutine i_temp_profile
 end interface

 abstract interface
  pure subroutine i_bctp( bc,btype , tc,breg )
   import :: wp, t_sol_testcase
   implicit none
   class(t_sol_testcase), intent(in) :: tc
   integer, intent(in)  :: breg
   integer, intent(out) :: bc, btype
  end subroutine i_bctp
 end interface

 abstract interface
  pure function i_bcvl(tc,breg,x) result(val)
   import :: wp, t_sol_testcase
   implicit none
   class(t_sol_testcase), intent(in) :: tc
   integer, intent(in)  :: breg
   real(wp), intent(in) :: x(:,:)
   real(wp) :: val(size(x,2))
  end function i_bcvl
 end interface

! Module variables

 ! public members
 logical, protected :: &
   mod_sol_testcases_base_initialized = .false.
 character(len=*), parameter :: &
   this_mod_name = 'mod_sol_testcases_base'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_sol_testcases_base_constructor()
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if(       (mod_messages_initialized.eqv..false.) .or. &
                (mod_kinds_initialized.eqv..false.) .or. &
              (mod_h2d_bcs_initialized.eqv..false.) .or. &
(mod_em_physical_constants_initialized.eqv..false.) .or. &
    (mod_magnetic_geometry_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_sol_testcases_base_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   mod_sol_testcases_base_initialized = .true.
 end subroutine mod_sol_testcases_base_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_sol_testcases_base_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_sol_testcases_base_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_sol_testcases_base_initialized = .false.
 end subroutine mod_sol_testcases_base_destructor

!-----------------------------------------------------------------------

end module mod_sol_testcases_base

