!> Collect some vetor operations
!!
!! This is a small module containing various vector operations, mostly
!! cross products and projections. Some of these operations appear in
!! the problem with multiple <em>flavours</em> depending on the
!! dimension of the vectors they operate on, which can be two- or
!! three-dimensional.
!!
!! We refer here to the geometrical setup discussed in \c
!! mod_magnetic_geometry, and in particular \ref vector_implementation
!! "here".
!<----------------------------------------------------------------------
module mod_vect_operators

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_vect_operators_constructor, &
   mod_vect_operators_destructor,  &
   mod_vect_operators_initialized, &
   cross_3eq3x3, cross_3eq3x2, cross_2eq3x2, &
   perp_proj, perp_proj_3eq2

 private

!-----------------------------------------------------------------------

! Module variables

 ! public members
 logical, protected ::               &
   mod_vect_operators_initialized = .false.
 ! private members
 character(len=*), parameter :: &
   this_mod_name = 'mod_vect_operators'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_vect_operators_constructor()
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_kinds_initialized.eqv..false.) .or. &
    (mod_messages_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_vect_operators_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   mod_vect_operators_initialized = .true.
 end subroutine mod_vect_operators_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_vect_operators_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_vect_operators_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_vect_operators_initialized = .false.
 end subroutine mod_vect_operators_destructor

!-----------------------------------------------------------------------

 !> Compute \f${\bf c}={\bf a}\times{\bf b}\f$
 pure function cross_3eq3x3(a,b) result(c)
  real(wp), intent(in) :: a(:,:), b(:,:)
  real(wp) :: c(3,size(a,2))

   c(1,:) = a(2,:)*b(3,:) - a(3,:)*b(2,:)
   c(2,:) = a(3,:)*b(1,:) - a(1,:)*b(3,:)
   c(3,:) = a(1,:)*b(2,:) - a(2,:)*b(1,:)
   
 end function cross_3eq3x3

 !> Same as \c cross_3eq3x3 but the second vector, \c b, is in the
 !! poloidal plane.
 pure function cross_3eq3x2(a,b) result(c)
  real(wp), intent(in) :: a(:,:), b(:,:)
  real(wp) :: c(3,size(a,2))

   c(1,:) =               - a(3,:)*b(2,:)
   c(2,:) = a(3,:)*b(1,:)
   c(3,:) = a(1,:)*b(2,:) - a(2,:)*b(1,:)
  
 end function cross_3eq3x2

 !> Same as \c cross_3eq3x2 but only the poloidal component of the
 !! result is computed
 pure function cross_2eq3x2(a,b) result(c)
  real(wp), intent(in) :: a(:,:), b(:,:)
  real(wp) :: c(2,size(a,2))

   c(1,:) =               - a(3,:)*b(2,:)
   c(2,:) = a(3,:)*b(1,:)
  
 end function cross_2eq3x2

!-----------------------------------------------------------------------

 !> Perpendicular projection \f${\bf b}=\left(\mathcal{I}-{\bf
 !! v}\otimes{\bf v}\right){\bf a}\f$ (all vectors have the same
 !! dimension)
 pure function perp_proj(v,a) result(b)
  real(wp), intent(in) :: v(:,:), a(:,:)
  real(wp) :: b(size(a,1),size(a,2))

  integer :: l

   do l=1,size(a,2)
     b(:,l) = a(:,l) - dot_product(v(:,l),a(:,l))*v(:,l)
   enddo
  
 end function perp_proj

 !> Same as perp_proj but returns a 3D vector given a poloidal input
 pure function perp_proj_3eq2(v,a) result(b)
  real(wp), intent(in) :: v(:,:) ! (3,:) 
  real(wp), intent(in) :: a(:,:) ! (2,:)
  real(wp) :: b(3,size(a,2))     ! (3,:)

  integer :: l

   b(1:2,:) = a
   b( 3 ,:) = 0.0_wp
   do l=1,size(a,2)
     b(:,l) = b(:,l) - dot_product(v(1:2,l),a(:,l))*v(:,l)
   enddo
  
 end function perp_proj_3eq2

!-----------------------------------------------------------------------

end module mod_vect_operators

