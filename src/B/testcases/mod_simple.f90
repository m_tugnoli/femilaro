module mod_simple_test

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_sol_testcases_base, only: &
   mod_sol_testcases_base_initialized, &
   t_b_2dv, t_b_2ds, t_b_2de, &
   b_dir,   b_neu,   b_ddc,   b_null, &
   t_phc, t_sol_testcase, &
   setup_magnetic_geometry, &
   compute_toroidal_magnetic_geom, &
   sharp_radial_profile, &
   compute_polynoml_temperat_prof

!-----------------------------------------------------------------------

 implicit none

!-----------------------------------------------------------------------

 public :: &
   mod_simple_test_constructor, &
   mod_simple_test_destructor,  &
   mod_simple_test_initialized, &
   test_name

 real(wp), parameter :: &
   ! geometry parameters (must be consistent with the grid)
   r0 = 165.0_wp,    & ! cm
   a  =  60.0_wp,    & ! cm
   rmin = 0.8_wp*a,  &
   rmax = 1.2_wp*a,  &
   ! magnetic field
   b0 =   2.5e4_wp,  & ! Gauss (1 Tesla = 10^4 Gauss)
   bp =   0.1_wp*b0, &
   ! initial density perturbation
   pert_number    = 10.0_wp, &
   pert_amplitude = 0.1_wp

 integer, parameter :: &
   !> boundary index for the internal/external boundary
   breg_internal = 1, &
   breg_external = 2

 character(len=*), parameter :: test_name = "simple"

 logical, protected :: &
   mod_simple_test_initialized = .false.
 character(len=*), parameter :: &
   this_mod_name = 'mod_simple_test'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_simple_test_constructor(tc)
  type(t_sol_testcase), intent(out) :: tc

  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if(       (mod_messages_initialized.eqv..false.) .or. &
                (mod_kinds_initialized.eqv..false.) .or. &
   (mod_sol_testcases_base_initialized.eqv..false.) ) then 
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_simple_test_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   ! 0) Copy the name of the testcase
   tc%test_name = test_name

   ! 1) Redefine some physical constants
   tc%phc%adiff_n = 100.0_wp * tc%phc%adiff_n
   tc%phc%adiff_p = 0.0_wp
   tc%phc%sigma_an_eps = 5.0e-9_wp ! anomalous conductivity

   ! 2) Geometrical setup
   call setup_magnetic_geometry("toroidal",r0 , tc%dx)

   ! 3) Define the profiles
   tc%magn_profile => magn_profile
   tc%dens_profile => dens_profile
   tc%temp_profile => temp_profile

   ! 4) Set the boundary conditions
   tc%dens_bctp => dens_bc_type
   tc%dens_bcvl => dens_bc_value
   tc%epot_bctp => epot_bc_type
   tc%epot_bcvl => epot_bc_value

   ! 5) Define a short description of the test case
   allocate( tc%test_description(4) )
   tc%test_description(1) = "Simple test case"
   tc%test_description(2) = "  magnetic geometry -> toroidal, analytic"
   write( tc%test_description(3) , '(a,*(e24.15e3:" , "))' ) &
     "       b0, bp = ", b0, bp
   write( tc%test_description(4) , '(a,*(e24.15e3:" , "))' ) &
     "  adiff_{n,p} = ", tc%phc%adiff_n, tc%phc%adiff_p 

   mod_simple_test_initialized = .true.
 end subroutine mod_simple_test_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_simple_test_destructor(tc)
  type(t_sol_testcase), intent(inout) :: tc

  character(len=*), parameter :: &
    this_sub_name = 'destructor'

   !Consistency checks ---------------------------
   if(mod_simple_test_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   deallocate( tc%test_description )
   nullify(tc%magn_profile)
   nullify(tc%dens_profile)
   nullify(tc%temp_profile)

   mod_simple_test_initialized = .false.
 end subroutine mod_simple_test_destructor

!-----------------------------------------------------------------------

 pure subroutine magn_profile( b,gradb,divb , bb,gradbb ,    &
                               sbb,gradsbb , curl_bfb , tc,x )
  class(t_sol_testcase), intent(in) :: tc
  real(wp), intent(in) :: x(:,:)
  real(wp), intent(out) :: b(:,:)
  real(wp), intent(out) :: gradb(:,:,:)
  real(wp), intent(out) :: divb(:)
  real(wp), intent(out) :: bb(:,:)
  real(wp), intent(out) :: gradbb(:,:,:)
  real(wp), intent(out) :: sbb(:)
  real(wp), intent(out) :: gradsbb(:,:)
  real(wp), intent(out) :: curl_bfb(:,:)

   call compute_toroidal_magnetic_geom(                       &
      b,gradb,divb , bb,gradbb , sbb,gradsbb , curl_bfb , x , &
                                                   r0,a,bp,b0 )

 end subroutine magn_profile  

!-----------------------------------------------------------------------

 pure function dens_profile(tc,x) result(n)
  class(t_sol_testcase), intent(in) :: tc
  real(wp), intent(in) :: x(:)
  real(wp) :: n

   n = sharp_radial_profile( x , r0,rmin,rmax,2.0_wp,      &
      tc%phc%nmin,tc%phc%nmax , pert_number,pert_amplitude )
   
 end function dens_profile

!-----------------------------------------------------------------------

 pure subroutine temp_profile( ti,gradti , te,gradte , tc,x )
  class(t_sol_testcase), intent(in) :: tc
  real(wp), intent(in) :: x(:,:)
  real(wp), intent(out) :: ti(:)
  real(wp), intent(out) :: gradti(:,:)
  real(wp), intent(out) :: te(:)
  real(wp), intent(out) :: gradte(:,:)

  real(wp) :: tmin

   associate( tmax => tc%phc%t0 )

   tmin = 0.001_wp * tmax 

   call compute_polynoml_temperat_prof(ti,gradti,te,gradte,x, &
                                    r0,rmin,rmax, tmax , tmin )

   end associate

 end subroutine temp_profile

!-----------------------------------------------------------------------

 pure subroutine dens_bc_type( bc,btype , tc,breg )
  class(t_sol_testcase), intent(in) :: tc
  integer, intent(in)  :: breg
  integer, intent(out) :: bc, btype

   bc    = b_dir
   btype = 0 ! not used for the time being
   
 end subroutine dens_bc_type

!-----------------------------------------------------------------------

 pure function dens_bc_value( tc,breg,x ) result(val)
  class(t_sol_testcase), intent(in) :: tc
  integer, intent(in)  :: breg
  real(wp), intent(in) :: x(:,:)
  real(wp) :: val(size(x,2))

   select case(breg)
    case(breg_internal)
     val = tc%phc%nmax
    case(breg_external)
     val = tc%phc%nmin
    case default
     val = -huge(val) ! something wrong
   end select

 end function dens_bc_value

!-----------------------------------------------------------------------

 pure subroutine epot_bc_type( bc,btype , tc,breg )
  class(t_sol_testcase), intent(in) :: tc
  integer, intent(in)  :: breg
  integer, intent(out) :: bc, btype

   select case(breg)
    case(breg_internal)
     bc = b_dir
    case(breg_external)
     bc = b_neu
    case default
     bc = b_null
   end select
   btype = 0 ! not used for the time being

 end subroutine epot_bc_type

!-----------------------------------------------------------------------

 pure function epot_bc_value( tc,breg,x ) result(val)
  class(t_sol_testcase), intent(in) :: tc
  integer, intent(in)  :: breg
  real(wp), intent(in) :: x(:,:)
  real(wp) :: val(size(x,2))

   val = 0.0_wp

 end function epot_bc_value

!-----------------------------------------------------------------------

end module mod_simple_test

