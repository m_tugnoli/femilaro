!>\brief
!!
!! Slab local test case.
!!
!! \n
!!
!! This is essentially a one-dimensional test case representing a
!! local region of the toroidal scrape-off layer. The considered
!! geometry, as well as the various parameters, are illustrated in the
!! following figure (see also
!! \fref{mod_magnetic_geometry,compute_local_magnetic_geom} for the
!! details of the magnetic geometry).
!!
!! \image html local-domain.png
!! \image latex local-domain.png "" width=0.3\textwidth
!!
!! We make the following assumptions:
!! <ul>
!!  <li> \f$\eta_0 = -\eta_1\f$
!!  <li> \f$\partial_{\xi}n=0\f$ and \f$\partial_{\xi}\Phi=0\f$ on the
!!  lateral boundaries
!!  <li> Dirichlet boundary conditions for \f$n\f$ for
!!  \f$\eta=\eta_{0,1}\f$
!!  <li> \f$\partial_{\eta}\Phi|_{\eta_0} = C\f$ and
!!  \f$\Phi|_{\eta_1}=0\f$,
!! </ul>
!! where the constant \f$C\f$ is determined in order to obtain a
!! closed-form analytic solution.
module mod_slab_analytic_test

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_sol_testcases_base, only: &
   mod_sol_testcases_base_initialized, &
   t_b_2dv, t_b_2ds, t_b_2de, &
   b_dir,   b_neu,   b_ddc,   b_null, &
   t_phc, t_sol_testcase, &
   setup_magnetic_geometry, &
   compute_local_magnetic_geom

!-----------------------------------------------------------------------

 implicit none

!-----------------------------------------------------------------------

 public :: &
   mod_slab_analytic_test_constructor, &
   mod_slab_analytic_test_destructor,  &
   mod_slab_analytic_test_initialized, &
   test_name

 real(wp), parameter :: &
   ! geometry parameters (must be consistent with the grid)
   r0 = 165.0_wp,    & ! cm
   theta = -90.0_wp * 3.14159265358979323846264338327950_wp/180.0_wp, &
   ! the following three dimensions should be consistent with the grid
   a    = 60.0_wp,   & ! cm
   rmin = 0.9_wp*a,  &
   rmax = 1.1_wp*a,  &
   ! magnetic field
   b0 =   2.5e4_wp     ! Gauss (1 Tesla = 10^4 Gauss)
 logical, parameter :: &
   cart_geom = .true.  ! use the Cartesian geometry
   ! constant temperature: nothing to do
 integer, parameter :: &
   ! markers of the boundary regions
   internal_side = 1, & ! eta = eta_0
   external_side = 3, & ! eta = eta_1
   side_side_xip = 2, & ! maximum xi
   side_side_xim = 4    ! minimum xi
 real(wp), parameter :: &
   phi_internal_side = -1.0031391775167622_wp

 real(wp) :: eta0, eta1
 real(wp) :: sigma_perp, sigma_an

 character(len=*), parameter :: test_name = "slab_analytic"

 logical, protected :: &
   mod_slab_analytic_test_initialized = .false.
 character(len=*), parameter :: &
   this_mod_name = 'mod_slab_analytic_test'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_slab_analytic_test_constructor(tc)
  type(t_sol_testcase), intent(out) :: tc

  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if(       (mod_messages_initialized.eqv..false.) .or. &
                (mod_kinds_initialized.eqv..false.) .or. &
   (mod_sol_testcases_base_initialized.eqv..false.) ) then 
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_slab_analytic_test_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   ! 0) Copy the name of the testcase
   tc%test_name = test_name

   ! 1) Redefine some physical constants
   associate( phc => tc%phc )

   ! redefine the Coulomb logarithm, which also affects tau_e
   phc%lambda  = 10.0_wp
   phc%tau_e   = 3.0_wp*sqrt(phc%me)                     &
                / (4.0_wp*phc%sqrt2pi*phc%lambda*phc%e**4)

   ! density profile
   phc%nmax = 1.0e13_wp
   phc%nmin = 494255002381.34491_wp

   ! set the density diffusion
   phc%adiff_n = phc%adiff_n / 1.0e4_wp
   phc%adiff_p = 0.0_wp

   ! scale the maximum temperature
   phc%t0      = 0.1_wp * phc%t0 ! 100 eV = 0.1 keV

   ! anomalous conductivity
   phc%sigma_an_eps = 5.0e-9_wp

   end associate

   ! 2) Geometrical setup
   eta0 = eta( (/ rmin*cos(theta)+r0 , rmin*sin(theta) /) ,theta)
   eta1 = eta( (/ rmax*cos(theta)    , rmax*sin(theta) /) ,theta)
   call setup_magnetic_geometry("Cartesian",r0 , tc%dx)

   ! 3) Set some module variables, used by other local functions
   associate( phc => tc%phc , te => tc%phc%t0 )
   sigma_perp = (phc%e**2/phc%me) * phc%tau_e * sqrt(te**3)
   sigma_an = phc%sigma_an_eps * sigma_perp
   end associate

   ! 4) Define the profiles
   tc%magn_profile => magn_profile
   tc%dens_profile => dens_profile
   tc%temp_profile => temp_profile

   ! 5) Set the boundary conditions
   tc%dens_bctp => dens_bc_type
   tc%dens_bcvl => dens_bc_value
   tc%epot_bctp => epot_bc_type
   tc%epot_bcvl => epot_bc_value

   ! 6) Define a short description of the test case
   allocate( tc%test_description(6) )
   tc%test_description(1) = "Slab geometry test case"
   write( tc%test_description(2) , '(a,*(e23.15e3:" , "))' ) &
     "        r0, theta = ", r0, theta
   write( tc%test_description(3) , '(a,*(e23.15e3:" , "))' ) &
     "       rmin, rmax = ", rmin, rmax
   write( tc%test_description(4) , '(a,*(e23.15e3:" , "))' ) &
     "       eta0, eta1 = ", eta0, eta1
   write( tc%test_description(5) , '(a,*(e23.15e3:" , "))' ) &
     "      adiff_{n,p} = ", tc%phc%adiff_n, tc%phc%adiff_p 
   write( tc%test_description(6) , '(a,*(e23.15e3:" , "))' ) &
     "  sigma_{perp,an} = ", sigma_perp, sigma_an

   mod_slab_analytic_test_initialized = .true.
 end subroutine mod_slab_analytic_test_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_slab_analytic_test_destructor(tc)
  type(t_sol_testcase), intent(inout) :: tc

  character(len=*), parameter :: &
    this_sub_name = 'destructor'

   !Consistency checks ---------------------------
   if(mod_slab_analytic_test_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   deallocate( tc%test_description )
   nullify(tc%magn_profile)
   nullify(tc%dens_profile)
   nullify(tc%temp_profile)

   mod_slab_analytic_test_initialized = .false.
 end subroutine mod_slab_analytic_test_destructor

!-----------------------------------------------------------------------

 pure subroutine magn_profile( b,gradb,divb , bb,gradbb ,    &
                               sbb,gradsbb , curl_bfb , tc,x )
  class(t_sol_testcase), intent(in) :: tc
  real(wp), intent(in) :: x(:,:)
  real(wp), intent(out) :: b(:,:)
  real(wp), intent(out) :: gradb(:,:,:)
  real(wp), intent(out) :: divb(:)
  real(wp), intent(out) :: bb(:,:)
  real(wp), intent(out) :: gradbb(:,:,:)
  real(wp), intent(out) :: sbb(:)
  real(wp), intent(out) :: gradsbb(:,:)
  real(wp), intent(out) :: curl_bfb(:,:)

   call compute_local_magnetic_geom(                          &
      b,gradb,divb , bb,gradbb , sbb,gradsbb , curl_bfb , x , &
                                                        r0,b0 )

 end subroutine magn_profile  

!-----------------------------------------------------------------------

 !> Linear profile in \f$\eta\f$
 pure function dens_profile(tc,x) result(n)
  class(t_sol_testcase), intent(in) :: tc
  real(wp), intent(in) :: x(:)
  real(wp) :: n

  real(wp) :: e, nv(1)

   ! linear profile
   !e = eta( x , theta )
   !n =  (eta1-e)/(eta1-eta0) * tc%phc%nmax &
   !   + (e-eta0)/(eta1-eta0) * tc%phc%nmin

   ! analytic profile
   nv = n_analytic(reshape( x , (/2,1/)),tc)
   n = nv(1)

 end function dens_profile

!-----------------------------------------------------------------------

 pure subroutine temp_profile( ti,gradti , te,gradte , tc,x )
  class(t_sol_testcase), intent(in) :: tc
  real(wp), intent(in) :: x(:,:)
  real(wp), intent(out) :: ti(:)
  real(wp), intent(out) :: gradti(:,:)
  real(wp), intent(out) :: te(:)
  real(wp), intent(out) :: gradte(:,:)

   ti = tc%phc%t0
   te = tc%phc%t0

   gradti = 0.0_wp
   gradte = 0.0_wp

 end subroutine temp_profile

!-----------------------------------------------------------------------

 pure subroutine dens_bc_type( bc,btype , tc,breg )
  class(t_sol_testcase), intent(in) :: tc
  integer, intent(in)  :: breg
  integer, intent(out) :: bc, btype

   !select case(breg)
   ! case(internal_side,external_side)
   !  bc = b_dir
   ! case(side_side_xip) ! inflow: xi = xi_max
   !  bc = b_dir
   ! case(side_side_xim)
   !  bc = b_neu
   ! case default
   !  bc = b_null
   !end select

   bc = b_dir

   btype = 0 ! not used for the time being
   
 end subroutine dens_bc_type

!-----------------------------------------------------------------------

 pure function dens_bc_value( tc,breg,x ) result(n)
  class(t_sol_testcase), intent(in) :: tc
  integer, intent(in)  :: breg
  real(wp), intent(in) :: x(:,:)
  real(wp) :: n(size(x,2))

   !select case(breg)

   ! ! The first three are Dirichlet conditions
   ! case(internal_side)
   !  n = tc%phc%nmax

   ! case(external_side,side_side_xip) ! use the exact solution
   !  n = n_analytic(x,tc)

   ! ! This is a Neumann condition
   ! case(side_side_xim)
   !  n = 0.0_wp

   ! case default
   !  n = -huge(n) ! something wrong
   !end select

   n = n_analytic(x,tc)

 end function dens_bc_value

!-----------------------------------------------------------------------

 pure subroutine epot_bc_type( bc,btype , tc,breg )
  class(t_sol_testcase), intent(in) :: tc
  integer, intent(in)  :: breg
  integer, intent(out) :: bc, btype

   select case(breg)
    case(internal_side,external_side)
     bc = b_dir
    case(side_side_xip,side_side_xim)
     bc = b_neu
    case default
     bc = b_null
   end select

   btype = 0 ! not used for the time being
   
 end subroutine  epot_bc_type

!-----------------------------------------------------------------------

 pure function epot_bc_value( tc,breg,x ) result(val)
  class(t_sol_testcase), intent(in) :: tc
  integer, intent(in)  :: breg
  real(wp), intent(in) :: x(:,:)
  real(wp) :: val(size(x,2))

   select case(breg)

    ! The first two are Neumann bcs
    case(side_side_xip,side_side_xim)
     val = 0.0_wp

    ! Now the Dirichlet bcs
    case(internal_side)
     val = phi_internal_side

    case(external_side)
     val = 0.0_wp

    case default
     val = -huge(val) ! something wrong
   end select

 end function epot_bc_value

!-----------------------------------------------------------------------

 !> Compute the local coordinate \f$\eta\f$
 !!
 !! Consider the following picture.
 !!
 !! \image html local-eta.png
 !! \image latex local-eta.png "" width=0.3\textwidth
 !!
 !! We have
 !! \f{displaymath}{
 !!  \begin{array}{rcl}
 !!   \xi & = & \sin\vartheta\,R - \cos\vartheta\,z + \bar{\xi} \\
 !!   \eta & = & \cos\vartheta\,R + \sin\vartheta\,z + \bar{\eta}.
 !!  \end{array}
 !! \f}
 !! The constants \f$\bar{\xi},\bar{\eta}\f$ are not precisely
 !! defined, so that only differences \f$\Delta\xi, \Delta\eta\f$
 !! should be used.
 pure function eta(x,theta)
  real(wp), intent(in) :: x(:), theta
  real(wp) :: eta

   eta = cos(theta)*x(1) + sin(theta)*x(2)

 end function eta

!-----------------------------------------------------------------------

 !> Analytic solution
 !!
 !! \f{displaymath}{
 !!  n = \frac{n_0}{1-\tilde{\eta}},
 !! \f}
 !! where
 !! \f{displaymath}{
 !!  \tilde{\eta} = \frac{ n_0(1+Z)Ze \beta}{\sigma_{\rm an}}
 !!   \left( \eta - \eta_0 \right), \qquad
 !!  \beta = \frac{c\sin\vartheta}{B_0R_0}.
 !! \f}
 pure function n_analytic(x,tc) result(n)
  class(t_sol_testcase), intent(in) :: tc
  real(wp), intent(in) :: x(:,:)
  real(wp) :: n(size(x,2))

  integer :: l
  real(wp) :: a, b, etat(size(x,2))

   associate( phc => tc%phc )

   b = phc%c * sin(theta)/(b0*r0)
   a = phc%nmax * (1.0_wp+phc%z) * phc%z * phc%e * b / sigma_an

   do l=1,size(etat)
     etat(l) = a*( eta(x(:,l),theta) - eta0 )
   enddo

   n = phc%nmax/( 1.0_wp - etat )

   end associate

 end function n_analytic

!-----------------------------------------------------------------------

end module mod_slab_analytic_test

