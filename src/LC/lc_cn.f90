!> \file
!!
!! Solve the harmonic map heat flow problem using the Crank-Nicolson
!! scheme for the time integration.
!!
!! \n
!!
!! Solve the harmonic map heat flow problem using the Crank-Nicolson
!! scheme for the time integration.
program lc_cn

 use mod_utils, only: &
   t_realtime, my_second

 use mod_messages, only: &
   mod_messages_constructor, &
   mod_messages_destructor,  &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_constructor, &
   mod_kinds_destructor,  &
   wp

 use mod_mpi_utils, only: &
   mod_mpi_utils_constructor, &
   mod_mpi_utils_destructor,  &
   mpi_logical, mpi_integer, wp_mpi, &
   mpi_comm_world, &
   mpi_init_thread, mpi_thread_single, mpi_thread_multiple, &
   mpi_finalize, &
   mpi_comm_size, mpi_comm_rank, &
   mpi_bcast, mpi_gather

 use mod_fu_manager, only: &
   mod_fu_manager_constructor, &
   mod_fu_manager_destructor,  &
   new_file_unit

 use mod_octave_io, only: &
   mod_octave_io_constructor, &
   mod_octave_io_destructor,  &
   real_format,    &
   write_octave,   &
   read_octave,    &
   read_octave_al
 
 use mod_linal, only: &
   mod_linal_constructor, &
   mod_linal_destructor

 use mod_perms, only: &
   mod_perms_constructor, &
   mod_perms_destructor

 use mod_octave_io_perms, only: &
   mod_octave_io_perms_constructor, &
   mod_octave_io_perms_destructor

 use mod_sympoly, only: &
   mod_sympoly_constructor, &
   mod_sympoly_destructor

 use mod_octave_io_sympoly, only: &
   mod_octave_io_sympoly_constructor, &
   mod_octave_io_sympoly_destructor

 use mod_numquad, only: &
   mod_numquad_constructor, &
   mod_numquad_destructor

 use mod_state_vars, only: &
   mod_state_vars_constructor, &
   mod_state_vars_destructor,  &
   c_stv

 use mod_sparse, only: &
   mod_sparse_constructor, &
   mod_sparse_destructor

 use mod_octave_io_sparse, only: &
   mod_octave_io_sparse_constructor, &
   mod_octave_io_sparse_destructor, &
   write_octave

 use mod_output_control, only: &
   mod_output_control_constructor, &
   mod_output_control_destructor,  &
   elapsed_format, &
   base_name

 use mod_master_el, only: &
   mod_master_el_constructor, &
   mod_master_el_destructor, &
   t_lagnodes, lag_nodes

 use mod_grid, only: &
   mod_grid_constructor, &
   mod_grid_destructor,  &
   t_grid, t_ddc_grid,   &
   new_grid, clear,      &
   write_octave

 use mod_base, only: &
   mod_base_constructor, &
   mod_base_destructor,  &
   t_base, clear, &
   write_octave

 use mod_bcs, only: &
   mod_bcs_constructor, &
   mod_bcs_destructor,    &
   t_bcs, new_bcs, clear, &
   write_octave
 
 use mod_fe_spaces, only: &
   mod_fe_spaces_constructor, &
   mod_fe_spaces_destructor,  &
   cg

 use mod_cgdofs, only: &
   mod_cgdofs_constructor, &
   mod_cgdofs_destructor,  &
   t_cgdofs, t_ddc_cgdofs, &
   new_cgdofs,           &
   clear,                &
   write_octave

 use mod_linsolver, only: &
   mod_linsolver_constructor, &
   mod_linsolver_destructor

 use mod_newton, only: &
   mod_newton_constructor, &
   mod_newton_destructor

 use mod_lc_state, only: &
   mod_lc_state_constructor, &
   mod_lc_state_destructor, &
   t_lc_stv, new_lc_stv, clear

 use mod_lc_locmat, only: &
   mod_lc_locmat_constructor, &
   mod_lc_locmat_destructor

 use mod_lc_cnstep, only: &
   mod_lc_cnstep_constructor, &
   mod_lc_cnstep_destructor, &
   lc_cnstep
 
 implicit none

 ! Define some general parameters
 integer, parameter :: max_char_len = 10000
 character(len=*), parameter :: this_prog_name = 'lc-cn'

 ! IO variables
 character(len=*), parameter :: input_file_name_def = 'lc-cn.in'
 character(len=max_char_len) :: input_file_name
 character(len=*), parameter :: out_file_nml_suff = '-nml.octxt'
 character(len=max_char_len+len(out_file_nml_suff)):: out_file_nml_name
 character(len=max_char_len) :: basename
 logical :: write_grid, write_sys
 character(len=*), parameter :: out_file_grid_suff = '-grid.octxt'
 character(len=max_char_len+len(out_file_grid_suff))::out_file_grid_name
 character(len=*), parameter :: out_file_base_suff = '-base.octxt'
 character(len=max_char_len+len(out_file_base_suff))::out_file_base_name
 integer :: fu, ierr
 ! additional output at selected times
 integer :: n_sot, nn_sot ! counter and number of special output times
 real(wp), allocatable :: selected_output_times(:)

 ! Grid
 type(t_grid), target :: grid
 type(t_ddc_grid) :: ddc_grid
 character(len=max_char_len) :: grid_file

 ! Base
 type(t_lagnodes), allocatable :: ref_nodes(:)
 integer :: gq_deg
 type(t_base) :: base

 ! CG-DOFS
 type(t_cgdofs)     :: dofs
 type(t_ddc_cgdofs) :: ddc_dofs

 ! BCS
 integer :: nbound ! # of boundary regions
 character(len=max_char_len) :: cbc_type
 type(t_bcs), target :: bcs
 integer, allocatable :: bc_type_t(:,:)

 ! Test case
 character(len=max_char_len) :: testname

 ! Prognostic variables
 type(t_lc_stv), allocatable :: xn, xn1

 ! Time integration
 integer :: nstep, n, n0, n_out
 real(wp) :: dt, tt_sta, tt_end, t_nm1, t_n
 logical :: l_checkpoint, l_output
 real(wp) :: time_last_out, dt_out, time_last_check, dt_check

 character(len=max_char_len) :: message(1)

 ! Timing
 real(t_realtime) :: t00, t0, t1, t0step

 ! MPI variables
 logical :: master_proc
 integer :: mpi_nd, mpi_id

 ! Input namelist
 namelist /input/ &
   ! test case
   testname, &
   ! output base name
   basename, &
   ! grid
   write_grid, &
   grid_file,  &
   ! base
   gq_deg, & ! accuracy of the Gaussian quadrature rule
!!   ! linear system
!!   write_sys, &
   ! boundary conditions
   nbound, cbc_type, &
   ! time stepping
   tt_sta, tt_end, dt, &
   ! output
   dt_out, &
   ! chepoints
   dt_check, &
   ! special output times
   nn_sot ! number of times in selected_output_times (possibly 0)

 ! Auxiliary namelist (to make the allocation)
 namelist /out_details/ &
   selected_output_times

 !---------------------------------------------------------------------
 ! Initializations and startup
 t00 = my_second()

 call mod_messages_constructor()

 call mod_kinds_constructor()

 call mpi_init_thread(mpi_thread_multiple,n,ierr) ! using n as tmp
 call mod_mpi_utils_constructor()
 call mpi_comm_size(mpi_comm_world,mpi_nd,ierr)
 call mpi_comm_rank(mpi_comm_world,mpi_id,ierr)
 master_proc = mpi_id.eq.0

 call mod_fu_manager_constructor()

 call mod_octave_io_constructor()

 call mod_linal_constructor()

 call mod_perms_constructor()
 call mod_octave_io_perms_constructor()

 call mod_sympoly_constructor()
 call mod_octave_io_sympoly_constructor()

 call mod_numquad_constructor()

 call mod_state_vars_constructor()

 !----------------------------------------------------------------------
 ! Read input file
 if(command_argument_count().gt.0) then
   call get_command_argument(1,value=input_file_name)
 else
   input_file_name = input_file_name_def
 endif

 call new_file_unit(fu,ierr)
 open(fu,file=trim(input_file_name), &
      status='old',action='read',form='formatted',iostat=ierr)
 read(fu,input)
 close(fu,iostat=ierr)
 ! add the partition index for parallel runs
 if(mpi_nd.gt.1) then
   write(basename, '(a,a,i3.3)') trim(basename),'-P',mpi_id
   write(grid_file,'(a,a,i3.3)') trim(grid_file),'.',mpi_id
 endif
 ! echo the input namelist
 out_file_nml_name = trim(basename)//out_file_nml_suff
 open(fu,file=trim(out_file_nml_name), &
      status='replace',action='write',form='formatted',iostat=ierr)
 write(fu,input)
 close(fu,iostat=ierr)

 allocate(bc_type_t(nbound,2))
 read(cbc_type,*) bc_type_t ! transposed, for simplicity
 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! output control setup
 call mod_output_control_constructor(basename)
 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! Define the grid, the bcs and the base
 t0 = my_second()

 call mod_master_el_constructor()

 call mod_grid_constructor()
 call new_grid(grid,trim(grid_file) , ddc_grid,mpi_comm_world)

 call mod_bcs_constructor()
 call new_bcs(bcs,grid,transpose(bc_type_t) , ddc_grid,mpi_comm_world)
 deallocate( bc_type_t )

 call mod_base_constructor()
 call mod_fe_spaces_constructor()
 call mod_cgdofs_constructor()
 call lag_nodes( ref_nodes , grid%me%d , 1 )
 base = cg( grid%me , 1 , ref_nodes , deg=gq_deg )
 call new_cgdofs( dofs , ref_nodes,grid,bcs,         &
                  mpi_comm_world,ddc_grid , ddc_dofs )

 ! write the octave output
 out_file_base_name = trim(base_name)//out_file_base_suff
 call new_file_unit(fu,ierr)
 open(fu,file=trim(out_file_base_name), &
      status='replace',action='write',form='formatted',iostat=ierr)
 call write_octave(base,'base',fu)
 call write_octave(ref_nodes,'ref_nodes',fu)
 close(fu,iostat=ierr)
 if(write_grid) then
   out_file_grid_name = trim(base_name)//out_file_grid_suff
   call new_file_unit(fu,ierr)
   open(fu,file=trim(out_file_grid_name), &
        status='replace',action='write',form='formatted',iostat=ierr)
   call write_octave(grid    ,'grid'    ,fu)
   call write_octave(ddc_grid,'ddc_grid',fu)
   call write_octave(bcs     ,'bcs'     ,fu)
   call write_octave(dofs    ,'dofs'    ,fu)
   call write_octave(ddc_dofs,'ddc_dofs',fu)
   close(fu,iostat=ierr)
 endif

 t1 = my_second()
 write(message(1),elapsed_format) &
   'Completed base and grid construction: elapsed time ',t1-t0,'s.'
 if(master_proc) &
   call info(this_prog_name,'',message(1))
 !----------------------------------------------------------------------

 !---------------------------------------------------------------------
 ! Read the remaining input namelist(s)
 if(nn_sot.eq.0) then ! no additional times are required
   ! This is the simplest way to avoid any special casing
   nn_sot = 1
   allocate(selected_output_times(1))
   selected_output_times = 2.0_wp*tt_end ! beyond the final time
 else ! read the additional namelist
   call new_file_unit(fu,ierr)
   open(fu,file=trim(input_file_name), &
        status='old',action='read',form='formatted',iostat=ierr)
    allocate(selected_output_times(nn_sot))
    read(fu,out_details)
   close(fu,iostat=ierr)
   ! echo the input namelist
   open(fu,file=trim(out_file_nml_name), status='old', & 
     action='readwrite',form='formatted',position='append',iostat=ierr)
    write(fu,out_details)
   close(fu,iostat=ierr)
 endif
 !---------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! Linear and nonlinear solvers

 call mod_sparse_constructor()
 call mod_octave_io_sparse_constructor()

 call mod_linsolver_constructor()

 call mod_newton_constructor()
 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! ODE setup

 call mod_lc_state_constructor()

 call mod_lc_locmat_constructor(grid,base,bcs)

 call mod_lc_cnstep_constructor(grid,ddc_grid,bcs,base,dofs,ddc_dofs, &
                                dt , "mumps" , mpi_comm_world )

 ! integration variables
 allocate( xn , xn1 )
 call new_lc_stv(xn  , grid,base,bcs,dofs)
 call new_lc_stv(xn1 , grid,base,bcs,dofs)
 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! Initial condition

 !xn%u(1 ,:) = sqrt(2.0_wp)/2.0_wp
 !xn%u(2:,:) = sqrt(2.0_wp)/2.0_wp
 xn%u = dofs%x
 xn%q = 0.0_wp

 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! Time integration
 !
 !   0        1                 n             nstep
 !   |--------|--------|--------|--------|------|
 ! tt_sta            t_nm1     t_n            tt_end
 !                     | step_n |

 n0 = 1 ! no bootstrapping steps
 call init_time_stepping()

 t0 = my_second()
 time_loop: do n=n0,nstep
 
   call init_time_step()
    
   call lc_cnstep( xn , xn1 )
   call swap_lc_state( xn , xn1 )
   
   ! Checkpoint
   if(l_checkpoint) then
     !t1 = my_second()
     !call check_step(t1-t0step , grid , ddc_grid , base , dt , &
     !                uuu0%u , ti_step_diag)
   endif

   ! Full output
   if(l_output) then
     write(*,*) 'Step ',n,' of ',nstep
     call write_out(n,t_n,n_out,xn)
   endif

 enddo time_loop
 t1 = my_second()
 write(message(1),elapsed_format) &
   'Completed time loop: elapsed time ',t1-t0,'s.'
 if(master_proc) &
   call info(this_prog_name,'',message(1))

 !----------------------------------------------------------------------

 !----------------------------------------------------------------------
 ! Clean-up

 call clear( xn )
 call clear( xn1 )
 deallocate( xn , xn1 )
 call mod_lc_cnstep_destructor()
 call mod_lc_locmat_destructor()
 call mod_lc_state_destructor()

 call mod_newton_destructor()

 call mod_linsolver_destructor()

 call mod_octave_io_sparse_destructor()
 call mod_sparse_destructor()

 call clear(ddc_dofs)
 call clear(dofs)
 call clear(base)
 deallocate( ref_nodes )
 call mod_cgdofs_destructor()
 call mod_fe_spaces_destructor()
 call mod_base_destructor()
 
 call clear(bcs)
 call mod_bcs_destructor()

 call clear(ddc_grid)
 call clear(grid)
 call mod_grid_destructor()

 call mod_master_el_destructor()

 call mod_output_control_destructor()

 call mod_state_vars_destructor()

 call mod_numquad_destructor()

 call mod_octave_io_sympoly_destructor()
 call mod_sympoly_destructor()

 call mod_octave_io_perms_destructor()
 call mod_perms_destructor()

 call mod_linal_destructor()

 call mod_octave_io_destructor()

 call mod_fu_manager_destructor()

 call mpi_finalize(ierr)
 call mod_mpi_utils_destructor()

 call mod_kinds_destructor()

 call mod_messages_destructor()
 !----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine init_time_stepping()
 ! Set the main time stepping variables in a consistent way on all the
 ! processes.
 
  integer, parameter :: &
   n_ibuff = 4, &
   n_rbuff = 2
  integer ::  intgbuff(n_ibuff)
  real(wp) :: realbuff(n_rbuff)

   if(mpi_id.eq.0) then ! master process
     nstep = ceiling((tt_end-tt_sta)/dt)
     n_out = 0
     n_sot = 1
     t_n             = tt_sta
     time_last_out   = 0.0_wp
     time_last_check = 0.0_wp
   endif

   if(mpi_nd.le.1) return

   if(mpi_id.eq.0) then ! master process

     intgbuff = (/ n0 , nstep , n_out , n_sot /)
     realbuff = (/ dt , t_n /)
     call mpi_bcast(intgbuff,n_ibuff,mpi_integer,0,mpi_comm_world,ierr)
     call mpi_bcast(realbuff,n_rbuff,wp_mpi     ,0,mpi_comm_world,ierr)

   else

     call mpi_bcast(intgbuff,n_ibuff,mpi_integer,0,mpi_comm_world,ierr)
     call mpi_bcast(realbuff,n_rbuff,wp_mpi     ,0,mpi_comm_world,ierr)
     n0    = intgbuff(1)
     nstep = intgbuff(2)
     n_out = intgbuff(3)
     n_sot = intgbuff(4)
     dt    = realbuff(1)
     t_n   = realbuff(2)

   endif
   
 end subroutine init_time_stepping
 
!-----------------------------------------------------------------------

 subroutine init_time_step()
 ! Sets the main time step variables in a consistent way on all the
 ! processes.

  integer, parameter :: &
   n_lbuff = 2, &
   n_ibuff = 2, &
   n_rbuff = 3
  logical ::  logcbuff(n_lbuff)
  integer ::  intgbuff(n_ibuff)
  real(wp) :: realbuff(n_rbuff)

   if(mpi_id.eq.0) then ! master process

     ! main time step variables
     t_nm1 = t_n                  ! t^{n-1}
     t_n = tt_sta + real(n,wp)*dt ! t^n

     ! checkpoint
     time_last_check = time_last_check + dt
     if(time_last_check.ge.dt_check) then
       l_checkpoint = .true.
       time_last_check = time_last_check - dt_check
     else
       l_checkpoint = .false.
     endif

     ! output
     time_last_out = time_last_out + dt
     if( (time_last_out.ge.dt_out) .or. &
         (t_n.ge.selected_output_times(n_sot)) ) then
       l_output = .true.
       n_out = n_out+1
       if(time_last_out.ge.dt_out) &
         time_last_out = time_last_out - dt_out
       if(t_n.ge.selected_output_times(n_sot)) then
         if(n_sot.lt.nn_sot) then
           n_sot = n_sot + 1
         else ! last selected time: redefine it to avoid other outputs
           selected_output_times(n_sot) = 2.0_wp*tt_end
         endif
       endif
     else
       l_output = .false.
     endif

     ! communication
     if(mpi_nd.gt.0) then
       logcbuff = (/ l_checkpoint , l_output /)
       intgbuff = (/ n_out , n_sot /)
       realbuff = (/ t_nm1 , t_n , dt /)
       call mpi_bcast(logcbuff,n_lbuff,mpi_logical,0,mpi_comm_world, &
             ierr)
       call mpi_bcast(intgbuff,n_ibuff,mpi_integer,0,mpi_comm_world, &
             ierr)
       call mpi_bcast(realbuff,n_rbuff,wp_mpi     ,0,mpi_comm_world, &
             ierr)
     endif

   else

     call mpi_bcast(logcbuff,n_lbuff,mpi_logical,0,mpi_comm_world,ierr)
     call mpi_bcast(intgbuff,n_ibuff,mpi_integer,0,mpi_comm_world,ierr)
     call mpi_bcast(realbuff,n_rbuff,wp_mpi     ,0,mpi_comm_world,ierr)
     l_checkpoint = logcbuff(1)
     l_output     = logcbuff(2)
     n_out = intgbuff(1)
     n_sot = intgbuff(2)
     t_nm1 = realbuff(1)
     t_n   = realbuff(2)
     dt    = realbuff(3)

   endif

 end subroutine init_time_step

!-----------------------------------------------------------------------
 subroutine swap_lc_state( x , y )
  type(t_lc_stv), allocatable, intent(inout) :: x, y
  type(t_lc_stv), allocatable :: w
  
   call move_alloc( to=w , from=x )
   call move_alloc( to=x , from=y )
   call move_alloc( to=y , from=w )
 end subroutine swap_lc_state

!-----------------------------------------------------------------------

 subroutine write_out(n,t,n_out,xn)
  integer, intent(in) :: n, n_out
  real(wp), intent(in) :: t
  type(t_lc_stv), intent(in) :: xn

  integer :: fu, ierr
  character(len=*), parameter :: time_stamp_format = '(i4.4)'
  character(len=4) :: time_stamp
  character(len=*), parameter :: suff1 = '-res-'
  character(len=*), parameter :: suff2 = '.octxt'
  character(len= len_trim(base_name) + len(suff1) + 4 + len(suff2)) :: &
     out_file_res_name

   call new_file_unit(fu,ierr)
   write(time_stamp,time_stamp_format) n_out
   out_file_res_name = trim(base_name)//suff1//time_stamp//suff2
   open(fu,file=out_file_res_name, &
        status='replace',action='write',form='formatted',iostat=ierr)
    !call write_octave(trim(tc%test_name)       ,'test_name'       ,fu)
    !associate( c => tc%test_description ) ! trim the lines
    !call write_octave(c(:)(:maxval(len_trim(c))),'test_description',fu)
    !end associate
    call write_octave(n  ,'n'  ,fu)
    call write_octave(t  ,'t'  ,fu)
    call write_octave(xn%u    ,'u',fu)
    call write_octave(xn%q,'c','q',fu)
    !if(write_sys) then
    !endif
   close(fu,iostat=ierr)

 end subroutine write_out

!-----------------------------------------------------------------------

end program lc_cn

