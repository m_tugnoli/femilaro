!>\brief
!!
!! Single Crank-Nicolson step for the liquid crystal model
!!
!! \n
!!
!! This module solves the nonlinear problem
!! \f{displaymath}{
!!  \begin{array}{rcl}
!!   \displaystyle
!!   \frac{1}{\Delta t}
!!    ( {\bf u}^{n+1}-{\bf u}^n , \bar{\bf u} )_h
!! + \gamma ( \nabla{\bf u}^{n+\frac{1}{2}},\nabla\bar{\bf u} )
!! + \gamma ( q^{n+\frac{1}{2}},i_Q\left( 
!!      \frac{{\bf u}^{n+\frac{1}{2}}}{|{\bf u}^{n+\frac{1}{2}}|^2}
!!      \cdot \bar{\bf u} \right) ) & = & 0 \\[4mm]
!!   \displaystyle
!! ( i_Q\left( {\bf u}^{n+1}\cdot{\bf u}^{n+1} \right),\bar{q} ) & = &
!! (1,\bar{q}) 
!!  \end{array}
!! \f}
!! computing \f${\bf u}^{n+1}\f$.
!!
!! \anchor lc_cnstep_nonlinpb
!! The first step is rewriting the system introducing \f${\bf w}={\bf
!! u}^{1+\frac{1}{2}}\f$, \f$s=q^{1+\frac{1}{2}}\f$, so that
!! \f{displaymath}{
!!  \begin{array}{rcl}
!!   \displaystyle
!!    ( {\bf w}-{\bf u}^n , \bar{\bf u} )_h
!! + \frac{\gamma\Delta t}{2} ( \nabla{\bf w},\nabla\bar{\bf u} )
!! + \frac{\gamma\Delta t}{2} ( s ,i_Q\left( 
!!      \frac{{\bf w}}{|{\bf w}|^2}
!!      \cdot \bar{\bf u} \right) ) & = & 0 \\[4mm]
!!   \displaystyle
!! ( i_Q\left( {\bf w}\cdot({\bf w}-{\bf u}^n) -\frac{1-{\bf
!! u}^n\cdot{\bf u}^n}{4} \right),\bar{q} ) & = & 0.
!!  \end{array}
!! \f}
!! Notice that the second equation amounts to requiring that the
!! argument of \f$i_Q\f$ vanishes at each node of the triangulation;
!! assuming \f${\bf w}\neq0\f$ we can reformulate this constraint as
!! \f{displaymath}{
!! \frac{\gamma\Delta t}{2}
!! ( i_Q\left( \frac{{\bf w}}{|{\bf w}|^2}\cdot({\bf w}-{\bf u}^n)
!! -\frac{1-{\bf u}^n\cdot{\bf u}^n}{4|{\bf w}|^2}
!! \right),\bar{q} ) = 0.
!! \f}
!! \note In fact, the last term appearing in the argument of \f$i_Q\f$
!! vanishes, since \f$1={\bf u}^n\cdot{\bf u}^n\f$, and could thus be
!! neglected. However, because of the error implied by the solution of
!! the nonlinear system, this constraint can not be enforced exactly,
!! so one has two options:
!! <ol>
!!  <li> neglect the term and normalize \f${\bf u}^n\f$ after solving
!!  the nonlinear problem, enforcing the constraint <em>exactly</em>
!!  for each \f$t^n\f$
!!  <li> keep the term, so that the error resulting from the nonlinear
!!  solver does not grow indefinitely with time, and \f$|{\bf
!!  u}^n|\approx1\f$ for all the values \f$n\f$.
!! </ol>
!! We will consider here the second option.
!!
!! \anchor lc_cnstep_linpb
!! To solve this system iteratively, we now introduce,
!! for a given \f$\tilde{\bf w}\f$, the linear problem
!! \f{displaymath}{
!!  \begin{array}{rcl}
!!   \displaystyle
!!    ( {\bf w} , \bar{\bf u} )_h
!! + \frac{\gamma\Delta t}{2} ( \nabla{\bf w},\nabla\bar{\bf u} )
!! + ( s ,i_Q\left( 
!!      \tilde{\bf p}
!!      \cdot \bar{\bf u} \right) ) & = & \displaystyle
!!    ( {\bf u}^n , \bar{\bf u} )_h \\[4mm]
!!   \displaystyle
!! ( i_Q\left( \tilde{\bf p}\cdot{\bf w} \right),\bar{q} ) & = &
!!  \displaystyle
!! ( i_Q\left( \tilde{\bf p}\cdot{\bf u}^n +\Gamma^n\right),\bar{q} ),
!!  \end{array}
!! \f}
!! for 
!! \f{displaymath}{
!!  \tilde{\bf p} = \frac{\gamma\Delta t}{2} \frac{{\bf w}}{|{\bf
!!  w}|^2}, \qquad
!! \Gamma^n = \frac{\gamma\Delta t}{2} \frac{1-|{\bf u}^n|^2}{4|{\bf
!! w}|^2}.
!! \f}
!! Notice that this problem is well defined for \f$\tilde{\bf
!! w}\neq0\f$; moreover, the system matrix is symmetric.
!!
!! To solve this linear system, first define
!! \f{displaymath}{
!!  A_{ij} = ( \bar{\bf u}_j , \bar{\bf u}_i )_h + \frac{\gamma\Delta
!!  t}{2} ( \nabla\bar{\bf u}_j ,\nabla\bar{\bf u}_i ), \qquad
!!  B^{\tilde{\bf p}}_{kj} = 
!!   ( i_Q\left( \tilde{\bf p}\cdot\bar{\bf u}_j \right),\bar{q}_k )
!! \f}
!! so that we have
!! \f{displaymath}{
!!  \left[
!!  \begin{array}{cc}
!!    A & (B^{\tilde{\bf p}})^T \\
!!    B^{\tilde{\bf p}} & 0
!!  \end{array}\right]\,
!!  \left[
!!  \begin{array}{c}
!!  {\tt w} \\ {\tt s}
!!  \end{array}\right] = 
!!  \left[
!!  \begin{array}{c}
!!  {\tt b}_{\bf w} \\ {\tt b}_s
!!  \end{array}\right].
!! \f}
!! Now, \f$A\f$ is a constant, invertible matrix, which can be
!! factorized once before the time integration (moreover, assuming
!! that the basis functions decouple the velocity components, it is
!! also block diagonal: see \ref lc_cnstep_bdiag "here"). At each
!! iteration, we thus need to compute
!! \f{displaymath}{
!!  \begin{array}{rcl}
!!   - B^{\tilde{\bf p}}A^{-1}(B^{\tilde{\bf p}})^T{\tt s} & = &
!!    {\tt b}_s - B^{\tilde{\bf p}}A^{-1}{\tt b}_{\bf w} \\[2mm]
!!   {\tt w} &=& A^{-1}({\tt b}_{\bf w} - (B^{\tilde{\bf p}})^T{\tt s})
!!  \end{array}
!! \f}
!! which is very well suited for the conjugate gradient method, and
!! thus for a Newton-Krylov approach to the solution of the nonlinear
!! problem.
!!
!! \section lc_cnstep_bdiag Block diagonal structure of the system
!!
!! Let us assume that the basis functions are \f$\bar{\bf u}_{\bf i}
!! = \bar{\bf u}_{ii_d} = {\bf e}_{i_d} \varphi_i\f$ and let us assume
!! that they are ordered using \f$i\f$ as the most internal index. We
!! have
!! \f{displaymath}{
!!  A_{ii_d\,jj_d} = \delta_{i_dj_d} \left[ ( \varphi_j , \varphi_i
!!  )_h + \frac{\gamma\Delta t}{2} ( \nabla\varphi_j
!!  ,\nabla\varphi_i)\right] = \delta_{i_dj_d} A^{1D}_{ij},
!! \f}
!! so that
!! \f{displaymath}{
!!  A = 
!!  \left[
!!  \begin{array}{cccc}
!!    A^{1D} \\ & A^{1D} \\ & & \ddots \\ & & & A^{1D}
!!  \end{array}\right].
!! \f}
!! Now, concerning the rectangular matrix, assume
!! \f$\bar{q}_i=\varphi_i\f$, so that
!! \f{displaymath}{
!!  B^{\tilde{\bf p}}_{k\,jj_d} = 
!!   ( i_Q\left( \tilde{p}_{j_d}\varphi_j \right),\varphi_k ).
!! \f}
!! \f$B^{\tilde{\bf p}}\f$ is, in general, a full matrix, with
!! structure
!! \f{displaymath}{
!!  B^{\tilde{\bf p}} = 
!!  \left[
!!  \begin{array}{cccc}
!!    B^{\tilde{p}_1} & B^{\tilde{p}_2} & \ldots & B^{\tilde{p}_M}
!!  \end{array}\right], \qquad B^{\tilde{p}_{j_d}}_{kj} = 
!!   ( i_Q\left( \tilde{p}_{j_d}\varphi_j \right),\varphi_k ),
!! \f}
!! where all the blocks \f$B^{\tilde{p}_{j_d}}\f$ have the same
!! sparsity pattern but different entries. The matrix appearing in the
!! system for \f${\tt s}\f$ is then
!! \f{displaymath}{
!!   - B^{\tilde{\bf p}}A^{-1}(B^{\tilde{\bf p}})^T = - \sum_{j_d=1}^M
!!   B^{\tilde{p}_{j_d}}(A^{1D})^{-1}(B^{\tilde{p}_{j_d}})^T.
!! \f}
!!
!! \subsection lc_cnstep_bdiag_numquad Numerical quadrature
!!
!! If, on the one hand, we replace the exact integration with a mass
!! lumping one in the two terms involving the Lagrange multiplier, the
!! matrices \f$B^{\tilde{p}_{j_d}}\f$ become diagonal, with
!! \f{displaymath}{
!!   B^{\tilde{p}_{j_d}}_{kj} = 
!!   ( i_Q\left( \tilde{p}_{j_d}\varphi_j \right),\varphi_k )_h = 
!!   \tilde{p}_{j_d}({\bf x}_j)\,m_j\delta_{kj},
!! \f}
!! where \f${\bf x}_j\f$ is the mesh node associated with
!! \f$\varphi_j\f$ and
!! \f{displaymath}{
!!  m_j = \left( \varphi_j ,\varphi_j \right)_h.
!! \f}
!!
!! If, on the other hand, we use a generic quadrature rule, without
!! mass lumping, observe that
!! \f{displaymath}{
!!   i_Q\left( \tilde{p}_{j_d}\varphi_j \right)({\bf x}) =
!!   \sum_k\tilde{p}_{j_d}({\bf x}_k) \underbrace{\varphi_j({\bf
!!   x}_k)}_{\delta_{jk}} \varphi_k({\bf x}) =
!!   \tilde{p}_{j_d}({\bf x}_j)\,\varphi_j({\bf x})
!! \f}
!! or, more simply,
!! \f{displaymath}{
!!   i_Q\left( \tilde{p}_{j_d}\varphi_j \right) = 
!!   \tilde{p}_{j_d}({\bf x}_j)\,\varphi_j.
!! \f}
!! Since \f$\tilde{p}_{j_d}({\bf x}_j)\f$ is a constant, we have
!! \f{displaymath}{
!!   B^{\tilde{p}_{j_d}}_{kj} = 
!!   ( i_Q\left( \tilde{p}_{j_d}\varphi_j \right),\varphi_k ) = 
!!   \tilde{p}_{j_d}({\bf x}_j) (\varphi_j,\varphi_k)
!! \f}
!! which is simply a mass matrix scaled by columns.
!!
!! To see the effect of these two alternatives on the solution, see
!! \ref lc_cnstep_bcs "this section".
!!
!! \section lc_cnstep_bcs Boundary conditions
!!
!! We enforce Dirichlet boundary conditions introducing a penalization
!! term
!! \f{displaymath}{
!!  \int_{\partial \Omega} \xi ({\bf u}^{n+1}-{\bf u}^b)\cdot 
!!   \bar{\bf u}\,dx.
!! \f}
!! This generates an additional block diagonal matrix which can be
!! added to \f$A^{1D}\f$ and then \f$A\f$. In fact we have
!! \f{displaymath}{
!!  A^{b}_{ii_d\,jj_d} = \delta_{i_dj_d} \int_{\partial \Omega} \xi
!!  \varphi_j\varphi_i\,dx = \delta_{i_dj_d} A^{1D,b}_{ij}.
!! \f}
!! For more details, see also \ref lc_cnstep_pres_bcs "this function"
!! and the computation of the local matrix in
!! \fref{mod_lc_locmat,lc_locmat_a} as well as
!! \fref{mod_lc_locmat,gxik}.
!!
!! \section lc_cnstep_lumped_b Lumping of the matrix B
!!
!! As already mentioned in \ref lc_cnstep_bdiag_numquad "this"
!! previous section, it is possible to diagonalize the matrices
!! \f$B^{\tilde{p}_{j_d}}\f$ using numerical quadrature. Here, we
!! analyze the effect of such operation on the numerical solution, and
!! show that
!! <ul>
!!  <li> the lumping of \f$B^{\tilde{p}_{j_d}}\f$ has no effect on
!!  \f${\bf u}^n\f$
!!  <li> the lumping of \f$B^{\tilde{p}_{j_d}}\f$ results in a
!!  regularization of the Lagrange multiplier \f$q\f$.
!! </ul>
!!
!! Define an operator \f$\mathcal{L}\f$ such that
!! \f{displaymath}{
!!  ( u , \varphi ) = (\mathcal{L} u , \varphi )_h , \quad \forall
!!  \varphi\in V_h.
!! \f}
!! The matrix representation of this operator is clearly given by the
!! identity
!! \f{displaymath}{
!!  M {\tt u} = M^DL\,{\tt u} \qquad \Longrightarrow \qquad L =
!!  (M^{D})^{-1}M,
!! \f}
!! where the two mass matrices have elements
!! \f{displaymath}{
!!  M_{ij} = (\varphi_i,\varphi_j), \qquad
!!  M^D_{ij} = (\varphi_i,\varphi_i)_h\,\delta_{ij}.
!! \f}
!! For piecewise linear finite elements, \f$M_{ij}\geq0\f$ and
!! \f$M^D_{ii} = \sum_j M_{ij}\f$, so that \f$\mathcal{L}\f$ is a
!! convex linear combination of the nodal degrees of freedom, i.e. a
!! regularization.
!!
!! Now, given a solution of the nonlinear equation using the complete
!! matrix \f$B^{\tilde{\bf p}}\f$, we can immediately substitute exact
!! and numerical integration in the second equation, i.e.
!! \f{displaymath}{
!! ( i_Q\left( {\bf u}^{n+1}\cdot{\bf u}^{n+1} \right),\bar{q} ) =
!! (1,\bar{q}) \quad \Longleftrightarrow \quad
!! ( i_Q\left( {\bf u}^{n+1}\cdot{\bf u}^{n+1} \right),\bar{q} )_h =
!! (1,\bar{q})_h,
!! \f}
!! because
!! \f{displaymath}{
!! ( i_Q\left( {\bf u}^{n+1}\cdot{\bf u}^{n+1} \right),\bar{q} ) =
!! ( i_Q\left( {\bf u}^{n+1}\cdot{\bf u}^{n+1} \right),L\bar{q} )_h,
!! \qquad (1,\bar{q}) = (1,L\bar{q})_h,
!! \f}
!! and \f$L\f$ is one-to-one in the finite element space \f$V_h\f$.
!! Concerning the first equation, by definition of \f$L\f$ we have
!! \f{displaymath}{
!!   ( q^{n+\frac{1}{2}},i_Q\left( \frac{{\bf
!!   u}^{n+\frac{1}{2}}}{|{\bf u}^{n+\frac{1}{2}}|^2} \cdot \bar{\bf
!!   u} \right) ) =
!!   ( L q^{n+\frac{1}{2}},i_Q\left( \frac{{\bf
!!   u}^{n+\frac{1}{2}}}{|{\bf u}^{n+\frac{1}{2}}|^2} \cdot \bar{\bf
!!   u} \right) )_h.
!! \f}
!! This proves that \f$({\bf u}^n,q^{n+\frac{1}{2}})\f$ is a solution
!! of the complete problem if and only if \f$({\bf u}^n,L
!! q^{n+\frac{1}{2}})\f$ is a solution of the problem with lumping of
!! the \f$B^{\tilde{\bf p}}\f$ matrix.
!<----------------------------------------------------------------------
module mod_lc_cnstep

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_octave_io, only: &
   mod_octave_io_initialized, &
   write_octave
 
 use mod_sparse, only: &
   mod_sparse_initialized, &
   ! sparse types
   t_intar,     &
   t_col,       &
   t_tri,       &
   t_pm_sk,     &
   ! construction of new objects
   new_col,     &
   new_tri,     &
   ! convertions
   col2tri,     &
   tri2col,     &
   tri2col_skeleton, &
   tri2col_skeleton_part, &
   col_assemble, &
   ! overloaded operators
   operator(+), &
   operator(*), &
   sum,         &
   transpose,   &
   matmul,      &
   ! error codes
   wrong_n,     &
   wrong_m,     &
   wrong_nz,    &
   wrong_dim,   &
   ! other functions
   nnz_col,     &
   nz_col,      &
   nz_col_i,    &
   get,         &
   set,         &
   diag,        &
   spdiag,      &
   ! deallocate
   clear

 use mod_octave_io_sparse, only: &
   mod_octave_io_sparse_initialized, &
   write_octave

 use mod_state_vars, only: &
   mod_state_vars_initialized, &
   c_stv

 use mod_linsolver, only: &
   mod_linsolver_initialized, &
   c_linpb, c_itpb, c_mumpspb, c_pastixpb, c_umfpackpb, &
   gmres

 use mod_newton, only: &
   mod_newton_initialized, &
   t_nwt_params, c_nwt, &
   newton

 use mod_master_el, only: &
   mod_master_el_initialized

 use mod_grid, only: &
   mod_grid_initialized, &
   t_grid, t_ddc_grid

 use mod_base, only: &
   mod_base_initialized, &
   t_base

 use mod_bcs, only: &
   mod_bcs_initialized, &
   t_bcs

 use mod_cgdofs, only: &
   mod_cgdofs_initialized, &
   t_cgdofs, t_ddc_cgdofs

 use mod_lc_state, only: &
   mod_lc_state_initialized, &
   t_lc_stv, new_lc_stv, clear

 use mod_lc_locmat, only: &
   mod_lc_locmat_initialized, &
   lc_locmat_a, lc_locmat_b, lc_locnlres

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_lc_cnstep_constructor, &
   mod_lc_cnstep_destructor,  &
   mod_lc_cnstep_initialized, &
   lc_cnstep

 private

!-----------------------------------------------------------------------

! Module types and parameters

 !> Newton problem
 !!
 !! This is the nonlinear problem defining \f$({\bf w},s)\f$ described
 !! \ref lc_cnstep_nonlinpb "here".
 type, extends(c_nwt) :: t_lc_nwt
  real(wp) :: dt
  type(t_lc_stv), pointer :: uqn => null()
  type(t_lc_stv) :: r
  integer :: mpi_id, comm
 contains
  !> Preconditioned ual
  procedure, pass(z) :: pres => lc_pres
  !> Residual norm
  procedure, pass(x) :: nres => lc_nres
  !> Increment norm
  procedure, pass(x) :: norm => lc_norm
  !> Copy a \c c_stv object into a \c c_nwt one
  procedure, pass(y) :: defy => lc_defy
  !> Print some data (for debugging)
  !procedure, pass(x) :: show => nwt_show
  ! No need to override the source allocation
 end type t_lc_nwt

 !> Linear solver interfaces
 type, extends(c_mumpspb) :: t_lc_mumps
 contains
  procedure, nopass :: xassign => general_lc_xassign
 end type t_lc_mumps

! Module variables

 !> Matrix \f$A\f$, see \ref lc_cnstep_bdiag "this section"
 type(t_col), target, save :: a1d, aaa
 type(t_intar), allocatable :: a1d2a(:)
 !> Matrix \f$B^{\tilde{\bf p}}\f$
 type(t_col), allocatable, save :: b1d(:) ! 1D B matrices
 type(t_intar), allocatable :: b1d_t2c(:) ! assemble the 1D B matrices
 type(t_intar), allocatable :: b1d2b(:)   ! compose: 1D B mat -> B mat
 type(t_col), save :: bbb
 !> Complete matrix 
 !! \f{displaymath}{
 !!  \left[ \begin{array}{cc}
 !!    A & (B^{\tilde{\bf p}})^T \\
 !!    B^{\tilde{\bf p}} & 0
 !!  \end{array}\right].
 !! \f}
 type(t_col), target, save :: mmm
 !> Index arrays to assemble \c mmm
 type(t_intar), allocatable :: ab2m(:)

 !> Linear problem
 class(c_linpb), allocatable :: linpb
 real(wp), allocatable, target :: linrhs(:)
 integer, allocatable, target :: lingij(:)

 !> Nonlinear problem
 type(t_lc_nwt), save :: nlinpb
 type(t_nwt_params) :: nlinparams

 !> \todo Define this coefficient in a better place.
 real(wp), parameter :: gamma = 0.7_wp

 logical, protected ::               &
   mod_lc_cnstep_initialized = .false.
 character(len=*), parameter :: &
   this_mod_name = 'mod_lc_cnstep'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_lc_cnstep_constructor(grid,ddc_grid , bcs , base , &
                                  dofs,ddc_dofs , dt,linsolver,comm)
  type(t_grid),   intent(in) :: grid
  type(t_ddc_grid), intent(in) :: ddc_grid
  type(t_bcs),    intent(in) :: bcs
  type(t_base),   intent(in) :: base
  type(t_cgdofs), intent(in) :: dofs
  type(t_ddc_cgdofs), intent(in) :: ddc_dofs
  real(wp),         intent(in) :: dt
  character(len=*), intent(in) :: linsolver
  integer,          intent(in) :: comm

  integer :: ie, i,j, pos
  integer, allocatable :: a1di(:), a1dj(:), b1di(:), b1dj(:)
  real(wp) :: aak(base%pk,base%pk)
  real(wp), allocatable :: a1dx(:)
  type(t_col), allocatable :: mat_array(:)
  type(t_intar), allocatable :: ii_b2m(:), jj_b2m(:)
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_kinds_initialized.eqv..false.) .or. &
    (mod_messages_initialized.eqv..false.) .or. &
   (mod_octave_io_initialized.eqv..false.) .or. &
      (mod_sparse_initialized.eqv..false.) .or. &
(mod_octave_io_sparse_initialized.eqv..false.) .or. &
  (mod_state_vars_initialized.eqv..false.) .or. &
   (mod_linsolver_initialized.eqv..false.) .or. &
      (mod_newton_initialized.eqv..false.) .or. &
   (mod_master_el_initialized.eqv..false.) .or. &
        (mod_grid_initialized.eqv..false.) .or. &
        (mod_base_initialized.eqv..false.) .or. &
         (mod_bcs_initialized.eqv..false.) .or. &
      (mod_cgdofs_initialized.eqv..false.) .or. &
    (mod_lc_state_initialized.eqv..false.) .or. &
   (mod_lc_locmat_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_lc_cnstep_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------


   ! 1) Build the matrix A1D
   associate( s => grid%ne*base%pk**2 )
   allocate( a1di(s) , a1dj(s) , a1dx(s) )
   end associate
   pos = 0
   do ie=1,grid%ne
     
     call lc_locmat_a( aak , base,grid%e(ie),bcs%b_e2be(ie), &
                       gamma*dt/2.0_wp )

     do j=1,base%pk
       do i=1,base%pk
         pos = pos + 1
         a1di(pos) = dofs%dofs(i,ie)
         a1dj(pos) = dofs%dofs(j,ie)
         a1dx(pos) = aak(i,j)
       enddo
     enddo

   enddo
   a1d = tri2col(new_tri( dofs%ndofs,dofs%ndofs , a1di-1,a1dj-1,a1dx ))
   deallocate( a1di,a1dj,a1dx )


   ! 2) Assemble the matrix A
   allocate( mat_array(grid%d) , ii_b2m(grid%d) , jj_b2m(grid%d) )
   mat_array = a1d ! d times the same matrix
   do i=1,grid%d
     allocate( ii_b2m(i)%i(dofs%ndofs) , jj_b2m(i)%i(dofs%ndofs) )
     ! diagonal blocks
     ii_b2m(i)%i = (/(grid%d*j+(i-1) , j=0,dofs%ndofs-1)/)
     jj_b2m(i)%i = ii_b2m(i)%i
   enddo
   associate( nm => grid%d*dofs%ndofs )
   call col_assemble( aaa,a1d2a , mat_array,ii_b2m,jj_b2m , nm,nm )
   end associate

   do i=1,grid%d
     call clear( mat_array(i) )
   enddo
   deallocate( mat_array , ii_b2m , jj_b2m )


   ! 3) Prepare the skeleton of the matrices Bj
   ! In fact, these matrices have the same pattern as a1d (unless one
   ! uses mass lumping); nevertheless, we compute them independently
   ! since this clarifies the whole procedure.
   associate( s => grid%ne*base%pk**2 )
   allocate( b1di(s) , b1dj(s) )
   end associate
   pos = 0
   do ie=1,grid%ne
     
     do j=1,base%pk
       do i=1,base%pk
         pos = pos + 1
         b1di(pos) = dofs%dofs(i,ie)
         b1dj(pos) = dofs%dofs(j,ie)
       enddo
     enddo

   enddo
   allocate( b1d(grid%d) , b1d_t2c(grid%d) )
   do i=1,grid%d
     call tri2col_skeleton( b1d(i) , b1d_t2c(i)%i ,                  &
             new_tri( dofs%ndofs,dofs%ndofs , b1di-1,b1dj-1,0.0_wp ) )
   enddo
   deallocate( b1di,b1dj )

   
   ! 4) Assemble the (skeleton of the) matrix B
   allocate( ii_b2m(grid%d) , jj_b2m(grid%d) )
   do i=1,grid%d
     allocate( ii_b2m(i)%i(dofs%ndofs) , jj_b2m(i)%i(dofs%ndofs) )
     ii_b2m(i)%i = (/(       j       , j=0,dofs%ndofs-1)/)
     jj_b2m(i)%i = (/(grid%d*j+(i-1) , j=0,dofs%ndofs-1)/) 
   enddo
   associate( n => dofs%ndofs , m => grid%d*dofs%ndofs )
   call col_assemble( bbb,b1d2b , b1d,ii_b2m,jj_b2m , n,m )
   end associate

   deallocate( ii_b2m , jj_b2m )


   ! 5) Assemble the skeleton of the complete matrix M
   allocate( ii_b2m(3) , jj_b2m(3) )
   ! 5.1) diagonal block: A
   associate( s=> grid%d*dofs%ndofs )
   allocate( ii_b2m(1)%i(s) , jj_b2m(1)%i(s) )
   ii_b2m(1)%i = (/( j , j=0,s-1)/) ! diagonal block
   jj_b2m(1)%i = ii_b2m(1)%i
   end associate
   associate( n => dofs%ndofs , m => grid%d*dofs%ndofs )
   ! 5.2) term B
   allocate( ii_b2m(2)%i(n) , jj_b2m(2)%i(m) )
   ii_b2m(2)%i = (/( j+grid%d*dofs%ndofs , j=0,n-1)/)
   jj_b2m(2)%i = (/( j                   , j=0,m-1)/)
   ! 5.3) term B^T
   allocate( ii_b2m(3)%i(m) , jj_b2m(3)%i(n) )
   ii_b2m(3)%i = (/( j                   , j=0,m-1)/)
   jj_b2m(3)%i = (/( j+grid%d*dofs%ndofs , j=0,n-1)/)
   end associate
   associate( nm => (grid%d+1)*dofs%ndofs )
   call col_assemble( mmm,ab2m , (/aaa,bbb,bbb/),ii_b2m,jj_b2m , &
                      nm,nm , (/.false.,.false.,.true./) )
   end associate

   deallocate( ii_b2m , jj_b2m )


   ! 6) Setup the linear solver
   select case(trim(linsolver))
    case('mumps')
     allocate(t_lc_mumps::linpb)
     select type(linpb); type is(t_lc_mumps)
      ! Solving the complete [A B^T B] system
      associate( gn => (grid%d+1)*dofs%ndofs ) ! global system size
      linpb%distributed    = .true.
      !linpb%poo           = <choose reordering>
      linpb%transposed_mat = .false.
      linpb%gn             = gn ! global # of dofs
      linpb%m              => mmm
      allocate(linrhs(gn))
      linpb%rhs            => linrhs
      allocate(lingij(gn))
      lingij = (/                                            &
                 ! u dofs
                 (/ (( grid%d*(ddc_dofs%gdofs(j)%gi-1) + i , &
                       i=1,grid%d) , j=1,dofs%ndofs) /) ,    &
                 ! q dofs
                 grid%d*ddc_dofs%ngdofs + ddc_dofs%gdofs%gi  &
               /) - 1 ! 0 based
      linpb%gij            => lingij
      linpb%mpi_comm       = comm
      end associate
     end select
    case default
     call error(this_sub_name,this_mod_name, 'Unknown linear solver.' )
   end select

   ! The matrix structure is now defined: we can call the analysis
   call linpb%factor('analysis')


   ! 7) Setup the nonlinear solver
   nlinpb%dt = dt
   call new_lc_stv(nlinpb%r , grid,base,bcs,dofs)
   nlinpb%mpi_id = ddc_grid%id
   nlinpb%comm   = comm
   nlinparams%nmax      = 250
   nlinparams%tolerance = 1.0e-9_wp






   open(55,file="bgtzhe")
   call write_octave( a1d , "a1d", 55)
   call write_octave( aaa , "aaa", 55)
   call write_octave( a1d2a(1)%i , "r", "a1d2am1", 55)
   call write_octave( a1d2a(2)%i , "r", "a1d2am2", 55)
   call write_octave( bbb , "bbb", 55)
   call write_octave( mmm , "mmm", 55)
   call write_octave( ab2m(1)%i , "r", "ab2m1", 55)
   call write_octave( ab2m(2)%i , "r", "ab2m2", 55)
   call write_octave( ab2m(3)%i , "r", "ab2m3", 55)
   call write_octave( lingij , "r", "lingij", 55)
   close(55)

   mod_lc_cnstep_initialized = .true.
 end subroutine mod_lc_cnstep_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_lc_cnstep_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'

  integer :: i
   
   !Consistency checks ---------------------------
   if(mod_lc_cnstep_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   call clear(nlinpb%r)
   
   call linpb%clean()
   deallocate( linpb )
   ! these might or might not be allocated, depending on the linsolver
   if(allocated(linrhs)) deallocate(linrhs)
   if(allocated(lingij)) deallocate(lingij)

   call clear(mmm)
   deallocate(ab2m)

   call clear(bbb)
   do i=1,size(b1d)
     call clear(b1d(i))
   enddo
   deallocate(b1d)
   deallocate( b1d2b , b1d_t2c )

   call clear(a1d)
   call clear(aaa)
   deallocate(a1d2a)

   mod_lc_cnstep_initialized = .false.
 end subroutine mod_lc_cnstep_destructor

!-----------------------------------------------------------------------

 subroutine lc_cnstep(xn,xn1)
  type(t_lc_stv), target, intent(in) :: xn
  type(t_lc_stv), intent(inout) :: xn1

  logical :: maxiter
  integer :: niter
  real(wp), allocatable :: tmp_q(:)
  real(wp), allocatable :: res(:,:) ! nonlinear residuals
  logical, parameter :: verbose = .true.
  character(len=*), parameter :: &
    this_sub_name = 'lc_cnstep'

  ! 1) solve the nonlinear problem

  call xn1%copy( xn ) ! initial guess:  xn1 = xn

  nlinpb%uqn => xn

  call newton(xn1,nlinpb, nlinparams, maxiter, niter, res, &
              nlinpb%mpi_id, nlinpb%comm)

  if(verbose) call step_diag_output()

  ! 2) recover the new time level:   un1 = 2*w - un

  ! Because of the way the time discretization is defined, q only
  ! appears at half time levels. So, we keep the q computed in the
  ! Newton method and return is instead of the theoretically more
  ! consistent value at t^{n+1}

  allocate( tmp_q(size(xn1%q)) ); tmp_q = xn1%q

  call xn1%tims(  2.0_wp  )
  call xn1%inlt(-1.0_wp,xn)

  xn1%q = tmp_q
  deallocate( tmp_q )

 contains

  subroutine step_diag_output()

   character(len=10), parameter :: n_res(5) = (/ character(len=10) :: &
                'residual', 'increment', 'omega', 'forcing', 'iters' /)
    
   integer :: i
   character(len=10000) :: message(3+size(n_res))

    write(message(1),'(a)')    "Completed Newton step:"
    write(message(2),'(a,l1)') "  maxiter = ",maxiter
    write(message(3),'(a,i0)') "    niter = ",niter
    do i=1,size(n_res)
      write(message(3+i),'(a,"=",*(e12.3))') n_res(i), res(i,:)
    enddo
    call info(this_sub_name,this_mod_name,message(1:3+size(n_res)))
  
  end subroutine step_diag_output

 end subroutine lc_cnstep

!-----------------------------------------------------------------------

 !> Preconditioned residual \f$z=-J_f^{-1} f(x)\f$
 !!
 !! Solve \ref lc_cnstep_linpb "this linear system". More precisely,
 !! given the \ref lc_cnstep_nonlinpb "nonlinear system" to be solved,
 !! we set \f${\tt x} = ({\bf w},s)\f$, so that the residual is
 !! defined as
 !! \f{displaymath}{
 !!  r = f({\tt x}) = \left\{
 !!  \begin{array}{l}
 !!   \displaystyle
 !!    ( {\bf w}-{\bf u}^n , \bar{\bf u}_i )_h
 !! + \frac{\gamma\Delta t}{2} ( \nabla{\bf w},\nabla\bar{\bf u}_i )
 !! +  ( s ,i_Q\left( \tilde{\bf p} \cdot \bar{\bf u}_i \right) )
 !! \\[4mm]
 !!   \displaystyle
 !! (i_Q\left(\tilde{\bf p}\cdot({\bf w}-{\bf u}^n) - \Gamma^n \right)
 !! ,\bar{q}_k) \end{array}\right.
 !! \f}
 !! for 
 !! \f{displaymath}{
 !!  \tilde{\bf p} = \frac{\gamma\Delta t}{2} \frac{{\bf w}}{|{\bf
 !!  w}|^2}.
 !! \f}
 !! Denoting then \f${\tt z} = (\delta{\bf w},\delta s)\f$, the
 !! preconditioner is
 !! \f{displaymath}{
 !!  J_f\,{\tt z} = \left\{
 !!  \begin{array}{l}
 !!   \displaystyle
 !!    ( \delta{\bf w} , \bar{\bf u}_i )_h
 !! + \frac{\gamma\Delta t}{2}(\nabla\delta{\bf w},\nabla\bar{\bf u}_i)
 !! + ( \delta s ,i_Q\left( 
 !!      \tilde{\bf p}
 !!      \cdot \bar{\bf u}_i \right) ) \\[4mm]
 !!   \displaystyle
 !! ( i_Q\left( \tilde{\bf p}\cdot\delta {\bf w} \right),\bar{q}_k )
 !! \end{array} \right.
 !! \f}
 !! Substituting in \f$J_f\,z=- f(x)\f$ yields the linear system
 !! \f{displaymath}{
 !!  \left[
 !!  \begin{array}{cc}
 !!    A & (B^{\tilde{\bf p}})^T \\
 !!    B^{\tilde{\bf p}} & 0
 !!  \end{array}\right]\,
 !!  \left[
 !!  \begin{array}{c}
 !!   \delta{\bf w} \\ \delta s
 !!  \end{array}\right] = 
 !!  \left[
 !!  \begin{array}{c}
 !!   -r_{\bf u} \\ -r_q
 !!  \end{array}\right],
 !! \f}
 !! which is exactly the previously referred \ref lc_cnstep_linpb
 !! "linear system" except that it is written for the increment,
 !! rather than for the whole variable.
 !!
 !! \section lc_cnstep_pres_bcs Boundary conditions
 !!
 !! Introducing Dirichlet boundary conditions as described \ref
 !! lc_cnstep_bcs "here", the first component of the residual becomes
 !! \f{displaymath}{
 !!    ( {\bf w}-{\bf u}^n , \bar{\bf u}_i )_h
 !! + \frac{\gamma\Delta t}{2} ( \nabla{\bf w},\nabla\bar{\bf u}_i )
 !! +  ( s ,i_Q\left( \tilde{\bf p} \cdot \bar{\bf u}_i \right) )
 !! + \Delta t\int_{\partial \Omega} \xi \left({\bf w} - \frac{{\bf
 !! u}^{n}+{\bf u}^b}{2}\right)\cdot \bar{\bf u}\,dx,
 !! \f}
 !! while the first component of the linear operator is
 !! \f{displaymath}{
 !!    ( \delta{\bf w} , \bar{\bf u}_i )_h
 !! + \frac{\gamma\Delta t}{2}(\nabla\delta{\bf w},\nabla\bar{\bf u}_i)
 !! + ( \delta s ,i_Q\left( 
 !!      \tilde{\bf p}
 !!      \cdot \bar{\bf u}_i \right) ) 
 !! + \Delta t\int_{\partial \Omega} \xi \delta{\bf w} \cdot \bar{\bf
 !! u}\,dx,
 !! \f}
 !! which again includes the correction to the system matrix described
 !! in \ref lc_cnstep_bcs "this section".
 subroutine lc_pres(z,x,eta,linres,liniter)
  class(c_nwt),    intent(inout) :: x
  class(t_lc_nwt), intent(inout) :: z
  real(wp), intent(in)  :: eta
  real(wp), intent(out) :: linres
  integer,  intent(out) :: liniter

  integer :: ie, jd, i,j, pos, udim
  real(wp) :: sigma
  real(wp), allocatable :: wk(:,:), bb_p(:,:,:)

   select type(x); type is(t_lc_nwt)
   select type(xst=>x%x); type is(t_lc_stv)
   select type(zst=>z%x); type is(t_lc_stv)

   associate( grid => xst%grid , base => xst%base , dofs => xst%dofs )
   associate( d => grid%d , ne => grid%ne , pk => base%pk )

   allocate( wk(d,pk) , bb_p(pk,pk,d) )

   sigma = gamma * x%dt / 2.0_wp

   !--------------------------------------------------------------------
   ! 1) update the nonconstant blocks in the system matrix

   ! 1.1) compute the 1D B matrices
   do jd=1,d
     b1d(jd)%ax = 0.0_wp
   enddo
   pos = 0
   do ie=1,ne
     
     wk = xst%u( : , dofs%dofs(:,ie) )

     call lc_locmat_b( bb_p ,base,grid%e(ie),sigma,wk)

     do j=1,pk
       do i=1,pk
         pos = pos + 1

         ! fill the d 1D B matrices
         do jd=1,d
           associate( set_el => b1d(jd)%ax( b1d_t2c(jd)%i(pos) ) )
           set_el = set_el + bb_p(i,j,jd)
           end associate
         enddo

       enddo
     enddo

   enddo

   deallocate( wk , bb_p )

   ! 1.2) transfer the values in the B matrix
   bbb%ax = 0.0_wp
   do jd=1,d
     bbb%ax( b1d2b(jd)%i ) = bbb%ax( b1d2b(jd)%i ) + b1d(jd)%ax
   enddo

   ! 1.3) transfer the in the M matrix, using no overlap blocks A and B
   mmm%ax( ab2m(2)%i ) = bbb%ax
   mmm%ax( ab2m(3)%i ) = bbb%ax
   !--------------------------------------------------------------------

   !--------------------------------------------------------------------
   ! 2) compute the residual: it has been done already in lc_defy
   !--------------------------------------------------------------------

   !--------------------------------------------------------------------
   ! 3) solve the linear system

   ! 3.1) set the rhs, i.e. -r
   udim = grid%d * dofs%ndofs
   associate( u_rhs => linrhs(:udim   ) , &
              q_rhs => linrhs( udim+1:) )
   u_rhs = reshape( -x%r%u , (/udim/) )
   q_rhs =          -x%r%q
   end associate

   ! 3.2) factorize and solve the system
   call linpb%factor('factorization')
   call linpb%solve( zst )
   !--------------------------------------------------------------------

   end associate
   end associate

   end select
   end select
   end select
   
   ! \todo These could be useful with iterative solvers
   linres  = 0.0_wp
   liniter = 1

 end subroutine lc_pres

!-----------------------------------------------------------------------

 !> Residual norm \f$\|f(x)\|\f$
 function lc_nres(x) result(n)
  class(t_lc_nwt), intent(inout) :: x
  real(wp) :: n

   n = sqrt( sum(x%r%u**2) + sum(x%r%q**2) )
   
 end function lc_nres

!-----------------------------------------------------------------------

 !> Increment norm \f$\|x\|\f$
 function lc_norm(x) result(n)
  class(t_lc_nwt), intent(in) :: x
  real(wp) :: n

   select type(st=>x%x); type is(t_lc_stv)

   n = sqrt( sum(st%u**2) + sum(st%q**2) )

   end select

 end function lc_norm

!-----------------------------------------------------------------------

 !> Copy a \c c_stv object into a \c c_nwt one; while doing this, also
 !! compute the residual, for the following calls to \c lc_pres and \c
 !! lc_nres.
 subroutine lc_defy(y,x)
  class(c_stv),    intent(in)    :: x
  class(t_lc_nwt), intent(inout) :: y

  integer :: ie
  real(wp) :: sigma
  real(wp), allocatable :: ruk(:,:), rqk(:), unk(:,:), wk(:,:), sk(:)

   ! 1) copy the state variable
   call y%x%copy( x )

   ! 2) compute the residual
   select type(x); type is(t_lc_stv)
   associate( grid => x%grid , base => x%base , dofs => x%dofs )
   associate( d => grid%d , ne => grid%ne , pk => base%pk )

   ! these allocations must be done in the select type
   allocate( ruk(d,pk) , rqk(pk) , unk(d,pk) , wk(d,pk) , sk(pk) )

   sigma = gamma * y%dt / 2.0_wp
   y%r%u = 0.0_wp
   y%r%q = 0.0_wp
   do ie=1,ne

     unk = y%uqn%u( : , dofs%dofs(:,ie) )
     wk  =     x%u( : , dofs%dofs(:,ie) )
     sk  =     x%q(     dofs%dofs(:,ie) )

     associate( e => grid%e(ie) , be => x%bcs%b_e2be(ie) )
     call lc_locnlres(ruk,rqk , base,e,be , unk,sigma,wk,sk)
     end associate

     y%r%u( : , dofs%dofs(:,ie) ) = y%r%u( : , dofs%dofs(:,ie) ) + ruk
     y%r%q(     dofs%dofs(:,ie) ) = y%r%q(     dofs%dofs(:,ie) ) + rqk

   enddo

   deallocate( ruk , rqk , unk , wk , sk )

   end associate
   end associate
   end select

 end subroutine lc_defy

!-----------------------------------------------------------------------

 subroutine general_lc_xassign(x,s,x_vec)
  real(wp),       intent(in)    :: x_vec(:)
  class(c_linpb), intent(inout) :: s
  class(c_stv),   intent(inout) :: x

  integer :: udim

   select type(x); type is(t_lc_stv)
    udim = x%grid%d * x%dofs%ndofs
    associate( u_vec => x_vec(:udim   ) , &
               q_vec => x_vec( udim+1:) )

    x%u = reshape( u_vec , (/ x%grid%d , x%dofs%ndofs /) )
    x%q =          q_vec

    end associate
   end select

 end subroutine general_lc_xassign

!-----------------------------------------------------------------------

end module mod_lc_cnstep

