!>\brief
!!
!! Local matrices for the liquid crystal model.
!!
!! \n
!!
!! Local matrices for the liquid crystal model.
!!
!! In this module, we precompute three local matrices for all the
!! elements: the complete and lumped mass matrices and the stiffness
!! matrix; all of them for a scalar filed. These matrices are then
!! used both to compute the residual and the system matrix.
!<----------------------------------------------------------------------
module mod_lc_locmat

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_master_el, only: &
   mod_master_el_initialized

 use mod_grid, only: &
   mod_grid_initialized, &
   t_grid, t_e, affmap, el_linear_size

 use mod_base, only: &
   mod_base_initialized, &
   t_base

 use mod_bcs, only: &
   mod_bcs_initialized, &
   t_bcs, t_b_e, p_t_b_e, &
   b_dir, b_neu, b_ddc

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_lc_locmat_constructor, &
   mod_lc_locmat_destructor,  &
   mod_lc_locmat_initialized, &
   t_bcs_error, &
   lc_locmat_a, lc_locmat_b, lc_locnlres

 private

!-----------------------------------------------------------------------

 !> A type to handle errors in the bcs
 type :: t_bcs_error
  logical :: lerr = .false.
  character(len=1000) :: message
 end type t_bcs_error

 logical, parameter :: lump_b_mat = .true.
 real(wp), parameter :: xi_coeff = 1000.0_wp

 !> \f$ \int_K \varphi_i\varphi_j\,dx \f$
 real(wp), allocatable :: gmmk(:,:,:)
 !> diagonal lumped mass matrix as a vector
 real(wp), allocatable :: gmmkh(:,:)
 !> \f$ \int_K\nabla\varphi_i\cdot\nabla\varphi_j\,dx \f$
 real(wp), allocatable :: gaak(:,:,:)
 !> Boundary condition terms
 !!
 !! For boundary elements, we compute a matrix associated with the
 !! penalization term, as discussed \ref lc_cnstep_bcs "here" and
 !! \ref lc_cnstep_pres_bcs "here". More precisely, this matrix is
 !! defined as
 !! \f{displaymath}{
 !!  \frac{\xi_0}{h_K} \int_{\partial K} \varphi_j \varphi_i \, dx,
 !! \f}
 !! for a fixed constant \f$\xi_0\f$.
 !!
 !! This array has size \field_fref{mod_bcs,t_bcs,nb_elem}, so that it
 !! must be accessed as <tt>gxik(:,: , be\%i )</tt>.
 real(wp), allocatable :: gxik(:,:,:)

 ! public members
 logical, protected :: &
   mod_lc_locmat_initialized = .false.
 ! private members
 character(len=*), parameter :: &
   this_mod_name = 'mod_lc_locmat'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_lc_locmat_constructor(grid,base,bcs)
  type(t_grid), intent(in) :: grid
  type(t_base), intent(in) :: base
  type(t_bcs),  intent(in) :: bcs

  integer :: ie, l,i,j
  real(wp) :: fdp1 ! 1/(d+1)
  real(wp) :: bit(grid%d,grid%d), wg(base%m), gradp(grid%d,base%pk)
  type(t_bcs_error) :: bcs_err
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_kinds_initialized.eqv..false.) .or. &
    (mod_messages_initialized.eqv..false.) .or. &
   (mod_master_el_initialized.eqv..false.) .or. &
        (mod_grid_initialized.eqv..false.) .or. &
        (mod_base_initialized.eqv..false.) .or. &
         (mod_bcs_initialized.eqv..false.) ) then 
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_lc_locmat_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   ! Precompute the local matrices
   allocate( gmmk(base%pk,base%pk,grid%ne) , & 
             gmmkh(base%pk,grid%ne)        , &
             gaak(base%pk,base%pk,grid%ne) )
   allocate( gxik(base%pk,base%pk,bcs%nb_elem) )
   
   fdp1 = 1.0_wp/real(grid%d+1,wp)
   
   do ie=1,grid%ne
     
     associate( mmk => gmmk(:,:,ie) , &
               mmkh => gmmkh(:,ie)  , &
                aak => gaak(:,:,ie) )

     bit = transpose(grid%e(ie)%bi)
     wg  = grid%e(ie)%det_b*base%wg

     mmk = 0.0_wp
     aak = 0.0_wp
     do l=1,base%m
       gradp = matmul( bit , base%gradp(:,:,l) )
       do j=1,base%pk
         do i=1,base%pk

           if(.not.lump_b_mat) &
             mmk(i,j) = mmk(i,j) + wg(l) * base%p(i,l)*base%p(j,l)

           aak(i,j) = aak(i,j) + wg(l)                              &
                                 * dot_product(gradp(:,i),gradp(:,j))

         enddo
       enddo
     enddo

     mmkh = fdp1*grid%e(ie)%vol

     ! diagonal B terms
     if(lump_b_mat) then
       do i=1,base%pk
         mmk(i,i) = mmk(i,i) + fdp1*grid%e(ie)%vol
       enddo
     endif

     end associate
   
     ! Boundary elements
     if(associated( bcs%b_e2be(ie)%p )) then
       associate(  be => bcs%b_e2be(ie)%p )
       associate( xik => gxik(:,:, be%i ) )
       call compute_xik( xik , base,be,bcs_err )

       if(bcs_err%lerr) &
         call error(this_sub_name,this_mod_name,bcs_err%message)

       end associate
       end associate
     endif
       
   enddo

   mod_lc_locmat_initialized = .true.
 
 contains

  pure subroutine compute_xik(xik,base,be,err)
   type(t_base), intent(in) :: base
   type(t_b_e),  intent(in) :: be
   real(wp),     intent(out) :: xik(:,:)
   type(t_bcs_error), intent(out) :: err

   integer :: isb, isl, i,j,l
   real(wp) :: h ! element linear size
   real(wp) :: wgsa(base%ms)

    xik = 0.0_wp
    do isb=1,be%nbs ! loop on the boundary sides
      btype: select case(be%bs(isb)%p%bc)

       case(b_dir)
        
        h = el_linear_size( be%e )

        ! local index of the side
        isl = be%bs(isb)%p%s%isl(1)

        ! side weights reordered and scaled by the side area
        wgsa = (be%e%s(isl)%p%a/base%me%voldm1) * &
                 base%wgs(base%stab(be%e%ip(isl),:))

        ! include the weighting coefficient
        wgsa = (xi_coeff/h) * wgsa

        do l=1,base%ms
          do i=1,base%pk
            do j=1,base%pk
              xik(i,j) = xik(i,j) + wgsa(l) * &
                  base%pb(j,l,isl) * base%pb(i,l,isl)
            enddo
          enddo
        enddo

       ! case(b_neu) todo
 
       case(b_ddc)
        ! nothing to do

       case default
        err%lerr = .true.
        write(err%message,'(a,i5,a,i5,a,i5,a)') &
          'Unknown boundary condition "',be%bs(isb)%p%bc,       &
          '" on side ',be%bs(isb)%p%s%i,', on element ',be%e%i,'.'

      end select btype
    enddo

  end subroutine compute_xik

 end subroutine mod_lc_locmat_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_lc_locmat_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_lc_locmat_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   deallocate( gmmk , gmmkh , gaak )
   deallocate( gxik )

   mod_lc_locmat_initialized = .false.
 end subroutine mod_lc_locmat_destructor

!-----------------------------------------------------------------------

 !> Local \f$A^{1D,K}\f$ matrix
 !!
 !! We compute here
 !! \f{displaymath}{
 !!  A^{1D,K}_{ij} = ( \varphi_j , \varphi_i )_{h,K} + \sigma (
 !!  \nabla\varphi_j , \nabla\varphi_i )_K = \frac{|K|}{d+1}\delta_{ij}
 !!  +\sigma\int_K \nabla\varphi_j \cdot \nabla\varphi_i \,dx
 !! \f}
 !! for an arbitrary coefficient \f$\sigma\f$; see also \ref
 !! lc_cnstep_bdiag "this section".
 !!
 !! For boundary elements, the matrix also includes the penalization
 !! term, as discussed \ref lc_cnstep_bcs "here" and \ref
 !! lc_cnstep_pres_bcs "here". Concerning the penalization
 !! coefficient, we set
 !! \f{displaymath}{
 !!  \xi = \frac{\gamma}{2} \frac{\xi_0}{h_K}
 !! \f}
 !! so that the required contribution ends up being
 !! \f{displaymath}{
 !!  A^{1D,b,K}_{ij} = \sigma\frac{\xi_0}{h_K}\int_{\partial K}\varphi_j
 !!  \varphi_i \, dx.
 !! \f}
 !! This is easily computed using the module array
 !! \fref{mod_lc_locmat,gxik}.
 !!
 !! \warning The present implementation is specific for first order
 !! elements, because of the treatment of the mass lumping.
 pure subroutine lc_locmat_a(aak,base,e,be,sigma)
  type(t_base), intent(in) :: base
  type(t_e),    intent(in) :: e
  type(p_t_b_e), intent(in) :: be
  real(wp),     intent(in) :: sigma
  real(wp),     intent(out) :: aak(:,:)

  integer :: i

   ! 1) gradient terms
   aak = sigma*gaak(:,:,e%i)

   ! 2) mass matrix terms
   do i=1,base%pk
     aak(i,i) = aak(i,i) + gmmkh(i,e%i)
   enddo

   ! 3) boundary terms, if required
   if(associated(be%p)) &
     aak(:,:) = aak(:,:) + sigma*gxik(:,:, be%p%i )
     
 end subroutine lc_locmat_a

!-----------------------------------------------------------------------

 !> Compute the local nonlinear residual
 !!
 !! The local residual is computed as defined in
 !! \fref{mod_lc_cnstep,lc_pres}.
 !!
 !! To clarify the computation of the off-diagonal terms, observe that
 !! \f{displaymath}{
 !!  ( s ,i_Q\left( \tilde{\bf p}\cdot\bar{\bf u}_i \right) ) =
 !!  ( s_k\varphi_k ,i_Q\left( \tilde{\bf p}\cdot {\bf e}_{i_d}
 !!   \varphi_i\right)) =
 !!  ( \varphi_k ,i_Q\left( \tilde{p}_{i_d} \varphi_i\right))s_k
 !! \f}
 !! and using the expression for \f$B^{\tilde{p}_{i_d}}_{ki}\f$ given
 !! \ref lc_cnstep_bdiag_numquad "here"
 !! \f{displaymath}{
 !!  ( s ,i_Q\left( \tilde{\bf p}\cdot\bar{\bf u}_i \right) ) =
 !!   \sum_k B^{\tilde{p}_{i_d}}_{ki}s_k = 
 !!   \sum_k \tilde{p}_{i_d}({\bf x}_i)(\varphi_i,\varphi_k)s_k .
 !! \f}
 !! Similarly, we have
 !! \f{displaymath}{
 !!  ( \bar{q}_k ,i_Q\left( \tilde{\bf p}\cdot({\bf w}-{\bf u}^n)
 !!  \right) ) =
 !!  ( \varphi_k ,i_Q\left( \tilde{\bf p}\cdot({\bf w}_j-{\bf u}^n_j)
 !!  \varphi_j \right) ) =
 !!  ( \varphi_k ,i_Q\left( \tilde{p}_{j_d} \varphi_j\right)) \, 
 !!   (w_{j_dj}-u^n_{j_dj})
 !! \f}
 !! so that
 !! \f{displaymath}{
 !!  ( \bar{q}_k ,i_Q\left( \tilde{\bf p}\cdot({\bf w}-{\bf u}^n)
 !!  \right) ) =
 !!  \sum_{j_d,j} B^{\tilde{p}_{j_d}}_{kj}
 !!   (w_{j_dj}-u^n_{j_dj}).
 !! \f}
 pure subroutine lc_locnlres(ruk,rqk,base,e,be,unk,sigma,wk,sk)
  type(t_base), intent(in) :: base
  type(t_e),    intent(in) :: e
  type(p_t_b_e), intent(in) :: be
  !> \f${\bf u}^n|_K\f$, nodal values
  real(wp),     intent(in) :: unk(:,:)
  real(wp),     intent(in) :: sigma
  !> \f${\bf w}|_K\f$, nodal values 
  real(wp),     intent(in) :: wk(:,:)
  !> \f$s|_K\f$, nodal values 
  real(wp),     intent(in) :: sk(:)
  real(wp),     intent(out) :: ruk(:,:)
  real(wp),     intent(out) :: rqk(:)

  integer :: id
  real(wp) :: dwk(e%d,base%pk), bb_p(base%pk,base%pk,e%d), &
    ubk(e%d,base%pk)

   ! 0) precompute w - u^n
   dwk = wk - unk

   ! 1) mass matrix terms
   do id=1,e%d
     ruk(id,:) = gmmkh(:,e%i) * dwk(id,:)
   enddo

   ! 2) gradient terms (using the symmetry of gaak)
   ruk = ruk + sigma * matmul( wk , gaak(:,:,e%i) )

   ! 3) s terms
   call lc_locmat_b(bb_p,base,e,sigma,wk)
   rqk = 0.0_wp
   do id=1,e%d
     ruk(id,:) = ruk(id,:) + matmul(   sk         , bb_p(:,:,id) )
     rqk       = rqk       + matmul( bb_p(:,:,id) , dwk(id,:)    )
   enddo
   ! Gamma^n term, using the same array gmmk included in bb_p, so that
   ! it works both with and without lumping of the B matrix
   rqk = rqk - matmul( gmmk(:,:,e%i) ,                        &
                 sigma*(1.0_wp-vabs2(unk))/(4.0_wp*vabs2(wk)) )
   
   ! 4) bcs terms if required
   if(associated(be%p)) then
     ! todo: fix the computation of the problem data
     !ubk = coeff_ub( -be%p%bs(1)%p%s%ie(2) )
     ubk = coeff_ub( be%p )
     ruk = ruk + sigma * matmul(wk-0.5_wp*(unk+ubk) , gxik(:,:,be%p%i))
   endif

 contains

  pure function vabs2(v) ! |v|^2 by columns
   real(wp), intent(in) :: v(:,:)
   real(wp) :: vabs2(size(v,2))

    vabs2 = sum( v**2 , 1 )

  end function vabs2

  pure function coeff_ub(be) result(ub)
   type(t_b_e), intent(in) :: be
   real(wp) :: ub(e%d,base%pk)

   integer :: iv, breg, ivl
   real(wp) :: xv(e%d)

    ! ub is set only for the nodes on the boundary
    ub = 0.0_wp

    do iv=1,be%nbv

      ivl  = be%ibvl(iv)

      breg = be%bv(iv)%p%breg
      xv   = be%bv(iv)%p%v%x

      select case(breg)
       case(1)
        ub(:,ivl) = xv
       case(2)
        ub(1 ,ivl) =  xv(2)
        ub(2 ,ivl) = -xv(1)
        ub(3:,ivl) = 0.0_wp
      end select
      ub(:,ivl) = ub(:,ivl)/sqrt(sum( ub(:,ivl)**2 )) 
    enddo

  end function coeff_ub

  !pure function coeff_ub(breg) result(ub)
  ! integer, intent(in) :: breg
  ! real(wp) :: ub(e%d,base%pk)

  !  ub = 0.0_wp
  !  !select case(breg)
  !  ! case(1)
  !  !  ub(1,:) = 1.0_wp
  !  ! case(2)
  !  !  ub(2,:) = 1.0_wp
  !  ! case(3)
  !  !  ub(1,:) = -1.0_wp
  !  ! case(4)
  !  !  ub(2,:) = -1.0_wp
  !  !end select
  !  !select case(breg)
  !  ! case(1)
  !  !  ub(2,:) = -1.0_wp
  !  ! case(2)
  !  !  ub(2,:) = -1.0_wp
  !  ! case(3)
  !  !  ub(2,:) = -1.0_wp
  !  ! case(4)
  !  !  ub(2,:) = -1.0_wp
  !  !end select
  !  select case(breg)
  !   case(1)
  !    ub(2,:) = -1.0_wp
  !   case(2)
  !    ub(2,:) = -1.0_wp
  !  end select

  !end function coeff_ub
 
 end subroutine lc_locnlres

!-----------------------------------------------------------------------

 !> Compute \f$B^{\tilde{p}_{j_d}}\f$
 pure subroutine lc_locmat_b(bb_p,base,e,sigma,wk)
  type(t_base), intent(in) :: base
  type(t_e),    intent(in) :: e
  real(wp),     intent(in) :: sigma
  !> \f${\bf w}|_K\f$, nodal values 
  real(wp),     intent(in) :: wk(:,:)
  !> \f$B^{\tilde{p}_{j_d}}_{kj}\f$, third index for \f$j_d\f$
  real(wp),     intent(out) :: bb_p(:,:,:)

  integer :: jd, j
  real(wp) :: p(e%d,base%pk)

   ! 1) compute tilde{p}
   do j=1,base%pk
     p(:,j) = sigma*wk(:,j)/sum(wk(:,j)**2) ! nodal values of \tilde{p}
   enddo

   ! 2) compute the local matrices
   do jd=1,e%d
     do j=1,base%pk
       bb_p(:,j,jd) = p(jd,j)*gmmk(:,j,e%i)
     enddo
   enddo

 end subroutine lc_locmat_b

!-----------------------------------------------------------------------

end module mod_lc_locmat

