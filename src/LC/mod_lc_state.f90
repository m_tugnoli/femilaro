!>\brief
!!
!! State variables for the liquid crystal model.
!!
!! \n
!!
!! State variables for the liquid crystal model: the state is
!! represented as a collection \f$({\bf u},q)\f$. Notice that,
!! according to the general framework of \c mod_time_integrators, the
!! Lagrange multiplier should be included in the diagnostic variables;
!! nevertheless, we are not interested in the time integrators of \c
!! mod_time_integrators, so introducing the multiplier directly in the
!! state seems reasonable.
!<----------------------------------------------------------------------
module mod_lc_state

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_octave_io, only: &
   mod_octave_io_initialized, &
   write_octave
 
 use mod_state_vars, only: &
   mod_state_vars_initialized, &
   c_stv

 use mod_grid, only: &
   mod_grid_initialized, &
   t_grid, t_ddc_grid

 use mod_base, only: &
   mod_base_initialized, &
   t_base

 use mod_bcs, only: &
   mod_bcs_initialized, &
   t_bcs

 use mod_cgdofs, only: &
   mod_cgdofs_initialized, &
   t_cgdofs, t_ddc_cgdofs

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_lc_state_constructor, &
   mod_lc_state_destructor,  &
   mod_lc_state_initialized, &
   t_lc_stv, new_lc_stv, clear

 private

!-----------------------------------------------------------------------

! Module types and parameters

 !> State information for the liquid crystal system.
 type, extends(c_stv) :: t_lc_stv
  type(t_grid),   pointer :: grid => null()
  type(t_base),   pointer :: base => null()
  type(t_bcs ),   pointer :: bcs  => null()
  type(t_cgdofs), pointer :: dofs => null()
  real(wp), allocatable :: u(:,:) !< \f${\bf u}\f$
  real(wp), allocatable :: q(:)   !< \f$q\f$
 contains
  procedure, pass(x) :: incr
  procedure, pass(x) :: tims
  procedure, pass(z) :: copy
  procedure, pass(x) :: scal => lc_scal
  ! no need to override the source allocation
 end type t_lc_stv

 interface clear
   module procedure clear_lc_stv
 end interface

! Module variables

 ! public members
 logical, protected ::               &
   mod_lc_state_initialized = .false.
 ! private members
 character(len=*), parameter :: &
   this_mod_name = 'mod_lc_state'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_lc_state_constructor()
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_kinds_initialized.eqv..false.) .or. &
    (mod_messages_initialized.eqv..false.) .or. &
   (mod_octave_io_initialized.eqv..false.) .or. &
  (mod_state_vars_initialized.eqv..false.) .or. &
        (mod_grid_initialized.eqv..false.) .or. &
        (mod_base_initialized.eqv..false.) .or. &
         (mod_bcs_initialized.eqv..false.) .or. &
      (mod_cgdofs_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_lc_state_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   mod_lc_state_initialized = .true.
 end subroutine mod_lc_state_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_lc_state_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_lc_state_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_lc_state_initialized = .false.
 end subroutine mod_lc_state_destructor

!-----------------------------------------------------------------------

 subroutine new_lc_stv(obj,grid,base,bcs,dofs)
  type(t_grid),   intent(in), target :: grid
  type(t_base),   intent(in), target :: base
  type(t_bcs),    intent(in), target :: bcs
  type(t_cgdofs), intent(in), target :: dofs
  type(t_lc_stv), intent(out) :: obj

   obj%grid => grid
   obj%base => base
   obj%bcs  => bcs
   obj%dofs => dofs
   allocate( obj%u(grid%d,dofs%ndofs) )
   allocate( obj%q(       dofs%ndofs) )

 end subroutine new_lc_stv
 
!-----------------------------------------------------------------------

 pure subroutine clear_lc_stv(obj)
  type(t_lc_stv), intent(out) :: obj

   nullify( obj%grid )
   nullify( obj%base )
   nullify( obj%bcs  )
   nullify( obj%dofs )
   ! allocatable components are implicitly deallocated
 
 end subroutine clear_lc_stv

!-----------------------------------------------------------------------

 subroutine incr(x,y)
  class(c_stv),    intent(in)    :: y
  class(t_lc_stv), intent(inout) :: x

   select type(y); type is(t_lc_stv)

   x%u = x%u + y%u
   x%q = x%q + y%q

   end select

 end subroutine incr

!-----------------------------------------------------------------------

 subroutine tims(x,r)
  real(wp),        intent(in)    :: r
  class(t_lc_stv), intent(inout) :: x

   x%u = r*x%u
   x%q = r*x%q

 end subroutine tims

!-----------------------------------------------------------------------

 subroutine copy(z,x)
  class(c_stv),    intent(in)    :: x
  class(t_lc_stv), intent(inout) :: z

   select type(x); type is(t_lc_stv)
   z%u = x%u
   z%q = x%q
   end select

 end subroutine copy

!-----------------------------------------------------------------------

 function lc_scal(x,y) result(s)
  class(t_lc_stv), intent(in) :: x
  class(c_stv),    intent(in) :: y
  real(wp) :: s

   select type(y); type is(t_lc_stv)

   s = sum(x%u*y%u) + dot_product(x%q,y%q)

   end select

 end function lc_scal

!-----------------------------------------------------------------------

end module mod_lc_state

