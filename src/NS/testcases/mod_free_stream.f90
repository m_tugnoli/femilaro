!>\brief
!!
!! Free stream
!!
!! \n
!!
!! A minimalistic test, mostly for debugging.
!<----------------------------------------------------------------------
module mod_free_stream_test

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_fu_manager, only: &
   mod_fu_manager_initialized, &
   new_file_unit

 use mod_physical_constants, only: &
   mod_physical_constants_initialized, &
   t_phc

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_free_stream_test_constructor, &
   mod_free_stream_test_destructor,  &
   mod_free_stream_test_initialized, &
   test_name,        &
   test_description, &
   phc,              &
   ntrcs,            &
   ref_prof,         &
   coeff_dir,        &
   coeff_neu,        &
   coeff_visc,       &
   coeff_init,       &
   coeff_outref

 private

!-----------------------------------------------------------------------

 ! public interface
 character(len=*), parameter   :: test_name = "free-stream"
 character(len=100), protected :: test_description(1)
 type(t_phc), save, protected  :: phc
 integer, parameter            :: ntrcs = 0
 character(len=*), parameter   :: ref_prof = "none"

 logical, protected :: mod_free_stream_test_initialized = .false.

 real(wp) :: &
   nu        ! kinematic viscosity
 character(len=*), parameter :: &
   test_input_file_name = 'free_stream.in'
 character(len=*), parameter :: &
   this_mod_name = 'mod_free_stream_test'

 ! Input namelist
 namelist /input/ &
   nu

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_free_stream_test_constructor(d)
  integer, intent(in) :: d

  integer :: fu, i, ierr
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false.)   .or. &
       (mod_kinds_initialized.eqv..false.)      .or. &
       (mod_fu_manager_initialized.eqv..false.) .or. &
       (mod_physical_constants_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_free_stream_test_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   ! Read input file
   call new_file_unit(fu,ierr)
   open(fu,file=trim(test_input_file_name), &
      status='old',action='read',form='formatted',iostat=ierr)
    if(ierr.ne.0) call error(this_sub_name,this_mod_name, &
      'Problems opening the input file')
    read(fu,input)
   close(fu,iostat=ierr)

   write(test_description(1),'(a,e9.3,a)') &
     'Free stream problem, viscosity nu = ',nu,'.'

   !------------------------
   ! Redefinition of phc constants

   ! This test case does not consider gravity nor Earth rotation
   phc%omega   = 0.0_wp
   phc%gravity = 0.0_wp
   ! Sea level reference pressure (from the state equation)
   phc%p_s     = phc%rgas
   phc%p_sf    = 1.0_wp/phc%p_s
   !------------------------

   mod_free_stream_test_initialized = .true.
 end subroutine mod_free_stream_test_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_free_stream_test_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_free_stream_test_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_free_stream_test_initialized = .false.
 end subroutine mod_free_stream_test_destructor

!-----------------------------------------------------------------------

  !> Viscosity
  pure function coeff_visc(x,rho,p,t) result(mu)
   real(wp), intent(in) :: x(:,:), rho(:), p(:), t(:)
   real(wp) :: mu(2,size(x,2))

    mu = nu
  end function coeff_visc

!-----------------------------------------------------------------------

 !> Uniform
 pure function coeff_init(x,u_r) result(u0)
  real(wp), intent(in) :: x(:,:), u_r(:,:)
  real(wp) :: u0(2+size(x,1)+ntrcs,size(x,2))

  u0(1,:) = 1.0_wp
  u0(2,:) = 2.0_wp
  !u0(2,:) = u0(2,:) + 0.1_wp*exp( -8.0_wp*(x(1,:)-0.5_wp)**2 &
  !                                -12.0_wp*(x(2,:)-0.5_wp)**2 )
  u0(3,:) = 0.25_wp
  u0(4:,:) = 0.0_wp

  u0(3,:) = u0(3,:) + 0.8_wp*x(2,:)*(1.0_wp-x(2,:))**3
  u0(4,:) = 0.4_wp*x(1,:)

 end function coeff_init
 
!-----------------------------------------------------------------------

 !> Use the initial condition.
 pure function coeff_outref(x,u_r) result(u0)
  real(wp), intent(in) :: x(:,:), u_r(:,:)
  real(wp) :: u0(2+size(x,1)+ntrcs,size(x,2))

   u0 = 0.0_wp

 end function coeff_outref

!-----------------------------------------------------------------------

 !> Use the initial condition.
 pure function coeff_dir(x,u_r,u_i,breg) result(ub)
  real(wp), intent(in) :: x(:,:)
  real(wp), intent(in), optional :: u_r(:,:), u_i(:,:)
  integer, intent(in), optional :: breg
  real(wp) :: ub(2+size(x,1)+ntrcs,size(x,2))

   ub = coeff_init(x,u_r)

 end function coeff_dir

!-----------------------------------------------------------------------

 pure function coeff_neu(x,u_r,breg) result(h)
  real(wp), intent(in) :: x(:,:)
  real(wp), intent(in), optional :: u_r(:,:)
  integer, intent(in), optional :: breg
  real(wp) :: h(size(x,1),1+size(x,1)+ntrcs,size(x,2))

   h = 0.0_wp

 end function coeff_neu

!-----------------------------------------------------------------------

end module mod_free_stream_test

