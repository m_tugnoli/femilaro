!> \brief
!! Flow past an obstacle with 1 axis of statistical symmetry (circular
!! or square cylinder, profile etc)
!!
!! \n
!!
!!
!!
!!
!<----------------------------------------------------------------------
module mod_onesymm_obst_test

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_fu_manager, only: &
   mod_fu_manager_initialized, &
   new_file_unit

 use mod_physical_constants, only: &
   mod_physical_constants_initialized, &
   t_phc

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_onesymm_obst_test_constructor, &
   mod_onesymm_obst_test_destructor,  &
   mod_onesymm_obst_test_initialized, &
   test_name,        &
   test_description, &
   phc,              &
   ntrcs,            &
   bar_mass_flow,    &
   ref_prof,         &
   coeff_dir,        &
   coeff_neu,        &
   coeff_sponge,     &
   coeff_visc,       &
   coeff_init,       &
   coeff_outref,     &
   coeff_f

 private

!-----------------------------------------------------------------------

 ! public members
 character(len=*), parameter   :: test_name = "onesymm_obst"
 character(len=100), protected :: test_description(3)
 type(t_phc), save, protected  :: phc
 integer, parameter            :: ntrcs = 0
 real(wp), parameter :: bar_mass_flow(3) = (/ 1.0_wp ,0.0_wp,0.0_wp /)
 character(len=*), parameter   :: ref_prof = "none"

 logical, protected :: mod_onesymm_obst_test_initialized = .false.

 ! private members
 real(wp), parameter :: &
   alpha = 0.7_wp,  & !< power low exponent for \f$\mu\f$
   pr    = 0.72_wp, & !< \f$Pr\f$
   gamma = 1.4_wp,  & !< \f$\gamma\f$
   ! coefficients of the forcing term
   alpha_p = 0.1_wp, &
   alpha_i = 0.5_wp
   ! the following variables are introduced for clarity
 real(wp), parameter :: &
   rho_r = 1.0_wp, &  !< \f$\rho_r\f$
   l_r   = 1.0_wp, &  !< \f$L_r\f$
   v_r   = 1.0_wp, &  !< \f$V_r\f$
   t_r   = 1.0_wp, &  !< \f$T_r\f$
   pi = 3.1415926535897932384626433832795029_wp, &
   box(3,2) = reshape(                                          &
     (/   0.0_wp  , -1.0_wp ,     0.0_wp       ,         &
        4.0_wp*pi ,  1.0_wp , 4.0_wp/3.0_wp*pi /),(/3,2/))
 real(wp) :: &
   re,     & !< \f$Re\f$
   ma,     & !< \f$Ma\f$
   re_tau, & !< \f$Re\f$
   delta0, & !< amplitude of the initial random perturbation
   x_max, &
   x_min, &
   x_sp_in, &
   x_sp_out
   

 ! Input file ----------
 character(len=*), parameter :: &
   test_input_file_name = 'onesymm_obstacle.in'

 character(len=*), parameter :: &
   this_mod_name = 'mod_onesymm_obst_test'

 ! Input namelist, to be read from turbulent_channel.in
 namelist /input/ &
   re, ma, re_tau, delta0, x_max, x_min, x_sp_in, x_sp_out

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_onesymm_obst_test_constructor(d)
  integer, intent(in) :: d

  integer :: fu, ierr
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized          .eqv..false.) .or. &
       (mod_kinds_initialized             .eqv..false.) .or. &
       (mod_fu_manager_initialized        .eqv..false.) .or. &
       (mod_physical_constants_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_onesymm_obst_test_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   ! Read input file
   call new_file_unit(fu,ierr)
   open(fu,file=trim(test_input_file_name), &
      status='old',action='read',form='formatted',iostat=ierr)
    if(ierr.ne.0) call error(this_sub_name,this_mod_name, &
      'Problems opening the input file')
    read(fu,input)
   close(fu,iostat=ierr)

   write(*,*) 'x_max, x_sp_out', x_max, x_sp_out
   write(test_description(1),'(a,i1)') &
     'onesymm_obst, dimension d = ',d
   write(test_description(2),'(a)') &  
     'viscosity given by power law '
   write(test_description(3),'(a)') &  
     'input velocity: incompressible parabolic profile'

   !------------------------
   ! Redefinition of phc constants

   ! This test case does not consider gravity nor Earth rotation
   phc%omega   = 0.0_wp
   phc%gravity = 0.0_wp
   ! Air as an ideal gas
   phc%rgas    = 1.0_wp/(gamma*ma**2)
   phc%cp      = 1.0_wp/((gamma-1.0_wp)*ma**2)
   phc%cv      = phc%cp - phc%rgas
   phc%gamma   = gamma
   phc%kappa   = phc%rgas/phc%cp
   ! Sea level reference pressure (from the state equation)
   phc%p_s     = phc%rgas
   phc%p_sf    = 1.0_wp/phc%p_s
   ! Reynolds number
   phc%re      = re
   phc%re_tau  = re_tau  
 !------------------------

   mod_onesymm_obst_test_initialized = .true.
 end subroutine mod_onesymm_obst_test_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_onesymm_obst_test_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_onesymm_obst_test_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_onesymm_obst_test_initialized = .false.
 end subroutine mod_onesymm_obst_test_destructor

!-----------------------------------------------------------------------

 !> Kinematic viscosity:
 !! \f$\nu=\frac{\mu_0}{\rho}T^\alpha=\frac{T^\alpha}{\rho\,Re}\f$
 pure function coeff_visc(x,rho,p,t) result(nu)
  real(wp), intent(in) :: x(:,:), rho(:), p(:), t(:)
  real(wp) :: nu(2,size(x,2))

  nu(2,:) = (t**alpha)/(rho*re)
  nu(1,:) = (1.0_wp/pr)*nu(2,:)

 end function coeff_visc

!-----------------------------------------------------------------------

 !> Perturbed logarithmic profile
 !!
 !! The perturbation is added to the velocity profile, while keeping
 !! the reference values for density and temperature.
 !!
 !! To reduce the divergence of the perturbed flow, we have \f$\delta
 !! U_{x_1}=\delta U_{x_1}(x_2)\f$, \f$\delta U_{x_2}=\delta
 !! U_{x_2}(x_3)\f$ and so on.
 pure function coeff_init(x,u_r) result(u0)
  real(wp), intent(in) :: x(:,:), u_r(:,:)
  real(wp) :: u0(2+size(x,1)+ntrcs,size(x,2))

  integer :: id, idp
  real(wp) :: xm, dx, xi(size(x,2))

   ! density: uniform profile
   u0(1,:) = rho_r
   ! velocity: uniform 0
   !u0(3:,:) = 0.0_wp
   ! velocity: nonuniform profile
   u0(3,:) = 1.0_wp
   u0(4,:) = 0.01_wp
   u0(5:,:) = 0.0_wp
   ! energy
   u0(2,:) = u0(1,:)*phc%cv*t_r + 0.5_wp*sum(u0(3:,:)**2,1)/u0(1,:)

   ! subtract the reference state
   u0(1,:) = u0(1,:) - u_r(1,:) ! rho
   u0(2,:) = u0(2,:) - u_r(2,:) ! e
 
 end function coeff_init

!-----------------------------------------------------------------------

 pure function coeff_outref(x,u_r) result(u0)
  real(wp), intent(in) :: x(:,:), u_r(:,:)
  real(wp) :: u0(2+size(x,1)+ntrcs,size(x,2))

   u0 = 0.0_wp

 end function coeff_outref

!-----------------------------------------------------------------------

 !> Most of the boundary condition is taken from the reference
 !! profile. However, we want to let the density fluctuate while
 !! keeping a constant temperature.
 pure function coeff_dir(x,u_r,u_i,breg) result(ub)
  real(wp), intent(in) :: x(:,:)
  real(wp), intent(in), optional :: u_r(:,:), u_i(:,:)
  integer, intent(in), optional :: breg
  real(wp) :: ub(2+size(x,1)+ntrcs,size(x,2))

   ub(1,:)  = rho_r
   ub(2,:)  = rho_r*phc%cv*t_r
   ub(3:,:) = 0.0_wp
   ub(3,:) = 1.0_wp

   ! stupid forced dirichlet everywhere
   if (present(breg)) then
     if( (breg.eq.5) ) then !cylinder wall
!~        ub(3,:) = 1.0_wp
       ub(3,:) = 0.0_wp
     endif
   endif

   if(present(u_i)) then ! fix T, leave generic rho
     ub(1,:) = u_i(1,:)
     ub(2,:) = ub(1,:)*phc%cv*t_r
   endif

   ! subtract the reference state
   ub(1,:) = ub(1,:) - u_r(1,:) ! rho
   ub(2,:) = ub(2,:) - u_r(2,:) ! e

 end function coeff_dir
 
 
 !-----------------------------------------------------------------------

 pure function coeff_neu(x,u_r,breg) result(h)
  real(wp), intent(in) :: x(:,:)
  real(wp), intent(in), optional :: u_r(:,:)
  integer, intent(in), optional :: breg
  real(wp) :: h(size(x,1),1+size(x,1)+ntrcs,size(x,2))

   h = 0.0_wp

 end function coeff_neu

!-----------------------------------------------------------------------

 !> Define the non-dimensional driving force vector.
 pure function coeff_f(x,mass_flow,imass_flow) result(f)
  real(wp), intent(in) :: x(:,:)
  real(wp), intent(in) :: mass_flow(:), imass_flow(:)
  real(wp) :: f(size(x,1),size(x,2))

   ! Driving force acts only in the x direction
  f = 0.0_wp

 end function coeff_f

!-----------------------------------------------------------------------

 pure subroutine coeff_sponge(tmask,tau,taup,x)
  real(wp), intent(in) :: x(:,:)
  logical, intent(out) :: tmask
  real(wp), intent(out) :: tau(:), taup(:)

  real(wp), parameter :: pi_trig = 3.141592653589793_wp, &
    big = huge(1.0_wp)
  integer :: l, i
  real(wp) :: z, zn, d_bnd, coeff
  
  real(wp) :: xx, x_n, yy

   ! initialization
   tau = 0.0_wp
   taup = 0.0_wp
   tmask = .false.

   do l=1,size(x,2)

     xx = x(1,l)
     yy = x(2,l)
     
     !outflow
     if(xx.gt.x_sp_out) then

       tmask = .true.

!TODO: review all this, I am using the expressions from Marco
       x_n = (xx-x_sp_out)/(x_max-x_sp_out)
       if(x_n.le.0.5_wp) then
         tau(l) = tau(l) + alpha/2.0_wp*(1.0_wp-cos(x_n*pi_trig))
         taup(l) = taup(l) + alpha/2.0_wp*(1.0_wp-cos(x_n*pi_trig))
       else
         tau(l) = tau(l) + alpha/2.0_wp*(1.0_wp+(x_n-0.5_wp)*pi_trig)
         taup(l) = taup(l) + alpha/2.0_wp*(1.0_wp+(x_n-0.5_wp)*pi_trig)
       endif

     endif
     
     !inflow
     if(xx.lt.x_sp_in) then

       tmask = .true.

       x_n = (xx-x_sp_in)/(x_min-x_sp_in)
       if(x_n.le.0.5_wp) then
         tau(l) = tau(l) + alpha/2.0_wp*(1.0_wp-cos(x_n*pi_trig))
         taup(l) = taup(l) + alpha/2.0_wp*(1.0_wp-cos(x_n*pi_trig))
       else
         tau(l) = tau(l) + alpha/2.0_wp*(1.0_wp+(x_n-0.5_wp)*pi_trig)
         taup(l) = taup(l) + alpha/2.0_wp*(1.0_wp+(x_n-0.5_wp)*pi_trig)
       endif

     endif


   enddo
   
 end subroutine coeff_sponge

!-----------------------------------------------------------------------

 !> Generate a chaotic field \f$\eta(\xi)\in[-1\,,\,1]\f$, with
 !! \f$\xi\in[-1\,,\,1]\f$.
 elemental function logistic_chaos(xi) result(eta)
  real(wp), intent(in) :: xi
  real(wp) :: eta

  integer,  parameter :: n = 20       !< number of iterations
  real(wp), parameter :: r = 3.999_wp !< must be < 4
  integer :: i

   ! eta is rescaled on [0,0.5] and the following iterations will
   ! generate values in [0,1].
   eta = 0.25_wp*(xi+1.0_wp)
   do i=1,n
     eta = r*eta*(1.0_wp-eta)
   enddo
   ! rescale eta on [-1,1]
   eta = 2.0_wp*(eta-0.5_wp)
 end function logistic_chaos

!-----------------------------------------------------------------------

end module mod_onesymm_obst_test
