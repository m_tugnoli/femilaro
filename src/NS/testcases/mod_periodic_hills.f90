!! Copyright (C) 2009,2010,2011,2012  Marco Restelli
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Marco Restelli                   <marco.restelli@gmail.com>

!>\brief
!!
!! Turbulent compressible flow over periodic hills.
!!
!! \n
!!
!! References for this test case can be found in various papers, with
!! various setups: 
!! <a
!! href="http://dx.doi.org/10.1016/j.compfluid.2008.05.002">[Breuer,
!! Peller, Rapp, Manhart, <em>Computers &amp; Fluids</em>, 2009]</a>,
!! <a
!! href="http://dx.doi.org/10.1017/S0022112004002812">[Fr&ouml;lich,
!! Mellen, Rodi, Temmerman, Leschziner, <em>J. Fluid Mech.</em>,
!! 2005]</a>.
!!
!! The overall structure of this module, including the dimensionless
!! formulation, are similar to \c mod_turb_channel_test. We recall
!! here some points.
!! <ul>
!!  <li> All the linear dimensions are assumed to be made
!!  dimensionless dividing by the hill height \f$H\f$, so that
!!  \f{displaymath}{
!!   H = 1, \qquad L_x = 9, \qquad L_y = 3.036, \qquad L_z=4.5
!!  \f}
!!  <li> The desired bulk velocity on the crest of the hill is 1,
!!  since this is the velocity chosen for the nondimensionalization.
!!  To enforce this in our computation, we need to translate this
!!  constraint in terms of the average bulk mass flow rate
!!  \f{displaymath}{
!!   {\tt mass\_flow} =
!!   \frac{1}{|\Omega|}\int_{\Omega}\underline{U}\,dx,
!!  \f}
!!  see \c mod_dgcomp_flowstate, since this is the quantity that is
!!  controlled by the forcing term, and provide the corresponding
!!  value as \c bar_mass_flow. On average, the mass flow along the
!!  \f$x\f$ direction is uniform in the domain, so we have
!!  \f{displaymath}{
!!   {\tt mass\_flow(1)} = \frac{L_x}{|\Omega|} \int_{y,z} U_x\,dy\,dz.
!!  \f}
!!  Evaluating the integral over the hill crest provides
!!  \f{displaymath}{
!!   {\tt mass\_flow(1)} = \frac{L_x}{|\Omega|} (L_y-1)L_z \rho^{\rm
!!   hill}u_x^{\rm hill}
!!  \f}
!!  and since by definition the bulk velocity over the hill crest is 1
!!  we arrive at
!!  \f{displaymath}{
!!   {\tt mass\_flow(1)} = \frac{L_x}{|\Omega|} (L_y-1)L_z \rho^{\rm
!!   hill}.
!!  \f}
!!  Determining \f$ \rho^{\rm hill}\f$ a priori is not possible;
!!  making the approximation that it is equal to the bulk density,
!!  which is 1 by definition, we finally arrive at
!!  \f{displaymath}{
!!   {\tt mass\_flow(1)} = \frac{L_x(L_y-1)L_z}{|\Omega|},
!!  \f}
!!  which is the value used in this module to define \c bar_mass_flow.
!!  <li> By defintion, the bulk temperature is 1, so that this is the
!!  value used as the initial, uniform temperature profile, as well as
!!  the prescribed temperature on the domain boundary.
!!  <li> Having prescribed the flow velocity and the temperature, the
!!  prescribed Mach number can be attained choosing the gas constants
!!  appropriately; this is done as discussed in \c
!!  mod_turb_channel_test.
!!  <li> The prescribed Reynolds number is enforced by choosing the
!!  viscosity coefficient appropriately; again, this is done as
!!  discussed in \c mod_turb_channel_test.
!!  <li> The forcing term has the following form:
!!  \f{displaymath}{
!!   f_x(t) = -\frac{\alpha_p}{\Sigma_x}(Q(t)-Q_0)
!!   -\frac{\alpha_i}{\Sigma_x}\int_0^t(Q(s)-Q_0)ds, \qquad \Sigma_x =
!!   \frac{|\Omega|}{L_x},
!!  \f}
!!  where \f$\alpha_p = t^{-1}\f$ and \f$\alpha_i = t^{-2}\f$.
!!  Substituting
!!  \f{displaymath}{
!!   Q(t) = \Sigma_x\,\,{\tt mass\_flow(1)}(t), \qquad
!!   Q_0 = \Sigma_x\,\,{\tt bar\_mass\_flow(1)}
!!  \f}
!!  yields the expression for the forcing term:
!!  \f{displaymath}{
!!   f_x(t) = -\alpha_p\left( {\tt mass\_flow(1)}(t) - {\tt
!!   bar\_mass\_flow(1)} \right)
!!   -\alpha_i\int_0^t\left( {\tt mass\_flow(1)}(s) - {\tt
!!   bar\_mass\_flow(1)} \right)ds.
!!  \f}
!! </ul>
!<----------------------------------------------------------------------
module mod_periodic_hills_test

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_fu_manager, only: &
   mod_fu_manager_initialized, &
   new_file_unit

 use mod_physical_constants, only: &
   mod_physical_constants_initialized, &
   t_phc

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_periodic_hills_test_constructor, &
   mod_periodic_hills_test_destructor,  &
   mod_periodic_hills_test_initialized, &
   test_name,        &
   test_description, &
   phc,              &
   ntrcs,            &
   bar_mass_flow,    &
   ref_prof,         &
   coeff_dir,        &
   coeff_visc,       &
   coeff_init,       &
   coeff_outref,     &
   coeff_f

 private

!-----------------------------------------------------------------------

 ! public members
 character(len=*), parameter   :: test_name = "periodic_hills"
 character(len=100), protected :: test_description(6)
 type(t_phc), save, protected  :: phc
 integer, parameter            :: ntrcs = 0

 real(wp), parameter :: &
   lx = 9.0_wp, ly = 3.036_wp, lz = 4.5_wp, &
   vol = 114.349847333724_wp ! volume, estimated from a fine mesh

 ! These parameters are introduced for clarity; however, they should
 ! not be changed because parts of the module assume that they are 1
 ! (see the module documentation).
 real(wp), parameter :: &
   rho_r = 1.0_wp, &
   t_r   = 1.0_wp

 real(wp), parameter :: &
   bar_mass_flow(3) = (/ lx*(ly-1.0_wp)*lz/vol ,0.0_wp,0.0_wp /)

 character(len=*), parameter   :: ref_prof = "none"

 logical, protected :: mod_periodic_hills_test_initialized = .false.

 ! private members
 real(wp), parameter :: &
   alpha = 0.7_wp,  & !< power low exponent for \f$\mu\f$
   pr    = 0.72_wp, & !< \f$Pr\f$
   gamma = 1.4_wp,  & !< \f$\gamma\f$
   ! coefficients of the forcing term
   alpha_p = 0.1_wp, &
   alpha_i = 0.5_wp

 real(wp), parameter :: &
   !> Simulation box.
   !!
   !! This is in fact the outer box, not taking into account the two
   !! hills. This ensures that the normalized coordinates used to
   !! define the initial condition are in \f$[-1\,,1]\f$.
   box(3,2) = reshape(                               &
     (/  0.0_wp  ,   0.0_wp   , -lz/2.0_wp ,         &
          lx     ,    ly      ,  lz/2.0_wp /),(/3,2/))

 real(wp) :: &
   re,     & !< \f$Re\f$
   ma,     & !< \f$Ma\f$
   re_tau, & !< \f$Re\f$
   delta0    !< amplitude of the initial random perturbation

 ! Input file ----------
 character(len=*), parameter :: &
   test_input_file_name = 'periodic_hills.in'

 character(len=*), parameter :: &
   this_mod_name = 'mod_periodic_hills_test'

 ! Input namelist, to be read from turbulent_channel.in
 namelist /input/ &
   re, ma, re_tau, delta0

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_periodic_hills_test_constructor(d)
  integer, intent(in) :: d

  integer :: fu, ierr
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized          .eqv..false.) .or. &
       (mod_kinds_initialized             .eqv..false.) .or. &
       (mod_fu_manager_initialized        .eqv..false.) .or. &
       (mod_physical_constants_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_periodic_hills_test_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   ! Read input file
   call new_file_unit(fu,ierr)
   open(fu,file=trim(test_input_file_name), &
      status='old',action='read',form='formatted',iostat=ierr)
    if(ierr.ne.0) call error(this_sub_name,this_mod_name, &
      'Problems opening the input file')
    read(fu,input)
   close(fu,iostat=ierr)

   write(test_description(1),'(a,i0)') &
     'Periodic hills, dimension d = ',d
   write(test_description(2),'(a,*(e14.5e3,:,","))') &  
     '  domain size: Lx, Ly, Lz, vol = ',(/ lx , ly , lz , vol /)
   write(test_description(3),'(a,*(e14.5e3,:,","))') &  
     '                 bar_mass_flow = ',bar_mass_flow
   write(test_description(4),'(a,e14.5e3)') &  
     '    power low viscosity, alpha = ',alpha
   write(test_description(5),'(a,*(e14.5e3,:,","))') &  
     '             Re, Ma, gamma, Pr = ',(/ re , ma , gamma , pr /)
   write(test_description(6),'(a,*(e14.5e3,:,","))') &  
     '     forcing: alpha_p, alpha_i = ',(/ alpha_p , alpha_i /)

   !------------------------
   ! Redefinition of phc constants

   ! This test case does not consider gravity nor Earth rotation
   phc%omega   = 0.0_wp
   phc%gravity = 0.0_wp
   ! Air as an ideal gas
   phc%rgas    = 1.0_wp/(gamma*ma**2)
   phc%cp      = 1.0_wp/((gamma-1.0_wp)*ma**2)
   phc%cv      = phc%cp - phc%rgas
   phc%gamma   = gamma
   phc%kappa   = phc%rgas/phc%cp
   ! Sea level reference pressure (from the state equation)
   phc%p_s     = phc%rgas
   phc%p_sf    = 1.0_wp/phc%p_s
   ! Reynolds number
   phc%re      = re
   phc%re_tau  = re_tau  
 !------------------------

   mod_periodic_hills_test_initialized = .true.
 end subroutine mod_periodic_hills_test_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_periodic_hills_test_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_periodic_hills_test_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_periodic_hills_test_initialized = .false.
 end subroutine mod_periodic_hills_test_destructor

!-----------------------------------------------------------------------

 !> Kinematic viscosity:
 !! \f$\nu=\frac{\mu_0}{\rho}T^\alpha=\frac{T^\alpha}{\rho\,Re}\f$
 pure function coeff_visc(x,rho,p,t) result(nu)
  real(wp), intent(in) :: x(:,:), rho(:), p(:), t(:)
  real(wp) :: nu(2,size(x,2))

   nu(2,:) = (t**alpha)/(rho*re)
   nu(1,:) = (1.0_wp/pr)*nu(2,:)

 end function coeff_visc

!-----------------------------------------------------------------------

 !> Perturbed logarithmic profile
 !!
 !! The perturbation is added to the velocity profile, while keeping
 !! the reference values for density and temperature.
 !!
 !! To reduce the divergence of the perturbed flow, we have \f$\delta
 !! U_{x_1}=\delta U_{x_1}(x_2)\f$, \f$\delta U_{x_2}=\delta
 !! U_{x_2}(x_3)\f$ and so on.
 pure function coeff_init(x,u_r) result(u0)
  real(wp), intent(in) :: x(:,:), u_r(:,:)
  real(wp) :: u0(2+size(x,1)+ntrcs,size(x,2))

  integer :: id, idp
  real(wp) :: xm, dx, xi(size(x,2))

   ! density: uniform profile
   u0(1,:) = rho_r
   ! velocity: log profile with perturbations
   do id=1,size(x,1)
     idp = mod(id,size(x,1))+1
     xm = 0.5_wp*sum(box(idp,:))
     dx = box(idp,2)-box(idp,1)
     xi=(x(idp,:)-xm)*(2.0_wp/dx)
     u0(2+id,:) = delta0 * logistic_chaos(xi)
   enddo
   xi = (x(2,:)-0.5_wp*sum(box(2,:)))*(2.0_wp/(box(2,2)-box(2,1)))
   u0(3,:) = u0(3,:) + uu_par(xi)
   ! energy
   u0(2,:) = u0(1,:)*phc%cv*t_r + 0.5_wp*sum(u0(3:,:)**2,1)/u0(1,:)

   ! subtract the reference state
   u0(1,:) = u0(1,:) - u_r(1,:) ! rho
   u0(2,:) = u0(2,:) - u_r(2,:) ! e
 
 end function coeff_init

!-----------------------------------------------------------------------

 !> Velocity parabolic profile for \f$y\in[-1\,,\,1]\f$
 elemental function uu_par(y) result(uu)
  real(wp), intent(in) :: y
  real(wp) :: uu

   uu = 1.5_wp*(1.0_wp-y**2)
       
 end function uu_par

!-----------------------------------------------------------------------

 pure function coeff_outref(x,u_r) result(u0)
  real(wp), intent(in) :: x(:,:), u_r(:,:)
  real(wp) :: u0(2+size(x,1)+ntrcs,size(x,2))

   u0 = 0.0_wp

 end function coeff_outref

!-----------------------------------------------------------------------

 !> Most of the boundary condition is taken from the reference
 !! profile. However, we want to let the density fluctuate while
 !! keeping a constant temperature.
 pure function coeff_dir(x,u_r,u_i,breg) result(ub)
  real(wp), intent(in) :: x(:,:)
  real(wp), intent(in), optional :: u_r(:,:), u_i(:,:)
  integer, intent(in), optional :: breg
  real(wp) :: ub(2+size(x,1)+ntrcs,size(x,2))

   ub(1,:)  = rho_r
   ub(2,:)  = rho_r*phc%cv*t_r
   ub(3:,:) = 0.0_wp

   if(present(u_i)) then ! fix T, leave generic rho
     ub(1,:) = u_i(1,:)
     ub(2,:) = ub(1,:)*phc%cv*t_r
   endif

   ! subtract the reference state
   ub(1,:) = ub(1,:) - u_r(1,:) ! rho
   ub(2,:) = ub(2,:) - u_r(2,:) ! e

 end function coeff_dir

!-----------------------------------------------------------------------

 !> Define the non-dimensional driving force vector.
 pure function coeff_f(x,mass_flow,imass_flow) result(f)
  real(wp), intent(in) :: x(:,:)
  real(wp), intent(in) :: mass_flow(:), imass_flow(:)
  real(wp) :: f(size(x,1),size(x,2))

   ! Driving force acts only in the x direction
   f(1,:)  = - alpha_p*(mass_flow(1)-bar_mass_flow(1)) &
             - alpha_i*imass_flow(1)
   f(2:,:) = 0.0_wp

 end function coeff_f
 
!-----------------------------------------------------------------------

 !> Generate a chaotic field \f$\eta(\xi)\in[-1\,,\,1]\f$, with
 !! \f$\xi\in[-1\,,\,1]\f$.
 elemental function logistic_chaos(xi) result(eta)
  real(wp), intent(in) :: xi
  real(wp) :: eta

  integer,  parameter :: n = 20       !< number of iterations
  real(wp), parameter :: r = 3.999_wp !< must be < 4
  integer :: i

   ! eta is rescaled on [0,0.5] and the following iterations will
   ! generate values in [0,1].
   eta = 0.25_wp*(xi+1.0_wp)
   do i=1,n
     eta = r*eta*(1.0_wp-eta)
   enddo
   ! rescale eta on [-1,1]
   eta = 2.0_wp*(eta-0.5_wp)

 end function logistic_chaos

!-----------------------------------------------------------------------

end module mod_periodic_hills_test

