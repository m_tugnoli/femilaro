! This code compiles but produces as error at runtime.
! gfortran alloc-comps.f90 -o bug
! ./bug
!*** glibc detected *** ./bug: double free or corruption (fasttop):
!0x000000000060a5b0 ***
!
! The bug is reported here:
! http://gcc.gnu.org/bugzilla/show_bug.cgi?id=40850
!
! Last verified: 2009/11/05
module mod_all

 implicit none

 public :: &
   t2, new2, operator(+)

 private

 type t1
   integer :: n1
   integer, allocatable :: d1(:)
 end type t1

 type t2
   integer :: n2
   type(t1), allocatable :: d2(:)
 end type t2

 interface operator(+)
   module procedure add
 end interface

 interface new2
   module procedure new2, new2_2
 end interface 

contains

!-----------------------------------------------------------------------

 pure function new1(d1) result(m)
  integer, intent(in) :: d1(:)
  type(t1) :: m

   m%n1 = size(d1)
   allocate(m%d1(m%n1))
   m%d1 = d1

 end function new1

!-----------------------------------------------------------------------

 pure function new2(d2) result(p)
  type(t1), intent(in) :: d2(:)
  type(t2) :: p

   p%n2 = size(d2)
   allocate(p%d2(p%n2))
   p%d2 = d2

 end function new2

!-----------------------------------------------------------------------

 pure function new2_2() result(p)
  type(t2) :: p

   p = new2( (/ new1((/1,1/)) /) )
 end function new2_2

!-----------------------------------------------------------------------

 elemental function add(p1,p2) result(p)
  type(t2), intent(in) :: p1,p2
  type(t2) :: p

   p = new2((/ p1%d2 , p2%d2 /))

 end function add

!-----------------------------------------------------------------------

end module mod_all

!-----------------------------------------------------------------------

program a_main

 use mod_all

 type(t2) :: q(3)

  q(1) = new2()
  q(2) = new2()

  q(3) = q(2) + q(1)

end program a_main

