! See bug 42112
!
!The attached code produces an error at runtime, however it seems fine
!to me. Notice that there is no error accessing the function with the
!specific name "g", while there is an error when using the generic name
!"gen_g".
!
!gfortran --version
!GNU Fortran (GCC) 4.5.0 20091105 (experimental)
!
!gfortran ./abc.f90 -o abc
!
!./abc 
!At line 23 of file ./abc.f90
!Fortran runtime error: Attempting to allocate already allocated array 'j'


module mod_m
 implicit none
 public :: f
 private

 interface gen_g
   module procedure g
 end interface

contains

 pure function f() result(i)
  integer :: i
  integer, allocatable :: loc_ar(:)
   allocate(loc_ar(1))
   loc_ar = gen_g() ! does not work
   !loc_ar = g() ! no problem here
   deallocate(loc_ar)
 end function f

 pure function g() result(j)
  integer, allocatable :: j(:)
   allocate( j(1) )
   j = 2
 end function g

end module mod_m

!--------------------------

program abc_main

 use mod_m, only: f
 implicit none
 integer :: p
  p = f()

end program abc_main

