!Hi all,
!   gfortran refuses the attached code as
!
!pure function i_g(x,p) result(y)
!                     1
!Error: Dummy procedure 'p' of PURE procedure at (1) must also be PURE
!
!Notice however that i_f is an abstract interface to a PURE function.
!
!This problem is discussed here
! http://gcc.gnu.org/bugzilla/show_bug.cgi?id=45366


module m1
 implicit none
 abstract interface
  pure function i_f(x) result(y)
   real, intent(in) :: x
   real :: y
  end function i_f
 end interface
end module m1

module m2
 use m1, only: i_f
 implicit none
contains
 pure function i_g(x,p) result(y)
  real, intent(in) :: x
  procedure(i_f), pointer, intent(in) :: p
  real :: y
   y = p(x)
 end function i_g
end module m2

