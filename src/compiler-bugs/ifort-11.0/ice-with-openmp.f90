! Bug name: ifort-omp-ice-with-interface

! ICE when compiled with
!   ifort -c -openmp ice.f90
! Compiler version:
! Intel(R) Fortran Intel(R) 64 Compiler Professional for applications
! running on Intel(R) 64, Version 11.0    Build 20090131 Package ID:
! l_cprof_p_11.0.081

! This problem is solved in the 11.1 series.

module mod_error_norms
 implicit none
 private
contains
 
 subroutine hybrid_err()

   !$omp parallel

   !$omp end parallel
 
 end subroutine hybrid_err
 
 subroutine primal_err(uuu)
  interface
    pure function uuu(xy) result(u)
     real, intent(in) :: xy(:)
     real :: u(size(xy))
    end function uuu
  end interface

 end subroutine primal_err

end module mod_error_norms

