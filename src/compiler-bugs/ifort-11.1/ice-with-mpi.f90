! This is a problem with ifort-11.1 and OpenMP which essentially makes
! this compiler is useless for the OpenMP version of the code.
!
! The bug has been reported here:
! http://software.intel.com/en-us/forums/intel-fortran-compiler-for-linux-and-mac-os-x/topic/66888/
!
! This bug affects the version 11.1.038, 11.1.046, 11.1.056 and
! 11.1.059 11.1.064
! The bug is SOLVED in th version 11.1.072

! ifort -openmp -c ice-with-mpi.f90 
!: catastrophic error: **Internal compiler error: segmentation
!violation signal raised** Please report this error along with the
!circumstances in which it occurred in a Software Problem Report.
!Note: File and line given may not be explicit cause of this error.

module my_mod

implicit none
private

contains

subroutine sub( cck )
real, intent(out), allocatable :: cck(:)

!$omp parallel private(cck)
!$omp end parallel

end subroutine sub

end module my_mod
