! When compiling the attached code with ifort I get:
!
! import-problem.f90(31): error #6484: Global entities may not be used as IMPORT-name entities   [I_F]
!    import :: i_f
! -------------^
! import-problem.f90(34): error #8169: The specified interface is not declared.   [I_F]
!    procedure(i_f), pointer, intent(in) :: p
! -------------^
! compilation aborted for import-problem.f90 (code 1)
!
! ifort -V
! Intel(R) Fortran Intel(R) 64 Compiler Professional for applications running on Intel(R) 64, Version 11.1    Build 20100414 Package ID: l_cprof_p_11.1.072
! Copyright (C) 1985-2010 Intel Corporation.  All rights reserved.
!
! This problem is reported here
! http://software.intel.com/en-us/forums/showthread.php?t=76869&o=d&s=lr


module m1
 implicit none
 abstract interface
  function i_f(x) result(y)
   real, intent(in) :: x
   real :: y
  end function i_f
 end interface
end module m1

module m2
 use m1, only: i_f
 implicit none
 abstract interface
  function i_g(x,p) result(y)
   import :: i_f ! does not work
   !use m1, only: i_f ! works fine
   real, intent(in) :: x
   procedure(i_f), pointer, intent(in) :: p
   real :: y
  end function i_g
 end interface
end module m2

