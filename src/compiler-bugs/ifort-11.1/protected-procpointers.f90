! When compiling the attached code with ifort I get:
!
! error #8171: This is not a valid attribute for the procedure declaration statement.   [PROTECTED]
!  procedure(i_f), pointer, protected :: p_f => null()
! --------------------------^
! compilation aborted for protected-procpointers.f90 (code 1)
!
! However, from the standard I see
!
!   C535 (R501) The PROTECTED attribute is permitted only for a
!   procedure pointer or named variable that is not in a common block.
!
! so I think that the code is fine.
!
! ifort -V
! Intel(R) Fortran Intel(R) 64 Compiler Professional for applications running on Intel(R) 64, Version 11.1    Build 20100414 Package ID: l_cprof_p_11.1.072
! Copyright (C) 1985-2010 Intel Corporation.  All rights reserved.
!
! This bug has been reported here
! http://software.intel.com/en-us/forums/showthread.php?t=74892&o=d&s=lr
! and the ID is DPD200156432.

module m
 implicit none
 abstract interface
  pure function i_f(x) result(y)
   real, intent(in) :: x
   real :: y
  end function i_f
 end interface
 !procedure(i_f), pointer :: p_f => null() ! this works
 procedure(i_f), pointer, protected :: p_f => null()
end module m
