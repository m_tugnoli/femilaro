!! Copyright (C) 2009,2010,2011,2012  Marco Restelli
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Marco Restelli                   <marco.restelli@gmail.com>

!>\brief
!! Simple representation of a 2D hybrid grid with triangles,
!! quadrilaterals and possibly arbitrary polygons.
!!
!! \details
!!
!! This module is rather simple because in 2D the element sides are
!! segments, regardless whether the elements are triangles or
!! quadrilaterals or generic polygons.
!!
!! Most of the data structures are inspired by those defined in \c
!! mod_grid.
!!
!! \section h2d_g_local_orient Local ordering of the element vertexes
!!
!! The input structure \c t of \c new_h2d_base_grid specifies a local
!! ordering for the vertices of each element. For a given element,
!! many possible orderings can be defined, and from the viewpoint of
!! this module some of them are equivalent while others are not.
!!
!! The essential point here is the following: <em>sides are defined by
!! each pair of two successive vertexes in local ordering</em>.
!!
!! Thus, all the local orderings which define the correct sides are
!! acceptable, while local orderings defining incorrect sides are not
!! allowed. For simplexes (triangles, in 2D), any local ordering is
!! admissible, since all the possible collections of \f$d\f$ vertices
!! of a \f$d\f$-dimensional simplex corresponds to sides of the
!! simplex itself, so that it is not possible to define spurious sides
!! (see \c mod_grid for more details). For general polygons, however,
!! not all the pairs of vertices define a side. An example is shown in
!! the following figure.
!!
!! \image html pentagon-element.png
!! \image latex pentagon-element.png "" width=0.3\textwidth
!!
!! Admissible orderings are, for instance, \f$ [ 3 \,, 45 \,,  8 \,,
!! 21 \,, 33 ] \f$, \f$ [ 8 \,, 21 \,, 33 \,, 3 \,, 45 ] \f$, \f$ [ 3
!! \,, 33 \,, 21 \,, 8 \,, 45 ] \f$, while examples of non admissible
!! orderings are \f$ [ 3 \,, 45 \,,  8 \,, 33 \,, 21 ] \f$ (this would
!! define the non existent sides \f$(v_8,v_{33})\f$ and
!! \f$(v_{21},v_{3})\f$) and \f$ [ 8 \,, 21 \,, 45 \,, 3 \,, 33 ] \f$
!! (this would define the non existent sides \f$(v_{21},v_{45})\f$ and
!! \f$(v_{33},v_{8})\f$).
!!
!! \note That no assumptions are made concerning the local
!! orientation: both clockwise and counterclockwise orientations are
!! possible.
!!
!! \note There is no general assumption concerning the
!! <em>ordering</em> of the sides: for instance, in the above figure,
!! we should not assume any relation between an admissible ordering of
!! the element vertices and the local position of the side
!! \f$(v_8,v_{21})\f$. Sides are generated from the element vertices
!! in \c mod_h2d_master_el, during the creation of the master
!! elements, and any further reference should happen through
!! \field_fref{mod_h2d_master_el,c_h2d_me_base,snodes}.
!!
!! \section connection_master_element Dealing with the master elements
!!
!! Each element refers to two master element objects: one for the
!! geometry and one for the definition of the basis functions; all the
!! details are discussed in \c mod_h2d_master_el.
!!
!! The main reason why the master element affects the definition of
!! the grid connectivity is that the element sides, as specified in
!! the previous section \ref h2d_g_local_orient, are defined in
!! \field_fref{mod_h2d_master_el,c_h2d_me_base,snodes}, i.e. in the
!! geometry master element, so that we need this object to build the
!! grid connectivity. On the other hand, in order to know which
!! \fref{mod_h2d_master_el,c_h2d_me_base} objects are required, one
!! needs to know the grid. For this reason, the construction of the
!! base, collecting the master elements, and of the grid are done
!! together in this module.
!!
!! \section h2d_grid_ddc Domain decomposition
!!
!! The grid partitioning is completely described in terms of abstract
!! entities like neighbouring/global vertexes, sides and elements, and
!! does not require any special casing for the hybrid grid. So, we use
!! the same type defined in \c mod_grid. However, for consistency, we
!! rename it here as \c t_ddc_h2d_grid. Hopefully, this distinction
!! can vanish once this module and \c mod_grid can be integrated.
!!
!! \note Even if we use here \c mod_grid, the constructor of such
!! module should not be called and \c mod_grid should be completely
!! hidden by the user code.
!!
!! The subroutine \c compute_ddc is almost a copy-and-paste of
!! \fref{mod_grid,compute_ddc}; unfortunately using the same function
!! is complicated because some types differ, however this duplication
!! should also be resolved once a unified grid becomes available.
!<----------------------------------------------------------------------
module mod_h2d_grid

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_fu_manager, only: &
   mod_fu_manager_initialized, &
   new_file_unit

 use mod_mpi_utils, only: &
   mod_mpi_utils_initialized, &
   mpi_integer,                  &
   mpi_max,                      &
   mpi_status_size,              &
   mpi_comm_size, mpi_comm_rank, &
   mpi_request_null,             &
   mpi_irecv, mpi_isend,         &
   mpi_bcast,                    &
   mpi_wait, mpi_waitall,        &
   mpi_allreduce,                &
   mpi_alltoall, mpi_alltoallv

 use mod_octave_io, only: &
   mod_octave_io_initialized, &
   write_octave,   &
   read_octave,    &
   read_octave_al, &
   locate_var

 use mod_perms, only: &
   mod_perms_constructor, &
   mod_perms_destructor,  &
   mod_perms_initialized, &
   t_perm, t_dperm, &
   operator(.eq.),  &
   operator(.ne.),  &
   operator(.lt.),  &
   operator(.le.),  &
   operator(.gt.),  &
   operator(.ge.),  &
   operator(*),     &
   operator(**),    &
   perm_table,      &
   idx,             &
   dperm_reduce,    &
   fact

 use mod_linal, only: &
   mod_linal_initialized, &
   qr, invmat

 use mod_h2d_master_el, only: &
   mod_h2d_master_el_initialized, &
   c_h2d_me_geom, h2d_me_geom_init, &
   t_elem_spec

 use mod_h2d_base, only: &
   mod_h2d_base_initialized, &
   t_h2d_base, new_base
 
 ! Only for internal use
 use mod_grid, only: &
   t_ddc_h2d_grid => t_ddc_grid, &
   t_grid, t_ddc_gv, t_ddc_nv, &
   compute_ddc_simple, clear, write_octave, read_octave_al

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_h2d_grid_constructor, &
   mod_h2d_grid_destructor,  &
   mod_h2d_grid_initialized, &
   t_2dv, t_2ds, t_2de, &
   t_el_collection, &
   t_h2d_grid, new_base_and_grid, clear, &
   t_ddc_h2d_grid, &
   write_octave, read_octave_al, &
   ! Only for testing ----
   mod_h2d_grid_constructor_test

 private

!-----------------------------------------------------------------------

! Module types and parameters

 ! pointer arrays
 type :: p_t_2dv
   type(t_2dv), pointer :: p => null()
 end type p_t_2dv
 type :: p_t_2ds
   type(t_2ds), pointer :: p => null()
 end type p_t_2ds
 type :: p_t_2de
   type(t_2de), pointer :: p => null()
 end type p_t_2de

 !> vertex
 type :: t_2dv
  integer :: i                       !< vertex index
  real(wp) :: x(2)                   !< coordinates
  !> connected sides (edges)
  integer :: ns                      !< # connected sides
  integer, allocatable :: is(:)      !< ind. connected sides
  type(p_t_2ds), allocatable :: s(:) !< connected sides
  !> connected elements
  integer :: ne                      !< # connected elements
  integer, allocatable :: ie(:)      !< ind. connected elements
  type(p_t_2de), allocatable :: e(:) !< connected elements
 end type t_2dv

 !> sides
 type :: t_2ds
  integer :: i                       !< side index
  integer :: iv(2)                   !< ind. vertices
  type(p_t_2dv) :: v(2)              !< vertices
  !> connected elements
  integer :: ie(2)                   !< ind. elements
  type(p_t_2de) :: e(2)              !< elements
  integer :: isl(2)                  !< local indx. on e
  !> geometry
  real(wp) :: xb(2)                  !< barycenter
  real(wp) :: a                      !< surface (length)
 end type t_2ds

 !> elements
 !!
 !! There is no need to declare different types for elements with a
 !! different number of vertices; if a specific master element is
 !! required, a pointer can be included.
 type :: t_2de
  integer :: i                       !< element index
  integer :: poly                    !< number of vertices/sides
  integer, allocatable :: iv(:)      !< ind. vertices
  type(p_t_2dv), allocatable :: v(:) !< vertices      (poly)
  !> connected sides
  integer, allocatable :: is(:)      !< ind. sides    (poly)
  type(p_t_2ds), allocatable :: s(:) !< sides         (poly)
  integer, allocatable :: pi(:)      !< el2side perm. (poly)
  integer, allocatable :: ip(:)      !< side2el perm. (poly)
  !> connected elements
  integer, allocatable :: ie(:)      !< ind. neigh. elements (poly)
  type(p_t_2de), allocatable :: e(:) !< neighboring elements (poly)
  integer, allocatable :: iel(:)     !< local indx. on neigs (poly)
  !> geometry
  real(wp) :: xb(2)                  !< barycenter
  real(wp) :: vol                    !< volume
  real(wp), allocatable :: n(:,:)    !< normals
  !> master element (geometry)
  class(c_h2d_me_geom), allocatable :: me
 end type t_2de

 !> grid
 !!
 !! \note All the variables of type \c t_h2d_grid should have the
 !! TARGET attribute, so that each element can point to the
 !! permutation table and also all the internal pointers can be set.
 type :: t_h2d_grid
  integer :: ne !< \# elements
  integer :: nv !< \# vertices
  integer :: ns !< ns = ni+nb
  integer :: nb !< \# boundary sides
  integer :: ni !< \# internal sides
  type(t_2dv), allocatable :: v(:) !< vertices
  type(t_2ds), allocatable :: s(:) !< sides
  type(t_2de), allocatable :: e(:) !< elements
  !> geometry
  real(wp) :: vol !< total volume
 end type t_h2d_grid

 !> element collection
 !!
 !! This type is used to group elements with different number of nodes
 !! in the grid connectivity.
 type :: t_el_collection
  integer, allocatable :: ie(:)   !< element indexes
  integer, allocatable :: it(:,:) !< element connectivity
 end type t_el_collection

 ! private members

! Module variables

 ! public members
 logical, protected ::               &
   mod_h2d_grid_initialized = .false.

 ! private members
 character(len=*), parameter :: &
   this_mod_name = 'mod_h2d_grid'

 interface new_base_and_grid
   module procedure new_h2d_base_grid, new_h2d_base_grid_file
 end interface

 interface clear
   module procedure clear_h2d_grid
 end interface

 interface write_octave
   module procedure write_h2d_grid_struct, write_h2d_vert_struct, &
                    write_h2d_side_struct, write_h2d_elem_struct
 end interface write_octave

 interface read_octave_al
   module procedure read_al_el_collection
 end interface read_octave_al
 
!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_h2d_grid_constructor()
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false.) .or. &
          (mod_kinds_initialized.eqv..false.) .or. &
     (mod_fu_manager_initialized.eqv..false.) .or. &
      (mod_mpi_utils_initialized.eqv..false.) .or. &
      (mod_octave_io_initialized.eqv..false.) .or. &
          (mod_perms_initialized.eqv..false.) .or. &
          (mod_linal_initialized.eqv..false.) .or. &
  (mod_h2d_master_el_initialized.eqv..false.) .or. &
       (mod_h2d_base_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_h2d_grid_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   mod_h2d_grid_initialized = .true.
 end subroutine mod_h2d_grid_constructor

 subroutine mod_h2d_grid_constructor_test()
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false.) .or. &
          (mod_kinds_initialized.eqv..false.) .or. &
   ! not needed if we avoid write -> mod_fu_manager_initialized
      (mod_mpi_utils_initialized.eqv..false.) .or. &
   ! not needed if we avoid write -> mod_octave_io_initialized
          (mod_perms_initialized.eqv..false.) .or. &
          (mod_linal_initialized.eqv..false.) .or. &
  (mod_h2d_master_el_initialized.eqv..false.) .or. &
       (mod_h2d_base_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_h2d_grid_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   mod_h2d_grid_initialized = .true.
 end subroutine mod_h2d_grid_constructor_test

!-----------------------------------------------------------------------
 
 subroutine mod_h2d_grid_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_h2d_grid_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_h2d_grid_initialized = .false.
 end subroutine mod_h2d_grid_destructor

!-----------------------------------------------------------------------

 !> Build a \c t_grid object reading data from file.
 !!
 !! Most of this subroutine follows \fref{mod_grid,new_grid_file}.
 subroutine new_h2d_base_grid_file( base,grid , &
      grid_file_name,elem_spec , ddc_grid, comm )
  type(t_h2d_base),         intent(out) :: base
  type(t_h2d_grid), target, intent(out) :: grid
  character(len=*),         intent(in)  :: grid_file_name
  type(t_elem_spec),        intent(inout) :: elem_spec
  type(t_ddc_h2d_grid),     intent(out), optional :: ddc_grid
  integer,                  intent(in),  optional :: comm

  integer :: d, ierr, fu
  integer, allocatable :: e(:,:)
  type(t_el_collection), allocatable :: t(:)
  real(wp), allocatable :: p(:,:)
  character(len=*), parameter :: &
    this_sub_name = 'new_h2d_base_grid_file'
  character(len=1000+len_trim(grid_file_name)) :: message(2)

   ! read the mesh file
   call new_file_unit(fu,ierr)
   open(fu,file=trim(grid_file_name), &
        iostat=ierr,status='old',action='read')
    if(ierr.ne.0) then
      write(message(1),'(a)') 'Problems opening the grid file'
      write(message(2),'(a,a,a)') '  "',trim(grid_file_name),'"'
      call error(this_sub_name,this_mod_name,message(1:2))
    endif
     call read_octave(   d,'d',fu)
     call read_octave_al(p,'p',fu) ! allocate( p )
     call read_octave_al(e,'e',fu) ! allocate( e )
     call read_octave_al(t,'t',fu) ! allocate( t )
   close(fu,iostat=ierr)

   ! validate the inpute
   if(d.ne.2) call error(this_sub_name,this_mod_name, &
     'Only 2D grids are supported.')
   if(size(p,1).lt.2) call error(this_sub_name,this_mod_name, &
     '"p" must have (at least) two rows.')
   if(size(p,1).gt.2) p = p(1:2,:) ! reallocate p

   ! build the mesh
   call new_base_and_grid( base,grid, p, e, t, elem_spec , &
                           ddc_grid, grid_file_name, comm )

   deallocate( p , e , t )

 end subroutine new_h2d_base_grid_file
 
!-----------------------------------------------------------------------

 !> Build a \c t_h2d_grid object.
 !!
 !! Most of this subroutine follows \fref{mod_grid,new_grid}. In
 !! particular, a triangular grid can be read by both subroutines.
 !!
 !! Elements with an arbitrary number of vertices do not require any
 !! change in \c p and \c e compared to the triangular case, while
 !! some care must be taken with \c t. To minimize the required
 !! storage, allowing any numbering of the elements, we assume that \c
 !! t is an array of \c t_el_collection objects, each of which
 !! specifies an arbitrary number of elements, all with the same
 !! shape, with an arbitrary order.
 !!
 !! \note The present implementation does not consider elements with
 !! less than three vertexes; this notion however could be useful (at
 !! least in principle) to treat lower dimensional problems.
 !!
 !! \todo It could be useful to reshuffle the allocations so that
 !! allocations of the output variable are not interleaved with those
 !! of the working arrays.
 subroutine new_h2d_base_grid( base,grid, p, e, t, elem_spec , &
                               ddc_grid, grid_file_name, comm )
  type(t_h2d_base), intent(out) :: base
  type(t_h2d_grid), target, intent(out) :: grid
  type(t_el_collection), intent(in) :: t(:)
  integer,  intent(in) :: e(:,:)
  real(wp), intent(in) :: p(:,:)
  !> See the comments in \fref{mod_h2d_base,new_base} for this input
  !! specification.
  type(t_elem_spec), intent(inout) :: elem_spec
  type(t_ddc_h2d_grid), intent(out), optional :: ddc_grid
  character(len=*), intent(in),  optional :: grid_file_name
  integer,          intent(in),  optional :: comm

  type t_side_list !< type for the side linked list
   integer :: iv(2,2)    !< vertices (before reordering)
   type(t_dperm) :: v(2) !< vertices (after reordering)
   integer :: ie(2)      !< elements
   integer :: isl(2)     !< local side indexes
   type(t_side_list), pointer :: next => null()
  end type t_side_list
  type t_e2side !< from element to one side in the side list
   integer, pointer :: ie2   !< opposite element (or boundary marker)
   integer, pointer :: isl2  !< local position on ie2
   integer, pointer :: iv(:) !< vertices (before reordering)
   type(t_perm), pointer :: pi !< permutation to reduced form
  end type t_e2side
  type t_e2sides
   !> each element can have an arbitrary number of sides
   type(t_e2side), allocatable :: s(:)
  end type t_e2sides
  type t_v2se_list !< list for sides/elements connected to a vertex
   integer :: nse = 0
   integer :: ise
   type(t_v2se_list), pointer :: next => null()
  end type t_v2se_list

  logical, allocatable :: required_shapes(:)
  integer :: i, j, ine, iv, ivl, ie, iie, ie2, is, isl, isl2, isb
  integer :: nt, ddc_counter, ddc_marker, rqs1, rqs2, ierr, fu, &
    sendb(1), recvb(1) ! buffers must be arrays
  type(t_dperm) :: side_list_tail
  type(t_side_list), allocatable, target :: side_list(:)
  type(t_e2sides), allocatable :: e2side_list(:)
  type(t_v2se_list), allocatable, target :: v2s_list(:), v2e_list(:)
  type(t_ddc_nv), allocatable :: ivl2ivn(:)
  type(t_grid) :: tmp_grid

  character(len=10000) :: message(5)
  character(len=*), parameter :: &
    this_sub_name = 'new_h2d_base_grid'


   !0) define some simple grid parameters
   nt = size(t)
   grid%ne = sum( (/( size(t(i)%it,2), i=1,nt )/) )
   grid%nv = size(p,2)

   ! a consistency check
   if( any( (/(size(t(i)%ie) .ne. size(t(i)%it,2) ,i=1,nt)/) ) ) &
     call error(this_sub_name,this_mod_name,                          &
       'The dimension of (at least) one "ie" field is not consistent' &
       //' with that of the corresponding "it" field.')


   ! 1) define the base
   rqs1 = minval( (/( size(t(i)%it,1), i=1,nt )/) )
   rqs2 = maxval( (/( size(t(i)%it,1), i=1,nt )/) )
   allocate( required_shapes( rqs1 : rqs2 ) )
   required_shapes = .false.
   required_shapes( (/( size(t(i)%it,1), i=1,nt )/) ) = .true.
   call new_base(base,required_shapes,rqs1,rqs2,elem_spec)
   deallocate( required_shapes )


   ! 2) build the side list (lexicographic order)
   allocate(side_list(grid%nv))
   ! terminate the lists with grid%nv+1, larger than any iv
   side_list_tail = dperm_reduce( (grid%nv+1)*(/1,1/) )
   do iv=1,grid%nv
     allocate(side_list(iv)%next)
     side_list(iv)%next%v(1) = side_list_tail
   enddo
   ! allocate the array used to read the side_list
   allocate(e2side_list(grid%ne))
   do i=1,nt
     ine = size(t(i)%it,1)
     do ie=1,size(t(i)%ie)
       allocate( e2side_list(t(i)%ie(ie))%s(ine) )
     enddo
   enddo
   ! build the list: while doing this count the domain decomposition
   ! sides as follows
   ! a) each time a new side is created, consider it as ddc
   ! b) each time a second element is inserted eliminate one ddc
   ! c) for each entry in e, eliminate one ddc
   ! d) count the remaining ddcs
   ddc_counter = 0
   ! At this point we don't know whether there are ddc sides or not,
   ! but we already need the corresponding marker to initialize the
   ! sides (see insert_side_list). We start considering the following
   ! local value:
   if(size(e,2).gt.0) then ! the boundary could be empty
     ddc_marker = maxval(e(5,:)) + 1
   else
     ddc_marker = 1
   endif
   ! The marker is obtained as the first free integer following 
   ! the maximum integer used to denote the domain faces. If
   ! we have 6 faces with 6 different indexes, ddc_marker will be
   ! equal to 7.
   ! Now, we have the following cases:
   ! - if ddc_grid is not present
   !  * if there are no ddc sides, the previous choice for ddc_marker
   !    gives no problems
   !  * if there are some ddc sides, there will be an error
   ! - if ddc_grid is present, we assume that the communicator is also
   !   present and we set ddc_marker to the proper global value; this
   !   will yield the correct results whether or not there are ddc
   !   sides
   if(present(ddc_grid)) then
     if(.not.present(comm)) call error(this_sub_name,this_mod_name, &
                'The optional argument "comm" must be present.')
     ! Make sure all subdomains agree about the value of ddc_marker
     sendb(1) = ddc_marker
     call mpi_allreduce(sendb,recvb,1,mpi_integer,mpi_max,comm,ierr)
     ddc_marker = recvb(1)
   endif
   do i=1,nt
     ine = size(t(i)%it,1)
     do ie=1,size(t(i)%ie)
       do isl=1,ine ! local side loop
         ! the sides are generated from the master element
         call insert_side_list( t(i)%ie(ie) , isl ,        &
                t(i)%it( base%e(ine)%me%snodes(:,isl) ,ie) )
       enddo
     enddo
   enddo
   ! add boundary information
   do isb=1,size(e,2)
     if(e(5,isb).le.0) then
       write(message(1),'(a,i7,a,i7)') &
         'Wrong marker ',e(5,isb),' on boundary side ',isb
       write(message(2),'(a)') &
         'Boundary markers must be positive.'
       call error(this_sub_name,this_mod_name,message(1:2))
     endif
     call insert_side_list_b(e(5,isb),e(1:2,isb))
   enddo
   

   ! 3) allocate the grid (we now know how many ddc)
   grid%nb = size(e,2) + ddc_counter
   ! total number of sides: this requires considering the various
   ! element shapes, essentially we count each side twice and divide
   ! by two at the end
   grid%ns = grid%nb
   do i=1,nt
     grid%ns = grid%ns + product(shape(t(i)%it))
   enddo
   grid%ns = grid%ns/2 ! integer division (must be even)
   grid%ni = grid%ns - grid%nb

   ! allocate the main grid variables
   allocate( grid%v(grid%nv) )
   allocate( grid%s(grid%ns) )
   allocate( grid%e(grid%ne) )
   ! prepare the element vector; while doing this, check that all the
   ! elements are defined exactly once
   grid%e%i = -1 ! verify that no elements are skipped
   do i=1,nt
     ine = size(t(i)%it,1)
     do ie=1,size(t(i)%ie)
       iie = t(i)%ie(ie) ! element index
       grid%e(iie)%i    = iie
       grid%e(iie)%poly = ine
       allocate( grid%e(iie)%iv(ine) ); grid%e(iie)%iv = t(i)%it(:,ie)
       allocate( grid%e(iie)%v( ine) )
       do j=1,ine
         grid%e(iie)%v(j)%p => grid%v( grid%e(iie)%iv(j) )
       enddo
       allocate( grid%e(iie)%is(ine) )
       allocate( grid%e(iie)%s( ine) )
       allocate( grid%e(iie)%pi(ine) )
       allocate( grid%e(iie)%ip(ine) )
       allocate( grid%e(iie)%ie(ine) )
       allocate( grid%e(iie)%e (ine) )
       allocate( grid%e(iie)%iel(ine) )
     enddo
   enddo
   if(any(grid%e%i.le.0)) call error(this_sub_name,this_mod_name, &
     'Some elements have not been defined.')


   ! 4) copy back into the grid variable: while doing this, define the
   !  side ordering leaving boundary sides after internal ones
   is = 0
   isb = grid%ni
   allocate( v2s_list(grid%nv) , v2e_list(grid%nv) )
   grid%s%i = -1 ! check that all the sides are initialized
   do ie=1,grid%ne ! element loop

     ! update the v2e list
     do ivl=1,grid%e(ie)%poly
       call v2se_list_add(v2e_list,ie, grid%e(ie)%iv(ivl) )
     enddo

     ! build the sides
     do isl=1,grid%e(ie)%poly ! local side loop

       ie2 = e2side_list(ie)%s(isl)%ie2 ! opposite element
       if(ie2.gt.ie) then ! we have to create a new side

         is = is+1
         isl2 = e2side_list(ie)%s(isl)%isl2 ! local position on ie2

         ! vertex information
         ! edge
         do ivl=1,2 ! loop over side vertices
           iv = e2side_list(ie)%s(isl)%iv(ivl)
           call v2se_list_add(v2s_list,is,iv)
         enddo

         ! side information
         grid%s(is)%i = is
         ! vertex
         ! the side gets the ordering induced by e1
         grid%s(is)%iv = e2side_list(ie)%s(isl)%iv
         do ivl=1,2
           grid%s(is)%v(ivl)%p => grid%v(grid%s(is)%iv(ivl))
         enddo
         ! element
         grid%s(is)%ie  = (/ ie  , ie2  /)
         grid%s(is)%e(1)%p => grid%e(ie )
         grid%s(is)%e(2)%p => grid%e(ie2)
         grid%s(is)%isl = (/ isl , isl2 /)

         ! element information
         ! edge
         grid%e(ie)%is(isl) = is
         grid%e(ie)%s(isl)%p => grid%s(is)
         ! We know the permutations to bring the element induced
         ! orders (left and right) into the reduced order, and we know
         ! that the side ordering is induced by the first element (see
         ! above). Using this, we can reconstruct pi and ip.
         grid%e(ie)%pi(isl) = 1 ! identity (side ordering from e1)
         grid%e(ie)%ip(isl) = 1 ! inverse of the identity
         grid%e(ie2)%is(isl2) = is
         grid%e(ie2)%s(isl2)%p => grid%s(is)
         grid%e(ie2)%pi(isl2) = idx( & ! e2 -> reduced -> e1 eqv side
     ((e2side_list(ie)%s(isl)%pi)**(-1)) * e2side_list(ie2)%s(isl2)%pi )
         grid%e(ie2)%ip(isl2) = idx( & ! inverse of above
     ((e2side_list(ie2)%s(isl2)%pi)**(-1)) * e2side_list(ie)%s(isl)%pi )
         ! neighboring elements
         grid%e(ie )%ie( isl )   = ie2
         grid%e(ie )%e(  isl )%p => grid%e(ie2)
         grid%e(ie )%iel(isl )   = isl2
         grid%e(ie2)%ie( isl2)   = ie
         grid%e(ie2)%e(  isl2)%p => grid%e(ie)
         grid%e(ie2)%iel(isl2)   = isl
 
       elseif(ie2.lt.0) then ! we have to create a boundary side
         
         ! for boundary edges the ordering is provided by e
         isb = isb + 1

         ! vertex information
         ! edge
         do ivl=1,2 ! loop over side vertices
           iv = e2side_list(ie)%s(isl)%iv(ivl)
           call v2se_list_add(v2s_list,isb,iv)
         enddo

         ! side information
         grid%s(isb)%i = isb
         ! vertex
         ! the side gets the ordering induced by e1
         grid%s(isb)%iv = e2side_list(ie)%s(isl)%iv
         do ivl=1,2
           grid%s(isb)%v(ivl)%p => grid%v(grid%s(isb)%iv(ivl))
         enddo
         ! element
         grid%s(isb)%ie = (/ ie , ie2 /) ! ie2 is -marker
         grid%s(isb)%e(1)%p => grid%e(ie)
         grid%s(isb)%e(2)%p => null()
         grid%s(isb)%isl = (/ isl , 0 /)

         ! element information
         ! edge
         grid%e(ie)%is(isl) = isb
         grid%e(ie)%s(isl)%p => grid%s(isb)
         grid%e(ie)%pi(isl) = 1 ! identity
         grid%e(ie)%ip(isl) = 1 ! inverse of the identity
         ! neighboring elements
         grid%e(ie)%ie( isl)   = ie2
         grid%e(ie)%e(  isl)%p => null()
         grid%e(ie)%iel(isl)   = 0

       endif
     enddo
   enddo
   if(any(grid%s%i.lt.0)) then ! something wrong
     write(message(1),'(a)') &
       'The following sides do not belong to any element: '
     i = 1
     do is=1,grid%ns
       if(grid%s(is)%i.lt.0) then
         write(message(2)(i:i+6),'(i7)') is
         i = i+8
         if(i+8.gt.len(message(2))) exit
       endif
     enddo
     write(message(3),'(a)') 'Possible reasons are:'
     write(message(4),'(a)') '  * too many columns in "e"'
     write(message(5),'(a)') '  * zero boundary markers in "e"'
     call error(this_sub_name,this_mod_name,message(1:5))
   endif

   ! Almost done: vertex information
   do iv=1,grid%nv
     grid%v(iv)%i = iv
     ! The coordinates should be set in the geometry computations, but
     ! storing them now will allow us to deallocate p together with e
     ! and t, so that it's convenient.
     grid%v(iv)%x = p(:,iv)
     call copy_v2se(iv) ! also set the pointers s and clean the list
   enddo


   ! 5) Domain decomposition optional output
   if(ddc_counter.gt.0) then
     if( (.not.present(ddc_grid)).or. &
         (.not.present(grid_file_name)) ) then
       write(message(1),'(a,a,a,i9,a)') &
         'The grid in "',trim(grid_file_name),'" has ',ddc_counter, &
         ' domain decomposition sides.'
       write(message(2),'(a,a)') &
         ' You must use the optional arguments to retrive the related', &
         ' information.'
       call error(this_sub_name,this_mod_name,message(1:2))
     endif
     call new_file_unit(fu,ierr)
     open(fu,file=trim(grid_file_name), &
          iostat=ierr,status='old',action='read')
       call read_octave_al(ivl2ivn,'ivl2ivn',fu) ! allocate( ivl2ivn )
     close(fu,iostat=ierr)

     ! We can now define the ddc grid
     call compute_ddc( ddc_grid , grid , comm , ddc_marker , ivl2ivn )
   else
     if(present(ddc_grid)) then
       ! build a simple t_grid variable setting only the fields
       ! required by this function
       tmp_grid%nv = grid%nv
       tmp_grid%ns = grid%ns
       tmp_grid%ne = grid%ne
       call compute_ddc_simple( ddc_grid , tmp_grid , ddc_marker )
     endif
   endif


   ! Clean up
   deallocate( e2side_list , v2s_list , v2e_list )
   call side_list_clean()


   ! Compute geometrical quantities
   call compute_geometry(grid,base)
   ! test that all the elements are defined (compute_geometry is pure)
   if(any((/ (.not.allocated(grid%e(ie)%me),ie=1,grid%ne) /))) &
     call error(this_sub_name,this_mod_name,                           &
       (/ 'Some master element(s) are not defined;                 ' , &
          ' -> this likely means that their shape is not supported.' /))


  contains

   subroutine insert_side_list(ie,isl,isv)
    integer, intent(in) :: ie, isl
    integer, intent(in) :: isv(2)
     
    type(t_dperm) :: isv_red
    type(t_side_list), pointer :: p, new_side

     isv_red = dperm_reduce(isv) ! reduce to basic form
     ! insert in the linked list
     p => side_list(isv_red%x(1)) ! the first vertex indexes the array
     search: do
       ! Notice: the two values v(1) and v(2) have the same field x
       ! (the one used to sort the sides) and different fields pi, so
       ! that the test on the ordering can be done on v(1).
       if(isv_red.lt.p%next%v(1)) then ! add new side to the list
         allocate(new_side)
         new_side%iv(:,1) = isv
         ! each side stores the permutations from the element induced
         ! ordering to the reduced one, for both elements; these
         ! permutations will be used to set pi and ip for the elements
         ! (observe however that these permutations are *not* the same
         ! as those included in pi and ip)
         new_side%v(1)   = isv_red
         new_side%ie(1)  = ie
         new_side%isl(1) = isl
         ! Setting ie(2) as -ddc_marker indicates that the newly
         ! created side is a ddc side. If there will be a second
         ! element, or the side appears in e, this value will be
         ! overwritten, so that after building the side list the ddc
         ! sides can be identified testing ie(2).eq.-ddc_marker
         new_side%ie(2) = -ddc_marker ! default initialization
         e2side_list(ie)%s(isl)%ie2  => new_side%ie(2)
         e2side_list(ie)%s(isl)%isl2 => new_side%isl(2)
         e2side_list(ie)%s(isl)%iv   => new_side%iv(:,1)
         ! this pointer will be used to compute the element pi and ip
         e2side_list(ie)%s(isl)%pi   => new_side%v(1)%pi
         new_side%next   => p%next
         p%next => new_side
         ddc_counter = ddc_counter + 1
         exit search
       elseif(isv_red.eq.p%next%v(1)) then ! add el. to an existing side
         p%next%iv(:,2) = isv
         p%next%v(2)   = isv_red
         p%next%ie(2)  = ie
         p%next%isl(2) = isl
         e2side_list(ie)%s(isl)%ie2  => p%next%ie(1)
         e2side_list(ie)%s(isl)%isl2 => p%next%isl(1)
         e2side_list(ie)%s(isl)%iv   => p%next%iv(:,2)
         e2side_list(ie)%s(isl)%pi   => p%next%v(2)%pi
         ddc_counter = ddc_counter - 1
         exit search
       endif

       p => p%next
     enddo search

   end subroutine insert_side_list

   subroutine insert_side_list_b(marker,isv)
   ! Analogous to insert_side_list, but here we can be sure that the
   ! side is already present.
    integer, intent(in) :: marker, isv(2)

    type(t_dperm) :: isv_red
    type(t_side_list), pointer :: p
     
     isv_red = dperm_reduce(isv) ! reduce to basic form
     ! insert in the linked list
     p => side_list(isv_red%x(1))
     search: do
       if(isv_red.eq.p%next%v(1)) then ! add boundary
         ! check that the same boundary side is not already defined
         error_if: if(p%next%ie(2).ne.-ddc_marker) then
           ! For the check, see the comments in insert_side_list
           write(message(1),'(a)') &
             "Conflicting definition of a boundary side."
           write(message(3),'(a,i5,a)') '(a,',size(isv),'i9)'
           write(message(2),message(3)) &
             "  side vertexes: ", isv
           write(message(3),'(a,i9)') &
             "  ie(2) is already: ", p%next%ie(2)
           call error(this_sub_name,this_mod_name,message(1:3))
         endif error_if
         ! p%next%v(2) left undefined
         p%next%ie(2) = -marker
         ! p%next%isl(2) left undefined
         ddc_counter = ddc_counter - 1
         exit search
       endif

       p => p%next

       if(.not.(associated(p%next))) then
         write(message(1),"(a,i0,a,i0,a)") &
           "A boundary condition has been specified for the side (", &
           isv(1),",",isv(2),") which does not exist."
         call error(this_sub_name,this_mod_name,message(1))
       endif
     enddo search

   end subroutine insert_side_list_b

   subroutine side_list_clean()
    type(t_side_list), pointer :: p, p2

     do iv=1,grid%nv
       p => side_list(iv)%next
       do
        if(.not.associated(p)) exit
         p2 => p
         p => p%next
         deallocate(p2)
       enddo
       side_list(iv)%next => null()
     enddo
     deallocate(side_list)
   end subroutine side_list_clean

   subroutine v2se_list_add(list,ise,iv)
   ! add side/element ise to the list of vertex iv
    type(t_v2se_list), target :: list(:)
    integer, intent(in) :: ise, iv

    type(t_v2se_list), pointer :: p

     p => list(iv)
     do
      if(.not.associated(p%next)) exit
       p => p%next
     enddo
     list(iv)%nse = list(iv)%nse + 1
     allocate(p%next)
     p%next%ise = ise

   end subroutine v2se_list_add

   subroutine copy_v2se(iv)
    integer, intent(in) :: iv

    type(t_v2se_list), pointer :: p, p2

     grid%v(iv)%ns = v2s_list(iv)%nse
     grid%v(iv)%ne = v2e_list(iv)%nse
     allocate( grid%v(iv)%is(grid%v(iv)%ns) , &
               grid%v(iv)%s (grid%v(iv)%ns)  )
     allocate( grid%v(iv)%ie(grid%v(iv)%ne) , &
               grid%v(iv)%e (grid%v(iv)%ne)  )

     p => v2s_list(iv)%next
     do
      if(.not.associated(p)) exit
       grid%v(iv)%is(v2s_list(iv)%nse) = p%ise
       grid%v(iv)%s(v2s_list(iv)%nse)%p => grid%s(p%ise)
       v2s_list(iv)%nse = v2s_list(iv)%nse-1
       p2 => p
       p => p%next
       deallocate(p2)
     enddo

     p => v2e_list(iv)%next
     do
      if(.not.associated(p)) exit
       grid%v(iv)%ie(v2e_list(iv)%nse) = p%ise
       grid%v(iv)%e(v2e_list(iv)%nse)%p => grid%e(p%ise)
       v2e_list(iv)%nse = v2e_list(iv)%nse-1
       p2 => p
       p => p%next
       deallocate(p2)
     enddo

     v2s_list(iv)%next => null()
     v2e_list(iv)%next => null()
   end subroutine copy_v2se

 end subroutine new_h2d_base_grid

!-----------------------------------------------------------------------

 pure subroutine clear_h2d_grid( grid )
  type(t_h2d_grid), intent(inout) :: grid

  integer :: ie

   ! Deallocate the master elements
   do ie=1,grid%ne
     call grid%e(ie)%me%clear( grid%e(ie)%me )
   enddo

   ! Deallocate the main fields
   deallocate(grid%v)
   deallocate(grid%s)
   deallocate(grid%e)
   
 end subroutine clear_h2d_grid

!-----------------------------------------------------------------------

 !> Set the domain decomposition data structures
 !!
 !! \warning This subroutine is almost a copy-and-paste of
 !! \fref{mod_grid,compute_ddc}; this code duplication should be
 !! eliminated as soon as possible.
 subroutine compute_ddc( ddc_grid , grid , comm , ddc_marker , ivl2ivn )
  type(t_ddc_h2d_grid), intent(out) :: ddc_grid
  type(t_h2d_grid), intent(in) :: grid
  integer, intent(in) :: comm, ddc_marker
  type(t_ddc_nv), intent(in) :: ivl2ivn(:)

  !> Number of vertices per side: this is the main difference compared
  !! with the general function in mod_grid.
  integer, parameter :: nv4side = 2
  integer :: i, id, iv, ivl, ivn, ivseg(3), is, isn, ie, dim1, ierr
  integer :: req
  integer, allocatable :: sendbuf(:,:), recvbuf(:,:), dim2_id(:), &
    is_s(:), reqs(:), mpi_stat(:,:), ids(:)
  type(t_dperm) :: p1, p2
  character(len=1000) :: message(3)
  character(len=*), parameter :: &
    this_sub_name = 'compute_ddc'

   ! 1) General setup: nd, id, ddc_marker
   call mpi_comm_size(comm,ddc_grid%nd,ierr)
   call mpi_comm_rank(comm,ddc_grid%id,ierr)
   allocate( ddc_grid%nnv_id(0:ddc_grid%nd-1) , &
             ddc_grid%nns_id(0:ddc_grid%nd-1) )
   ddc_grid%nnv_id = 0
   ddc_grid%nns_id = 0
   ddc_grid%ddc_marker = ddc_marker

   ! 2) Global entity information
   allocate( ddc_grid%gv(grid%nv) , &
             ddc_grid%gs(grid%ns) , &
             ddc_grid%ge(grid%ne) )

   ! 3) Neighbour entity information: nnv, nns, gv%ni, gs%ni
   ddc_grid%nnv = 0
   ddc_grid%nns = 0
   do is=grid%ni+1,grid%ns
     if(grid%s(is)%ie(2).eq.-ddc_grid%ddc_marker) then
       ddc_grid%nns = ddc_grid%nns + 1
       ddc_grid%gs(is)%ni = ddc_grid%nns
       do ivl=1,nv4side
         iv = grid%s(is)%iv(ivl)
         if(ddc_grid%gv(iv)%ni.eq.0) then
           ddc_grid%nnv = ddc_grid%nnv + 1
           ddc_grid%gv(iv)%ni = ddc_grid%nnv
         endif
       enddo
     endif
   enddo
   allocate( ddc_grid%nv(ddc_grid%nnv) , &
             ddc_grid%ns(ddc_grid%nns) )

   ! 4) Neighbour vertexes: nv%all_fields, nnv_id
   do ivn=1,size(ivl2ivn)
     ! This works in both cases: whether ivl2ivn contains only ddc
     ! nodes or all the nodes (which depends on the input file)
     if(ivl2ivn(ivn)%nd.gt.0) then
       iv = ddc_grid%gv(ivl2ivn(ivn)%i)%ni
       ddc_grid%nv(iv) = ivl2ivn(ivn)
       ! recall that nv%id is an array, possibly with repetitions
       do i=1,ddc_grid%nv(iv)%nd
         associate(entry => ddc_grid%nnv_id(ddc_grid%nv(iv)%v2v(i)%id))
         entry = entry + 1
         end associate
       enddo
     endif
   enddo
   ! Check that the input file was complete
   do iv=1,grid%nv
     if(ddc_grid%gv(iv)%ni.gt.0) then
       ivn = ddc_grid%gv(iv)%ni
       if(.not.allocated(ddc_grid%nv(ivn)%v2v)) then
         write(message(1),'(a,i0,a,i0,a)') &
       'Domain decomposition information is missing for vertex ' , &
       iv,' on subdomain ',ddc_grid%id,'.'
         call error(this_sub_name,this_mod_name,message(1))
       endif
     endif
   enddo
   ! Consistency check: nnv_id should be the same for each domain pair
   allocate(recvbuf(0:ddc_grid%nd-1,1))
   call mpi_alltoall( ddc_grid%nnv_id , 1 , mpi_integer ,            &
                       recvbuf(:,1)   , 1 , mpi_integer , comm, ierr )
   do id=0,ddc_grid%nd-1
     if(recvbuf(id,1).ne.ddc_grid%nnv_id(id)) then
       write(message(1),'(a,i0,a)') &
         'Inconsistency in ddc_grid%nnv_id, on proc. ',ddc_grid%id,':'
       write(message(2),'(a,i0,a,i0,a,i0)') &
         ' locally  nnv_id(',id,') = ',ddc_grid%nnv_id(id), &
         '  while the neighbour proc. reports ',recvbuf(id,1)
       write(message(3),'(a)') &
         'This is often a problem in the grid partitioning.'
       call error(this_sub_name,this_mod_name,message(1:3))
     endif
   enddo
   deallocate(recvbuf)

   ! 5) Neighbour Side information

   ! 5.1) Neighbour sides: ns%i, ns%is, nns_id
   call ns_disambiguate('init',grid,ddc_grid)
   do is=grid%ni+1,grid%ns
     if(ddc_grid%gs(is)%ni.ne.0) then
       ddc_grid%ns(ddc_grid%gs(is)%ni)%i  = is
       ! The id field is initialized to -1 to check later whether a
       ! side has more that one neighbour, which indicates an error in
       ! the grid files.
       ddc_grid%ns(ddc_grid%gs(is)%ni)%id = -1

       call get_siden_id(ids,ddc_grid%nv(ddc_grid%gv(grid%s(is)%iv)%ni))
       if(size(ids).eq.1) then
         id = ids(1) ! usually this is the case
         ddc_grid%ns(ddc_grid%gs(is)%ni)%id = id
         ! the remaining fields require a communication
         ddc_grid%nns_id(id) = ddc_grid%nns_id(id)+1
       else
         if(size(ids).eq.0) then
           write(message(1),*) "Can't find a neighbour for side ",is
           call error(this_sub_name,this_mod_name,message(1))
         else ! we need to disambiguate
           call ns_disambiguate('insert',grid,ddc_grid,is=is,ids=ids)
         endif
       endif
     endif
   enddo
   call ns_disambiguate('disambiguate',grid,ddc_grid,comm=comm)

   ! 5.2) all to all communication: ns%in, ns%p_s2s
   dim1 = nv4side+1
   allocate(sendbuf(dim1,ddc_grid%nns))
   allocate(recvbuf(dim1,ddc_grid%nns))
   allocate(is_s(0:ddc_grid%nd-1))
   do i=0,ddc_grid%nd-1 ! set the offset array
     is_s(i) = sum(ddc_grid%nns_id(0:i-1))
   enddo
   do isn=1,ddc_grid%nns
     id = ddc_grid%ns(isn)%id
     is_s(id) = is_s(id) + 1
     ! first lines: neighbour vertex indexes
     do ivl=1,nv4side
       ivn = ddc_grid%gv(grid%s(ddc_grid%ns(isn)%i)%iv(ivl))%ni
       i = findloc( ddc_grid%nv(ivn)%v2v%id , id , dim=1 )
       sendbuf(ivl,is_s(id)) = ddc_grid%nv(ivn)%v2v(i)%in
     enddo
     ! last line: local side index
     sendbuf(dim1 ,is_s(id)) = ddc_grid%ns(isn)%i
   enddo
   do i=0,ddc_grid%nd-1 ! reset the offset array
     is_s(i) = sum(ddc_grid%nns_id(0:i-1))
   enddo
   call mpi_alltoallv(                                           &
          sendbuf, dim1*ddc_grid%nns_id, dim1*is_s, mpi_integer, &
          recvbuf, dim1*ddc_grid%nns_id, dim1*is_s, mpi_integer, &
                      comm,ierr)
   do isn=1,ddc_grid%nns ! loop on the received buffer
     ! local side index
     is = get_siden_is(grid%v(recvbuf(1:nv4side,isn)))
     ddc_grid%ns(ddc_grid%gs(is)%ni)%in = recvbuf(nv4side+1,isn)
     ! now find the permutation: s%iv -> v
     p1 = dperm_reduce(grid%s(is)%iv)
     p2 = dperm_reduce(recvbuf(1:nv4side,isn))
     ddc_grid%ns(ddc_grid%gs(is)%ni)%p_s2s = idx( (p2%pi**(-1))*p1%pi )
   enddo
   deallocate(sendbuf,recvbuf,is_s)

   ! 6) Global vertex/side/element numbering: gv%gi, gs%gi, ge%gi

   ! This is done in three steps:
   ! 1) get the indexes from the subdomains with lower id 
   ! 2) assign the remaining indexes
   ! 3) send the indexes to the subdomains with higher id

   ! To allow a correct global numbering of the degrees of freedom,
   ! each domain must also tell the next one what is its maximum
   ! global index. This is done with the following send/recv.
   if(ddc_grid%id.gt.0) then
     call mpi_irecv( ivseg(1) , 3 , mpi_integer ,         &
                     ddc_grid%id-1, 1 , comm , req , ierr )
   else
     ivseg = 0
   endif
   allocate(dim2_id(0:ddc_grid%id-1))
   dim2_id = ddc_grid%nnv_id(0:ddc_grid%id-1) + &
             ddc_grid%nns_id(0:ddc_grid%id-1) 
   allocate( reqs    (                0:ddc_grid%id-1) , &
             mpi_stat(mpi_status_size,0:ddc_grid%id-1) )
   allocate(recvbuf(2,sum(dim2_id)))
   reqs = mpi_request_null
   is = 1
   do i=0,ddc_grid%id-1
     if(dim2_id(i).gt.0) &
       call mpi_irecv( recvbuf(1,is) , 2*dim2_id(i) , mpi_integer , &
                       i, 2, comm, reqs(i), ierr )
     is = is + dim2_id(i)
   enddo
   if(ddc_grid%id.gt.0) call mpi_wait( req , mpi_stat(:,0) , ierr )
   call mpi_waitall( size(reqs) , reqs , mpi_stat , ierr )
   deallocate(reqs,mpi_stat)
   ddc_grid%gv%gi = 0
   ddc_grid%gs%gi = 0
   ddc_grid%ge%gi = 0
   allocate(is_s(0:ddc_grid%id-1))
   do i=0,ddc_grid%id-1 ! set the vertex offsets
     is_s(i) = sum( dim2_id(:i-1) )
   enddo
   do i=0,ddc_grid%id-1
     do is=is_s(i)+1,is_s(i)+ddc_grid%nnv_id(i)
       iv = recvbuf(1,is)
       if(iv.gt.grid%nv) then ! somebody is sending wrong data
         write(message(1),'(a,i10,a,i10,a,i10,a,a,i10)')               &
           'Subdomain ',i,' is sending local index ',iv,               &
           ' (position in the two-procs buffer ',is-is_s(i),')',       &
           ' which is larger than the number of local vertexes ',grid%nv
         call error(this_sub_name,this_mod_name,message)
       endif
       if(ddc_grid%gv(iv)%gi.eq.0) then ! define the global index
         ddc_grid%gv(iv)%gi = recvbuf(2,is)
       else ! in principle, nothing to do, however better to check
         if(ddc_grid%gv(iv)%gi.ne.recvbuf(2,is)) &
           call error(this_sub_name,this_mod_name, &
  'Two neighbours are trying to define two different global indexes.')
       endif
     enddo
   enddo
   ! set the side offsets
   is_s = is_s + ddc_grid%nnv_id(0:ddc_grid%id-1)
   do i=0,ddc_grid%id-1
     do is=is_s(i)+1,is_s(i)+ddc_grid%nns_id(i)
       iv = recvbuf(1,is) ! is already used as shift
       ! This is simpler than for the vertexes, since each side is
       ! shared by exactly two subdomains.
       ddc_grid%gs(iv)%gi = recvbuf(2,is)
     enddo
   enddo
   deallocate(dim2_id,is_s,recvbuf)
   allocate(dim2_id(ddc_grid%id+1:ddc_grid%nd-1))
   dim2_id = ddc_grid%nnv_id(ddc_grid%id+1:ddc_grid%nd-1) + &
             ddc_grid%nns_id(ddc_grid%id+1:ddc_grid%nd-1) 
   allocate(sendbuf(2,sum(dim2_id)))
   allocate(is_s(ddc_grid%id+1:ddc_grid%nd-1))
   do i=ddc_grid%id+1,ddc_grid%nd-1 ! set the offset array
     is_s(i) = sum( dim2_id(:i-1) )
   enddo
   do iv=1,grid%nv
     if(ddc_grid%gv(iv)%gi.eq.0) then ! define the global index
       ivseg(1) = ivseg(1)+1
       ddc_grid%gv(iv)%gi = ivseg(1)
     endif
     if(ddc_grid%gv(iv)%ni.ne.0) then ! possibly fill in sendbuf
       ivn = ddc_grid%gv(iv)%ni
       do i=1,ddc_grid%nv(ivn)%nd
         id = ddc_grid%nv(ivn)%v2v(i)%id
         if(id.eq.ddc_grid%id) then ! self-connection
call warning(this_sub_name,this_mod_name, &
  'TODO: see the same case in mod_grid.')
         elseif(id.gt.ddc_grid%id) then ! send this info
           is_s(id) = is_s(id)+1
           sendbuf(1,is_s(id)) = ddc_grid%nv(ivn)%v2v(i)%in
           sendbuf(2,is_s(id)) = ddc_grid%gv(iv)%gi
         endif
       enddo
     endif
   enddo
   do is=1,grid%ns
     if(ddc_grid%gs(is)%gi.eq.0) then ! define the global index
       ivseg(2) = ivseg(2)+1
       ddc_grid%gs(is)%gi = ivseg(2)
     endif
     if(ddc_grid%gs(is)%ni.ne.0) then ! possibly fill in sendbuf
       isn = ddc_grid%gs(is)%ni
       id  = ddc_grid%ns(isn)%id
       if(id.gt.ddc_grid%id) then ! send this info
         is_s(id) = is_s(id)+1
         sendbuf(1,is_s(id)) = ddc_grid%ns(ddc_grid%gs(is)%ni)%in
         sendbuf(2,is_s(id)) = ddc_grid%gs(is)%gi
       endif
     endif
   enddo
   do ie=1,grid%ne
     ivseg(3) = ivseg(3)+1
     ddc_grid%ge(ie)%gi = ivseg(3)
   enddo
   deallocate(is_s)
   allocate( reqs    (                ddc_grid%id+1:ddc_grid%nd-1) , &
             mpi_stat(mpi_status_size,ddc_grid%id+1:ddc_grid%nd-1) )
   if(ddc_grid%id.lt.ddc_grid%nd-1) &
     call mpi_isend( ivseg(1) , 3 , mpi_integer , ddc_grid%id+1 , 1 , &
                     comm , req , ierr )
   reqs = mpi_request_null
   is = 1
   do i=ddc_grid%id+1,ddc_grid%nd-1
     if(dim2_id(i).gt.0) &
       call mpi_isend( sendbuf(1,is) , 2*dim2_id(i) , mpi_integer , &
                      i, 2, comm, reqs(i) , ierr )
     is = is + dim2_id(i)
   enddo
   ! We have to wait before deallocating the send buffer
   deallocate(dim2_id)

   ! 7) Almost done, the last subdomain must update the others on the
   ! total number of generated entities
   allocate(dim2_id(3)) ! used as temporary
   dim2_id(1) = maxval(ddc_grid%gv%gi)
   dim2_id(2) = maxval(ddc_grid%gs%gi)
   dim2_id(3) = maxval(ddc_grid%ge%gi)
   call mpi_bcast(dim2_id,3,mpi_integer,ddc_grid%nd-1,comm,ierr)
   ddc_grid%ngv = dim2_id(1)
   ddc_grid%ngs = dim2_id(2)
   ddc_grid%nge = dim2_id(3)
   deallocate(dim2_id)

   if(ddc_grid%id.lt.ddc_grid%nd-1) &
     call mpi_wait( req , mpi_stat(:,ddc_grid%id+1) , ierr )
   call mpi_waitall( size(reqs) , reqs , mpi_stat , ierr )
   deallocate(sendbuf,reqs,mpi_stat)

 contains

  !> The id of the side neighboring subdomain is obtained as the
  !! unique intersection of the id-s of the neighboring subdomains of
  !! the vertexes.
  !!
  !! It is possible (albeit rare) that more than two domains share all
  !! the vertexes of a face, even if only two of them can share the
  !! face. This subroutine returns the id of all the subdomains wich
  !! share all the face vertexes. Notice that the only way to find out
  !! which subdomain really shares the face is by communication.
  !<
  pure subroutine get_siden_id(id,vid)
   integer, allocatable, intent(out) :: id(:)  !< neighbour subdomain
   type(t_ddc_nv),       intent(in)  :: vid(:) !< side vertexes

   logical :: found
   integer :: i, ii, n, wid, wids(vid(1)%nd)

    n = 0
    search_do: do i=1,vid(1)%nd
      wid = vid(1)%v2v(i)%id ! candidate domain
      found = .true.
      check_do: do ii=2,size(vid)
        found = any(vid(ii)%v2v%id.eq.wid)
        if(.not.found) exit check_do
      enddo check_do
      if(found) then
        n = n+1
        wids(n) = wid
      endif
    enddo search_do

    allocate(id(n)); id = wids(1:n)
  end subroutine get_siden_id

  !> The is of the side is obtained as the unique intersection of the
  !! is-s of the sides connected with the specified vertexes.
  !<
  pure function get_siden_is(v) result(is)
   type(t_2dv), intent(in) :: v(:) !< vertexes
   integer :: is !< side index

   logical :: found
   integer :: i, ii

    search_do: do i=1,v(1)%ns
      is = v(1)%is(i)
      found = .true.
      check_do: do ii=2,size(v)
        found = found .and. any(v(ii)%is.eq.is)
        if(.not.found) exit check_do
      enddo check_do
      if(found) exit search_do
    enddo search_do

    if(.not.found) is = huge(is) ! error
  end function get_siden_is

  pure function findloc(v,x,dim) result(i)
  ! This function should be eliminated once the new intrinsic with the
  ! same name is supported
   integer, intent(in) :: v(:), x
   integer, intent(in), optional :: dim
   integer :: i
   integer :: j
    i = 0
    search_do: do j=1,size(v)
      if(v(j).eq.x) then
        i = j
        exit search_do
      endif
    enddo search_do
  end function findloc

 end subroutine compute_ddc

!-----------------------------------------------------------------------

 !> This is also a copy-and-paste of \fref{mod_grid,ns_disambiguate}
 subroutine ns_disambiguate(action,grid,ddc_grid,is,ids,comm)
  character(len=*), intent(in) :: action
  type(t_h2d_grid),     intent(in) :: grid
  type(t_ddc_h2d_grid), intent(inout) :: ddc_grid
  integer, intent(in), optional :: is, ids(:), comm

  type t_dis
    integer :: counter
    integer, allocatable :: is(:) ! sides to check with a neighbour
  end type t_dis
  type(t_dis), allocatable, save :: dis_status(:)
  character(len=*), parameter :: this_sub_name = "ns_disambiguate"

  integer, parameter :: nv4side = 2
  integer :: id, i, j, k, inv, ierr
  integer, allocatable :: tmp(:), rec_counter(:), senoff(:), &
    recoff(:), sendbuf(:,:), recvbuf(:,:)
  character(len=1000) :: message(2)

   action_case: select case(action)

    case('init')
     allocate(dis_status(0:ddc_grid%nd-1))
     do id=0,ddc_grid%nd-1
       dis_status(id)%counter = 0
       allocate(dis_status(id)%is(0))
     enddo

    case('insert')
     do i=1,size(ids) ! loop on the candidate neighboring subdomains
       id = ids(i)
       allocate( tmp(dis_status(id)%counter) )
       tmp = dis_status(id)%is
       deallocate(dis_status(id)%is)
       dis_status(id)%counter = dis_status(id)%counter + 1
       allocate(dis_status(id)%is(dis_status(id)%counter))
       dis_status(id)%is(1:dis_status(id)%counter-1) = tmp
       dis_status(id)%is(  dis_status(id)%counter  ) = is
       deallocate(tmp)
     enddo

    case('disambiguate') ! and clean
     ! we don't know how much data we are going to receive
     allocate(rec_counter(0:ddc_grid%nd-1))
     call mpi_alltoall( dis_status%counter , 1 , mpi_integer , &
                        rec_counter        , 1 , mpi_integer , &
                        comm, ierr )
     ! now we can send/receive the side data
     allocate(senoff(0:ddc_grid%nd-1),recoff(0:ddc_grid%nd-1))
     senoff(0) = 0; recoff(0) = 0
     do id=1,ddc_grid%nd-1
       senoff(id) = senoff(id-1) + dis_status(id-1)%counter
       recoff(id) = recoff(id-1) + rec_counter(id-1)
     enddo
     allocate( sendbuf(nv4side,sum(dis_status%counter)) , &
               recvbuf(nv4side,sum(   rec_counter    )) )
     j = 0
     do id=0,ddc_grid%nd-1
       do i=1,dis_status(id)%counter
         j = j+1
         do k=1,nv4side
           inv = ddc_grid%gv(grid%s(dis_status(id)%is(i))%iv(k))%ni
           sendbuf(k,j) = ddc_grid%nv(inv)%v2v(         &
             findloc( ddc_grid%nv(inv)%v2v%id , id ) )%in
         enddo
       enddo
     enddo
     call mpi_alltoallv(                                               &
       sendbuf,nv4side*dis_status%counter,nv4side*senoff, mpi_integer, &
       recvbuf,nv4side*   rec_counter    ,nv4side*recoff, mpi_integer, &
                        comm,ierr)
     ! send/receive the feedback
     deallocate(sendbuf); allocate( sendbuf(1,sum(rec_counter)) )
     do i=1,size(recvbuf,2) ! loop on the received data
       sendbuf(1,i) = check_side_exists(grid%v(recvbuf(:,i)))
     enddo
     deallocate(recvbuf); allocate( recvbuf(1,sum(dis_status%counter)) )
     call mpi_alltoallv(                                               &
       sendbuf,    rec_counter    , recoff, mpi_integer, &
       recvbuf, dis_status%counter, senoff, mpi_integer, &
                        comm,ierr)
     j = 0
     do id=0,ddc_grid%nd-1
       do i=1,dis_status(id)%counter
         j = j+1
         if(recvbuf(1,j).eq.1) then ! positive feedback
           ! Make sure that each side has at most one positive feedback
           k = ddc_grid%ns(ddc_grid%gs(dis_status(id)%is(i))%ni)%id
           if(k.ge.0) then
             write(message(1),'(a,i6)')      &
               'Unable to identify a neighbour on side ', &
               dis_status(id)%is(i)
             write(message(2),'(a,i4,a,i4)') &
               '  possible candidates are (at least) ',k,' and ',id
             call error(this_sub_name,this_mod_name,message(1:2))
           endif
           ddc_grid%ns(ddc_grid%gs(dis_status(id)%is(i))%ni)%id = id
           ! the remaining fields require a communication
           ddc_grid%nns_id(id) = ddc_grid%nns_id(id)+1
         endif
       enddo
     enddo

     deallocate(dis_status)
     deallocate(rec_counter,senoff,recoff,sendbuf,recvbuf)

   end select action_case

 contains

  !> A side is checked as the (unique, if any) intersection of the
  !! sides connected to the d vertexes. Notice that this makes sense
  !! only if called with d vertexes.
  !<
  pure function check_side_exists(v) result(yesno)
   type(t_2dv), intent(in) :: v(:)
   integer :: yesno

   logical :: found
   integer :: i, is, ii
    
    yesno = 0
    search_do: do i=1,v(1)%ns
      is = v(1)%is(i) ! candidate side
      found = .false.
      check_do: do ii=2,size(v)
        found = any(v(ii)%is.eq.is)
        if(.not.found) exit check_do
      enddo check_do
      if(found) then
        yesno = 1
        exit search_do
      endif
    enddo search_do
  end function check_side_exists

  pure function findloc(v,x,dim) result(i)
  ! This function should be eliminated once the new intrinsic with the
  ! same name is supported
   integer, intent(in) :: v(:), x
   integer, intent(in), optional :: dim
   integer :: i
   integer :: j
    i = 0
    search_do: do j=1,size(v)
      if(v(j).eq.x) then
        i = j
        exit search_do
      endif
    enddo search_do
  end function findloc

 end subroutine ns_disambiguate

!-----------------------------------------------------------------------

 !> Compute all the geometric properties of the grid.
 subroutine compute_geometry( grid , base )
  type(t_h2d_base),         intent(in) :: base
  type(t_h2d_grid), target, intent(inout) :: grid
 
  integer :: is, ie, i, iv1, iv2
  real(wp) :: xb(2), dx1, dx2, dy1, dy2, a2
  type(t_2ds), pointer :: s
  character(len=*), parameter :: &
    this_sub_name = 'compute_geometry'

   ! vertices are completed already

   ! element loop
   do ie=1,grid%ne
     associate(   e => grid%e(ie)        )
     associate( bme => base%e(e%poly)%me )

     ! Let us first get an approximation of the barycenter (for
     ! triangles, this is already the exact value)
     xb = 0.0_wp
     do i=1,e%poly
       xb = xb + e%v(i)%p%x
     enddo
     xb = xb/real(e%poly,wp)

     ! To compute the volume, we rely on the fact that the vertices
     ! are numbered consecutively, so we can compute the areas of all
     ! the triangles obtained connecting a side with the barycenter
     ! (with sign) and adding such areas. This works also for
     ! nonconvex polygons. After the computation, we take the absolute
     ! value to have a positive area for both clockwise and
     ! counterclockwise elements.
     !
     ! Using these signed areas as weights we can also compute the
     ! correct barycenter.
     e%vol = 0.0_wp
     e%xb  = 0.0_wp
     do i=1,e%poly
       iv1 = e%iv(     i           )
       iv2 = e%iv( mod(i,e%poly)+1 )
       ! Compute the signed area of the corresponding triangle
       dx1 = grid%v(iv1)%x(1) - xb(1)
       dx2 = grid%v(iv2)%x(1) - xb(1)
       dy1 = grid%v(iv1)%x(2) - xb(2)
       dy2 = grid%v(iv2)%x(2) - xb(2)
       a2 = dx1*dy2-dx2*dy1 ! twice the area
       ! Accumulate the areas and compute the barycenter
       e%vol = e%vol + a2
       e%xb  = e%xb + a2*( grid%v(iv1)%x + grid%v(iv2)%x + xb )
     enddo
     e%xb  = 1.0_wp/(3.0_wp*e%vol) * e%xb ! we need the sign of vol
     e%vol = 0.5_wp * abs(e%vol)

     ! Now the master element information
     call h2d_me_geom_init( e%me,                                  &
       reshape( (/ ( e%v(i)%p%x , i=1,e%poly ) /) , (/2,e%poly/) ) )

     ! Outward normal: the outward normal transforms like a 1-form (up
     ! to normalization), i.e. like the gradient of the basis
     ! functions. So, we can use the corresponding method to compute
     ! the normal in physical space from the one in the master
     ! element. This has the advantage of working for both clockwise
     ! and counterclockwise orientation of the element.
     !allocate(e%n(2,e%poly))
     ! ifort compiler bug: see
     ! https://software.intel.com/en-us/forums/topic/532496#comment-1800814
     allocate(grid%e(ie)%n(2,e%poly))
     e%n = reshape( e%me%scale_gradp(bme%nmlpts,                   &
             reshape( bme%normls , (/2,1,size(bme%normls,2)/) )) , &
                                   (/2 , size(bme%normls,2)/) )
     do i=1,e%poly ! normalization
       e%n(:,i) = e%n(:,i)/sqrt(sum(e%n(:,i)**2))
     enddo

     end associate
     end associate
   enddo

   ! side loop
   do is=1,grid%ns
     s => grid%s(is)

     s%xb = 0.5_wp*( s%v(1)%p%x + s%v(2)%p%x )
     s%a = sqrt(sum((s%v(1)%p%x - s%v(2)%p%x)**2))
   enddo

   grid%vol = sum(grid%e%vol)
 
 end subroutine compute_geometry

!-----------------------------------------------------------------------

 !> Read an element collection
 !!
 !! This subroutine can handle two cases
 !! <ul>
 !!  <li> a cell array where each entry is a struct corresponding to
 !!  \c t_el_collection
 !!  <li> a single array with the element connectivity; in this case
 !!  the element ordering is deduced from the column ordering of the
 !!  array; this option is included so that one can directly read the
 !!  triangle grids used by \c mod_grid and assumes that the elements
 !!  are triangles.
 !! </ul>
 !! In both cases, the output is formatted as an array of \c
 !! t_el_collection objects.
 subroutine read_al_el_collection(c,var_name,fu)
  integer, intent(in) :: fu
  type(t_el_collection), allocatable, intent(out) :: c(:)
  character(len=*), intent(in) :: var_name

  integer :: ierr, inrow, incol, nentries, i
  integer, allocatable :: tmp_t(:,:)
  character(len=1000) :: message
  character(len=*), parameter :: &
    this_sub_name = 'read_al_el_collection'

   call locate_var(fu,var_name,ierr)
   if(ierr.ne.0) then
     write(message,'(A,A,A,I3)') &
       'Problems locating "',var_name,'": iostat = ',ierr
     call warning(this_sub_name,this_mod_name,message)
   else

     ! get the number of entries in the collection  
     read(fu,'(A)') message ! get the type line: "# type: cell/matrix"
     select case(trim(adjustl(message(8:))))

      case("cell") ! a whole collection
       read(fu,'( A7,I10)') message, inrow ! "# rows:"
       read(fu,'(A10,I10)') message, incol ! "# columns:"
       nentries = inrow*incol

       allocate(c(nentries))
       do i=1,nentries
         ! for each entry, read the two fields
         call read_octave_al(c(i)%ie,'ie',fu,norewind=.true.)
         call read_octave_al(c(i)%it,'it',fu,norewind=.true.)
       enddo

      case("matrix") ! a single matrix

       ! back off till the beginning of the object, so that it can be
       ! read with the read_octave function
       back_off: do
         backspace(fu)
         read(fu,'(A)') message
         backspace(fu)
        if(message(1:7).eq."# name:") exit back_off
       enddo back_off
       backspace(fu)

       allocate(c(1))
       call read_octave_al(tmp_t,var_name,fu,norewind=.true.)
       ! there can be an additional row which must be eliminated
       allocate( c(1)%it(3,size(tmp_t,2)) )
       c(1)%it = tmp_t(1:3,:)
       deallocate( tmp_t )
       ! allocate the ie field, which simply include all the elements
       allocate(c(1)%ie(size(c(1)%it,2)))
       c(1)%ie = (/( i, i=1,size(c(1)%it,2) )/)

      case default
       call error(this_sub_name,this_mod_name,                   &
         'Unknown type "'//trim(adjustl(message(8:)))//'" for "' &
         //var_name//'".')
         
     end select

   endif
  
 end subroutine read_al_el_collection

!-----------------------------------------------------------------------

 subroutine write_h2d_grid_struct(grid,var_name,fu)
 ! octave output for the complete grid
  integer, intent(in) :: fu
  type(t_h2d_grid), intent(in) :: grid
  character(len=*), intent(in) :: var_name
 
  character(len=*), parameter :: &
    this_sub_name = 'write_grid_struct'
 
   write(fu,'(a,a)')    '# name: ',var_name
   write(fu,'(a)')      '# type: struct'
   write(fu,'(a)')      '# length: 9' ! number of fields

   ! field 01 : ne
   write(fu,'(a)')      '# name: ne'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(grid%ne,'<cell-element>',fu)

   ! field 02 : nv
   write(fu,'(a)')      '# name: nv'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(grid%nv,'<cell-element>',fu)

   ! field 03 : ns
   write(fu,'(a)')      '# name: ns'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(grid%ns,'<cell-element>',fu)

   ! field 04 : nb
   write(fu,'(a)')      '# name: nb'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(grid%nb,'<cell-element>',fu)

   ! field 05 : ni
   write(fu,'(a)')      '# name: ni'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(grid%ni,'<cell-element>',fu)

   ! field 06 : v
   write(fu,'(a)')      '# name: v'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(grid%v,'<cell-element>',fu)

   ! field 07 : s
   write(fu,'(a)')      '# name: s'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(grid%s,'<cell-element>',fu)

   ! field 08 : e
   write(fu,'(a)')      '# name: e'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(grid%e,'<cell-element>',fu)

   ! field 09 : vol
   write(fu,'(a)')      '# name: vol'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(grid%vol,'<cell-element>',fu)

 end subroutine write_h2d_grid_struct

!-----------------------------------------------------------------------

 subroutine write_h2d_vert_struct(v,var_name,fu)
  integer, intent(in) :: fu
  type(t_2dv), intent(in) :: v(:)
  character(len=*), intent(in) :: var_name
 
  integer :: iv
  character(len=*), parameter :: &
    this_sub_name = 'write_h2d_vert_struct'
 
   write(fu,'(a,a)')    '# name: ',var_name
   write(fu,'(a)')      '# type: struct'
   call write_dims()
   write(fu,'(a)')      '# length: 6' ! number of fields

   ! field 01 : i
   write(fu,'(a)')      '# name: i'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do iv=1,size(v)
     call write_octave(v(iv)%i,'<cell-element>',fu)
   enddo

   ! field 02 : x
   write(fu,'(a)')      '# name: x'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do iv=1,size(v)
     call write_octave(v(iv)%x,'c','<cell-element>',fu)
   enddo

   ! field 03 : ns
   write(fu,'(a)')      '# name: ns'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do iv=1,size(v)
     call write_octave(v(iv)%ns,'<cell-element>',fu)
   enddo

   ! field 04 : is
   write(fu,'(a)')      '# name: is'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do iv=1,size(v)
     call write_octave(v(iv)%is,'r','<cell-element>',fu)
   enddo

   ! field 05 : ne
   write(fu,'(a)')      '# name: ne'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do iv=1,size(v)
     call write_octave(v(iv)%ne,'<cell-element>',fu)
   enddo

   ! field 06 : ie
   write(fu,'(a)')      '# name: ie'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do iv=1,size(v)
     call write_octave(v(iv)%ie,'r','<cell-element>',fu)
   enddo

 contains

  subroutine write_dims()
   write(fu,'(a)')      '# ndims: 2'
   write(fu,'(i0,a,i0)') 1," ",size(v)
  end subroutine write_dims

 end subroutine write_h2d_vert_struct
 
!-----------------------------------------------------------------------

 subroutine write_h2d_side_struct(s,var_name,fu)
  integer, intent(in) :: fu
  type(t_2ds), intent(in) :: s(:)
  character(len=*), intent(in) :: var_name
 
  integer :: is
  character(len=*), parameter :: &
    this_sub_name = 'write_h2d_side_struct'
 
   write(fu,'(a,a)')    '# name: ',var_name
   write(fu,'(a)')      '# type: struct'
   call write_dims()
   write(fu,'(a)')      '# length: 6' ! number of fields

   ! field 01 : i
   write(fu,'(a)')      '# name: i'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do is=1,size(s)
     call write_octave(s(is)%i,'<cell-element>',fu)
   enddo

   ! field 02 : iv
   write(fu,'(a)')      '# name: iv'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do is=1,size(s)
     call write_octave(s(is)%iv,'r','<cell-element>',fu)
   enddo

   ! field 03 : ie
   write(fu,'(a)')      '# name: ie'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do is=1,size(s)
     call write_octave(s(is)%ie,'r','<cell-element>',fu)
   enddo

   ! field 04 : isl
   write(fu,'(a)')      '# name: isl'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do is=1,size(s)
     call write_octave(s(is)%isl,'r','<cell-element>',fu)
   enddo

   ! field 05 : xb
   write(fu,'(a)')      '# name: xb'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do is=1,size(s)
     call write_octave(s(is)%xb,'c','<cell-element>',fu)
   enddo

   ! field 06 : a
   write(fu,'(a)')      '# name: a'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do is=1,size(s)
     call write_octave(s(is)%a,'<cell-element>',fu)
   enddo

 contains

  subroutine write_dims()
   write(fu,'(a)')      '# ndims: 2'
   write(fu,'(i0,a,i0)') 1," ",size(s)
  end subroutine write_dims

 end subroutine write_h2d_side_struct

!-----------------------------------------------------------------------

 subroutine write_h2d_elem_struct(e,var_name,fu)
  integer, intent(in) :: fu
  type(t_2de), intent(in) :: e(:)
  character(len=*), intent(in) :: var_name
 
  integer :: ie
  character(len=*), parameter :: &
    this_sub_name = 'write_h2d_elem_struct'
 
   write(fu,'(a,a)')    '# name: ',var_name
   write(fu,'(a)')      '# type: struct'
   call write_dims()
   write(fu,'(a)')      '# length: 12' ! number of fields

   ! field 01 : i
   write(fu,'(a)')      '# name: i'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do ie=1,size(e)
     call write_octave(e(ie)%i,'<cell-element>',fu)
   enddo

   ! field 02 : poly
   write(fu,'(a)')      '# name: poly'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do ie=1,size(e)
     call write_octave(e(ie)%poly,'<cell-element>',fu)
   enddo

   ! field 03 : iv
   write(fu,'(a)')      '# name: iv'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do ie=1,size(e)
     call write_octave(e(ie)%iv,'r','<cell-element>',fu)
   enddo

   ! field 04 : is
   write(fu,'(a)')      '# name: is'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do ie=1,size(e)
     call write_octave(e(ie)%is,'r','<cell-element>',fu)
   enddo

   ! field 05 : pi
   write(fu,'(a)')      '# name: pi'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do ie=1,size(e)
     call write_octave(e(ie)%pi,'r','<cell-element>',fu)
   enddo

   ! field 06 : ip
   write(fu,'(a)')      '# name: ip'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do ie=1,size(e)
     call write_octave(e(ie)%ip,'r','<cell-element>',fu)
   enddo

   ! field 07 : ie
   write(fu,'(a)')      '# name: ie'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do ie=1,size(e)
     call write_octave(e(ie)%ie,'r','<cell-element>',fu)
   enddo

   ! field 08 : iel
   write(fu,'(a)')      '# name: iel'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do ie=1,size(e)
     call write_octave(e(ie)%iel,'r','<cell-element>',fu)
   enddo

   ! field 09 : xb
   write(fu,'(a)')      '# name: xb'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do ie=1,size(e)
     call write_octave(e(ie)%xb,'c','<cell-element>',fu)
   enddo

   ! field 10 : vol
   write(fu,'(a)')      '# name: vol'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do ie=1,size(e)
     call write_octave(e(ie)%vol,'<cell-element>',fu)
   enddo

   ! field 11 : n
   write(fu,'(a)')      '# name: n'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do ie=1,size(e)
     call write_octave(e(ie)%n,'<cell-element>',fu)
   enddo

   ! field 12 : me
   write(fu,'(a)')      '# name: me'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do ie=1,size(e)
     call e(ie)%me%write_octave('<cell-element>',fu)
   enddo

 contains

  subroutine write_dims()
   write(fu,'(a)')      '# ndims: 2'
   write(fu,'(i0,a,i0)') 1," ",size(e)
  end subroutine write_dims

 end subroutine write_h2d_elem_struct

!-----------------------------------------------------------------------

end module mod_h2d_grid

