!! Copyright (C) 2009,2010,2011,2012  Marco Restelli
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Marco Restelli                   <marco.restelli@gmail.com>

!> \brief
!!
!! Master element module for the 2D hybrid grid
!!
!! \n
!!
!! This is the counterpart of \c mod_master_el for the case of a 2D
!! hybrid grid. On the one hand, this module is simpler than the
!! general dimension one, because we restrict ourselves to the 2D
!! case. However, some complications arise because different element
!! shapes must be considered. An important consequence is that we
!! distinguish here among a master element to represent the geometry
!! of the element, which is meant to be used in the description of the
!! grid, and another master element which is meant to represent the
!! local shape functions and will be used in the base objects. The
!! reason for making this distinction is that each element in the grid
!! will have to include a master element object, so that the amount of
!! information in such a type must be reduced as much as possible and
!! should not include duplicated information like the shape functions
!! (for the type \fref{mod_master_el,t_me}, a single master element is
!! shared by a whole grid, so that this problem does not exist).
!!
!! \section h2d_master_el_sides Side ordering
!!
!! According to \ref h2d_g_local_orient, sides are defined by each
!! pair of consecutive vertexes in the list of the element vertexes,
!! and are generated for each element using the template provided by
!! \field_fref{mod_h2d_master_el,c_h2d_me_base,snodes}.
!!
!! The side ordering defined by
!! \field_fref{mod_h2d_master_el,c_h2d_me_base,snodes} is arbitrary,
!! provided that it is consistent with the above assumption. For
!! instance, for the pentagon shown \ref h2d_g_local_orient "here",
!! valid \field_fref{mod_h2d_master_el,c_h2d_me_base,snodes} could be
!! \f$\left[\begin{array}{cccccc} 1 & 2 & 3 & 4 & 5 & 6 \\ 2 & 3 & 4 &
!! 5 & 6 & 1 \end{array}\right]\f$ as well as 
!! \f$\left[\begin{array}{cccccc} 1 & 6 & 2 & 5 & 3 & 4 \\ 2 & 1 & 3 &
!! 6 & 4 & 5 \end{array}\right]\f$ and also
!! \f$\left[\begin{array}{cccccc} 1 & 1 & 3 & 3 & 5 & 5 \\ 2 & 6 & 2 &
!! 4 & 4 & 6 \end{array}\right]\f$. However,
!! \f$\left[\begin{array}{cccccc} 1 & 2 & 3 & 4 & 5 & 6 \\ 3 & 1 & 4 &
!! 5 & 6 & 2 \end{array}\right]\f$ would not be a valid side
!! definition, since it defines sides with nonconsecutive nodes:
!! \f$(1,3)\f$ and \f$(6,2)\f$.
!!
!! \section h2d_master_el_lagrnodes Lagrangian nodes
!!
!! This module, together with \c mod_h2d_cgdofs, is responsible for
!! the definition of the Lagrangian nodes for continuous finite
!! elements. In general terms, we have to ensure that each element
!! receives its global degrees of freedom in the proper order,
!! consistently with the definition of the local bases. Since the
!! global degrees of freedom are attached to various grid entities, we
!! must also ensure some consistency with \c mod_h2d_grid.
!!
!! In 2D, Lagrangian nodes are attached to vertexes, sides and
!! elements. Let us consider each case separately.
!!
!! \subsection h2d_master_el_vertx_dofs Vertex degrees of freedom
!!
!! A local vertex numbering is defined for each element in \c
!! mod_h2d_grid. The correspondence between Lagrangian nodes and
!! geometrical vertexes results from the combination of the element
!! map, given in \field_fref{mod_h2d_master_el,c_h2d_me_geom,map}, and
!! the local Lagrangian nodes, specified in
!! \field_fref{mod_h2d_master_el,c_h2d_me_base,xnodes}. This module
!! guarantees that the first \field_fref{mod_h2d_grid,t_2de,poly}
!! Lagrangian nodes, once mapped from the base master element into the
!! physical space, coincide with the vertexes listed in
!! \field_fref{mod_h2d_grid,t_2de,v}.
!!
!! \subsection h2d_master_el_side_dofs Side degrees of freedom
!!
!! Side degrees of freedom follow the vertex ones in
!! \field_fref{mod_h2d_master_el,c_h2d_me_base,xnodes}, ordered
!! according to the side ordering. The side ordering, as discussed
!! \ref h2d_master_el_sides "above", is controlled by
!! \field_fref{mod_h2d_master_el,c_h2d_me_base,snodes} and
!! consistently followed in \field_fref{mod_h2d_grid,t_2de,s}.
!!
!! On each side, an intrinsic orientation is defined by the side
!! vertexes listed in \field_fref{mod_h2d_grid,t_2ds,v}, and the
!! permutation from this intrinsic orientation and the one induced by
!! each connected element is specified in
!! \field_fref{mod_h2d_grid,t_2de,pi} and
!! \field_fref{mod_h2d_grid,t_2de,ip}. The global degrees of freedom
!! are defined on each side following the side orientation, and on
!! each side of the master element following the orientation given by
!! \field_fref{mod_h2d_master_el,c_h2d_me_base,snodes}. As a result,
!! the same permutations \field_fref{mod_h2d_grid,t_2de,pi} and
!! \field_fref{mod_h2d_grid,t_2de,ip} can be used to assign to each
!! element the global degrees of freedom in the correct order.
!!
!! \subsection h2d_master_el_elem_dofs Element degrees of freedom
!!
!! The global element degrees of freedom are defined mapping the
!! corresponding nodes of the master element with
!! \field_fref{mod_h2d_master_el,c_h2d_me_geom,map}, so that for these
!! nodes there are no ambiguities.
!!
!! \section h2d_master_el_other_comments Other comments
!!
!! \todo This module uses procedures defined in \c mod_master_el and
!! mod_fe_spaces in a rather messy way. In the long term, the whole
!! master element tools should be rearranged, merging this module and
!! \c mod_master_el. Some ideas:
!! <ul>
!!  <li> \c mod_master_el should become a module for the connectivity
!!  of a simplex master element, which will then provide the basis for
!!  the \c me_base_tri element
!!  <li> some (all?) of the functions defined in \c mod_fe_spaces
!!  should be changed to work exclusively on symbolic base functions
!!  and used in this module in the construction of the base master
!!  element
!! </ul>
!<----------------------------------------------------------------------
module mod_h2d_master_el

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   pure_abort, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_octave_io, only: &
   mod_octave_io_initialized, &
   write_octave

 use mod_linal, only: &
   mod_linal_initialized, &
   sort

 use mod_perms, only: &
   mod_perms_initialized, &
   t_perm, perm_table, cart_table, cart_idx

 use mod_octave_io_perms, only: &
   mod_octave_io_perms_initialized, &
   write_octave

 use mod_symfun, only: &
   mod_symfun_initialized, &
   c_symfun, t_funcont, &
   assignment(=),       &
   fnpack, snpack

 use mod_sympoly, only: &
   mod_sympoly_initialized, &
   t_sympol, new_sympoly, &
   assignment(=), f_to_sympoly, &
   operator(**), &
   var_change, &
   f_to_sympoly

 use mod_sympwfun, only: &
   mod_sympwfun_initialized, &
   t_sympwfun, t_sym_dp, new_sympwfun, &
   assignment(=)

 use mod_octave_io_sympoly, only: &
   mod_octave_io_sympoly_initialized, &
   write_octave

 use mod_linal, only: &
   mod_linal_initialized, &
   invmat

 use mod_numquad, only: &
   mod_numquad_initialized, &
   o_get_quad_nodes => get_quad_nodes

 use mod_master_el, only: &
   mod_master_el_initialized, &
   t_me, new_me, clear,   &
   t_lagnodes, lag_nodes

 use mod_fe_spaces, only: &
   mod_fe_spaces_initialized, &
   lagrange_poly, el_nodes

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_h2d_master_el_constructor, &
   mod_h2d_master_el_destructor,  &
   mod_h2d_master_el_initialized, &
   c_h2d_me_geom, c_h2d_me_base, &
   ! allocate the element according to the number of nodes.
   t_elem_spec, me_standard, me_p1_iso_pk, &
   h2d_me_geom_init, h2d_me_base_init, &
   ! Only for testing ----
   mod_h2d_master_el_constructor_test, &
   t_dp_quad_pk_iso_p1

 private

!-----------------------------------------------------------------------

! Module types and parameters

 !> This is the most basic type for a master element
 !!
 !! This type does not provide much information, and indeed it could
 !! be eliminated; its purpose is simply to emphasize that we are
 !! defining a master element.
 type, abstract :: c_h2d_me
 contains
  !> Octave output
  procedure(i_h2d_me_wo), deferred, pass(me) :: write_octave
 end type c_h2d_me

 abstract interface
  subroutine i_h2d_me_wo(me,var_name,fu)
   import :: wp, c_h2d_me
   implicit none
   integer, intent(in) :: fu
   class(c_h2d_me),  intent(in) :: me
   character(len=*), intent(in) :: var_name
  end subroutine i_h2d_me_wo
 end interface

 !> Geometry master element
 !!
 !! This type defines the main methods associated with the geometrical
 !! element, namely destruction and the forward coordinate
 !! transformation \f$\underline{x}=\underline{x}(\underline{\xi})\f$.
 !!
 !! Concerning intialization, since this includes allocating the
 !! master element object, it can not be done with a type bound
 !! procedure: for this, we provide the subroutine \c h2d_me_geom_init
 !! which works for all the possible shapes.
 type, extends(c_h2d_me), abstract :: c_h2d_me_geom
 contains
  !> Finalizes and deallocate the element
  procedure(i_h2d_me_geom_clear), deferred, nopass :: clear
  !> Computes \f$\underline{x}=\underline{x}(\underline{\xi})\f$.
  procedure(i_h2d_me_geom_map),   deferred, pass(me) :: map
  !> Scale the Gaussian weights (or any other value) by the Jacobian
  !! determinant.
  !!
  !! \note If you only need the value of the Jacobian at selected
  !! points, pass 1 as \c wg.
  procedure(i_h2d_me_geom_sw),    deferred, pass(me) :: scale_wg
  !> Apply the push-forward transformation to gradients defined on the
  !! reference element.
  !!
  !! Using the relation \f$\varphi(\underline{x})=\widehat{\varphi}(
  !! \underline{\xi} )\f$, we arrive at the transformation
  !! \f{displaymath}{
  !!   \nabla\varphi = \frac{d\underline{x}}{d\underline{\xi}}^{-T}
  !!   \widehat{\nabla}\widehat{\varphi},
  !! \f}
  !! which is the operation provided by this function.
  procedure(i_h2d_me_geom_sgp),   deferred, pass(me) :: scale_gradp
 end type c_h2d_me_geom

 !> Base master element
 !!
 !! This type defines the main methods and fields associated with the
 !! definition, representation and manipulation of the local shape
 !! functions.
 !!
 !! Most of these fields are analogous to those given in
 !! \fref{mod_base,t_base}.
 !!
 !! \todo The fields describing the geometry are extremely reduced
 !! compared with what is available in \c mod_master_el. In the
 !! future, one should include a more descriptive object analogous to
 !! the present \c t_me, but suitable also for quadrilaterals, cubes
 !! and so on.
 type, extends(c_h2d_me), abstract :: c_h2d_me_base
  integer :: k !< order (has a different meaning for different basis)
  ! Geometry of the reference element ----------------------------------
  !> 1D Lagrangian nodes (if any)
  !!
  !! These are the internal 1D side Lagrangian nodes; vertexes are
  !! omitted since it is easier to handle them separately.
  !!
  !! For consistency with \c mod_master_el, we use an allocatable
  !! array. In fact, however, this will always have one element:
  !! describing the internal nodes of the 1-faces.
  type(t_lagnodes), allocatable :: s_lagnodes(:)
  !> Lagrangian nodes
  !!
  !! See the \ref h2d_master_el_lagrnodes "module documentation" for
  !! details concerning this field.
  real(wp), allocatable :: xnodes(:,:)
  !> side nodes
  !!
  !! See the \ref h2d_master_el_sides "module documentation" for
  !! additional details.
  integer,  allocatable :: snodes(:,:) !< side nodes
  !> points at which the outward normal is given
  real(wp), allocatable :: nmlpts(:,:)
  real(wp), allocatable :: normls(:,:) !< outward normals
  ! Gaussian quadrature ("nodes" means actually "Gaussian nodes") ------
  integer :: m  !< \# of nodes in \f$\hat{K}\f$
  integer :: ms !< \# of side nodes on \f$s\subset\partial\hat{K}\f$
  integer :: deg  !< exact degree in \f$\hat{K}\f$
  integer :: degs !< exact degree on \f$\partial\hat{K}\f$
  real(wp), allocatable :: xig(:,:)  !< nodes in \f$\hat{K}\f$
  real(wp), allocatable :: wg(:)     !< weights in \f$\hat{K}\f$
  type(t_perm), allocatable :: pi_tab(:) !< side permutation table
  integer,  allocatable :: stab(:,:) !< side table
  real(wp), allocatable :: xigs(:,:) !< nodes in \f$s\f$
  real(wp), allocatable :: wgs(:)    !< weights in \f$s\f$
  real(wp), allocatable :: xigb(:,:,:)!< nodes on \f$\partial\hat{K}\f$
  ! Primal basis -------------------------------------------------------
  integer :: pk !< \# of basis functions \f$\varphi\f$
  type(t_funcont), allocatable :: p_s(:)      !< \f$\varphi\f$ (pk)
  type(t_funcont), allocatable :: gradp_s(:,:)!< \f$\nabla\varphi\f$
  real(wp), allocatable :: p(:,:)      !< \f$\varphi(\xi_G)\f$
  real(wp), allocatable :: gradp(:,:,:)!< \f$\nabla\varphi(\xi_G)\f$
  real(wp), allocatable :: pb(:,:,:)   !< \f$\varphi(\xi_{G_b})\f$
  !> \f$\nabla\varphi(\xi_{G_b})\f$
  real(wp), allocatable :: gradpb(:,:,:,:)
  ! Side bubble functions ----------------------------------------------
  integer :: bk !< \# of bubble functions per side \f$\beta\f$
  type(t_sympol), allocatable :: b_s(:,:)       !< \f$\beta\f$
  type(t_sympol), allocatable :: gradb_s(:,:,:) !< \f$\nabla\beta\f$
  real(wp), allocatable :: b(:,:,:)      !< \f$\beta(\xi_G)\f$
  real(wp), allocatable :: gradb(:,:,:,:)!< \f$\nabla\beta(\xi_G)\f$
  real(wp), allocatable :: bb(:,:,:,:)   !< \f$\beta(\xi_{G_b})\f$
  !> \f$\nabla\beta(\xi_{G_b})\f$
  real(wp), allocatable :: gradbb(:,:,:,:,:)
 contains
  !> Finalizes and deallocate the element
  procedure(i_h2d_me_base_clear), deferred, nopass :: clear
 end type c_h2d_me_base

 !> Available finite element families (see \c t_elem_spec)
 !!
 !! \note Mass lumping is not considered here, since it is better
 !! dealing with it at the point of computing the local matrices. In
 !! any case, however, consider that mass lumping means selecting as
 !! quadrature nodes the nodes of the Lagrangian basis, so that the
 !! resulting weights are the integrals of the Lagrangian basis
 !! functions: all such quantities can be easily computed given the
 !! informations provided by this module.
 enum, bind(c)
  enumerator :: &
   !> standard Pk/Qk polynomial space for tri/quad
   me_standard, &
   !> linear elements on the nodes of a quadratic grid
   me_p1_iso_pk
 endenum

 !> Specify which element should be created
 !!
 !! This simple type collects all the parameters which can be used to
 !! select a specific finite element in the calls to \c
 !! h2d_me_geom_init and \c h2d_me_base_init. This includes the
 !! required quadrature accuracy as well as the specification of the
 !! basis functions: degree, presence of bubble functions and so on.
 type :: t_elem_spec
  !> polynomial order
  integer :: k
  !> exactness of the volume quad. rule
  !!
  !! This field can be set to a negative value, in which case nodes
  !! and weights are taken directly from the corresponding fields of
  !! the master element pointed by
  !! \field_fref{mod_h2d_master_el,t_elem_spec,cloned_me}.
  integer :: deg
  !> exactness of the side quad. rule
  !!
  !! As for \field_fref{mod_h2d_master_el,t_elem_spec,deg}, use a
  !! negative value to refer to
  !! \field_fref{mod_h2d_master_el,t_elem_spec,cloned_me}.
  integer :: degs
  !> special finite element families
  !!
  !! This field is used to specify finite element spaces which would
  !! not be easy to specify using the previous fields. Typically,
  !! using \field_fref{mod_h2d_master_el,t_elem_spec,fe_family}
  !! overrides some or all of the previous settings.
  integer(kind(me_standard)) :: fe_family = me_standard
  !> a \c c_h2d_me_base used as a model to create this one
  !!
  !! This is useful when generating multiple bases which must work
  !! together, for instance sharing the same quadrature formulae. This
  !! pointer is required to be associated only when some of the
  !! previous fields requests it.
  class(c_h2d_me_base), pointer :: cloned_me => null()
 end type t_elem_spec

 ! Specific shapes -----------------------------------------------------

 !> Triangles: see \c mod_grid for all the details
 type, extends(c_h2d_me_geom) :: t_h2d_me_geom_tri
  real(wp), allocatable :: b(:,:)
  real(wp) :: det_b
  real(wp), allocatable :: x0(:)
  real(wp), allocatable :: b_it(:,:) !< \f$ B^{-T}\f$
 contains
  procedure, nopass :: clear          => geom_tri_clear
  procedure, pass(me) :: map          => geom_tri_map
  procedure, pass(me) :: scale_wg     => geom_tri_scale_wg
  procedure, pass(me) :: scale_gradp  => geom_tri_scale_gradp
  procedure, pass(me) :: write_octave => geom_tri_wo
 end type t_h2d_me_geom_tri

 type, extends(c_h2d_me_base) :: t_h2d_me_base_tri
 contains
  procedure, nopass :: clear => base_tri_clear
  procedure, pass(me) :: write_octave => base_tri_wo
 end type t_h2d_me_base_tri

 !> Quadrilaterals
 !!
 !! As stated \ref h2d_master_el_sides "above", the local geometry and
 !! connectivity, including vertex and side ordering, of the master
 !! element is controlled by the fields
 !! \field_fref{mod_h2d_master_el,c_h2d_me_base,xnodes} and
 !! \field_fref{mod_h2d_master_el,c_h2d_me_base,snodes}.
 !!
 !! For a quadrilateral element, these are defined in \c
 !! base_init_quad. The resulting coordinate transformation mapping
 !! the master element on a physical one is
 !! \f[
 !!  \underline{x} =
 !!  \sum_{i=1}^4\varphi_i(\underline{\xi})\underline{v}_i
 !! \f]
 !! where \f$\underline{v}_i\f$ are the coordinates of the four
 !! vertexes of the physical element and the shape functions are
 !! \f[
 !!  \varphi_2 = \xi_1(1-\xi_2), \quad
 !!  \varphi_3 = \xi_1\xi_2, \quad
 !!  \varphi_4 = \xi_2(1-\xi_1), \quad
 !!  \varphi_1 = 1- \varphi_2-\varphi_3-\varphi_4.
 !! \f]
 !! This can be recast in the form
 !! \f[
 !!  \underline{x} = \underline{v}_1 + B\underline{\xi} +
 !!  \underline{c}\,\xi_1\xi_2
 !! \f]
 !! with
 !! \f[
 !!  B = \left[ \underline{v}_2-\underline{v}_1 ,
 !!  \underline{v}_4-\underline{v}_1 \right], \quad
 !!  \underline{c} = (\underline{v}_1 + \underline{v}_3) - 
 !!   (\underline{v}_2 + \underline{v}_4).
 !! \f]
 !! One also has
 !! \f[
 !!  \frac{d\underline{x}}{d\underline{\xi}} = B + \left[
 !!  \underline{c}\xi_2,\underline{c}\xi_1 \right].
 !! \f]
 type, extends(c_h2d_me_geom) :: t_h2d_me_geom_quad
  real(wp), allocatable :: v1(:)
  real(wp), allocatable :: b(:,:)
  real(wp), allocatable :: c(:)
 contains
  procedure, nopass :: clear          => geom_quad_clear
  procedure, pass(me) :: map          => geom_quad_map
  procedure, pass(me) :: scale_wg     => geom_quad_scale_wg
  procedure, pass(me) :: scale_gradp  => geom_quad_scale_gradp
  procedure, pass(me) :: write_octave => geom_quad_wo
 end type t_h2d_me_geom_quad

 type, extends(c_h2d_me_base) :: t_h2d_me_base_quad
 contains
  procedure, nopass :: clear => base_quad_clear
  procedure, pass(me) :: write_octave => base_quad_wo
 end type t_h2d_me_base_quad

 ! XXXXXXXX XXXXXX -----------------------------------------------------

 ! Geometry master element
 abstract interface
  pure subroutine i_h2d_me_geom_clear(me)
   import :: c_h2d_me_geom
   implicit none
   class(c_h2d_me_geom), allocatable, intent(inout) :: me
  end subroutine i_h2d_me_geom_clear
 end interface

 abstract interface
  pure function i_h2d_me_geom_map(me,csi) result(x)
   import :: wp, c_h2d_me_geom
   implicit none
   class(c_h2d_me_geom), intent(in) :: me
   real(wp), intent(in) :: csi(:,:)
   real(wp) :: x(size(csi,1),size(csi,2))
  end function i_h2d_me_geom_map
 end interface

 abstract interface
  pure function i_h2d_me_geom_sw(me,xig,wg) result(swg)
   import :: wp, c_h2d_me_geom
   implicit none
   class(c_h2d_me_geom), intent(in) :: me
   real(wp), intent(in) :: xig(:,:), wg(:)
   real(wp) :: swg(size(wg))
  end function i_h2d_me_geom_sw
 end interface

 abstract interface
  pure function i_h2d_me_geom_sgp(me,xig,gradp) result(sgradp)
   import :: wp, c_h2d_me_geom
   implicit none
   class(c_h2d_me_geom), intent(in) :: me
   real(wp), intent(in) :: xig(:,:), gradp(:,:,:)
   real(wp) :: sgradp(2,size(gradp,2),size(gradp,3))
  end function i_h2d_me_geom_sgp
 end interface

 ! Base master element
 abstract interface
  pure subroutine i_h2d_me_base_clear(me)
   import :: c_h2d_me_base
   implicit none
   class(c_h2d_me_base), allocatable, intent(inout) :: me
  end subroutine i_h2d_me_base_clear
 end interface

 ! Piecewise polynomial definition -------------------------------------

 !> Partition of a triangular element into local subelements
 type, extends(t_sym_dp) :: t_dp_tri_pk_iso_p1
  integer :: loc_ne
  integer, allocatable :: loc_t(:,:)
  real(wp), allocatable ::  b(:,:,:)
  real(wp), allocatable :: bi(:,:,:)
  real(wp), allocatable :: x0(  :,:)
  !?? procedure(i_sym_dp_woct),  pointer, pass(dp) :: write_octave => null()
 contains
  procedure, pass(dp) :: x2reg   => tri_dp_x2reg
  procedure, pass(a)  :: compare => tri_dp_compare
  procedure, pass(dp) :: tri_dp_setup !< subdomain setup
  procedure, pass(dp) :: tri_zoom     !< coordinate transformation
 end type t_dp_tri_pk_iso_p1

 !> Partition of a quadrilateral element into local subelements
 !!
 !! The local regions are assumed to constitute a regular grid with
 !! arbitrary spacing in \f$x\f$ and \f$y\f$. The regions however can
 !! have an arbitrary ordering
 type, extends(t_sym_dp) :: t_dp_quad_pk_iso_p1
  integer :: loc_ne
  integer,  allocatable :: loc_t(:,:)
  integer :: nreg1 !< \# of 1D regions
  real(wp), allocatable :: lbreg(:,:) !< left region boundaries
  real(wp), allocatable :: rbreg(:,:) !< right region boundaries
  integer,  allocatable :: reg2reg(:,:) !< 1D "parent" regions
  !?? procedure(i_sym_dp_woct),  pointer, pass(dp) :: write_octave => null()
 contains
  procedure, pass(dp) :: x2reg   => quad_dp_x2reg
  procedure, pass(a)  :: compare => quad_dp_compare
  procedure, pass(dp) :: quad_dp_setup !< subdomain setup
  procedure, pass(dp) :: quad_zoom     !< coordinate transformation
 end type t_dp_quad_pk_iso_p1

 ! XXXXXXXXX XXXXXXXXXX XXXXXXXXXX -------------------------------------

! Module variables

 procedure(o_get_quad_nodes), pointer :: get_quad_nodes => null()

 ! public members
 logical, protected ::               &
   mod_h2d_master_el_initialized = .false.
 character(len=*), parameter :: &
   this_mod_name = 'mod_h2d_master_el'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_h2d_master_el_constructor()
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false.) .or. &
          (mod_kinds_initialized.eqv..false.) .or. &
      (mod_octave_io_initialized.eqv..false.) .or. &
          (mod_linal_initialized.eqv..false.) .or. &
          (mod_perms_initialized.eqv..false.) .or. &
(mod_octave_io_perms_initialized.eqv..false.) .or. &
         (mod_symfun_initialized.eqv..false.) .or. &
        (mod_sympoly_initialized.eqv..false.) .or. &
(mod_octave_io_sympoly_initialized.eqv..false.) .or. &
          (mod_linal_initialized.eqv..false.) .or. &
        (mod_numquad_initialized.eqv..false.) .or. &
      (mod_master_el_initialized.eqv..false.) .or. &
      (mod_fe_spaces_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_h2d_master_el_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   get_quad_nodes => o_get_quad_nodes

   mod_h2d_master_el_initialized = .true.
 end subroutine mod_h2d_master_el_constructor

 subroutine mod_h2d_master_el_constructor_test(mock_get_quad_nodes)
  procedure(o_get_quad_nodes) :: mock_get_quad_nodes
  character(len=*), parameter :: &
    this_sub_name = 'constructor_test'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false.) .or. &
          (mod_kinds_initialized.eqv..false.) .or. &
   ! not needed if we avoid write -> mod_octave_io_initialized
          (mod_perms_initialized.eqv..false.) .or. &
   ! not needed if we avoid write -> mod_octave_io_perms_initialized
         (mod_symfun_initialized.eqv..false.) .or. &
        (mod_sympoly_initialized.eqv..false.) .or. &
   ! not needed if we avoid write -> mod_octave_io_sympoly_initialized
          (mod_linal_initialized.eqv..false.) .or. &
   ! mock -> mod_numquad_initialized
      (mod_master_el_initialized.eqv..false.) .or. &
      (mod_fe_spaces_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_h2d_master_el_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   get_quad_nodes => mock_get_quad_nodes

   mod_h2d_master_el_initialized = .true.
 end subroutine mod_h2d_master_el_constructor_test

!-----------------------------------------------------------------------
 
 subroutine mod_h2d_master_el_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_h2d_master_el_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   get_quad_nodes => null()

   mod_h2d_master_el_initialized = .false.
 end subroutine mod_h2d_master_el_destructor

!-----------------------------------------------------------------------

 !> Allocate a master element
 !!
 !! \note This function can not be pure because of the argument \c me.
 subroutine h2d_me_geom_init(me,x)
  class(c_h2d_me_geom), allocatable, intent(out) :: me
  real(wp), intent(in) :: x(:,:) !< nodal coordinates

  integer :: i
  character(len=*), parameter :: &
    this_sub_name = 'h2d_me_geom_init'

   poly_vertexes: select case(size(x,2))

    case(3)
     allocate( t_h2d_me_geom_tri::me )
     select type(me); class is(t_h2d_me_geom_tri)
      allocate( me%b(2,2) , me%x0(2) , me%b_it(2,2) )
      do i=1,2
        me%b(:,i) = x(:,i+1)-x(:,1)
      enddo
      me%x0 = x(:,1)
      ! the absolute value of the det will be included later, after
      ! using the signed value to compute the inverse
      me%det_b = me%b(1,1)*me%b(2,2) - me%b(1,2)*me%b(2,1)
      ! Inverse 2x2 matrix
      me%b_it(1,1) =  me%b(2,2); me%b_it(1,2) = -me%b(1,2)
      me%b_it(2,1) = -me%b(2,1); me%b_it(2,2) =  me%b(1,1)
      me%b_it = transpose( me%b_it/me%det_b )
      me%det_b = abs( me%det_b )
     end select

    case(4)
     allocate( t_h2d_me_geom_quad::me )
     select type(me); class is(t_h2d_me_geom_quad)
      allocate( me%v1(2) , me%b(2,2) , me%c(2) )
      me%v1 = x(:,1)
      me%b(:,1) = x(:,2)-x(:,1)
      me%b(:,2) = x(:,4)-x(:,1)
      me%c = x(:,1)-x(:,2) + x(:,3)-x(:,4)
     end select

    case default
     ! error: me is left deallocated

   end select poly_vertexes

 end subroutine h2d_me_geom_init

!-----------------------------------------------------------------------

 pure subroutine geom_tri_clear(me)
  class(c_h2d_me_geom), allocatable, intent(inout) :: me

  character(len=*), parameter :: &
    this_sub_name = 'geom_tri_clear'

   select type(me); class is(t_h2d_me_geom_tri)
    deallocate( me%b , me%x0 , me%b_it )
   end select

   ! Done, let us deallocate the element.
   ! Notice: this deallocation must be done outside the select type
   ! construct, because inside it me is not allocatable (see poin 3,
   ! page 260 of "The Fortran Handbook").
   deallocate(me)

 end subroutine geom_tri_clear

!-----------------------------------------------------------------------

 pure function geom_tri_map(me,csi) result(x)
  class(t_h2d_me_geom_tri), intent(in) :: me
  real(wp), intent(in) :: csi(:,:)
  real(wp) :: x(size(csi,1),size(csi,2))

  integer :: i
  character(len=*), parameter :: &
    this_fun_name = 'geom_tri_map'

   x = matmul(me%b,csi)
   do i=1,size(x,1)
     x(i,:) = x(i,:) + me%x0(i)
   enddo

 end function geom_tri_map

!-----------------------------------------------------------------------

 pure function geom_tri_scale_wg(me,xig,wg) result(swg)
  class(t_h2d_me_geom_tri), intent(in) :: me
  real(wp), intent(in) :: xig(:,:), wg(:)
  real(wp) :: swg(size(wg))

  character(len=*), parameter :: &
    this_fun_name = 'geom_tri_scale_wg'

   ! very easy: the Jacobian is constant
   swg = me%det_b * wg

 end function geom_tri_scale_wg

!-----------------------------------------------------------------------

 pure function geom_tri_scale_gradp(me,xig,gradp) result(sgradp)
  class(t_h2d_me_geom_tri), intent(in) :: me
  real(wp), intent(in) :: xig(:,:), gradp(:,:,:)
  real(wp) :: sgradp(2,size(gradp,2),size(gradp,3))

  integer :: l
  character(len=*), parameter :: &
    this_fun_name = 'geom_tri_scale_gradp'

   ! very easy: the Jacobian is constant
   do l=1,size(xig,2)
     sgradp(:,:,l) = matmul( me%b_it , gradp(:,:,l) )
   enddo

 end function geom_tri_scale_gradp
 
!-----------------------------------------------------------------------

 pure subroutine geom_quad_clear(me)
  class(c_h2d_me_geom), allocatable, intent(inout) :: me

  character(len=*), parameter :: &
    this_sub_name = 'geom_quad_clear'

   select type(me); class is(t_h2d_me_geom_quad)
    deallocate( me%v1, me%b , me%c )
   end select

   deallocate(me)

 end subroutine geom_quad_clear

!-----------------------------------------------------------------------

 pure function geom_quad_map(me,csi) result(x)
  class(t_h2d_me_geom_quad), intent(in) :: me
  real(wp), intent(in) :: csi(:,:)
  real(wp) :: x(size(csi,1),size(csi,2))

  integer :: i
  character(len=*), parameter :: &
    this_fun_name = 'geom_quad_map'
   
   x = matmul(me%b,csi)
   do i=1,size(x,2)
     x(:,i) = x(:,i) + me%v1 + me%c*product(csi(:,i))
   enddo

 end function geom_quad_map

!-----------------------------------------------------------------------

 pure function geom_quad_scale_wg(me,xig,wg) result(swg)
  class(t_h2d_me_geom_quad), intent(in) :: me
  real(wp), intent(in) :: xig(:,:), wg(:)
  real(wp) :: swg(size(wg))

  real(wp) :: det_j(size(wg))
  character(len=*), parameter :: &
    this_fun_name = 'geom_quad_scale_wg'

   det_j = &
      (me%b(1,1) + me%c(1)*xig(2,:)) * (me%b(2,2) + me%c(2)*xig(1,:)) &
    - (me%b(1,2) + me%c(1)*xig(1,:)) * (me%b(2,1) + me%c(2)*xig(2,:))

   swg = abs(det_j) * wg

 end function geom_quad_scale_wg

!-----------------------------------------------------------------------

 pure function geom_quad_scale_gradp(me,xig,gradp) result(sgradp)
  class(t_h2d_me_geom_quad), intent(in) :: me
  real(wp), intent(in) :: xig(:,:), gradp(:,:,:)
  real(wp) :: sgradp(2,size(gradp,2),size(gradp,3))

  integer :: l
  real(wp) :: det, jac_it(2,2)

  character(len=*), parameter :: &
    this_fun_name = 'geom_quad_scale_gradp'

   ! All the computations must be done on-the-fly
   do l=1,size(xig,2)

     ! 2x2 inverse, including transposition
     jac_it(1,1) =   me%b(2,2) + me%c(2)*xig(1,l)
     jac_it(1,2) = - me%b(2,1) - me%c(2)*xig(2,l)
     jac_it(2,1) = - me%b(1,2) - me%c(1)*xig(1,l)
     jac_it(2,2) =   me%b(1,1) + me%c(1)*xig(2,l)

     ! notice that the 2x2 det formula can be applied also to jac_it
     det = jac_it(1,1)*jac_it(2,2) - jac_it(1,2)*jac_it(2,1)
     jac_it = jac_it / det

     sgradp(:,:,l) = matmul( jac_it , gradp(:,:,l) )
   enddo

 end function geom_quad_scale_gradp
 
!-----------------------------------------------------------------------

 subroutine h2d_me_base_init(me,nn,elem_spec)
  class(c_h2d_me_base), allocatable, intent(out) :: me
  integer, intent(in) :: nn  !< number of vertexes
  type(t_elem_spec), intent(in) :: elem_spec

  character(len=*), parameter :: &
    this_sub_name = 'h2d_me_base_init'

   poly_vertexes: select case(nn)

    case(3)
     allocate( t_h2d_me_base_tri::me )
     select type(me); class is(t_h2d_me_base_tri)
      call base_init_tri(me,elem_spec)
     end select

    case(4)
     allocate( t_h2d_me_base_quad::me )
     select type(me); class is(t_h2d_me_base_quad)
      call base_init_quad(me,elem_spec)
     end select

    case default
     ! error: me is left deallocated
     return

   end select poly_vertexes

   ! these fields are not specific
   me%k = elem_spec%k
   if(elem_spec%deg.gt.0) then
     me%deg = elem_spec%deg
   else
     me%deg = elem_spec%cloned_me%deg
   endif

 end subroutine h2d_me_base_init

!-----------------------------------------------------------------------

 subroutine base_init_tri(me,elem_spec)
  class(t_h2d_me_base_tri), intent(out) :: me
  type(t_elem_spec), intent(in) :: elem_spec

  integer :: iel, ivl
  integer, allocatable :: loc_t(:,:)
  real(wp), allocatable :: xig(:,:), wg(:)
  type(t_dp_tri_pk_iso_p1) :: dp
  type(t_sympol) :: p1_s(3) ! P1 polynomial base
  type(t_sympol), allocatable :: p_sl(:) ! local symbolic definitions
  character(len=*), parameter :: &
    this_sub_name = 'base_init_tri'

  integer :: ifort_bug_i
  type(t_sympol), allocatable :: ifort_bug_p_s(:)

   ! 0) preliminary checks
   select case(elem_spec%fe_family)
    case(me_standard)
     ! OK
    case(me_p1_iso_pk)
     ! OK
    case default
     call error(this_sub_name,this_mod_name, 'Unknown FE family.' )
   end select

   ! 1) define the dofs of the Lagrangian base and the side normals
   call get_lag_nodes_from_mod_me( me%s_lagnodes,me%xnodes,me%snodes , &
                                   d=2 , k=elem_spec%k )
   allocate( me%nmlpts(2,3) , me%normls(2,3) ) ! side normals
   ! Todo: compared to how the nodes are computed in
   ! get_lag_nodes_from_mod_me, this part of the normals is somewhat
   ! "ad hoc" and could be improved.
   me%nmlpts(1,:) = (/ 0.5_wp , 0.0_wp , 0.5_wp /)
   me%nmlpts(2,:) = (/ 0.0_wp , 0.5_wp , 0.5_wp /)
   me%normls(1,:) = (/  0.0_wp ,-1.0_wp , 1.0_wp/sqrt(2.0_wp) /)
   me%normls(2,:) = (/ -1.0_wp , 0.0_wp , 1.0_wp/sqrt(2.0_wp) /)

   ! 2) set the triangular quadrature rule: me%xig, me%wg
   ! -- for P1-iso-Pk also define dp, the domain partitioning
   clone_quad_rule_if: if(elem_spec%deg.gt.0) then
     select case(elem_spec%fe_family)

      case(me_standard)
       call get_quad_nodes(2,elem_spec%deg,me%xig,me%wg)

      case(me_p1_iso_pk)
       ! 2.0) set the procedure pointer methds
       dp%write_octave => tri_dp_woct
       ! 2.1) construct the local grid
       call build_local_grid( loc_t , elem_spec%k,me%xnodes )
       ! 2.2) define the local partitioning
       call dp%tri_dp_setup( loc_t , me%xnodes )
       deallocate( loc_t ) ! has been copied in dp%loc_t
       ! 2.3) map the nodes on each local element
       call get_quad_nodes(2,elem_spec%deg,xig,wg)
       call dp%tri_zoom( me%xig , xig )
       ! 2.4) set the weights, using that the local elements are equal
       me%wg = reshape( spread( wg/real(dp%loc_ne,wp) , 2,dp%loc_ne ), &
                        (/ dp%loc_ne*size(wg) /) )

     end select
   else
     me%xig = elem_spec%cloned_me%xig ! reallocation
     me%wg  = elem_spec%cloned_me%wg  ! reallocation
   endif clone_quad_rule_if
   me%m = size(me%wg)

   ! 3) compute the Lagrangian basis (see also cg in mod_fe_spaces)
   ! Notice: having overloaded assignment to t_funcont with an
   ! elemental subroutine the allocation must be done explicitly.
   allocate( me%p_s(size(me%xnodes,2)) )
   select case(elem_spec%fe_family)

    case(me_standard)
     ! ifort bug - compiler bug : this should be related to
     ! https://software.intel.com/en-us/forums/topic/565015
     !me%p_s = lagrange_poly( 2 , elem_spec%k , me%xnodes )
     allocate(ifort_bug_p_s(size(me%p_s)))
     ifort_bug_p_s = lagrange_poly( 2 , elem_spec%k , me%xnodes )
     do ifort_bug_i=1,size(ifort_bug_p_s)
       me%p_s(ifort_bug_i) = ifort_bug_p_s(ifort_bug_i)
     enddo

    case(me_p1_iso_pk)
     ! Here, we build the p1 base and scale it on each local element.
     ! The local contributions obtained in this way are then assembled
     ! as in the case of a typical continuous finite element method.
     p1_s = lagrange_poly( 2 , 1 , me%xnodes(:,1:3) ) ! vertexes
     ! compiler bug: see
     ! https://gcc.gnu.org/bugzilla/show_bug.cgi?id=67539
     !me%p_s = f_to_sympoly( 0.0_wp )
     do iel=1,size( me%p_s )
       me%p_s(iel) = f_to_sympoly( 0.0_wp )
     enddo
     allocate( p_sl(dp%loc_ne) )
     p_sl = f_to_sympoly( 0.0_wp )
     do iel=1,dp%loc_ne
       do ivl=1,size(p1_s) ! local vertexes
         p_sl(iel) = var_change( p1_s(ivl) , dp%bi(:,:,iel) ,         &
                                 -matmul(dp%bi(:,:,iel),dp%x0(:,iel)) )
         me%p_s( dp%loc_t(ivl,iel) ) = me%p_s( dp%loc_t(ivl,iel) ) &
                                      + new_sympwfun( p_sl , dp )
         p_sl(iel) = f_to_sympoly( 0.0_wp ) ! restore 0
       enddo
     enddo
     deallocate( p_sl )

   end select
   me%pk = size(me%p_s)

   ! 4) now the side bubble basis functions
   me%bk = 0
   allocate( me%b_s(me%bk,3) )
   ! ! For each side, the corresponding bubble function is the product
   ! ! of the nodal bases of the two vertexes - up to normalization,
   ! ! which will be done later.
   ! do isl=1,3
   !   associate( snod => me%snodes(:,isl) )
   !   select case(elem_spec%fe_family)
   !    case(me_standard)
   !     me%b_s(1,isl) = me%p_s(snod(1)) * me%p_s(snod(2))
   !    case(me_p1_iso_pk)
   !     me%b_s(1,isl) =   p1_s(snod(1)) *   p1_s(snod(2))
   !   end select
   !   end associate
   ! enddo

   ! 5) fill all the general fields
   call init_common_tri_quad(                                 &
       ! output arguments
       me%pi_tab, me%xigs, me%wgs, me%stab, me%ms,            &
       me%xigb, me%gradp_s, me%p, me%gradp, me%pb, me%gradpb, &
       me%gradb_s, me%b, me%gradb, me%bb, me%gradbb,          &
       ! input arguments
       elem_spec%degs, 3, me%snodes, me%xnodes, me%pk, me%m,  &
       me%xig, me%bk, me%p_s, me%b_s , elem_spec )

 contains

  !> Small subroutine to recover the nodes from \c mod_master_el
  pure subroutine get_lag_nodes_from_mod_me(slgnod,xnodes,snodes , d,k)
   integer, intent(in) :: d, k 
   type(t_lagnodes), allocatable, intent(out) :: slgnod(:)
   real(wp),         allocatable, intent(out) :: xnodes(:,:)
   integer,          allocatable, intent(out) :: snodes(:,:)

   type(t_lagnodes), allocatable :: nodes(:) ! 1D and 2D nodes
   type(t_me) :: tri_me

    ! This computes the Lagrangian nodes for each sub-simplex
    call lag_nodes( nodes , d , k )

    ! The side nodes can be copied directly
    allocate( slgnod(1) )
    slgnod(1) = nodes(1)

    ! The complete node set must still be assembled
    call new_me( tri_me , d )
    call el_nodes( tri_me , nodes , x=xnodes )

    ! Finally, get the vertexes for each side (they are the first nodes)

    ! note: as discussed in mod_master_el::t_me::children, there are
    ! two definitions for the element sides: isv and children(d-1),
    ! and these definitions will not be the same. Here, it is
    ! important to use the same definition which is used in el_nodes
    ! to define the Lagrangian side nodes, which turns out to be
    ! children(1). This ensures that the Lagrangian nodes on the
    ! element sides stored in xnodes are consistent with the side
    ! numbering defined by snodes.
    snodes = tri_me%children(1)%s

  end subroutine get_lag_nodes_from_mod_me

  ! Build the local grid
  !
  ! The algorithm used here is limited to 2D - things are not so easy
  ! in arbitrary dimension. Here, we generate all the up and down
  ! triangles contained in the unit square a take those which vertexes
  ! are local Lagrangian nodes.
  !
  ! The algorithm works thanks to the following observations:
  ! 1) the two families of up and down triangles cover the whole unit
  ! square without overlap
  ! 2) these elements are either inside or outside the unit simplex,
  ! but never partly inside and partly outside
  !
  ! The difficulty in higher dimensions is generating all the "small"
  ! simplexes covering the hypercube and satisfying 1) and 2) above.
  pure subroutine build_local_grid( loc_t , k,xnodes )
   integer,              intent(in)  :: k
   real(wp),             intent(in)  :: xnodes(:,:)
   integer, allocatable, intent(out) :: loc_t(:,:)

   integer, parameter :: d = 2
   real(wp), parameter :: &
     x_down(2,3) = reshape( (/ 0.0_wp , 0.0_wp ,            &
                               1.0_wp , 0.0_wp ,            &
                               0.0_wp , 1.0_wp /) , (/2,3/) ), &
     x_up(2,3)   = reshape( (/ 1.0_wp , 1.0_wp ,            &
                               0.0_wp , 1.0_wp ,            &
                               1.0_wp , 0.0_wp /) , (/2,3/) )
   
   integer :: loc_ne

    loc_ne = k**d ! number of local elements
    allocate( loc_t(d+1,loc_ne) )

    loc_ne = 0

    ! down triangles
    call build_local_grid_internals( loc_ne,loc_t , &
                       d, x_down/real(k,wp), xnodes )

    ! up triangles
    call build_local_grid_internals( loc_ne,loc_t , &
                       d, x_up/real(k,wp)  , xnodes )

  end subroutine build_local_grid

  pure subroutine build_local_grid_internals(loc_ne,loc_t , d,xn,xnodes)
   integer,  intent(in)    :: d
   real(wp), intent(in)    :: xn(:,:)
   real(wp), intent(in)    :: xnodes(:,:)
   integer,  intent(inout) :: loc_ne
   integer,  intent(inout) :: loc_t(:,:)

   real(wp), parameter :: toll = 1.0e3_wp*epsilon(0.0_wp)

   integer :: in,jn, v_match(d+1), iv
   real(wp) :: xv(d)

    do in=1,size(xnodes,2) ! loop over the local nodes

      ! translate the local element in xnodes(in) and compare
      v_match = -1
      do iv=1,d+1
        xv = xnodes(:,in) + xn(:,iv)
        match_do: do jn=1,size(xnodes,2)
          if(norm2(xv-xnodes(:,jn)).le.toll) then
            v_match(iv) = jn
            exit match_do
          endif
        enddo match_do
      enddo
      if( all(v_match.gt.0) ) then ! found a local element
        loc_ne = loc_ne + 1
        loc_t(:,loc_ne) = v_match
      endif
      
    enddo

  end subroutine build_local_grid_internals

 end subroutine base_init_tri

!-----------------------------------------------------------------------

 subroutine base_init_quad(me,elem_spec)
  class(t_h2d_me_base_quad), intent(out) :: me
  type(t_elem_spec), intent(in) :: elem_spec

  ! these are some arrays used to build the tensor product base
  ! functions
  real(wp), parameter :: &
    mat_a(1,2) = reshape( (/0.0_wp,1.0_wp/) , (/1,2/) ) , &
    vec_b( 1 ) = (/ 0.0_wp /)

  integer :: i,j, l, iel
  integer,  allocatable :: nodij(:,:)
  real(wp), allocatable :: xig1(:,:), wg1(:), xig(:,:), wg(:)
  real(wp) :: loc_xy(2,2)
  type(t_dp_quad_pk_iso_p1) :: dp
  type(t_funcont), allocatable :: b1d(:,:) ! 1D basis functions in x,y
  type(t_sympol), allocatable :: p_sl(:) ! local symbolic definitions
  type(t_sympol) :: eb ! element bubble

  integer :: ifort_bug_i
  type(t_sympol), allocatable :: ifort_bug_p_s(:)

  character(len=*), parameter :: &
    this_sub_name = 'base_init_quad'

   ! 0) preliminary checks
   select case(elem_spec%fe_family)
    case(me_standard)
     ! OK
    case(me_p1_iso_pk)
     ! OK
    case default
     call error(this_sub_name,this_mod_name, 'Unknown FE family.' )
   end select

   ! 1) define the dofs of the Lagrangian base and the side normals
   call lag_nodes( me%s_lagnodes , d=1 , k=elem_spec%k )
   allocate( me%xnodes( 2 , (2+me%s_lagnodes(1)%nn)**2 ) , &
             me%snodes( 2 ,  4                         ) )
   ! nodij is used to track the Cartesian coordinates of each node;
   ! this is useful to generate the tensor product base functions
   ! without solving a 2D Vandermonde system.
   allocate( nodij(2,size(me%xnodes,2)) )
   ! 1.1) vertexes
   !  -> these must be consistent with the map in t_h2d_me_geom_quad
   me%xnodes(1,1:4) = (/ 0.0_wp , 1.0_wp , 1.0_wp , 0.0_wp /)
   me%xnodes(2,1:4) = (/ 0.0_wp , 0.0_wp , 1.0_wp , 1.0_wp /)
   nodij(    1,1:4) = (/ 1 , 2 , 2 , 1 /)
   nodij(    2,1:4) = (/ 1 , 1 , 2 , 2 /)
   ! 1.2) side vertexes: these are arbitrary, as long as consecutive
   me%snodes(1, : ) = (/ 1 , 4 , 1 , 2 /)
   me%snodes(2, : ) = (/ 2 , 3 , 4 , 3 /)
   ! 1.3) side nodes (if any)
   associate( ni1 => me%s_lagnodes(1)%nn   , & ! # of internal 1D nodes
              xi1 => me%s_lagnodes(1)%x(1,:) ) ! internal 1D nodes
   do j=1,4 ! side loop
     associate( is => 4+(j-1)*ni1+  1 , & ! start and end indexes
                ie => 4+(j-1)*ni1+ni1 , &
                ns => me%snodes(1,j)  , & ! start and end nodes
                ne => me%snodes(2,j)  )
     do i=1,2 ! space dimension
       me%xnodes(i, is:ie ) = & ! map 1D -> 2D
                             (1.0_wp-xi1)*me%xnodes(i,ns) &
                            +        xi1 *me%xnodes(i,ne)
       ! nodij here is not obvious; check the varying coord on the side
       if(     nodij(i,ns) .eq. nodij(i,ne) ) then
         nodij(  i, is:ie ) = nodij(i,ns)
       elseif( nodij(i,ns) .lt. nodij(i,ne) ) then
         nodij(  i, is:ie ) = 2 + (/ (i , i=1,ni1,+1) /)
       else
         nodij(  i, is:ie ) = 2 + (/ (i , i=ni1,1,-1) /)
       endif
     enddo
     end associate
   enddo
   ! 1.4) internal nodes (if any)
   !  -> these are obtained taking the tensor product - arbitrary order
   do j=1,ni1
     do i=1,ni1
       associate( ii => 4 + 4*ni1 + (j-1)*ni1 + i )
       me%xnodes(1, ii ) = xi1(i) ! no need to map
       me%xnodes(2, ii ) = xi1(j)
       nodij(    1, ii ) = 2+i
       nodij(    2, ii ) = 2+j
       end associate
     enddo
   enddo
   end associate
   ! 1.5) side normals
   allocate( me%nmlpts(2,4) , me%normls(2,4) )
   me%nmlpts(1,:) = (/ 0.5_wp , 0.5_wp , 0.0_wp , 1.0_wp /)
   me%nmlpts(2,:) = (/ 0.0_wp , 1.0_wp , 0.5_wp , 0.5_wp /)
   me%normls(1,:) = (/  0.0_wp , 0.0_wp , -1.0_wp , 1.0_wp /)
   me%normls(2,:) = (/ -1.0_wp , 1.0_wp ,  0.0_wp , 0.0_wp /)

   ! 2) set the square quadrature rule: me%xig, me%wg
   ! -- for P1-iso-Pk also define dp, the domain partitioning
   clone_quad_rule_if: if(elem_spec%deg.gt.0) then
     call get_quad_nodes(1,elem_spec%deg,xig1,wg1)
     associate( m1 => size(xig1,2) )
     allocate( xig(2,m1**2), wg(m1**2) )
     do j=1,m1
       do i=1,m1
         l = (j-1)*m1 + i
         xig(1,l) = xig1(1,i)
         xig(2,l) = xig1(1,j)
         wg(   l) = wg1(i)*wg1(j)
       enddo
     enddo
     deallocate( xig1 , wg1 )
     end associate

     select case(elem_spec%fe_family)

      case(me_standard)
       call move_alloc( from=xig , to=me%xig )
       call move_alloc( from=wg  , to=me%wg  )

      case(me_p1_iso_pk)
       ! 2.0) set the procedure pointer methds
       dp%write_octave => quad_dp_woct
       ! 2.1) define the local partitioning
       call dp%quad_dp_setup( xn=me%xnodes ,                           &
           xn1=spread( (/ 0.0_wp,1.0_wp,me%s_lagnodes(1)%x(1,:) /),2,2))
       ! 2.2) map the nodes on each local element
       call dp%quad_zoom( me%xig , xig ); deallocate( xig )
       ! 2.3) set the weights, using that the local elements are equal
       me%wg = reshape( spread( wg/real(dp%loc_ne,wp) , 2,dp%loc_ne ) ,&
                        (/ dp%loc_ne*size(wg) /) ); deallocate( wg )

     end select
   else
     me%xig = elem_spec%cloned_me%xig ! reallocation
     me%wg  = elem_spec%cloned_me%wg  ! reallocation
   endif clone_quad_rule_if
   me%m = size(me%wg)

   ! 3) compute the Lagrangian basis
   me%pk = size(me%xnodes,2)
   allocate( me%p_s(me%pk) )
   select case(elem_spec%fe_family)

    case(me_standard)
     ! Compute the 1D base and take the required products
     associate( n1 => me%s_lagnodes(1)%nn+2 ) ! # total 1D nodes
     allocate( b1d(n1,2) ) ! 1D base functions in x and y
     ! ifort bug - compiler bug : this should be related to
     ! https://software.intel.com/en-us/forums/topic/565015
     !b1d(:,1) = lagrange_poly( 1 , elem_spec%k ,                       &
     !   ! note: 0 and 1 are included separately, as before at 1)
     !   reshape((/ 0.0_wp,1.0_wp,me%s_lagnodes(1)%x(1,:) /),(/1,n1/)))
     allocate(ifort_bug_p_s(n1))
     ifort_bug_p_s = lagrange_poly( 1 , elem_spec%k ,                   &
        reshape((/ 0.0_wp,1.0_wp,me%s_lagnodes(1)%x(1,:) /),(/1,n1/)))
     do ifort_bug_i=1,size(ifort_bug_p_s)
       b1d(ifort_bug_i,1) = ifort_bug_p_s(ifort_bug_i)
     enddo
     deallocate(ifort_bug_p_s)
     ! y base functions are the same with x -> y
     do i=1,n1
       b1d(i,2) = var_change( f_to_sympoly(b1d(i,1)) , mat_a , vec_b )
     enddo
     ! now we have to repeat the construction used for the 2D nodes
     do i=1,me%pk
       me%p_s(i) = b1d(nodij(1,i),1)*b1d(nodij(2,i),2)
     enddo
     end associate

    case(me_p1_iso_pk)
     ! We use here the 1st order 1D base, as for the triangles
     allocate( b1d(2,2) )
     !b1d(:,1) = lagrange_poly( 1 , 1 ,             &
     !   reshape( (/ 0.0_wp , 1.0_wp /) , (/1,2/) ) )
        allocate(ifort_bug_p_s(2))
        ifort_bug_p_s = lagrange_poly( 1 , 1 ,             &
           reshape( (/ 0.0_wp , 1.0_wp /) , (/1,2/) ) )
        do ifort_bug_i=1,size(ifort_bug_p_s)
          b1d(ifort_bug_i,1) = ifort_bug_p_s(ifort_bug_i)
        enddo
        deallocate(ifort_bug_p_s)
     do i=1,2
       b1d(i,2) = var_change( f_to_sympoly(b1d(i,1)) , mat_a , vec_b )
     enddo
     ! compiler bug: see
     ! https://gcc.gnu.org/bugzilla/show_bug.cgi?id=67539
     !me%p_s = f_to_sympoly( 0.0_wp )
        do i=1,size( me%p_s )
          me%p_s(i) = f_to_sympoly( 0.0_wp )
        enddo
     allocate( p_sl(dp%loc_ne) )
     p_sl = f_to_sympoly( 0.0_wp )
     ! Note: the region vertexes in loc_t are generated in
     ! quad_dp_setup consecutively, as the element vertexes. This
     ! allows us to reuse for each region the same array nodij used
     ! for the construction of the global base functions.
     do iel=1,dp%loc_ne
       associate( loc_xnodes => me%xnodes(:,dp%loc_t(:,iel)) )
       loc_xy(:,1) = (/ loc_xnodes(1,1) , loc_xnodes(1,2) /)
       loc_xy(:,2) = (/ loc_xnodes(2,1) , loc_xnodes(2,4) /)
       end associate
       do i=1,4
         ! locally we can reuse the first four columns of nodij
         associate( n1 => nodij(1,i) , n2 => nodij(2,i) )
         p_sl(iel) = f_to_sympoly( & ! tensor product shape functions
            var_change( f_to_sympoly( b1d(n1,1) ) ,                  &
                     loc_mat(loc_xy(:,1),1) , loc_b(loc_xy(:,1),1) ) &
          * var_change( f_to_sympoly( b1d(n2,2) ) ,                  &
                     loc_mat(loc_xy(:,2),2) , loc_b(loc_xy(:,2),2) ) )
         me%p_s( dp%loc_t(i,iel) ) = me%p_s( dp%loc_t(i,iel) ) &
                                     + new_sympwfun( p_sl , dp )
         p_sl(iel) = f_to_sympoly( 0.0_wp ) ! restore 0
         end associate
       enddo
     enddo
     deallocate( p_sl )

   end select

   ! 4) now the side bubble basis functions
   ! TODO: one should find a good definition also for p1_iso_pk
   me%bk = 0
   allocate( me%b_s(me%bk,4) )
   ! ! Taking the product of the nodal basis functions, as for the
   ! ! triangular elements, would be sufficient, but we can simplify
   ! ! the function adding the element bubble.
   ! !eb = b1d(1,1) * b1d(2,1) * b1d(1,2) * b1d(2,2)
   ! eb = f_to_sympoly( b1d(1,1)%mult( b1d(2,1)%mult( b1d(1,2)%mult( b1d(2,2) ))) )
   ! call eb%show()
   ! do j=1,4 ! side loop
   !   associate( snod => me%snodes(:,j) )
   !   !me%b_s(1,j) = me%p_s(snod(1)) * me%p_s(snod(2)) + eb
   !   me%b_s(1,j) = f_to_sympoly( eb%add( me%p_s(snod(1))%mult( me%p_s(snod(2))) ))
   !   call me%b_s(1,j)%show()
   !   end associate
   ! enddo
   deallocate( b1d )

   ! 5) fill all the general fields
   call init_common_tri_quad(                                 &
       ! output arguments
       me%pi_tab, me%xigs, me%wgs, me%stab, me%ms,            &
       me%xigb, me%gradp_s, me%p, me%gradp, me%pb, me%gradpb, &
       me%gradb_s, me%b, me%gradb, me%bb, me%gradbb,          &
       ! input arguments
       elem_spec%degs, 4, me%snodes, me%xnodes, me%pk, me%m,  &
       me%xig, me%bk, me%p_s, me%b_s , elem_spec )

 contains

  ! This is a convenient function to avoid too many variables
  pure function loc_mat(lr,ivar) result(a)
   real(wp), intent(in) :: lr(2)
   integer,  intent(in) :: ivar
   real(wp) :: a(2,2)

    a = 0.0_wp
    select case(ivar)
     case(1)
      a(1,1) = 1.0_wp/(lr(2)-lr(1))
      a(2,2) = 1.0_wp
     case(2)
      a(1,1) = 1.0_wp
      a(2,2) = 1.0_wp/(lr(2)-lr(1))
    end select
  end function loc_mat

  ! This is a convenient function to avoid too many variables
  pure function loc_b(lr,ivar) result(b)
   real(wp), intent(in) :: lr(2)
   integer,  intent(in) :: ivar
   real(wp) :: b(2)

    select case(ivar)
     case(1)
      b(1) = -lr(1)/(lr(2)-lr(1))
      b(2) = 0.0_wp
     case(2)
      b(1) = 0.0_wp
      b(2) = -lr(1)/(lr(2)-lr(1))
    end select
  end function loc_b

 end subroutine base_init_quad

!-----------------------------------------------------------------------

 ! This subroutine collects all the operations which are common for
 ! both triangles and quadrilaterals, to avoid code duplications.
 !
 ! The components are passed separately to make the intent explicit.
 subroutine init_common_tri_quad(                        &
    ! output arguments
    pi_tab, xigs, wgs, stab, ms,                         &
    xigb, gradp_s, p, gradp, pb, gradpb,                 &
    gradb_s, b, gradb, bb, gradbb,                       &
    ! input arguments
    degs, poly, snodes, xnodes, pk, m, xig, bk, p_s, b_s,&
    elem_spec )
  integer,         intent(in) :: degs, poly, pk, m, bk
  integer,         intent(in) :: snodes(:,:)
  real(wp),        intent(in) :: xnodes(:,:)
  real(wp),        intent(in) :: xig(:,:)
  type(t_funcont), intent(in) :: p_s(:)
  type(t_sympol),  intent(inout) :: b_s(:,:) ! must be normalized
  type(t_elem_spec), intent(in) :: elem_spec
  ! side quadrature
  type(t_perm),    allocatable, intent(out) :: pi_tab(:)
  real(wp),        allocatable, intent(out) :: xigs(:,:)
  real(wp),        allocatable, intent(out) :: wgs(:)
  integer,         allocatable, intent(out) :: stab(:,:)
  integer,                      intent(out) :: ms
  ! boundary quadrature
  real(wp),        allocatable, intent(out) :: xigb(:,:,:)
  ! gradp_s
  type(t_funcont), allocatable, intent(out) :: gradp_s(:,:)
  ! Lagrangian basis evaluation
  real(wp),        allocatable, intent(out) :: p(:,:)
  real(wp),        allocatable, intent(out) :: gradp(:,:,:)
  real(wp),        allocatable, intent(out) :: pb(:,:,:)
  real(wp),        allocatable, intent(out) :: gradpb(:,:,:,:)
  ! side bubble functions
  type(t_sympol),  allocatable, intent(out) :: gradb_s(:,:,:)
  real(wp),        allocatable, intent(out) :: b(:,:,:)
  real(wp),        allocatable, intent(out) :: gradb(:,:,:,:)
  real(wp),        allocatable, intent(out) :: bb(:,:,:,:)
  real(wp),        allocatable, intent(out) :: gradbb(:,:,:,:,:)

  integer :: h,k, isl,jsl
  real(wp) :: xk1, xk2

   ! 1) Side boundary nodes

   ! notice: in 2D the sides are segments, i.e. 1D simplexes
   call perm_table(pi_tab,2)
   if(degs.gt.0) then
     call get_quad_nodes(1,degs,xigs,wgs,stab)
     if(elem_spec%fe_family.eq.me_p1_iso_pk) then
       ! This could be done better, but for a 1d side it is OK
       xigs = reshape( & ! reallocation
             (/( (xigs(1,:)+real(h-1,wp))/real(elem_spec%k,wp) ,    &
                                               h=1,elem_spec%k )/), &
             (/1,elem_spec%k*size(xigs)/) )
       wgs = (/( wgs/real(elem_spec%k,wp) , h=1,elem_spec%k )/)
       stab = reshape( & ! reallocation
             (/( (/h,size(xigs,2)-h+1/) , h=1,size(xigs,2) )/) , &
             (/2,size(xigs,2)/) )
     endif
   else
     xigs = elem_spec%cloned_me%xigs ! reallocation
     wgs  = elem_spec%cloned_me%wgs
     stab = elem_spec%cloned_me%stab
   endif
   ms = size(wgs)

   ! boundary nodes in 2D space
   allocate( xigb(2,ms,poly) )
   do isl=1,poly
     do k=1,2 ! space dimensions
       xk1 = xnodes(k,snodes(1,isl)) ! nodal k-th coord.
       xk2 = xnodes(k,snodes(2,isl))
       xigb(k,:,isl) = (xk2-xk1)*xigs(1,:) + xk1
     enddo
   enddo

   ! 2) Primal basis: symbolic and evaluated
   allocate( gradp_s(2,pk) )
   do h=1,2
     gradp_s(h,:) = p_s%pderj(h)
   enddo

   allocate( p(pk,m) , gradp(2,pk,m) )
   do k=1,pk
     p(k,:) = p_s(k)%ev( xig )
     do h=1,2
       gradp(h,k,:) = gradp_s(h,k)%ev( xig )
     enddo
   enddo

   allocate( pb(pk,ms,poly) , gradpb(2,pk,ms,poly) )
   do isl=1,poly
     do k=1,pk
       pb(k,:,isl) = p_s(k)%ev( xigb(:,:,isl) )
       do h=1,2
         gradpb(h,k,:,isl) = gradp_s(h,k)%ev( xigb(:,:,isl) )
       enddo
     enddo
   enddo

   ! 3) Side bubble basis functions
   allocate( gradb_s(2,bk,poly) )
   do h=1,2
     gradb_s(h,:,:) = b_s%pderj(h)
   enddo

   allocate( b(bk,poly,m) , gradb(2,bk,poly,m) )
   do isl=1,poly
     do k=1,bk
       b(k,isl,:) = b_s(k,isl)%ev( xig )
       do h=1,2
         gradb(h,k,isl,:) = gradb_s(h,k,isl)%ev( xig )
       enddo
     enddo
   enddo

   allocate( bb(bk,poly,ms,poly) , gradbb(2,bk,poly,ms,poly) )
   do isl=1,poly
     do jsl=1,poly
       do k=1,bk
         bb(k,jsl,:,isl) = b_s(k,jsl)%ev( xigb(:,:,isl) )
         do h=1,2
           gradbb(h,k,jsl,:,isl) = gradb_s(h,k,jsl)%ev( xigb(:,:,isl) )
         enddo
       enddo
     enddo
   enddo

   ! 3.1) we can now normalize the bubble functions
   do isl=1,poly
     do k=1,bk
       associate( xn1 => xnodes(:,snodes(1,isl)) , &
                  xn2 => xnodes(:,snodes(2,isl)) )
       xk1 = sqrt( sum( (xn2 - xn1)**2 ) ) &  !  \int_s beta ds
             * dot_product( wgs ,  bb(k,isl,:,isl) )
       end associate
       xk2 = 1.0_wp/xk1 ! normalization coefficients
       b_s(      k,isl)     = xk2 * b_s(      k,isl)
       !gradb_s(:,k,isl)     = xk2 * gradb_s(:,k,isl)
       gradb_s(:,k,isl)     = gradb_s(:,k,isl)%mult_s(xk2)
       b(        k,isl,:)   = xk2 * b(        k,isl,:)
       gradb(  :,k,isl,:)   = xk2 * gradb(  :,k,isl,:)
       bb(       k,isl,:,:) = xk2 * bb(       k,isl,:,:)
       gradbb( :,k,isl,:,:) = xk2 * gradbb( :,k,isl,:,:)
     enddo
   enddo

 end subroutine init_common_tri_quad

!-----------------------------------------------------------------------

 pure subroutine base_tri_clear(me)
  class(c_h2d_me_base), allocatable, intent(inout) :: me

  character(len=*), parameter :: &
    this_sub_name = 'base_tri_clear'

   deallocate( me%xnodes , me%snodes )
   deallocate( me%xig,me%wg , me%pi_tab,me%stab,me%xigs,me%wgs,me%xigb )
   deallocate( me%p_s,me%gradp_s , me%p,me%gradp , me%pb,me%gradpb )

   deallocate(me)

 end subroutine base_tri_clear

!-----------------------------------------------------------------------

 pure subroutine base_quad_clear(me)
  class(c_h2d_me_base), allocatable, intent(inout) :: me

  character(len=*), parameter :: &
    this_sub_name = 'base_quad_clear'

   deallocate( me%xnodes , me%snodes )
   deallocate( me%xig,me%wg , me%pi_tab,me%stab,me%xigs,me%wgs,me%xigb )
   deallocate( me%p_s,me%gradp_s , me%p,me%gradp , me%pb,me%gradpb )

   deallocate(me)

 end subroutine base_quad_clear

!-----------------------------------------------------------------------

 !> Locate the subelement containing \c x
 !!
 !! The algorithm is rather crude: compute the barycentric coordinates
 !! with respect to all the subelements and pick the one where the
 !! minimum xi is maximum (for internal elements, this is the only one
 !! where it is positive).
 pure function tri_dp_x2reg(dp,x) result(reg)
  class(t_dp_tri_pk_iso_p1), intent(in) :: dp
  real(wp), intent(in) :: x(:)
  integer :: reg

  integer :: i, d
  real(wp) :: xi(size(x)+1), xi_min(dp%loc_ne)
  
   d = size(x)

   do i=1,dp%loc_ne
     xi(1:d) = matmul( dp%bi(:,:,i) , x-dp%x0(:,i) ) 
     xi(d+1) = 1.0_wp - sum(xi(1:d))
     xi_min(i) = minval(xi)
   enddo

   reg = maxloc(xi_min,1)
   
 end function tri_dp_x2reg
 
!-----------------------------------------------------------------------

 elemental function tri_dp_compare(a,b) result(bool)
  class(t_dp_tri_pk_iso_p1), intent(in) :: a
  class(t_sym_dp),           intent(in) :: b
  logical :: bool

  real(wp), parameter :: toll = 100.0_wp * epsilon(0.0_wp)

   bool = .false.
   select type(b); class is(t_dp_tri_pk_iso_p1)
   
   bool = a%loc_ne .eq. b%loc_ne
   if(bool) bool = sum(abs( a%x0 - b%x0 )) .lt. toll
   if(bool) bool = sum(abs( a%b  - b%b  )) .lt. toll

   end select
   
 end function tri_dp_compare

!-----------------------------------------------------------------------

 subroutine tri_dp_woct(dp,var_name,fu)
  integer, intent(in) :: fu
  class(t_sym_dp), intent(in) :: dp
  character(len=*), intent(in) :: var_name

  character(len=*), parameter :: &
    this_sub_name = 'tri_dp_woct'
 
   select type(dp); class is(t_dp_tri_pk_iso_p1)

   write(fu,'(a,a)')    '# name: ',var_name
   write(fu,'(a)')      '# type: struct'
   write(fu,'(a)')      '# length: 5' ! number of fields

   ! field 01 : loc_ne
   write(fu,'(a)')      '# name: loc_ne'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a,i7)')   '# columns: 1'
   call write_octave(dp%loc_ne,'<cell-element>',fu)

   ! field 02 : loc_t
   write(fu,'(a)')      '# name: loc_t'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a,i7)')   '# columns: 1'
   call write_octave(dp%loc_t,'<cell-element>',fu)

   ! field 03 : b
   write(fu,'(a)')      '# name: b'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a,i7)')   '# columns: 1'
   call write_octave(dp%b,'<cell-element>',fu)

   ! field 04 : bi
   write(fu,'(a)')      '# name: bi'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a,i7)')   '# columns: 1'
   call write_octave(dp%bi,'<cell-element>',fu)

   ! field 05 : x0
   write(fu,'(a)')      '# name: x0'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a,i7)')   '# columns: 1'
   call write_octave(dp%x0,'<cell-element>',fu)

   class default
    call error(this_sub_name,this_mod_name, 'Unsupported type.' )
   end select

 end subroutine tri_dp_woct

!-----------------------------------------------------------------------

 !> Set-up a \c t_dp_tri_pk_iso_p1 object
 pure subroutine tri_dp_setup(dp , loc_t,xn)
  integer,                   intent(in)  :: loc_t(:,:)
  real(wp),                  intent(in)  :: xn(:,:)
  class(t_dp_tri_pk_iso_p1), intent(inout) :: dp

  integer :: iel, i

   dp%loc_t = loc_t ! reallocation on assignment

   dp%loc_ne = size(dp%loc_t,2)
   allocate( dp%b( 2,2,dp%loc_ne) , &
             dp%bi(2,2,dp%loc_ne) , &
             dp%x0(  2,dp%loc_ne) )

   ! For these fields, see also mod_grid::compute_geometry
   do iel=1,dp%loc_ne
     associate( lt => dp%loc_t(:,iel) )
     do i=1,2
       dp%b(:,i,iel) = xn(:, lt(i+1) ) - xn(:, lt(1) )
     enddo
     dp%x0(:,iel) = xn(:, lt(1) )
     call invmat( dp%b(:,:,iel) , dp%bi(:,:,iel) )
     end associate
   enddo

 end subroutine tri_dp_setup

!-----------------------------------------------------------------------

 !> Given the points \c x in the elements, replicate them on each
 !! subelement.
 pure subroutine tri_zoom(xl,dp,x)
  class(t_dp_tri_pk_iso_p1), intent(in)  :: dp
  real(wp),                  intent(in)  :: x(:,:)
  real(wp), allocatable,     intent(out) :: xl(:,:)

  integer :: il, id

   allocate( xl( size(x,1) , dp%loc_ne*size(x,2) ) )
   do il=1,dp%loc_ne
     associate( xli => xl( : , (il-1)*size(x,2)+1:il*size(x,2) ) )
     xli = matmul( dp%b(:,:,il) , x )
     do id=1,size(x,1)
       xli(id,:) = xli(id,:) + dp%x0(id,il)
     enddo
     end associate
   enddo

 end subroutine tri_zoom

!-----------------------------------------------------------------------

 !> Locate the subelement containing \c x
 !!
 !! See \fref{mod_h2d_master_el,quad_dp_setup} on the use of
 !! \fref{mod_perms,cart_idx} to recover the region index from the 1D
 !! indexes.
 pure function quad_dp_x2reg(dp,x) result(reg)
  class(t_dp_quad_pk_iso_p1), intent(in) :: dp
  real(wp),                   intent(in) :: x(:)
  integer :: reg

  integer :: id, reg1(size(x))
  real(wp) :: xi(2,dp%nreg1) ! 1D barycentric coords

   ! On each 1d partition, locate the closest interval

   do id=1,size(x) ! dimension loop
     xi(2,:) = x(id) - dp%lbreg(:,id) ! left bound
     xi(1,:) = dp%rbreg(:,id) - x(id) ! right bound
     reg1(id) = maxloc(minval(xi,1),1) ! 1D index
   enddo

   reg = cart_idx( reg1 , dp%nreg1 )

 end function quad_dp_x2reg
 
!-----------------------------------------------------------------------

 elemental function quad_dp_compare(a,b) result(bool)
  class(t_dp_quad_pk_iso_p1), intent(in) :: a
  class(t_sym_dp),            intent(in) :: b
  logical :: bool

  real(wp), parameter :: toll = 100.0_wp * epsilon(0.0_wp)

   bool = .false.
   select type(b); class is(t_dp_quad_pk_iso_p1)

   bool = a%loc_ne .eq. b%loc_ne
   if(bool) bool = all( a%loc_t .eq. b%loc_t )
   if(bool) bool = sum(abs( a%lbreg - b%lbreg )) .lt. toll
   if(bool) bool = sum(abs( a%rbreg - b%rbreg )) .lt. toll

   end select

 end function quad_dp_compare

!-----------------------------------------------------------------------

 subroutine quad_dp_woct(dp,var_name,fu)
  integer, intent(in) :: fu
  class(t_sym_dp), intent(in) :: dp
  character(len=*), intent(in) :: var_name

  character(len=*), parameter :: &
    this_sub_name = 'quad_dp_woct'
 
   select type(dp); class is(t_dp_quad_pk_iso_p1)

   write(fu,'(a,a)')    '# name: ',var_name
   write(fu,'(a)')      '# type: struct'
   write(fu,'(a)')      '# length: 6' ! number of fields

   ! field 01 : loc_ne
   write(fu,'(a)')      '# name: loc_ne'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a,i7)')   '# columns: 1'
   call write_octave(dp%loc_ne,'<cell-element>',fu)

   ! field 02 : loc_t
   write(fu,'(a)')      '# name: loc_t'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a,i7)')   '# columns: 1'
   call write_octave(dp%loc_t,'<cell-element>',fu)

   ! field 03 : nreg1
   write(fu,'(a)')      '# name: nreg1'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a,i7)')   '# columns: 1'
   call write_octave(dp%nreg1,'<cell-element>',fu)

   ! field 04 : lbreg
   write(fu,'(a)')      '# name: lbreg'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a,i7)')   '# columns: 1'
   call write_octave(dp%lbreg,'<cell-element>',fu)

   ! field 05 : rbreg
   write(fu,'(a)')      '# name: rbreg'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a,i7)')   '# columns: 1'
   call write_octave(dp%rbreg,'<cell-element>',fu)

   ! field 06 : reg2reg
   write(fu,'(a)')      '# name: reg2reg'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a,i7)')   '# columns: 1'
   call write_octave(dp%reg2reg,'<cell-element>',fu)

   class default
    call error(this_sub_name,this_mod_name, 'Unsupported type.' )
   end select

 end subroutine quad_dp_woct

!-----------------------------------------------------------------------

 !> Set-up a \c t_dp_quad_pk_iso_p1 object
 !!
 !! We are given the 1D nodes and the corresponding, tensor product
 !! \f$d\f$-D ones and need to define the local regions. The
 !! difficulty is that we have no control over the ordering of these
 !! two sets of nodes. We proceed as follows:
 !! <ul>
 !!  <li> sort the 1D nodes
 !!  <li> generate one-by-one the local \f$d\f$-D regions
 !!  <li> locate the vertexes of the local region among the \f$d\f$-D
 !!  nodes
 !! </ul>
 !!
 !! \note We assume here that \c xn1 includes also the vertexes.
 !!
 !! \note Given the algorithm used to generate the regions, it is
 !! possible to recover the region index from the 1D region indexes
 !! using \fref{mod_perms,cart_idx}.
 pure subroutine quad_dp_setup(dp , xn,xn1)
  real(wp),                   intent(in) :: xn(:,:), xn1(:,:)
  class(t_dp_quad_pk_iso_p1), intent(inout) :: dp

  integer :: i,j, id
  integer, allocatable :: idxl(:,:), dxlr(:,:)
  integer ::  xn1i(size(xn1,1),size(xn1,2))
  real(wp) :: xn1s(size(xn1,1),size(xn1,2)), xv(size(xn1,2))

   associate( d => size(xn1,2) , & ! number of space dimensions
             n2 => size(xn,2)  )   ! number of 2D nodes

   do id=1,d ! sort each 1D node collection
     xn1s(:,id) = xn1(:,id)
     xn1i(:,id) = (/( i , i=1,size(xn1,1) )/)
     call sort( xn1s(:,id) , xn1i(:,id) )
   enddo

   dp%nreg1 = size(xn1,1)-1
   dp%loc_ne = dp%nreg1**d
   allocate( dp%loc_t( 2**d , dp%loc_ne )      , &
             dp%lbreg( dp%nreg1 , d )          , &
             dp%rbreg( dp%nreg1 , d )          , &
             dp%reg2reg( d , dp%loc_ne ) )

   ! once we have sorted the 1D nodes, bregs are easy
   dp%lbreg = xn1s( :dp%nreg1,:)
   dp%rbreg = xn1s(2:        ,:)

   ! generate the regions and locate the vertexes
   call cart_table( idxl , (/(i , i=1,dp%nreg1)/),d) ! left node
   call cart_table( dxlr , (/ 0 , 1 /)           ,d) ! side deltas
   do i=1,dp%loc_ne ! loop over the regions
     do j=1,2**d ! loop over the region vertexes
       do id=1,d ! build the vertex coordinates
         xv(id) = xn1s(idxl(id,i)+dxlr(id,j),id)
       enddo
       dp%loc_t( j , i ) = minloc(sum(abs(xn - spread(xv,2,n2)),1),1)
     enddo
     ! the left node has the same multi-index as the region
     dp%reg2reg(:,i) = idxl(:,i)
   enddo

   end associate

 end subroutine quad_dp_setup

!-----------------------------------------------------------------------

 !> Given the points \c x in the elements, replicate them on each
 !! subelement.
 pure subroutine quad_zoom(xl,dp,x)
  class(t_dp_quad_pk_iso_p1), intent(in)  :: dp
  real(wp),                   intent(in)  :: x(:,:)
  real(wp), allocatable,      intent(out) :: xl(:,:)

  integer :: ir, id

   allocate( xl( size(x,1) , dp%loc_ne*size(x,2) ) )
   do ir=1,dp%loc_ne
     associate( xli => xl( : , (ir-1)*size(x,2)+1:ir*size(x,2) ) )
     do id=1,size(x,1)
       associate( lb => dp%lbreg( dp%reg2reg(id,ir) ,id) , &
                  rb => dp%rbreg( dp%reg2reg(id,ir) ,id) )
       xli(id,:) = lb + (rb-lb)*x(id,:)
       end associate
     enddo
     end associate
   enddo

 end subroutine quad_zoom

!-----------------------------------------------------------------------
 
 subroutine geom_tri_wo(me,var_name,fu)
  integer, intent(in) :: fu
  class(t_h2d_me_geom_tri), intent(in) :: me
  character(len=*), intent(in) :: var_name
 
  character(len=*), parameter :: &
    this_sub_name = 'geom_tri_wo'
 
   write(fu,'(a,a)')    '# name: ',var_name
   write(fu,'(a)')      '# type: struct'
   write(fu,'(a)')      '# length: 7' ! number of fields

   ! field 01 : b
   write(fu,'(a)')      '# name: b'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a,i7)')   '# columns: 1'
   call write_octave(me%b,'<cell-element>',fu)

   ! field 02 : det_b
   write(fu,'(a)')      '# name: det_b'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a,i7)')   '# columns: 1'
   call write_octave(me%det_b,'<cell-element>',fu)

   ! field 03 : x0
   write(fu,'(a)')      '# name: x0'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a,i7)')   '# columns: 1'
   call write_octave(me%x0,'c','<cell-element>',fu)

   ! field 04 : b_it
   write(fu,'(a)')      '# name: b_it'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a,i7)')   '# columns: 1'
   call write_octave(me%b_it,'<cell-element>',fu)

   ! field 05 : map
   write(fu,'(a)')      '# name: map'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a,i7)')   '# columns: 1'
   call write_octave("function_handle","<cell-element>", &
                     "geom_tri_map",fu)

   ! field 06 : scale_wg
   write(fu,'(a)')      '# name: scale_wg'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a,i7)')   '# columns: 1'
   call write_octave("function_handle","<cell-element>", &
                     "geom_tri_scale_wg",fu)

   ! field 07 : scale_gradp
   write(fu,'(a)')      '# name: scale_gradp'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a,i7)')   '# columns: 1'
   call write_octave("function_handle","<cell-element>", &
                     "geom_tri_scale_gradp",fu)

 end subroutine geom_tri_wo

!-----------------------------------------------------------------------

 subroutine geom_quad_wo(me,var_name,fu)
  integer, intent(in) :: fu
  class(t_h2d_me_geom_quad), intent(in) :: me
  character(len=*), intent(in) :: var_name
 
  character(len=*), parameter :: &
    this_sub_name = 'geom_quad_wo'
 
   write(fu,'(a,a)')    '# name: ',var_name
   write(fu,'(a)')      '# type: struct'
   write(fu,'(a)')      '# length: 6' ! number of fields

   ! field 01 : v1
   write(fu,'(a)')      '# name: v1'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a,i7)')   '# columns: 1'
   call write_octave(me%v1,'c','<cell-element>',fu)

   ! field 02 : b
   write(fu,'(a)')      '# name: b'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a,i7)')   '# columns: 1'
   call write_octave(me%b,'<cell-element>',fu)

   ! field 03 : c
   write(fu,'(a)')      '# name: c'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a,i7)')   '# columns: 1'
   call write_octave(me%c,'c','<cell-element>',fu)

   ! field 04 : map
   write(fu,'(a)')      '# name: map'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a,i7)')   '# columns: 1'
   call write_octave("function_handle","<cell-element>", &
                     "geom_quad_map",fu)

   ! field 05 : scale_wg
   write(fu,'(a)')      '# name: scale_wg'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a,i7)')   '# columns: 1'
   call write_octave("function_handle","<cell-element>", &
                     "geom_quad_scale_wg",fu)

   ! field 06 : scale_gradp
   write(fu,'(a)')      '# name: scale_gradp'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a,i7)')   '# columns: 1'
   call write_octave("function_handle","<cell-element>", &
                     "geom_quad_scale_gradp",fu)

 end subroutine geom_quad_wo

!-----------------------------------------------------------------------

 !> Notice: in fact we don't really need to distinguish between
 !! triangles and quadrilaterals for the base elements.
 subroutine base_tri_wo(me,var_name,fu)
  integer, intent(in) :: fu
  class(t_h2d_me_base_tri), intent(in) :: me
  character(len=*), intent(in) :: var_name

   call base_general_wo(me,var_name,fu)
 end subroutine base_tri_wo

 subroutine base_quad_wo(me,var_name,fu)
  integer, intent(in) :: fu
  class(t_h2d_me_base_quad), intent(in) :: me
  character(len=*), intent(in) :: var_name

   call base_general_wo(me,var_name,fu)
 end subroutine base_quad_wo

 subroutine base_general_wo(me,var_name,fu)
  integer, intent(in) :: fu
  class(c_h2d_me_base), intent(in) :: me
  character(len=*), intent(in) :: var_name
 
  integer :: i, j, k
  character(len=*), parameter :: &
    this_sub_name = 'base_general_wo'
 
   write(fu,'(a,a)')    '# name: ',var_name
   write(fu,'(a)')      '# type: struct'
   write(fu,'(a)')      '# length: 28' ! number of fields

   ! field 01 : k
   write(fu,'(a)')      '# name: k'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(me%k,'<cell-element>',fu)

   ! field 02 : xnodes
   write(fu,'(a)')      '# name: xnodes'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(me%xnodes,'<cell-element>',fu)

   ! field 03 : snodes
   write(fu,'(a)')      '# name: snodes'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(me%snodes,'<cell-element>',fu)

   ! field 04 : m
   write(fu,'(a)')      '# name: m'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(me%m,'<cell-element>',fu)

   ! field 05 : ms
   write(fu,'(a)')      '# name: ms'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(me%ms,'<cell-element>',fu)

   ! field 06 : deg
   write(fu,'(a)')      '# name: deg'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(me%deg,'<cell-element>',fu)

   ! field 07 : degs
   write(fu,'(a)')      '# name: degs'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(me%degs,'<cell-element>',fu)

   ! field 08 : xig
   write(fu,'(a)')      '# name: xig'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(me%xig,'<cell-element>',fu)

   ! field 09 : wg
   write(fu,'(a)')      '# name: wg'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(me%wg,'r','<cell-element>',fu)

   ! field 10 : pi_tab
   write(fu,'(a)')      '# name: pi_tab'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(me%pi_tab,'<cell-element>',fu)

   ! field 11 : stab
   write(fu,'(a)')      '# name: stab'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(me%stab,'<cell-element>',fu)

   ! field 12 : xigs
   write(fu,'(a)')      '# name: xigs'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(me%xigs,'<cell-element>',fu)

   ! field 13 : wgs
   write(fu,'(a)')      '# name: wgs'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(me%wgs,'r','<cell-element>',fu)

   ! field 14 : xigb
   write(fu,'(a)')      '# name: xigb'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(me%xigb,'<cell-element>',fu)

   ! field 15 : pk
   write(fu,'(a)')      '# name: pk'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(me%pk,'<cell-element>',fu)

   ! field 16 : p_s
   write(fu,'(a)')      '# name: p_s'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   write(fu,'(a)')      '# name: <cell-element>'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# ndims: 2'
   write(fu,'(*(i0,:," "))') (/ 1 , size(me%p_s) /)
   do i=1,size(me%p_s)
     call write_octave(me%p_s(i),'<cell-element>',fu)
   enddo

   ! field 17 : gradp_s
   write(fu,'(a)')      '# name: gradp_s'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   write(fu,'(a)')      '# name: <cell-element>'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# ndims: 2'
   write(fu,'(*(i0,:," "))') shape(me%gradp_s)
   do j=1,size(me%gradp_s,2)
     do i=1,size(me%gradp_s,1)
       call write_octave(me%gradp_s(i,j),'<cell-element>',fu)
     enddo
   enddo

   ! field 18 : p
   write(fu,'(a)')      '# name: p'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(me%p,'<cell-element>',fu)

   ! field 19 : gradp
   write(fu,'(a)')      '# name: gradp'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(me%gradp,'<cell-element>',fu)

   ! field 20 : pb
   write(fu,'(a)')      '# name: pb'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(me%pb,'<cell-element>',fu)

   ! field 21 : gradpb
   write(fu,'(a)')      '# name: gradpb'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(me%gradpb,'<cell-element>',fu)

   ! field 22 : bk
   write(fu,'(a)')      '# name: bk'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(me%bk,'<cell-element>',fu)

   ! field 23 : b_s
   write(fu,'(a)')      '# name: b_s'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   write(fu,'(a)')      '# name: <cell-element>'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# ndims: 2'
   write(fu,'(*(i0,:," "))') shape(me%b_s)
   do j=1,size(me%b_s,2)
     do i=1,size(me%b_s,1)
       call write_octave(me%b_s(i,j),'<cell-element>',fu)
     enddo
   enddo

   ! field 24 : gradb_s
   write(fu,'(a)')      '# name: gradb_s'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   write(fu,'(a)')      '# name: <cell-element>'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# ndims: 3'
   write(fu,'(*(i0,:," "))') shape(me%gradb_s)
   do k=1,size(me%gradb_s,3)
     do j=1,size(me%gradb_s,2)
       do i=1,size(me%gradb_s,1)
         call write_octave(me%gradb_s(i,j,k),'<cell-element>',fu)
       enddo
     enddo
   enddo

   ! field 25 : b
   write(fu,'(a)')      '# name: b'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(me%b,'<cell-element>',fu)

   ! field 26 : gradb
   write(fu,'(a)')      '# name: gradb'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(me%gradb,'<cell-element>',fu)

   ! field 27 : bb
   write(fu,'(a)')      '# name: bb'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(me%bb,'<cell-element>',fu)

   ! field 28 : gradbb
   write(fu,'(a)')      '# name: gradbb'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(me%gradbb,'<cell-element>',fu)

 end subroutine base_general_wo

!-----------------------------------------------------------------------

end module mod_h2d_master_el

