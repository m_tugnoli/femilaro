function swg = geom_quad_scale_wg(me,xig,wg)
% swg = geom_quad_scale_wg(me,xig,wg)
%
% See mod_h2d_master_el::geom_quad_scale_wg

 det_j = ...
    (me.b(1,1) + me.c(1)*xig(2,:)) .* (me.b(2,2) + me.c(2)*xig(1,:)) ...
  - (me.b(1,2) + me.c(1)*xig(1,:)) .* (me.b(2,1) + me.c(2)*xig(2,:));

 swg =  det_j .* wg;

return
