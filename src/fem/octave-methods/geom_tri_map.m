function x = geom_tri_map(me,csi)
% x = geom_tri_map(me,csi)
%
% See mod_h2d_master_el::geom_tri_map

 x = me.b * csi;
 for i=1:size(x,1)
   x(i,:) = x(i,:) + me.x0(i);
 end

return
