function swg = geom_tri_scale_wg(me,xig,wg)
% swg = geom_tri_scale_wg(me,xig,wg)
%
% See mod_h2d_master_el::geom_tri_scale_wg

 swg = me.det_b * wg;
return
