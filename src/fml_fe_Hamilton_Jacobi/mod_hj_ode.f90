!! Copyright (C) 2014  Marco Restelli
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Marco Restelli                   <marco.restelli@gmail.com>

!>\brief
!!
!! Hamilton-Jacobi equation.
!!
!! \n
!!
!! This module defines the ODE for a scalar Hamilton-Jacobi equation
!! in \f$d\f$ dimensions, for a generic Hamiltonian. We follow the
!! method described in <a
!! href="http://dx.doi.org/10.1016/j.jcp.2010.09.022">[Yan, Osher,
!! JCP, 2011]</a>.
!!
!! We use here the LDG method applied to a first order method. The
!! typical DG implementation for a first order problem would require
!! two loops, one over the elements and one over the sides, and no
!! auxiliary variables. However, in the Hamilton-Jacobi equation, the
!! gradient of the unknown \f$\Phi\f$ is the argument of the
!! Hamiltonian \f$H\f$, so we need to know the whole discrete gradient
!! before computing the right-hand-side. In <a
!! href="http://dx.doi.org/10.1016/j.jcp.2010.09.022">[Yan, Osher,
!! JCP, 2011]</a>, this is done following the LDG method and
!! introducing an auxiliary variable
!! \f$\underline{p}\approx\nabla\Phi\f$. This requires the following
!! algorithm:
!! <ul>
!!  <li> loop on the sides and collect the values \f$\Phi|_{\partial
!!  K}\f$ of the two traces at the side quadrature nodes (in the
!!  parallel implementation, this includes the communications)
!!  <li> loop on the elements; for each element
!!  <ul>
!!   <li> compute \f$\underline{p}|_{K}\f$ using both the internal
!!   values of \f$\Phi\f$ and the precomputed traces
!!   <li> compute the right-hand-side of the equation.
!!  </ul>
!! </ul>
!! Notice that the auxiliary variable \f$\underline{p}\f$ does not
!! need to be stored for the whole grid, since it can be computed and
!! used element-by-element. This allows solving the Hamilton-Jacobi
!! equation with (almost) the same structure used for for a first
!! order conservation low; the only difference is that the numerical
!! traces \f$\Phi|_{\partial K}\f$ are collected in a diagnostic
!! variable, rather than being used directly to compute the rhs.
!!
!! \section formulae Details of the weak formulation
!!
!! Let us consider more in details the discrete problem. For most of
!! these terms, we refer to the derivations given in \c mod_vp_ode and
!! \c mod_poisson_pb.
!!
!! The discrete form of the differential equation is rather
!! straightforward:
!! \f{displaymath}{
!!  \frac{d}{dt} \int_K \Phi\,\phi_{\underline{i}}\,dx = - \int_K
!!  \widehat{H}(\underline{p}^+,\underline{p}^-)\phi_{\underline{i}}
!!  \,dx
!! \f}
!! yields
!! \f{displaymath}{
!!  w_{\underline{i}}J \frac{d}{dt}
!!  \Phi(\underline{x}_{\underline{i}}^G) = -
!!  w_{\underline{i}}J \widehat{H}(\underline{p}^+,\underline{p}^-)
!!  (\underline{x}^G_{\underline{i}}),
!! \f}
!! where we can simplify the weights (mass matrices) and use the fact
!! that the Hamiltonian is an algebraic function, so that
!! \f{displaymath}{
!!  \widehat{H}(\underline{p}^+,\underline{p}^-)
!!  (\underline{x}^G_{\underline{i}}) = 
!!  \widehat{H}\left(\underline{p}^+(\underline{x}^G_{\underline{i}})\, ,
!!  \,\underline{p}^-(\underline{x}^G_{\underline{i}})\right),
!! \f}
!! to obtain the collocated equation
!! \f{displaymath}{
!!  \frac{d}{dt} \Phi(\underline{x}_{\underline{i}}^G) = -
!!  \widehat{H}\left(\underline{p}^+(\underline{x}^G_{\underline{i}})\, ,
!!  \,\underline{p}^-(\underline{x}^G_{\underline{i}})\right).
!! \f}
!!
!! Concerning now the auxiliary variables, we have (setting
!! \f$\underline{q}_{k\underline{i}} =
!! \phi_{\underline{i}}\underline{e}_k\f$)
!! \f{displaymath}{
!!  \int_K \underline{p}^{\pm}\cdot\underline{q}_{k\underline{i}}\, dx
!!  = -
!!  \int_K \Phi \nabla\cdot \underline{q}_{k\underline{i}}\, dx +
!!  \int_{\partial K}\Phi^{\pm} \underline{q}_{k\underline{i}}\cdot
!!  \underline{n}\, d\sigma.
!! \f}
!! All these terms can be computed as in \c mod_poisson_pb. In
!! particular, we have
!! \f{displaymath}{
!!  \int_K \underline{p}^{\pm}\cdot\underline{q}_{k\underline{i}}\, dx
!!  = w_{\underline{i}}Jp^{\pm}_k(\underline{x}_{\underline{i}}^G),
!! \f}
!! for the mass matrix and
!! \f{displaymath}{
!!  \int_K \Phi \nabla\cdot \underline{q}_{k\underline{i}}\, dx =
!!  \sum_{\underline{l}}
!!  w_{\underline{l}}J\Phi(\underline{x}_{\underline{l}}^G)
!!  \frac{\partial\phi_{\underline{i}}}{\partial x^k}
!!  (\underline{x}_{\underline{l}}^G) =
!!  \sum_{h=1}^{p_k} w_{\underline{i}_{\backslash k,h}} J 
!!  \Phi(\underline{x}_{\underline{i}_{\backslash k,h}}^G)
!!  J^{-1}_k \phi'_{\underline{i}(k)}(x_h^G) =
!! \frac{w_{\underline{i}}J}{w_{\underline{i}(k)}J_k}
!!  \sum_{h=1}^{p_k} w_{h} \phi'_{\underline{i}(k)}(x_h^G) 
!!  \Phi(\underline{x}_{\underline{i}_{\backslash k,h}}^G)
!! \f}
!! for the differential term. Using the matrix \f$D\f$ defined in \c
!! mod_vp_ode this becomes
!! \f{displaymath}{
!!  \int_K \Phi \nabla\cdot \underline{q}_{k\underline{i}}\, dx =
!! \frac{w_{\underline{i}}J}{w_{\underline{i}(k)}J_k}
!!  \sum_{h=1}^{p_k} D_{\underline{i}(k)\,h} 
!!  \Phi(\underline{x}_{\underline{i}_{\backslash k,h}}^G),
!! \f}
!! which corresponds to multiplying \f$D\f$ with the nodal values of
!! \f$\Phi\f$ along the \f$k\f$-th dimension. Finally, for the side
!! term we have
!! \f{displaymath}{
!!  \int_{\partial K\cap s}\Phi^{\pm}
!!  \underline{q}_{k\underline{i}}\cdot \underline{n}\, d\sigma =
!!  \sum_{\underline{l}^s}
!!  w_{\underline{l}^s}J^s\,
!!  \Phi^{\pm}(\underline{x}_{\underline{l}^s}^G)\,
!!  \phi_{\underline{i}}(\underline{x}_{\underline{l}^s}^G)\, n_k,
!! \f}
!! where clearly only one term is nonzero in the summation. Notice
!! that here \f$n_k\f$ denotes the \f$k\f$-th component of the
!! <em>outward</em> normal on \f$K\f$.
!<----------------------------------------------------------------------
module mod_hj_ode

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 !$ use omp_lib

 !$ use mod_omp_utils, only: &
 !$   mod_omp_utils_initialized, &
 !$   detailed_timing_omp, &
 !$   omput_push_key,    &
 !$   omput_pop_key,     &
 !$   omput_start_timer, &
 !$   omput_close_timer, &
 !$   omput_write_time

 use mod_state_vars, only: &
   mod_state_vars_initialized, &
   c_stv

 use mod_tps_phs_grid, only: &
   mod_tps_phs_grid_initialized, &
   t_tps_grid

 use mod_tps_base, only: &
   mod_tps_base_initialized, &
   t_tps_base

 use mod_time_integrators, only: &
   mod_time_integrators_initialized, &
   c_ode, c_ods

 use mod_phi_state, only: &
   mod_phi_state_initialized, &
   t_phi_state

 use mod_testcases, only: &
   mod_testcases_initialized, &
   coeff_h,    & ! Hamiltonian function
   coeff_a       ! coefficient alpha for the Lax-Friedrichs stab.

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_hj_ode_constructor, &
   mod_hj_ode_destructor,  &
   mod_hj_ode_initialized, &
   t_hj_ode, t_hj_ods, new_hj_ode, clear

 private

!-----------------------------------------------------------------------

! Module types and parameters

 !> Hamilton-Jacobi ODE
 type, extends(c_ode) :: t_hj_ode
  type(t_tps_grid), pointer :: grid => null()
  type(t_tps_base), pointer :: base => null()
 contains
  procedure, pass(ode) :: rhs  => hj_rhs
 end type t_hj_ode

 type, extends(c_ods) :: t_hj_ods
  real(wp), allocatable :: phi_pm(:,:,:)
 end type t_hj_ods

 interface clear
   module procedure clear_hj_ode
 end interface

! Module variables

 logical, protected ::               &
   mod_hj_ode_initialized = .false.
 character(len=*), parameter :: &
   this_mod_name = 'mod_hj_ode'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_hj_ode_constructor()
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_kinds_initialized.eqv..false.) .or. &
    (mod_messages_initialized.eqv..false.) .or. &
!$      ( (detailed_timing_omp.eqv..true.).and. &
!$ (mod_omp_utils_initialized.eqv..false.) ) .or. &
  (mod_state_vars_initialized.eqv..false.) .or. &
(mod_tps_phs_grid_initialized.eqv..false.) .or. &
    (mod_tps_base_initialized.eqv..false.) .or. &
(mod_time_integrators_initialized.eqv..false.) .or. &
   (mod_phi_state_initialized.eqv..false.) .or. &
   (mod_testcases_initialized.eqv..false.) ) then  
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_hj_ode_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   mod_hj_ode_initialized = .true.
 end subroutine mod_hj_ode_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_hj_ode_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_hj_ode_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_hj_ode_initialized = .false.
 end subroutine mod_hj_ode_destructor

!-----------------------------------------------------------------------

 subroutine new_hj_ode(ode,ods,grid,base)
  type(t_tps_grid), intent(in), target :: grid
  type(t_tps_base), intent(in), target :: base
  type(t_hj_ode),   intent(out) :: ode
  type(t_hj_ods),   intent(out) :: ods

   ode%grid => grid
   ode%base => base

   allocate( ods%phi_pm(2,base%spkd,grid%ns) )

 end subroutine new_hj_ode
 
!-----------------------------------------------------------------------
 
 pure subroutine clear_hj_ode(ode,ods)
  type(t_hj_ode), intent(out) :: ode
  type(t_hj_ods), intent(out) :: ods

   nullify( ode%grid )
   nullify( ode%base )

   ! ods components deallocated automatically
 
 end subroutine clear_hj_ode
 
!-----------------------------------------------------------------------

 !> Compute the right-hand side of the Hamilto-Jacobi equation
 !!
 !! \note We assume that all the quadrature weights are defined on the
 !! interval \f$[0\,,1]\f$, so that the Jacobians are the element
 !! sizes.
 !!
 !! \note The mass matrix is not considered explicit: the differential
 !! operators already include the inverse of the diagonal mass matrix.
 subroutine hj_rhs(tnd,ode,t,uuu,ods,term)
  class(t_hj_ode), intent(in) :: ode
  real(wp),     intent(in)    :: t
  class(c_stv), intent(in)    :: uuu
  class(c_ods), intent(inout) :: ods
  class(c_stv), intent(inout) :: tnd
  integer,      intent(in), optional :: term(2)

  integer :: ie, i, sie(2), k, si, ii(2), im(ode%grid%d), k1,k2,k3, &
             is(2), iu
  ! alpha depends on the point, so that one can use a local maximum
  real(wp) :: w, rj(2), ww(2), alpha( ode%grid%d , ode%base%pkd )
  real(wp) :: pp( 2 , ode%grid%d , ode%base%pkd ), &
    p_ave(ode%grid%d,ode%base%pkd), p_dlt(ode%grid%d,ode%base%pkd)

   select type(uuu); type is(t_phi_state)
     select type(tnd); type is(t_phi_state)
       select type(ods); type is(t_hj_ods)

   ! 1) Collect the traces of PHI
   side_do: do i=1,ode%grid%ns
     sie = ode%grid%s(i)%ie ! side elements
     k   = ode%grid%s(i)%n  ! side normal direction

     do si=1,ode%base%spkd ! side degrees of freedom
       
       ! local degrees of freedom
       ii = ode%base%s_dofs(:,k,si) ! local dof index

       ! 1 -> right values
       ods%phi_pm(1,si,i) = uuu%phi( ii(1) , sie(2) )
       ! 2 -> left values
       ods%phi_pm(2,si,i) = uuu%phi( ii(2) , sie(1) )

     enddo
   enddo side_do

   ! 2) Element loop to compute the RHS
   elem_do: do ie=1,ode%grid%ne

     ! 2.1) diagnose the auxiliary variable

     ! volume terms
     dof_do: do i=1,ode%base%pkd
       im = ode%base%mldx(i,:) ! multiindex

       dim_do: do k=1,ode%grid%d
         ! weight, including the inverse mass matrix
         w = 1.0_wp/(ode%base%wgl(im(k))*ode%grid%e(ie)%bw(k))

         ! get the indexes to multiply along the k-th direction at im
         call ode%base%dotmap(k1,k2,k3 , im,k &
                             !$ , dotmap_work &
                             )

         ! for each coordinate there are two upwind directions; the
         ! element integral is the same, while the boundary integrals
         ! are different.
         pp(:,k,i) = - w *                                          &
            dot_product(ode%base%wdphi(im(k),:),uuu%phi(k1:k2:k3,ie))

       enddo dim_do
     enddo dof_do

     ! boundary terms
     sdim_do: do k=1,ode%grid%d ! dimensions
       is = ode%grid%e(ie)%is(:,k) ! sides along direction k
       rj = ode%grid%s(is)%a/ode%grid%e(ie)%vol ! ratio Js/J
       ! include the sign of the outward normal
       rj = (/ -1.0_wp , 1.0_wp /) * rj

       sdof_do: do si=1,ode%base%spkd ! side degrees of freedom
         ii = ode%base%s_dofs(:,k,si) ! local dof index
         ! weights, including the inverse mass matrix and the normal
         ww = rj/ode%base%wgl( ode%base%mldx(ii,k) )
         
         supw_do: do iu=1,2 ! for each coordinate, two upwind directions

           pp(iu,k,ii) = pp(iu,k,ii) + ww * ods%phi_pm(iu,si,is)

         enddo supw_do
       enddo sdof_do
     enddo sdim_do

     ! 2.2) compute the RHS: Lax-Friedrichs flux
     p_ave = 0.5_wp * sum(pp,1)
     p_dlt = pp(1,:,:) - pp(2,:,:)
     alpha = coeff_a( pp )

     tnd%phi(:,ie) = -( coeff_h(p_ave)               &
                       - 0.5_wp * sum(alpha * p_dlt,1) )

   enddo elem_do

       end select
     end select
   end select
 end subroutine hj_rhs

!-----------------------------------------------------------------------
 
end module mod_hj_ode

