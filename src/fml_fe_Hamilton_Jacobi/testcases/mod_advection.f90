!! Copyright (C) 2014 Marco Restelli
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Marco Restelli                   <marco.restelli@gmail.com>

!>\brief
!!
!! Advection equation
!!
!! \n
!!
!! The equation is
!! \f{displaymath}{
!!  \partial_t \Phi + \underline{a}\nabla\Phi = 0
!! \f}
!! with initial condition
!! \f{displaymath}{
!!  \Phi(0,\underline{x}) = -\cos\left( \frac{\pi}{d}\sum_{i=1}^d x^i
!!  \right).
!! \f}
!<----------------------------------------------------------------------
module mod_advection_test

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_advection_test_constructor, &
   mod_advection_test_destructor,  &
   mod_advection_test_initialized, &
   test_name,        &
   test_description, &
   coeff_init, &
   coeff_h,    &
   coeff_a

 private

!-----------------------------------------------------------------------

 ! public members
 character(len=*), parameter   :: test_name = "advection"
 character(len=100), protected :: test_description(1)

 logical, protected :: mod_advection_test_initialized = .false.

 ! private members
 real(wp), allocatable :: aa(:) ! avection coefficient
 character(len=*), parameter :: &
   this_mod_name = 'mod_advection'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_advection_test_constructor(d)
  integer, intent(in) :: d

  integer :: i
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false.) .or. &
          (mod_kinds_initialized.eqv..false.) ) then 
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_advection_test_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   allocate(aa(d)); aa = -real( (/ (i, i=1,d) /) , wp)

   write(test_description(1),'(a,i1)') &
     'Advection equation, dimension ',d

   mod_advection_test_initialized = .true.
 end subroutine mod_advection_test_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_advection_test_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_advection_test_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   deallocate(aa)

   mod_advection_test_initialized = .false.
 end subroutine mod_advection_test_destructor

!-----------------------------------------------------------------------

 pure function coeff_init(x) result(phi0)
  real(wp), intent(in) :: x(:,:)
  real(wp) :: phi0(size(x,2))

  real(wp), parameter :: pi = 3.1415926535897932384626433832795028_wp

   !phi0 = -cos( pi/real(size(x,1),wp) * sum(x,1) )

   integer :: l
   do l=1,size(x,2)
     if(abs(x(1,l)).lt.0.5_wp) then
       phi0(l) = 1.0_wp
     else
       phi0(l) = 0.0_wp
     endif
   enddo
   
 end function coeff_init

!-----------------------------------------------------------------------

 pure function coeff_h(p) result(h)
  real(wp), intent(in) :: p(:,:)
  real(wp) :: h(size(p,2))

   h = matmul(aa,p)

 end function coeff_h

!-----------------------------------------------------------------------

 pure function coeff_a(p) result(a)
  real(wp), intent(in) :: p(:,:,:)
  real(wp) :: a(size(p,2),size(p,3))

  integer :: l

   do l=1,size(p,3)
     a(:,l) = abs(aa)
   enddo

 end function coeff_a

!-----------------------------------------------------------------------

end module mod_advection_test

