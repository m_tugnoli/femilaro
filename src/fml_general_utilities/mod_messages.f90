!! Copyright (C) 2009,2010,2011,2012  Marco Restelli
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Marco Restelli                   <marco.restelli@gmail.com>

!>\brief
!!
!! Module to handle stderr and stdout output.
!!
!! \n
!!
!! Typically, messages are written to the standard output and standard
!! error, and errors abort the execution. It is possible however to
!! capture these messages using character buffers, as well as to allow
!! continuation after an error. This is controlled by the module
!! constructor.
!!
!! \section messages_capturing_messages Capturing messages
!!
!! If the argument \c error is passed to \c mod_messages_constructor,
!! it must be a target character variable. Messages are appended to
!! this variable; the variable itself must be cleared by the client
!! code.
module mod_messages

!-----------------------------------------------------------------------

 use iso_fortran_env, only: &
  output_unit, error_unit

 use mod_utils, only: &
   my_abort, pure_abort => my_pure_abort

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_messages_constructor, &
   mod_messages_destructor,  &
   mod_messages_initialized, &
   pure_abort, &
   error,   &
   warning, &
   info

 private

!-----------------------------------------------------------------------

! Module types and parameters

 ! private members
 logical :: capture_error, capture_output
 integer :: &
  erru, &
  outu
 character(len=:), pointer :: &
  ! gfortran bug - compiler bug
  ! see https://gcc.gnu.org/bugzilla/show_bug.cgi?id=77643
  errs , & !=> null(), &
  outs     !=> null()
 integer :: errs_idx, outs_idx
 
 logical :: stop_on_error = .true.

 character(len=*), parameter :: nl = achar(10) ! new-line

! Module variables

 ! public members
 logical ::               &
   mod_messages_initialized = .false.
 ! private members
 character(len=*), parameter :: &
   this_mod_name = 'mod_messages'

 interface error
   module procedure error, error_multiline
 end interface
 interface warning
   module procedure warning, warning_multiline
 end interface
 interface info
   module procedure info, info_multiline
 end interface

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

subroutine mod_messages_constructor(do_not_stop_on_error,error,output)
  logical, intent(in), optional :: do_not_stop_on_error
  character(len=*), target, intent(inout), optional :: error, output

  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if(mod_messages_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module already initialized')
   endif
   !----------------------------------------------

   ! default, set in the module, is aborting on error
   if(present(do_not_stop_on_error)) &
     stop_on_error = .not.do_not_stop_on_error

   capture_error = present(error)
   if(capture_error) then
     errs => error
     errs_idx = 1
   else
     erru = error_unit
   endif

   capture_output = present(output)
   if(capture_output) then
     outs => output
     outs_idx = 1
   else
     outu = output_unit
   endif

   mod_messages_initialized = .true.
 end subroutine mod_messages_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_messages_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_messages_initialized.eqv..false.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is not initialized')
   endif
   !----------------------------------------------

   mod_messages_initialized = .false.
 end subroutine mod_messages_destructor

!-----------------------------------------------------------------------
 
 recursive subroutine error(caller,caller_mod,text)
  character(len=*), intent(in) :: &
    caller, caller_mod, text
 
  ! compiler bug -- gfortran bug
  ! https://gcc.gnu.org/bugzilla/show_bug.cgi?id=77649
  character(len=len_trim(text)) :: gfortran_bug_text(1)
  character(len=*), parameter :: &
    this_sub_name = 'error'

   call module_check(this_sub_name)

   gfortran_bug_text(1) = trim(text)
   call put_msg( 1 , format_out_string(                            &
       !'ERROR in "', caller , caller_mod , '"!' , (/trim(text)/) ) )
       'ERROR in "', caller , caller_mod , '"!' , gfortran_bug_text ) )
 
   if(stop_on_error) call my_abort()
 
 end subroutine error
 
!-----------------------------------------------------------------------

 subroutine error_multiline(caller,caller_mod,text)
  character(len=*), intent(in) :: &
    caller, caller_mod, text(:)
 
  character(len=*), parameter :: &
    this_sub_name = 'error'

   call module_check(this_sub_name)

   call put_msg( 1 , format_out_string(                  &
       'ERROR in "', caller , caller_mod , '"!' , text ) )
 
   if(stop_on_error) call my_abort()
 
 end subroutine error_multiline

!-----------------------------------------------------------------------
 
 subroutine warning(caller,caller_mod,text)
  character(len=*), intent(in) :: &
    caller, caller_mod, text
 
  ! compiler bug -- gfortran bug
  ! https://gcc.gnu.org/bugzilla/show_bug.cgi?id=77649
  character(len=len_trim(text)) :: gfortran_bug_text(1)
  character(len=*), parameter :: &
    this_sub_name = 'warning'

   call module_check(this_sub_name)

   gfortran_bug_text(1) = trim(text)
   call put_msg( 1 , format_out_string(                              &
       !'WARNING in "', caller , caller_mod , '"!' , (/trim(text)/) ) )
       'WARNING in "', caller , caller_mod , '"!' , gfortran_bug_text ) )
 
 end subroutine warning
 
!-----------------------------------------------------------------------
 
 subroutine warning_multiline(caller,caller_mod,text)
  character(len=*), intent(in) :: &
    caller, caller_mod, text(:)
 
  character(len=*), parameter :: &
    this_sub_name = 'error'

   call module_check(this_sub_name)

   call put_msg( 1 , format_out_string(                    &
       'WARNING in "', caller , caller_mod , '"!' , text ) )

 end subroutine warning_multiline

!-----------------------------------------------------------------------
 
 subroutine info(caller,caller_mod,text)
  character(len=*), intent(in) :: &
    caller, caller_mod, text
 
  ! compiler bug -- gfortran bug
  ! https://gcc.gnu.org/bugzilla/show_bug.cgi?id=77649
  character(len=len_trim(text)) :: gfortran_bug_text(1)
  character(len=*), parameter :: &
    this_sub_name = 'info'

   call module_check(this_sub_name)
 
   gfortran_bug_text(1) = trim(text)
   call put_msg( 2 , format_out_string(                             &
       !'INFO from "', caller , caller_mod , '":' , (/trim(text)/) ) )
       'INFO from "', caller , caller_mod , '":' , gfortran_bug_text ) )

 end subroutine info
 
!-----------------------------------------------------------------------
 
 subroutine info_multiline(caller,caller_mod,text)
  character(len=*), intent(in) :: &
    caller, caller_mod, text(:)
 
  character(len=*), parameter :: &
    this_sub_name = 'error'

   call module_check(this_sub_name)

   call put_msg( 2 , format_out_string(                   &
       'INFO from "', caller , caller_mod , '":' , text ) )

 end subroutine info_multiline

!-----------------------------------------------------------------------

 ! It is important that this module is initialized before any
 ! subroutine is called, since the constructor sets various module
 ! switchws.
 subroutine module_check(sub_name)
  character(len=*), intent(in) :: sub_name

   if(.not.mod_messages_initialized) then
     mod_messages_initialized = .true. ! to avoid returning here...
     call error(sub_name,this_mod_name, &
       'This module must be inizialized before using it!')
   endif

 end subroutine module_check

!-----------------------------------------------------------------------

 !> Make a string and count its length
 function format_out_string(msg,caller,caller_mod,end, text ) result(s)
  character(len=*), intent(in) :: msg, caller, caller_mod, end
  character(len=*), intent(in) :: text(:)
  integer :: i
  character( &
    len = 1+len(msg)+len(caller)+14+len(caller_mod)+len(end)    &
         +1+sum((/( 2+len_trim(text(i))+1 , i=1,size(text) )/)) &
           ) :: s

   ! See ifort bug
   ! DPD200411297
   ! https://software.intel.com/en-us/forums/intel-fortran-compiler-for-linux-and-mac-os-x/topic/629432
   ! and also
   ! https://software.intel.com/en-us/comment/1878150#comment-1878150
   write(s,'(a,*(a))') &
       nl//msg // caller // '", in module "' // caller_mod // end &
       // nl, ( "  "//trim(text(i))//nl ,i=1,size(text))

 end function format_out_string

!-----------------------------------------------------------------------
 
 !> Write a message to the selcted channel
 subroutine put_msg( msg_type , msg )
  integer, intent(in) :: msg_type
  character(len=*), intent(in) :: msg

  character(len=*), parameter :: &
    this_sub_name = 'put_msg'

   msg_type_case: select case(msg_type)

    case(1) ! error
     if(capture_error) then
       call append_buffered_text( errs_idx , errs , msg )
     else
       write(erru,'(a)') msg
     endif

    case(2) ! info
     if(capture_output) then
       call append_buffered_text( outs_idx , outs , msg )
     else
       write(outu,'(a)') msg
     endif

    case default
     call error(this_sub_name,this_mod_name,         &
       'Internal module error: unknown message type.')
   end select msg_type_case

 contains

  pure subroutine append_buffered_text(idx,buf,msg)
   character(len=*), intent(in) :: msg
   integer,          intent(inout) :: idx
   character(len=*), intent(inout) :: buf

    idx = len_trim(buf) + 1
    if( idx+len(msg)-1 .gt. len(buf) ) then ! reset the buffer
      buf = ''
      idx = 1
    endif
    buf(idx:) = msg ! msg is truncated if too long
  end subroutine append_buffered_text

 end subroutine put_msg

!-----------------------------------------------------------------------

end module mod_messages

