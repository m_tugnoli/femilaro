!! Copyright (C) 2009,2010,2011,2012  Marco Restelli
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Marco Restelli                   <marco.restelli@gmail.com>

!> \todo This module in fact writes all the symbolic function objects;
!! maybe it could be renamed mod_octave_io_symfun.
module mod_octave_io_sympoly
!General comments: octave io for symbolic polynomials. A polynomial is
!written as a two dimensional array, so that each row has the
!following meaning:
!
!  [   coefficients  ]
!  [ exponents of x1 ]
!  [ exponents of x2 ]
!  [       ...       ]
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_octave_io, only: &
   mod_octave_io_initialized, &
   write_octave, &
   real_format

 use mod_symfun, only: &
   mod_symfun_initialized, &
   c_symfun, t_funcont, &
   assignment(=),       &
   fnpack, snpack

 use mod_sympoly, only: &
   mod_sympoly_initialized, &
   t_symmon,        &
   t_sympol,        &
   test,            &
   wrong_input,     &
   wrong_previous,  &
   wrong_monomials, &
   wrong_type,      &
   ! in this module, we need to access the impl. details of t_sympol
   get__t_sympol__nmon, &
   get__t_sympol__mons

 use mod_sympwfun, only: &
   mod_sympwfun_initialized, &
   t_sympwfun, t_sym_dp, assignment(=), &
   wrong_intervals, &
   wrong_local,     &
   ! getters for the private components: avoid using them if possible
   get__t_sympwfun__funs, get__t_sympwfun__dp

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_octave_io_sympoly_constructor, &
   mod_octave_io_sympoly_destructor,  &
   mod_octave_io_sympoly_initialized, &
   write_octave

 private

!-----------------------------------------------------------------------

! Module types and parameters

! Module variables

 ! public members
 logical, protected ::               &
   mod_octave_io_sympoly_initialized = .false.

 character(len=*), parameter :: &
   this_mod_name = 'mod_octave_io_sympoly'

 interface write_octave
   module procedure write_symfun
 end interface

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_octave_io_sympoly_constructor()
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false.) .or. &
       (mod_kinds_initialized.eqv..false.)    .or. &
       (mod_octave_io_initialized.eqv..false.).or. &
       (mod_sympoly_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_octave_io_sympoly_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   mod_octave_io_sympoly_initialized = .true.
 end subroutine mod_octave_io_sympoly_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_octave_io_sympoly_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_octave_io_sympoly_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_octave_io_sympoly_initialized = .false.
 end subroutine mod_octave_io_sympoly_destructor

!-----------------------------------------------------------------------
 
 recursive subroutine write_symfun(f,var_name,fu)
  integer, intent(in) :: fu
  class(c_symfun), intent(in) :: f
  character(len=*), intent(in) :: var_name

   select type(f)

    type is(t_funcont)
     call write_funcont(f,var_name,fu)
 
    type is(t_sympol)
     call write_sympol(f,var_name,fu)

    type is(t_sympwfun)
     call write_sympwfun(f,var_name,fu)

   end select

 end subroutine write_symfun
 
!-----------------------------------------------------------------------

 recursive subroutine write_funcont(cnt,var_name,fu)
  integer, intent(in) :: fu
  type(t_funcont), intent(in) :: cnt
  character(len=*), intent(in) :: var_name

   ! containers are removed
   call write_octave( cnt%f ,var_name,fu)

 end subroutine write_funcont

!-----------------------------------------------------------------------

 subroutine write_sympol(p,var_name,fu)
  integer, intent(in) :: fu
  type(t_sympol), intent(in) :: p
  character(len=*), intent(in) :: var_name
 
  integer :: i, ix, nmon
  real(wp), allocatable :: pmat(:,:)
  type(t_symmon), allocatable :: mons(:)
  character(len=*), parameter :: &
    this_sub_name = 'write_sympol'
 
   select case(test(p))
    case(0)

     nmon = get__t_sympol__nmon(p)
     allocate(mons(nmon))
     mons = get__t_sympol__mons(p)

     allocate(pmat(maxval(mons(:)%nsv)+1,nmon))
     pmat = 0.0_wp
     do i=1,nmon
       pmat(1,i) = mons(i)%coeff
       do ix=1,mons(i)%nsv
         pmat(1+ix,i) = real(mons(i)%degs(ix),wp)
       enddo
     enddo
     call write_octave(pmat,var_name,fu)

     deallocate(pmat,mons)
     
    case(wrong_input)
     call write_octave('wrong_input',var_name,fu)
    case(wrong_previous)
     call write_octave('wrong_previous',var_name,fu)
    case(wrong_monomials)
     call write_octave('wrong_monomials',var_name,fu)
    case(wrong_type)
     call write_octave('wrong_type',var_name,fu)
   end select

 end subroutine write_sympol
 
!-----------------------------------------------------------------------

 recursive subroutine write_sympwfun(f,var_name,fu)
  integer, intent(in) :: fu
  type(t_sympwfun), intent(in) :: f
  character(len=*), intent(in) :: var_name
 
  integer :: i, nfields
  type(t_funcont), allocatable :: funs(:)
  class(t_sym_dp), allocatable :: dp

   allocate( dp , source=get__t_sympwfun__dp(f) )

   if(associated(dp%write_octave)) then
     nfields = 3
   else
     nfields = 2
   endif

   write(fu,'(a,a)')    '# name: ',var_name
   write(fu,'(a)')      '# type: struct'
   write(fu,'(a,i0)')   '# length: ', nfields ! number of fields

   ! field 1 : nreg
   write(fu,'(a)')      '# name: nreg'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(f%nreg,'<cell-element>',fu)

   !allocate( funs , source=get__t_sympwfun__funs(f) )
   call gfortran_bug_workaround( funs , f )

   ! field 2 : funs
   write(fu,'(a)')      '# name: funs'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   write(fu,'(a)')      '# name: <cell-element>'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# ndims: 2'
   write(fu,'(*(i0,:," "))') (/ 1 , size(funs) /)
   do i=1,size(funs)
     call write_octave(funs(i),'<cell-element>',fu)
   enddo

   if(associated(dp%write_octave)) then
     ! field 3 : dp
     write(fu,'(a)')      '# name: dp'
     write(fu,'(a)')      '# type: cell'
     write(fu,'(a)')      '# rows: 1'
     write(fu,'(a)')      '# columns: 1'
     call dp%write_octave('<cell-element>',fu)
   endif

   deallocate( dp )

 contains

  ! This function obviates the following gfortran bug:
  !
  !  allocate( funs , source=get__t_sympwfun__funs(pwf) )
  !
  ! returns an array with incorrect indexes starting from 0.
  !
  ! Note: see also https://gcc.gnu.org/bugzilla/show_bug.cgi?id=67125
  subroutine gfortran_bug_workaround( funs , pwf )
   type(t_sympwfun),             intent(in)  :: pwf
   type(t_funcont), allocatable, intent(out) :: funs(:)
    
    allocate( funs( size(get__t_sympwfun__funs(pwf)) ) )
    funs = get__t_sympwfun__funs(pwf)

  end subroutine gfortran_bug_workaround

 end subroutine write_sympwfun
 
!-----------------------------------------------------------------------

end module mod_octave_io_sympoly

