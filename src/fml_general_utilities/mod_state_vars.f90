!! Copyright (C) 2009,2010,2011,2012  Marco Restelli
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Marco Restelli                   <marco.restelli@gmail.com>

!>\brief
!!
!! Define a state variable class.
!!
!! \n
!!
!! This module defines a state variable abstract type \c c_stv which
!! can be used as a building block for the implementation of linear
!! and nonlinear solvers, time integrators and so on. The main purpose
!! of this type is decoupling the details of the state variable
!! representation, such as internal fields and arrays with an
!! arbitrary number of dimensions, from the implementation of some
!! general purpose algorithms. Moreover, this allows a single \c c_stv
!! variable to be used in various such algorithms, avoiding copies.
!!
!! The operators defined for \c c_stv essentially are those required
!! by a vector space, optionally including a scalar product (Hilbert
!! space)
!!
!! \section state_vars_impl_details Implementation details
!!
!! This section collects some comments concerning the choices made for
!! the layout of \c c_stv.
!!
!! \subsection state_vars_pure_attr About the PURE attribute
!!
!! In principle, all the methods of the \c c_stv class, whether
!! subroutines or functions, should have no side effects and should be
!! declared <em>pure</em>. However, there are many restrictions
!! concerning pure procedures which essentially prohibit using them
!! together with polymorphic objects: see section <em>12.7 Pure
!! procedures</em> of the Fortran 2008 standard, and especially C1278a
!! in <em>CORRIGENDUM 1</em>; plus the fact that pure procedure can
!! not make any MPI call. For this reason, none of the methods has the
!! \c pure attribute.
!!
!! \subsection state_vars_fun_vs_subr Functions vs. subroutines
!!
!! Concerning the choice between functions and subroutine, whenever
!! the output is of class \c c_stv we choose subroutines, since
!! functions would require frequent allocations/deallocations of the
!! function result, while the subroutine arguments can be allocated
!! once and then passed around using <tt>intent(inout)</tt>.
!!
!! This explains also why many arguments which logically should have
!! <tt>intent(out)</tt> in fact are declared as
!! <tt>intent(inout)</tt>: this ensures that allocatable components
!! are not deallocated on entry.
!!
!! For subroutines, as a general rule, the passed argument is the one
!! which is changed by the procedure. This preserves the order of the
!! arguments that would appear using arithmetic operators, as in:
!! \code
!!  z = x + y           ! array syntax
!!  call z%add( x , y ) ! c_stv syntax
!! \endcode
!!
!! \subsection state_vars_impl_details_copy Copy constructor
!!
!! When dealing with complex objects, there are many possible
!! interpretations for <tt>z = y</tt>. For instance this could be:
!! <ul>
!!  <li> copy the value of \c y into \c z, assuming that \c z is
!!  already defined as object - intrinsic assignment without
!!  reallocation
!!  <li> allocate \c z to the same type of \c y and then copy the
!!  value of \c y into \c z - intrinsic assignment with reallocation;
!!  \c source allocation
!!  <li> allocate \c z to the same type of \c y, without assigning any
!!  value to it - \c mold allocation.
!! </ul>
!! The picture is further complicated by other elements:
!! <ul>
!!  <li> array bounds and dynamic type are part of the <em>allocation
!!  status</em> for an allocatable variable and part of the
!!  <em>value</em> for a variable with allocatable components
!!  <li> whenever there are pointer components, both a shallow and a
!!  deep copy can be made
!!  <li> the assignment operator can be redefined by the user
!!  extending \c c_stv
!!  <li> the \c source and \c mold allocations can not be overloaded.
!! </ul>
!! To simplify writing generic code based on the \c c_stv class as
!! well as client code for concrete derived classes, we provide two
!! copy constructors:
!! <ul>
!!  <li> \field_fref{mod_state_vars,c_stv,source} corresponds to
!!  allocating \c z to the same type of \c y and assigning it the
!!  value of \c y; here, \c z must be allocatable
!!  <li> \field_fref{mod_state_vars,c_stv,copy} corresponds to
!!  assigning the value of \c y to \c z, assuming that \c z is already
!!  allocated.
!! </ul>
!! Both methods can/must be defined by the concrete class, so that the
!! precise meaning of <em>value</em> is delegated to the client code.
!!
!! Since the semantic of \field_fref{mod_state_vars,c_stv,source}, is
!! very close to the Fortran definition of \c source allocation, this
!! module provides a default implementation which does exactly this,
!! namely
!! \code
!!  call x_old%source( x_new )
!!  ! same as
!!  allocate( x_new , source=x_old )
!! \endcode
!! and
!! \code
!!  call x_old%source( x_new , m )
!!  ! same as
!!  allocate( x_new(m) , source=x_old )
!! \endcode
!!
!! Concerning \field_fref{mod_state_vars,c_stv,copy}, it must be
!! provided by the concrete class.
!!
!! \subsection state_vars_impl_details_destr Destructor
!!
!! \c c_stv has no "clean-up" method. There are two reasons for this:
!! <ul>
!!  <li> there is no ambiguity about the meaning of deallocation:
!!  deallocating an object implies destroying its value as well
!!  <li> the Fortran standard allows defining a \c final procedure,
!!  and since this procedure is called automatically there is no need
!!  to include it in the abstract interface.
!! </ul>
!!
!! As far as concrete classes allocate memory with allocatable
!! components, there is no need for a \c final procedure. However,
!! <em>if a concrete class allocates memory through pointer
!! components, a \c final procedure is required to avoid memory
!! leaks</em>. To see why this is so, see also the examples in \ref
!! state_vars_c_stv_gen.
!!
!! \section state_vars_c_stv_using Using c_stv objects
!!
!! We consider here the problem of working with \c c_stv objects from
!! two viewpoints: a library is required to provide a general
!! algorithm in terms of \c c_stv objects, without knowing the
!! concrete types, while client code must instantiate concrete objects
!! with base type \c c_stv and pass them to various libraries.
!!
!! \subsection state_vars_c_stv_gen Generic programming with c_stv
!!
!! Of course, general purpose subroutines will use the vector space
!! operations defined by \c c_stv.
!!
!! The difficulty however arises when the general purpose code
!! requires local variables of the same type of the \c c_stv objects
!! that are passed to it. For instance, a Krylov space method must
!! generate a Krylov space composed of objects with the same type as
!! the arguments that it receives from the client code and release it
!! once the computation is completed.
!!
!! The proper way to code such an algorithm is using local allocatable
!! variables of base type \c c_stv and allocate them with
!! \field_fref{mod_state_vars,c_stv,source} (i.e. using the
!! corresponding specific subroutines
!! \field_fref{mod_state_vars,c_stv,source_scal} and
!! \field_fref{mod_state_vars,c_stv,source_vect}), using the received
!! object as source value.
!!
!! Once the local variables are allocated, any assignment to allocated
!! variables should be done through
!! \field_fref{mod_state_vars,c_stv,copy}.
!!
!! Finally, once a variable is no longer needed, it can be
!! deallocated.
!!
!! Summarizing, generic code will look like
!! \code
!!  subroutine s( x )
!!   class(c_stv), intent(inout) :: x
!!
!!   class(c_stv), allocatable :: tmp
!!
!!    x%source( tmp )   ! allocate( tmp , source=x )
!!
!!    call make_something( tmp )
!!
!!    x%copy( tmp )     ! x = y
!!
!!    deallocate( tmp ) ! possibly calling a final method
!!   
!!  end subroutine s
!! \endcode
!! By doing this, any resource used by the concrete class which needs
!! special treatment is under control of the user code, both during
!! creation of <tt>tmp</tt> and its deallocation.
!! 
!! \warning Generic code should never use:
!! <ul>
!!  <li> the "=" operator
!!  <li> \c allocate, either as <tt>allocate( tmp , source=x )</tt> or
!!  <tt>allocate( tmp , mold=x )</tt>
!! </ul>
!! since the client code has little or no control on these.
!!
!! \subsection state_vars_c_stv_client Client code for c_stv
!!
!! The user is responsible for setting all the fields of any \c c_stv
!! object he/she defines; none of the functions introduced here is
!! meant for initializing a new object.
!!
!! Generic library code working on \c c_stv objects will use a user
!! provided object as template to create their own working variables
!! calling \field_fref{mod_state_vars,c_stv,source}, as described in
!! \ref state_vars_c_stv_gen.
!!
!! \section state_vars_internal_repr Internal representation
!!
!! Sometimes multiple representations of the same state object must
!! coexist. An example is provided by different distribution patterns
!! for MPI parallelized code, as discussed in \ref
!! state_vars_int_repr_mpi_distr.
!!
!! In such a case, a solution is introducing a flag in the concrete
!! class identifying the representation. Setting this flag, and acting
!! accordingly on \c c_stv objects, is entirely responsibility of the
!! client code, and must be done in the initialization of the objects
!! as well as in all the methods which modify an object of the client
!! class.
!!
!! A more sophisticated variant is making the concrete class a
!! container for the representation, as in
!! \code
!!  type, abstract :: c_repr
!!  end type c_repr
!!
!!  type, extends(c_stv) :: t_state
!!   class(c_repr), allocatable :: repr
!!  end type t_state
!! \endcode
!! Since the representation is now hidden from the generic code, it is
!! only handled by the methods of the concrete class, and the client
!! code has control on if and when the representation must be
!! reallocated.
!!
!! \note Deriving the specific representations from \c c_stv is not a
!! good idea, because then the generic code can now work combining
!! objects with different representations. For instance, a time
!! integrator using \c c_stv for both the unknown and the
!! right-hand-side would force the same representation on both.
!!
!! \subsection state_vars_int_repr_mpi_distr MPI data distribution
!!
!! In general terms, there are two representations of distributed data
!! with duplicated entries: either shared data are assumed to be
!! copies of the same value or they are supposed to be added together.
!! For instance, for a linear system, it is more natural to use the
!! first one for the unknown and the second one for the
!! right-hand-side.
!!
!! Apart from \field_fref{mod_state_vars,c_stv,scal}, all the
!! operations defined in \c c_stv are linear in the \c c_stv variables
!! and thus they are formally invariant for both kinds of distributed
!! data (as far as all the operands are of the same type), and provide
!! a result which is distributed in the same way as their input. For
!! instance, adding two arrays is done adding the local arrays on each
!! processor regardless of the treatment of shared values. For the
!! scalar product, the two different representations require different
!! algorithms.
!!
!! In any case, this is a special case of what is discussed above in
!! \ref state_vars_internal_repr and as such there is nothing in this
!! module referring to data distribution patterns, nor the generic
!! code should be concerned with this. Proper treatment of the chosen
!! data distribution patterns is completely delegated to the client
!! code implementing the concrete state variable type.
!<----------------------------------------------------------------------
module mod_state_vars

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_state_vars_constructor, &
   mod_state_vars_destructor,  &
   mod_state_vars_initialized, &
   c_stv

 private

!-----------------------------------------------------------------------

! Module types and parameters

 ! public members

 !> State variable
 !!
 !! Note that for efficiency reasons it might be useful to override
 !! the implementation of the various operators provided here.
 type, abstract :: c_stv
 contains
  !> \f$x+=y\f$
  procedure(i_incr), deferred, pass(x) :: incr
  !> \f$x*=r\f$
  procedure(i_tims), deferred, pass(x) :: tims
  !> \f$x+=ry\f$ (requires a temporary)
  procedure,                   pass(x) :: inlt
  !> \f$z=x\f$
  !!
  !! See \ref state_vars_impl_details_copy for further details.
  procedure(i_copy), deferred, pass(z) :: copy
  !> \f$z=x+y\f$
  procedure,                   pass(z) :: add
  !> \f$z=rx\f$
  procedure,                   pass(z) :: mlt
  !> \f$z=x+ry\f$
  procedure,                   pass(z) :: alt
  !> \f$x+=\sum_i r_iy_i\f$
  procedure,                   pass(x) :: inlv
  !> \f$z=\sum_i r_iy_i\f$
  procedure,                   pass(z) :: lcb
  !> \f$s=x\cdot y\f$
  !!
  !! \note For parallel execution, the scalar product must be returned
  !! on <em>all</em> the processors.
  !!
  !! There are situations where a scalar product is never required: to
  !! simplify the implementation we provide here a dummy version
  !! of this subroutine.
  procedure,                   pass(x) :: scal
  !> show something, for debug
  procedure,                   pass(x) :: show
  !> copy constructors
  !!
  !! See \ref state_vars_impl_details_copy for further details.
  !!
  !! \note Typically, if you override one of the overloaded
  !! \field_fref{mod_state_vars,c_stv,source} subroutines, you want to
  !! override the other ones as well.
  generic :: source => source_scal, source_vect
  procedure, private, pass(x) :: source_scal
  procedure, private, pass(x) :: source_vect
 end type c_stv

 abstract interface
  subroutine i_incr(x,y)
   import :: c_stv
   implicit none
   class(c_stv), intent(in)    :: y
   class(c_stv), intent(inout) :: x
  end subroutine i_incr
 end interface

 abstract interface
  subroutine i_tims(x,r)
   import :: wp, c_stv
   implicit none
   real(wp),     intent(in)    :: r
   class(c_stv), intent(inout) :: x
  end subroutine i_tims
 end interface

 abstract interface
  subroutine i_copy(z,x)
   import :: c_stv
   implicit none
   class(c_stv), intent(in)    :: x
   class(c_stv), intent(inout) :: z
  end subroutine i_copy
 end interface

 ! private members

! Module variables

 ! public members
 logical, protected ::               &
   mod_state_vars_initialized = .false.
 ! private members
 character(len=*), parameter :: &
   this_mod_name = 'mod_state_vars'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_state_vars_constructor()
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false.) .or. &
       (mod_kinds_initialized.eqv..false.   ) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_state_vars_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   mod_state_vars_initialized = .true.
 end subroutine mod_state_vars_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_state_vars_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_state_vars_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_state_vars_initialized = .false.
 end subroutine mod_state_vars_destructor

!-----------------------------------------------------------------------

 subroutine inlt(x,r,y)
  real(wp),     intent(in) :: r
  class(c_stv), intent(in) :: y
  class(c_stv), intent(inout) :: x

  class(c_stv), allocatable :: tmp

   call   y%source( tmp )
   call tmp%mlt( r , y )
   call   x%incr( tmp )
   deallocate(tmp)

 end subroutine inlt
 
!-----------------------------------------------------------------------

 subroutine add(z,x,y)
  class(c_stv), intent(in) :: x, y
  class(c_stv), intent(inout) :: z

   call z%copy( x )
   call z%incr( y )

 end subroutine add
 
!-----------------------------------------------------------------------

 subroutine mlt(z,r,x)
  real(wp), intent(in) :: r
  class(c_stv), intent(in) :: x
  class(c_stv), intent(inout) :: z

   call z%copy( x )
   call z%tims( r )

 end subroutine mlt
 
!-----------------------------------------------------------------------

 subroutine alt(z,x,r,y)
  real(wp), intent(in) :: r
  class(c_stv), intent(in) :: x, y
  class(c_stv), intent(inout) :: z

   call z%mlt( r , y )
   call z%incr( x )

 end subroutine alt
 
!-----------------------------------------------------------------------

 subroutine inlv(x,r,y)
  real(wp), intent(in) :: r(:)
  class(c_stv), intent(in) :: y(:)
  class(c_stv), intent(inout) :: x

  integer :: i

   do i=1,size(r)
     call x%inlt( r(i) , y(i) )
   enddo

 end subroutine inlv

!-----------------------------------------------------------------------

 subroutine lcb(z,r,y)
  real(wp), intent(in) :: r(:)
  class(c_stv), intent(in) :: y(:)
  class(c_stv), intent(inout) :: z

  integer :: i

   call z%mlt( r(1) , y(1) )
   do i=2,size(r)
     call z%inlt( r(i) , y(i) )
   enddo

 end subroutine lcb

!-----------------------------------------------------------------------

 !> Scalar product
 function scal(x,y) result(s)
  class(c_stv), intent(in) :: x, y
  real(wp) :: s
   
  character(len=*), parameter :: &
    this_fun_name = 'scal'
  character(len=*), parameter :: &
    err_msg(4) = (/ &
   "Please, consider that you have to provide a real implementation ",&
   "for this function if you want to define a Hilber space.         ",&
   "This function in mod_state_vars is provided only to simplify the",&
   "implementation for cases where no scalar product is required.   " &
                 /)

   call error(this_fun_name,this_mod_name,err_msg)
 end function scal

!-----------------------------------------------------------------------

 subroutine show(x)
  class(c_stv), intent(in) :: x
   call warning('show',this_mod_name,        &
     'Overload this function if you need it.')
 end subroutine show

!-----------------------------------------------------------------------

 !> Default copy constructor (source allocation)
 !!
 !! \warning If this subroutine is overridden, then one should also
 !! override the other array versions.
 subroutine source_scal(z,x)
  class(c_stv), intent(in)               :: x
  class(c_stv), allocatable, intent(out) :: z

   allocate( z , source=x )
 end subroutine source_scal

!-----------------------------------------------------------------------

 subroutine source_vect(z,x,m)
  integer, intent(in) :: m
  class(c_stv), intent(in) :: x
  class(c_stv), allocatable, intent(out) :: z(:)

   ! Notice: since an object can not be half-allocated, we can not
   ! allocate here the array dimension m and then call source_scal to
   ! define the kind. This means that overloading source_scal does not
   ! affect this subroutine, and vice versa.
   allocate( z(m) , source=x )
 end subroutine source_vect

!-----------------------------------------------------------------------

end module mod_state_vars

