!! Copyright (C) 2015 Marco Restelli
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Marco Restelli                   <marco.restelli@gmail.com>

!>\brief
!! Piecewise defined functions.
!!
!! \n
!!
!! A piecewise defined function is represented through a list of local
!! definitions and an index function which returns, for a given
!! coordinate, the index of the corresponding definition.
!!
!! Binary operations among two \c t_sympwpol objects make sense if the
!! domains of definition are the same; binary operations involving a
!! \c t_sympwpol object and another \c c_symfun object are evaluated
!! using the \c c_symfun object within each definition region of the
!! \c t_sympwpol one.
!<----------------------------------------------------------------------
module mod_sympwfun

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   pure_abort, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_symfun, only: &
   mod_symfun_initialized, &
   c_symfun, t_funcont, &
   assignment(=),       &
   fnpack, snpack

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_sympwfun_constructor, &
   mod_sympwfun_destructor,  &
   mod_sympwfun_initialized, &
   t_sympwfun, t_sym_dp, new_sympwfun, assignment(=), &
   wrong_intervals, &
   wrong_local,     &
   ! getters for the private components: avoid using them if possible
   get__t_sympwfun__funs, get__t_sympwfun__dp

 private

!-----------------------------------------------------------------------

! Module types and parameters

 ! public and private members
 integer, parameter :: &
   wrong_intervals = 1, &
   wrong_local     = 2
 
 !> Domain partition for a \c t_sympwfun function
 !!
 !! Often different functions must be created for different \c
 !! t_sympwfun objects. These functions typically differ from
 !! each-other because of some parameters: to allow this, it is better
 !! to use a polymorphic type rather than a function pointer.
 !!
 !! The type provided here is fully functional, so that if all what is
 !! needed is a function pointer there is no need to extend it: simply
 !! set \field_fref{mod_sympwfun,t_sym_dp,p_x2reg}.
 type :: t_sym_dp
  procedure(i_x2reg_simple), pointer, nopass :: p_x2reg => null()
  procedure(i_sym_dp_woct),  pointer, pass(dp) :: write_octave => null()
 contains
  procedure, pass(dp) :: x2reg ! region index
  generic :: operator(.eq.) => compare
  procedure, pass(a)  :: compare
 end type t_sym_dp

 !> Piecewise defined function
 type, extends(c_symfun) :: t_sympwfun
  private
  integer, public :: nreg !< number of regions
  class(t_sym_dp), allocatable :: dp      !< domain partitioning
  type(t_funcont), allocatable :: funs(:) !< piecewise definitions
 contains
  private
  ! from c_symfun:
  !   generic, public :: operator(+) => add
  !   generic, public :: operator(*) => mult, mult_s
  !   generic, public :: ev    => ev_scal, ev_1d
  !   generic, public :: pderj => pderj_1, pderj_k
  procedure, public, pass(f) :: show => sympwfun_show
  ! ifort bug: add should be private, see DPD200374263
  ! https://software.intel.com/en-us/forums/topic/563741
  procedure, public, pass(x) :: add
  procedure, public, pass(x) :: mult
  procedure, public, pass(x) :: mult_s
  procedure, pass(f) :: ev_scal
  procedure, pass(f) :: ev_1d
  procedure, pass(f) :: pderj_1
 end type t_sympwfun

 abstract interface
  pure function i_x2reg_simple(x) result(reg)
   import :: wp
   implicit none
   real(wp), intent(in) :: x(:)
   integer :: reg
  end function i_x2reg_simple
 end interface

 abstract interface
  subroutine i_sym_dp_woct(dp,var_name,fu)
   import :: t_sym_dp
   implicit none
   integer, intent(in) :: fu
   class(t_sym_dp), intent(in) :: dp
   character(len=*), intent(in) :: var_name
  end subroutine i_sym_dp_woct
 end interface

! Module variables

 type(t_sym_dp), save :: dp_everywhere

 logical, protected :: &
   mod_sympwfun_initialized = .false.
 character(len=*), parameter :: &
   this_mod_name = 'mod_sympwfun'

 interface assignment(=)
   module procedure to_sympwfun
 end interface

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_sympwfun_constructor()
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false.) .or. &
          (mod_kinds_initialized.eqv..false.) .or. &
         (mod_symfun_initialized.eqv..false.) ) then 
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_sympwfun_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   dp_everywhere%p_x2reg => everywhere

   mod_sympwfun_initialized = .true.
 end subroutine mod_sympwfun_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_sympwfun_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_sympwfun_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_sympwfun_initialized = .false.
 end subroutine mod_sympwfun_destructor

!-----------------------------------------------------------------------

 pure function new_sympwfun(funs,dp) result(pwf)
  class(c_symfun), intent(in) :: funs(:)
  class(t_sym_dp), intent(in) :: dp
  type(t_sympwfun) :: pwf

  integer :: ifort_compiler_bug_i
  
   pwf%nreg  =  size(funs)
   ! Note: since the assignment for pwf%funs is overloaded and
   ! elemental, reallocation-on-assignment is not available.
   allocate( pwf%funs(pwf%nreg) ) 
   ! ifort compiler bug: this problem could be related to
   ! https://software.intel.com/en-us/forums/topic/565015
   !pwf%funs  = funs
   do ifort_compiler_bug_i=1,pwf%nreg
     call ifort_compiler_bug_sub( pwf%funs(ifort_compiler_bug_i) , funs(ifort_compiler_bug_i) )
   enddo
   allocate( pwf%dp , source=dp )

 contains
  pure subroutine ifort_compiler_bug_sub( y , x )
   class(c_symfun), intent(in) :: x
   type(t_funcont), intent(out) :: y
    y = x
  end subroutine ifort_compiler_bug_sub
 end function new_sympwfun

!-----------------------------------------------------------------------

 elemental subroutine to_sympwfun(y,x)
  type(t_funcont),  intent(in)  :: x
  type(t_sympwfun), intent(out) :: y
   
  type(t_funcont) :: xnp
  class(c_symfun), allocatable :: gfortran_bug_workaround(:)

   xnp = x ! normalizing the packaging
 
   select type( f => xnp%f )

    type is(t_sympwfun)
     y = f ! default assignment

    class default
     ! make a function defined on a single interval and equal to x
     !y = new_sympwfun( (/ f /) , dp_everywhere )
     allocate( gfortran_bug_workaround(1), source=f )
     y = new_sympwfun( gfortran_bug_workaround , dp_everywhere )

   end select

 end subroutine to_sympwfun

!-----------------------------------------------------------------------

 elemental function add(x,y) result(z)
  class(t_sympwfun), intent(in) :: x
  class(c_symfun),   intent(in) :: y
  type(t_funcont) :: z

  integer :: i_ifort_bug
  !class(c_symfun), allocatable :: gfortran_bug_tmp(:)
  type(t_funcont) :: ifort_bug_scal
  type(t_funcont), allocatable :: ifort_bug_tmp(:)
 
   select type(y)

    type is(t_sympwfun)
     ! Homogeneous input: addition is defined only if the two
     ! definition intervals are equal
     ! ifort compiler bug: using .eq. the wrong method is used: this
     ! is likely a manifestation of the following bug:
     ! https://software.intel.com/en-us/forums/topic/563741
     ! As a workaround, we use here directly the compare method.
     !if( (x%nreg.eq.y%nreg) .and. (x%dp.eq.y%dp) ) then
                         if( (x%nreg.eq.y%nreg) .and. x%dp%compare(y%dp) ) then
       !z = new_sympwfun( x%funs + y%funs , x%dp )
       ! also an ifort bug: see
       ! https://software.intel.com/en-us/forums/topic/563741
       !allocate( gfortran_bug_tmp , source=x%funs%add(y%funs) )
       !z = new_sympwfun( gfortran_bug_tmp , x%dp )
       allocate( ifort_bug_tmp(1:size(x%funs)) )
       do i_ifort_bug = 1,size(x%funs)
         ifort_bug_scal = x%funs(i_ifort_bug)%add(y%funs(i_ifort_bug)) 
         call ifort_bug_set( ifort_bug_tmp(i_ifort_bug) , ifort_bug_scal )
       enddo
       z = new_sympwfun( ifort_bug_tmp , x%dp )

       call check_loc_err(z)
     else
       allocate( t_sympwfun :: z%f )
       call z%f%set_err( wrong_intervals )
     endif

    type is(t_funcont)
     ! Extract the contained object and retry
     z = x + y%f

    class default
     ! y is a scalar function which is added to each x%funs
     !z = new_sympwfun( x%funs + y , x%dp )
     !allocate( gfortran_bug_tmp , source=x%funs%add(y) )
     !z = new_sympwfun( gfortran_bug_tmp , x%dp )
     allocate( ifort_bug_tmp(1:size(x%funs)) )
     do i_ifort_bug = 1,size(x%funs)
       ifort_bug_scal = x%funs(i_ifort_bug)%add(y) 
       call ifort_bug_set( ifort_bug_tmp(i_ifort_bug) , ifort_bug_scal )
     enddo
     z = new_sympwfun( ifort_bug_tmp , x%dp )

     call check_loc_err(z)

   end select

 contains

  pure subroutine ifort_bug_set(y,x)
   type(t_funcont), intent(in) :: x
   type(t_funcont), intent(out) :: y
    y = x
  end subroutine ifort_bug_set
 end function add

!-----------------------------------------------------------------------

 !> See add for the general idea.
 elemental function mult(x,y) result(z)
  class(t_sympwfun), intent(in) :: x
  class(c_symfun),   intent(in) :: y
  type(t_funcont) :: z

  integer :: i_ifort_bug
  type(t_funcont) :: ifort_bug_scal
  type(t_funcont), allocatable :: ifort_bug_tmp(:)
 
   select type(y)

    type is(t_sympwfun)
     if( (x%nreg.eq.y%nreg) .and. (x%dp.eq.y%dp) ) then
       !z = new_sympwfun( x%funs * y%funs , x%dp )
       allocate( ifort_bug_tmp(1:size(x%funs)) )
       do i_ifort_bug = 1,size(x%funs)
         ifort_bug_scal = x%funs(i_ifort_bug)%mult(y%funs(i_ifort_bug)) 
         call ifort_bug_set( ifort_bug_tmp(i_ifort_bug) , ifort_bug_scal )
       enddo
       z = new_sympwfun( ifort_bug_tmp , x%dp )

       call check_loc_err(z)
     else
       allocate( t_sympwfun :: z%f )
       call z%f%set_err( wrong_intervals )
     endif

    type is(t_funcont)
     z = x * y%f

    class default
     !z = new_sympwfun( x%funs * y , x%dp )
     allocate( ifort_bug_tmp(1:size(x%funs)) )
     do i_ifort_bug = 1,size(x%funs)
       ifort_bug_scal = x%funs(i_ifort_bug)%mult(y) 
       call ifort_bug_set( ifort_bug_tmp(i_ifort_bug) , ifort_bug_scal )
     enddo
     z = new_sympwfun( ifort_bug_tmp , x%dp )

     call check_loc_err(z)

   end select

 contains

  pure subroutine ifort_bug_set(y,x)
   type(t_funcont), intent(in) :: x
   type(t_funcont), intent(out) :: y
    y = x
  end subroutine ifort_bug_set
 end function mult

!-----------------------------------------------------------------------

 elemental function mult_s(r,x) result(y)
  real(wp),          intent(in) :: r
  class(t_sympwfun), intent(in) :: x
  type(t_funcont) :: y
 
  integer :: i_ifort_bug
  type(t_funcont) :: ifort_bug_scal
  type(t_funcont), allocatable :: ifort_bug_tmp(:)
 
   !y = new_sympwfun( r * x%funs , x%dp )
   allocate( ifort_bug_tmp(1:size(x%funs)) )
   do i_ifort_bug = 1,size(x%funs)
     ifort_bug_scal = r * x%funs(i_ifort_bug)
     call ifort_bug_set( ifort_bug_tmp(i_ifort_bug) , ifort_bug_scal )
   enddo
   y = new_sympwfun( ifort_bug_tmp , x%dp )

   call check_loc_err(y)

 contains

  pure subroutine ifort_bug_set(y,x)
   type(t_funcont), intent(in) :: x
   type(t_funcont), intent(out) :: y
    y = x
  end subroutine ifort_bug_set
 end function mult_s

!-----------------------------------------------------------------------

 pure recursive function ev_scal(f,x) result(z)
  class(t_sympwfun), intent(in) :: f
  real(wp),          intent(in) :: x(:)
  real(wp) :: z

   z = f%funs( f%dp%x2reg(x) )%ev(x)

 end function ev_scal

!-----------------------------------------------------------------------

 pure recursive function ev_1d(f,x) result(z)
  class(t_sympwfun), intent(in) :: f
  real(wp),          intent(in) :: x(:,:)
  real(wp) :: z(size(x,2))

  integer :: i

   ! We don't know whether all the points belong to the same region:
   ! treating them one by one is the simplest option.
   do i=1,size(z)
     z(i) = f%funs( f%dp%x2reg(x(:,i)) )%ev(x(:,i))
   enddo

 end function ev_1d

!-----------------------------------------------------------------------

 elemental function pderj_1(f,j) result(djf)
  class(t_sympwfun), intent(in) :: f
  integer,           intent(in) :: j
  type(t_funcont) :: djf

  integer :: i_gfortran_bug
  type(t_funcont), allocatable :: gfortran_bug_workaround(:)

   !djf = new_sympwfun( f%funs%pderj( j ) , f%dp )
   allocate( gfortran_bug_workaround(f%nreg) )
   do i_gfortran_bug=1,f%nreg
     allocate( gfortran_bug_workaround(i_gfortran_bug)%f , source = f%funs(i_gfortran_bug)%pderj( j ) )
   enddo
   djf = new_sympwfun( gfortran_bug_workaround , f%dp )

   call check_loc_err(djf)

 end function pderj_1
 
!-----------------------------------------------------------------------

 subroutine sympwfun_show(f)
  class(t_sympwfun), intent(in) :: f

  integer :: i

   write(*,*) "*** Piecewise defined function ***"
   write(*,*) "number of regions: ",f%nreg
   do i=1,f%nreg
     write(*,*) "  region ", i
     call f%funs(i)%f%show()
   enddo
   write(*,*) "**********************************"

 end subroutine sympwfun_show

!-----------------------------------------------------------------------

 pure function x2reg(dp,x) result(reg)
  class(t_sym_dp), intent(in) :: dp
  real(wp), intent(in) :: x(:)
  integer :: reg

   reg = dp%p_x2reg(x)
 end function x2reg

!-----------------------------------------------------------------------

 elemental function compare(a,b) result(bool)
  class(t_sym_dp), intent(in) :: a, b
  logical :: bool
   
   bool = associated(a%p_x2reg , b%p_x2reg)
 end function compare

!-----------------------------------------------------------------------

 !> Define a single definition interval
 !!
 !! This function is useful to convert a given function into a
 !! t_sympwfun object, using only one interval.
 pure function everywhere(x) result(reg)
  real(wp), intent(in) :: x(:)
  integer :: reg

   reg = 1 ! first and only definition interval
 end function everywhere

!-----------------------------------------------------------------------

 !> Propagate errors in the local definition to the global def.
 pure subroutine check_loc_err(res)
  type(t_funcont), intent(inout) :: res

   select type( f => res%f ); type is(t_sympwfun)
   if(any( f%funs%ierr.ne.0 )) call f%set_err( wrong_local )
   end select
 end subroutine check_loc_err

!-----------------------------------------------------------------------

 pure function get__t_sympwfun__funs(pwf) result(funs)
  class(t_sympwfun), intent(in) :: pwf
  type(t_funcont) :: funs(pwf%nreg)

   funs = pwf%funs
 end function get__t_sympwfun__funs

!-----------------------------------------------------------------------

 function get__t_sympwfun__dp(pwf) result(dp)
  class(t_sympwfun), intent(in) :: pwf
  class(t_sym_dp), allocatable :: dp

   allocate( dp , source=pwf%dp )
 end function get__t_sympwfun__dp

!-----------------------------------------------------------------------

end module mod_sympwfun

