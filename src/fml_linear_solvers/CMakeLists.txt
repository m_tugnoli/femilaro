# This variable will be used in the parent directory to set the
# include directory to compile the sources listed here.
SET( fml_linear_solvers_INC )


#---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---
# Select the supported solvers
IF( PASTIX_FOUND AND MURGE_FOUND )
  SET( fml_linear_solvers_INC ${PASTIX_INCLUDE_DIRS} )
  SET( pastixintf_SRC mod_pastixintf.f90 )
  SET( pastixintf_LNK ${MURGE_LIBRARIES} ${PASTIX_LIBRARIES}
	  ${PTSCOTCH_LIBRARIES} ${SCOTCH_LIBRARIES} ${BLAS_LIBRARIES} )
ELSE()
  SET( pastixintf_SRC mod_pastixintf_dummy.f90 )
ENDIF()

IF( MUMPS_FOUND )
  SET( mumpsintf_SRC mod_mumpsintf.f90 )
ELSE()
  SET( mumpsintf_SRC mod_mumpsintf_dummy.f90 )
ENDIF()

IF( UMFPACK_FOUND )
  SET( umfpackintf_SRC umf4_f77wrapper.c mod_umfpack.f90 )
ELSE()
  SET( umfpackintf_SRC mod_umfpack_dummy.f90 )
ENDIF()
#---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---


# Source files for the library: should be more or less the same as the
# variable OBJ in the Makefile, taking into account which libraries
# have been detected and which not.
SET( fml_linear_solvers_SRC 
  mod_linsolver_base.f90
  mod_iterativesolvers_base.f90
  mod_gmres.f90
  mod_umfintf.f90
  ${pastixintf_SRC}
  ${mumpsintf_SRC}
  ${umfpackintf_SRC}
  mod_linsolver.f90
  mod_newton.f90 )

# Prepend the path
FOREACH(x ${fml_linear_solvers_SRC})
    SET( path_fml_linear_solvers_SRC 
      ${path_fml_linear_solvers_SRC} ./fml_linear_solvers/${x} )
ENDFOREACH(x)

# Dependencies: no need to add any dependence: they result
# automatically from the module dependencies and the dependencies
# specified in fml_general_utilities
SET( fml_linear_solvers_DEP )

# Update the parent scope
LIST(APPEND femilaro_src_list ${path_fml_linear_solvers_SRC})
SET(femilaro_src_list "${femilaro_src_list}" PARENT_SCOPE)

LIST(APPEND femilaro_dep_list      ${fml_linear_solvers_DEP})
SET(femilaro_dep_list "${femilaro_dep_list}" PARENT_SCOPE)

LIST(APPEND femilaro_inc_list      ${fml_linear_solvers_INC})
SET(femilaro_inc_list "${femilaro_inc_list}" PARENT_SCOPE)

# The test files are compiled here: no need to mess with the parent
# directory.
ADD_EXECUTABLE( test_fml_linear_solvers1 unit_test1.F90 )
TARGET_LINK_LIBRARIES( test_fml_linear_solvers1
  femilaro ${pastixintf_LNK} )

ADD_EXECUTABLE( test_fml_linear_solvers2 unit_test2.F90 )
TARGET_LINK_LIBRARIES( test_fml_linear_solvers2
  femilaro ${pastixintf_LNK} )

